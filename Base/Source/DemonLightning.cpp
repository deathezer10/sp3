#include "DemonLightning.h"

#include "SceneBase.h"
#include "Player.h"

#include "SpriteAnimation.h"
#include "UI_Bar.h"
#include "ColliderFactory.h"


DemonLightning::DemonLightning(Scene * scene) : GameObject(scene, Scene::GEO_ENEMYDEMON_LASER) {
	b_EnableLighting = false;
	b_EnablePhysics = true;
	b_EnableTrigger = true;
	b_EnableGravity = false;

	active = true;

	color.Set(1, 1, 1);

	scale.Set(5, 4, 1);

	bounciness = 0;

	collider = ColliderFactory::CreateOBBCollider(this, Vector3(2, 0, 1));

	MAX_VELOCITY.y = 0;

	m_AnimLightning = static_cast<SpriteAnimation*>(scene->meshList[Scene::GEO_ENEMYDEMON_LASER]);
	m_Anim = new Animation();
	m_Anim->Set(0, 9, 1, 0.5f, true);

}

DemonLightning::~DemonLightning(){
	if (m_Anim)
		delete m_Anim;
}



bool DemonLightning::Update() {

	collider.bboxSize.y = scale.y;

	return true;
}

void DemonLightning::Render() {

	m_AnimLightning->Update(m_Anim, m_scene->m_dt);
	m_Anim_CurrentFrame = m_Anim->m_currentFrame;

	GameObject::Render();
}

void DemonLightning::OnCollisionHit(GameObject * other) {

	if (other->type == Scene::GEO_PLAYER && m_scene->m_elapsedTime >= m_NextDamageTime) {
		m_scene->playerManager.ReduceHealth(30);
		m_NextDamageTime = m_scene->m_elapsedTime + 0.5f;
	}

}
