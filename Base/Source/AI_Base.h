#ifndef AI_Base_H
#define AI_Base_H

#include "GameObject.h"

class Player;

class AI_Base : public GameObject {

public:
	enum AI_STATES
	{
		CHASE,
		ATTACK,
		JUMP,
		STUN,
		ON_PLAYER,
		FLEE,
		//for allies ai
		INTERCEPT,

		
	};

	AI_Base(Scene* scene, unsigned type, Player * player, float defaultHP = 100, float defaultSpeed = 10, int defaultStunCounter = 1);
	virtual ~AI_Base();

	float GetDefaultSpeed() { return m_DefaultSpeed; }

	float GetDefaultHealth() { return m_DefaultHP; }
	float GetCurrentHealth() { return m_CurrentHP; }

	// Set the Health of AI
	void SetHealth(float value);

	// Set Stun Counter for AI
	void SetStunCounter(int value);

	// Damage the AI 
	void ReduceHealth(float damage, bool spawnWool = true);

	bool Update();


	// Called when the AI Unit takes damage
	virtual void OnReduceHealth(float damage) {};

	virtual void OnCollisionHit(GameObject * other);

	// Updating AI's chase state
	virtual void AI_Chase(Player* player, double dt);

	// Updating AI's attack state
	virtual void AI_Attack(Player* player, double dt);

	//Updating AI's jump state
	virtual void AI_Jump(double dt);

	//Updating AI's stun state
	virtual void AI_Stun();

	//Updating AI's flee state
	virtual void AI_Flee(Player* player, double dt);


	// Stuns the AI for the duration, then return them to the AI_STATES where it was before
	void StunAI(float duration);

	AI_STATES GetCurrentState() { return m_CurrentState; }
	bool IsAlly() { return b_isAlly; }

	// Does this AI unit take damage?
	bool b_IsInvulnerable = false;

	void ReduceStunCounter(int stunRemove);

	Player* thePlayer;

protected:
	AI_STATES m_CurrentState = CHASE;
	AI_STATES m_PreviousStunState;
	AI_STATES m_PreviousState;

	float m_DefaultHP;
	float m_DefaultSpeed;

	float m_CurrentHP;
	float m_Damage;
	int m_DropChance = 20; // Chance that this unit will drop an Armor upon death

	bool is_attacking = false;
	bool is_jumping = true;
	bool b_IsFacingRight = true;
	bool b_completeLoop = false;
	bool b_isAlly = false;
	float m_weaponRotation = 0.f;
	float m_attackTimer = 0.f;

	float _dt = 0;
	float _elapsedTime = 0;
	
	// Stun variables for when hit by player
	float m_stunTimer = 0.f;
	float m_stunDuration = 1.f;
	float m_stunThreshold = 500.f;
	int m_stunCounter;
	int m_defaultStunCounter;
};
#endif