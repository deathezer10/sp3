#include "Enemy_Archer.h"
#include "Player.h"
#include "Scene.h"
#include "GameObject.h"
#include "PlayerManager.h"
#include "SpriteAnimation.h"
#include "ColliderFactory.h"
#include "Enemy_arrow.h"
#include "UI_Bar.h"

#include "WeaponInfo.h"

Enemy_Archer::Enemy_Archer(Scene* scene, Player* player) :AI_Base(scene, Scene::GEO_ENEMYARCHER, player, 100, 7)
{
	b_EnableGravity = true;
	b_EnablePhysics = true;
	b_EnableLighting = false;
	b_isAlly = false;

	scale.Set(6.f, 6.f, 1.f);
	this->collider = ColliderFactory::CreateOBBCollider(this, Vector3(3.5f, 6.0f, 0.0f));

	m_AnimEnemyArcher = static_cast<SpriteAnimation*>(scene->meshList[Scene::GEO_ENEMYARCHER]);
	m_Anim = new Animation();
	m_Anim->Set(0, 0, 1, 1.f, true);
}

Enemy_Archer::~Enemy_Archer()
{
	if (m_Anim)
		delete m_Anim;
}

bool Enemy_Archer::Update()
{
	if (m_CurrentState == CHASE)
		AI_Chase(thePlayer, m_scene->m_dt);
	if (m_CurrentState == JUMP)
		AI_Jump(m_scene->m_dt);
	if (m_CurrentState == ATTACK)
		AI_Attack(thePlayer, m_scene->m_dt);
	if (m_CurrentState == STUN)
		AI_Stun();
	if (m_CurrentState == FLEE)
		AI_Flee(thePlayer, m_scene->m_dt);

	if (m_CurrentState != FLEE)
		thePlayer->pos.x > pos.x ? b_IsFacingRight = true : b_IsFacingRight = false;
	else
		thePlayer->pos.x > pos.x ? b_IsFacingRight = false : b_IsFacingRight = true;


		return true;
}

void Enemy_Archer::AI_Chase(Player* player, double dt)
{

	if (pos.x < player->pos.x && (player->pos.x - pos.x) >(player->scale.x * player->scale.x *1.5f))
	{
		m_force.x = 70;

		if (collider.manifold.normal.x != 0 && !is_jumping)
		{
			m_CurrentState = JUMP;
			is_jumping = true;
		}
	}
	else if (pos.x > player->pos.x && (player->pos.x - pos.x) < -(player->scale.x * player->scale.x *1.5f))
	{
		m_force.x = -70;

		if (collider.manifold.normal.x != 0 && !is_jumping)
		{
			m_CurrentState = JUMP;
			is_jumping = true;
		}
	}
	else
		m_CurrentState = ATTACK;
	if (m_scene->m_elapsedTime > m_attackTimer)
	{
		is_attacking = true;
		m_attackTimer = m_scene->m_elapsedTime + 1.f;
	}
}

void Enemy_Archer::AI_Attack(Player* player, double dt)
{
	// Calculating angle between player and AI
	m_firingAngle = Math::RadianToDegree(atan2(thePlayer->pos.y - pos.y, thePlayer->pos.x - pos.x));

	// For rotating weapon
	m_weaponRotation = m_firingAngle;


	if (m_scene->m_elapsedTime > m_attackTimer)
	{
		float angle;
		angle = Math::DegreeToRadian(m_firingAngle);

		Enemy_arrow *arrow = new Enemy_arrow(m_scene);
		arrow->fireArrow(angle, pos, collider);
		m_scene->goManager.CreateObjectQueue(arrow);
		is_attacking = false;
		m_attackTimer = m_scene->m_elapsedTime + m_attackCoolDown;
	}

	if (!is_attacking)
		m_CurrentState = CHASE;
}

void Enemy_Archer::Render()
{
	if (vel.x < 1 && vel.x > -1 && m_CurrentState != FLEE) {
		m_Anim->Set(19, 19, 19, 0.5f, true);
	}
	else {
		m_Anim->Set(18, 20, 1, 0.5f, true);
	}

	if (b_IsFacingRight) {
		rotation.y = 180;
	}
	else {
		rotation.y = 0;
	}

	m_AnimEnemyArcher->Update(m_Anim, m_scene->m_dt);

	glDisable(GL_CULL_FACE);
	GameObject::Render();
	glDisable(GL_DEPTH_TEST);

	// Weapon rotation & position
	m_scene->modelStack.PushMatrix();
	if (!b_IsFacingRight)
	{
		m_scene->modelStack.Translate(pos.x - 1.5f, pos.y, currentIndex * 0.1f);

		m_scene->modelStack.Rotate(180, 0, 1, 0);

		if (m_CurrentState == ATTACK) {
			m_scene->modelStack.Rotate(-m_weaponRotation + 180, 0.f, 0.f, 1.f);
		}
	}
	else
	{
		m_scene->modelStack.Translate(pos.x + 1.5f, pos.y, currentIndex * 0.1f);

		if (m_CurrentState == ATTACK)
			m_scene->modelStack.Rotate(m_weaponRotation, 0.f, 0.f, 1.f);
	}


	m_scene->modelStack.Scale(2.5f, 5.f, 1.f);
	m_Anim_CurrentFrame = m_Anim->m_currentFrame;
	m_scene->RenderMesh(m_scene->meshList[Scene::GEO_ENEMYBOW], false);

	m_scene->modelStack.PopMatrix();
	glEnable(GL_DEPTH_TEST);

	glEnable(GL_CULL_FACE);

	// Render Health bar
	Vector3 barPos = pos;
	barPos.y += collider.bboxSize.y;
	Mesh** meshList = m_scene->meshList;
	UI_BAR::RenderBar(m_scene, meshList[Scene::GEO_HP_BACKGROUND], meshList[Scene::GEO_HP_FOREGROUND], meshList[Scene::GEO_HP_FRAME], barPos, Vector3(4, 0.5f), (float)GetCurrentHealth() / m_DefaultHP);

}


