#include "GL\glew.h"
#include "Particle.h"
#include "MatrixStack.h"
#include "Scene.h"
#include "Camera3.h"
#include "Mesh.h"



Particle::Particle(Scene* scene, unsigned meshType) {
	m_scene = scene;
	type = meshType;

	pos.SetZero();
	vel.SetZero();
	color.Set(1, 1, 1); // Default white color
}

void Particle::SetLifeTime(float time){
	lifetime = time;
}

void Particle::UpdateMovement(const Vector3 &emitterPos) {

	if (currentLifetime == 0)
		pos = emitterPos;

	if (currentLifetime >= lifetime) {
		pos = emitterPos;
		currentLifetime = 0;
	}

	currentLifetime += m_scene->m_dt;

	pos += vel * m_scene->m_dt;

	if (b_EnableGravity)
		pos.y -= gravity * m_scene->m_dt;

	currentRotation += rotationSpeed * m_scene->m_dt;

}

void Particle::Render(const Vector3 &emitterPos) {

	m_scene->modelStack.PushMatrix();
	m_scene->modelStack.Translate(pos.x, pos.y, pos.z);
	m_scene->modelStack.Rotate(currentRotation, 0, 0, 1);
	m_scene->modelStack.Scale(scale, scale, scale);
	glDisable(GL_DEPTH_TEST);
	m_scene->meshList[type]->color = color;
	m_scene->meshList[type]->alpha = 1;
	m_scene->RenderMesh(m_scene->meshList[type], false);
	glEnable(GL_DEPTH_TEST);
	m_scene->modelStack.PopMatrix();
}
