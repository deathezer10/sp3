#pragma once

#include <vector>

class ParticleEmitter;

class ParticleManager {

public:
	ParticleManager();
	~ParticleManager();

	// Adds a Emitter into the collection for Updating & Rendering
	void AddEmitter(ParticleEmitter* emitter);

	// Updates all Particle's logic
	void UpdateParticles();

	// Render all existing Particles
	void RenderParticles();

	// Do necessary clean up for all Emitters in the collection
	void Exit();

private:
	std::vector<ParticleEmitter*> m_pEmitterList;

};