#pragma once

#include <vector>
#include "GameObject.h"


class UltimateWave : public GameObject {

public:
	UltimateWave(Scene* scene);
	~UltimateWave() {};

	virtual bool Update();
	virtual void Render();
	virtual void OnCollisionHit(GameObject* other);

private:
	float m_TraveledDistance = 0;
	const float m_MaxDistance = 80;

	std::vector<GameObject*> m_EnemyHit;
};