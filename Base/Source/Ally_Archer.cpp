#include "Ally_Archer.h"
#include "Player.h"
#include "Scene.h"
#include "GameObject.h"
#include "PlayerManager.h"
#include "SpriteAnimation.h"
#include "ColliderFactory.h"
#include "UI_Bar.h"
#include "Dart.h"


#include "WeaponInfo.h"

Ally_Archer::Ally_Archer(Scene* scene, Player* player) :AI_Base(scene, Scene::GEO_ALLYARCHER, player, 100, 10)
{
	b_EnableGravity = true;
	b_EnablePhysics = true;
	b_EnableLighting = false;
	b_EnableTrigger = true;

	scale.Set(5.f, 5.f, 1.f);
	this->collider = ColliderFactory::CreateOBBCollider(this, Vector3(5.0f, 5.0f, 0.0f));

	m_AnimAllyWarrior = static_cast<SpriteAnimation*>(scene->meshList[Scene::GEO_ALLYARCHER]);
	m_Anim = new Animation();
	m_Anim->Set(0, 0, 1, 1.f, true);

	b_isAlly = true;

	b_HitSomething = false;
	m_spawnTime = 0.f;
	m_lifeTime = 0.f;

	if (thePlayer->b_isFacingRight)
		b_playerDir = true;
	else
		b_playerDir = false;

}

Ally_Archer::~Ally_Archer() {
	if (m_Anim)
		delete m_Anim;
}

bool Ally_Archer::Update()
{
	//Old Archer codes
	{
		//float lowestdistance = 999999;
		//bool assigned = false;

		//for (std::vector<GameObject*>::iterator it = m_scene->goManager.m_goList.begin(); it != m_scene->goManager.m_goList.end(); ++it)
		//{

		//	GameObject* obj = *it;
		//	if (obj->active)
		//	{
		//		AI_Base* Enemy = dynamic_cast<AI_Base*>(obj);

		//		if (Enemy != nullptr && Enemy != this)
		//		{
		//			float newEnemyLength = (Enemy->pos - pos).Length();

		//			if (Enemy->IsAlly() == false && newEnemyLength < threshold) {

		//				if (newEnemyLength <= lowestdistance) {
		//					lowestdistance = newEnemyLength;
		//					m_CurrentTarget = Enemy;
		//					assigned = true;
		//				}

		//			}
		//		}

		//	}

		//	if (assigned == false && it == m_scene->goManager.m_goList.end() - 1) {
		//		m_CurrentTarget = nullptr;
		//	}
		//}


		//if (m_CurrentState == CHASE)
		//	AI_Chase(thePlayer, m_scene->m_dt);
		//if (m_CurrentState == JUMP)
		//	AI_Jump(m_scene->m_dt);
		//if (m_CurrentState == ATTACK)
		//	Engage(m_CurrentTarget, m_scene->m_dt);

		//thePlayer->pos.x > pos.x ? b_IsFacingRight = true : b_IsFacingRight = false;
	}

	if (b_HitSomething)
	{
		b_EnablePhysics = false;
		if (m_scene->m_elapsedTime > m_spawnTime)
		{
			//SoundManager::getInstance().Play2DSound("Ultimate3", false, true, false);
			m_spawnTime = m_scene->m_elapsedTime + 1.f;
			for (int i = 0; i < 7; ++i)
			{
				Dart* obj = new Dart(m_scene);
				obj->b_EnableTrigger = true; // Disable response
				obj->pos = pos;
				Mtx44 rotation;
				//rotation.SetToRotation(25.7f + (i + 1) * 15.f, 0, 0, 1);
				if(b_playerDir)
				rotation.SetToRotation(10.f + i * 5.f, 0, 0, 1);
				else 
				rotation.SetToRotation(170.f - i * 5.f, 0, 0, 1);

				obj->vel = rotation * Vector3(1.f, 0.f, 0) * 75.f;
				obj->vel.z = 0.f;
				obj->collider = ColliderFactory::CreateOBBCollider(obj, Vector3(7.5f, 2.f, 1.f));
				obj->scale.Set(4.f, 4.f, 1.f);
				Vector3 offset;
				offset = obj->vel.Normalized() * (obj->collider.bboxSize);
				obj->pos += offset;
				obj->b_EnablePhysicsResponse = false;
				m_scene->goManager.CreateObjectQueue(obj);
			}
		}
		else if (m_scene->m_elapsedTime > m_lifeTime)
		{
			m_scene->goManager.DestroyObjectQueue(this);
			return false;
		}
	}
	else if (!vel.IsZero())
		dir = vel.Normalized();
	else if (vel.y <= 0)
	{
		m_lifeTime = m_scene->m_elapsedTime + 7.5f;
		b_HitSomething = true;
	}
	return true;
}

void Ally_Archer::AI_Jump(double dt)
{
	if (is_jumping) {
		m_CurrentState = m_PreviousState;
		return;
	}

	m_force.y = 1700;

	if (b_IsFacingRight)
	{
		m_force.x = 55;

	}
	else
	{
		m_force.x = -55;

	}
	m_CurrentState = m_PreviousState;


}

void Ally_Archer::Engage(AI_Base * AI, double dt)
{
	//if (m_CurrentTarget == nullptr) {
	//	m_CurrentState = CHASE;
	//	return;
	//}

	//if (pos.x < thePlayer->pos.x - 60.f && (thePlayer->pos.x - pos.x - 60.f) > thePlayer->scale.x || pos.x > thePlayer->pos.x + 60.f && (thePlayer->pos.x - pos.x + 60.f) < -thePlayer->scale.x || m_CurrentTarget == nullptr)
	//{
	//	m_CurrentState = CHASE;
	//}

	//if (m_CurrentTarget != nullptr)
	//{
	//	if (m_CurrentTarget->pos.x > 100000.f || m_CurrentTarget->pos.x < -100000.f)
	//	{
	//		m_CurrentTarget = nullptr;
	//		m_CurrentState = CHASE;
	//	}
	//}

	//// Calculating angle between ally and AI
	//if (m_CurrentTarget != nullptr)
	//{

	//	m_firingAngle = Math::RadianToDegree(atan2(m_CurrentTarget->pos.y - pos.y, m_CurrentTarget->pos.x - pos.x));

	//	// For rotating weapon
	//	m_weaponRotation = m_firingAngle;


	//	if (m_scene->m_elapsedTime > m_attackTimer)
	//	{
	//		float angle;
	//		angle = Math::DegreeToRadian(m_firingAngle);

	//		Projectile *arrow = new Projectile(m_scene);
	//		arrow->collider = ColliderFactory::CreateOBBCollider(arrow, Vector3(7.5f, 2, 1));

	//		arrow->fireArrow(angle, pos, collider);
	//		m_scene->goManager.CreateObjectQueue(arrow);
	//		is_attacking = false;
	//		m_attackTimer = m_scene->m_elapsedTime + m_attackCoolDown;
	//	}
	//}
	//else if (!is_attacking || m_CurrentTarget == nullptr)
	//	m_CurrentState = CHASE;
}

void Ally_Archer::AI_Chase(Player * player, double dt)
{
	//Old Archer codes
	{
		//if (pos.x < player->pos.x - 30.f && (player->pos.x - pos.x - 30.f) >player->scale.x)
		//{
		//	m_force.x = 100;

		//	if (collider.manifold.normal.x != 0 && !is_jumping)
		//	{
		//		m_PreviousState = m_CurrentState;
		//		m_CurrentState = JUMP;
		//		is_jumping = true;
		//	}
		//}
		//else if (pos.x > player->pos.x + 30.f && (player->pos.x - pos.x + 30.f) < -player->scale.x)
		//{
		//	m_force.x = -100;

		//	if (collider.manifold.normal.x != 0 && !is_jumping)
		//	{
		//		m_PreviousState = m_CurrentState;
		//		m_CurrentState = JUMP;
		//		is_jumping = true;
		//	}
		//}
		//else if (m_CurrentTarget != nullptr)
		//	m_CurrentState = ATTACK;
	}


}

void Ally_Archer::Render()
{
	if (vel.x < 1 && vel.x > -1) {
		m_Anim->Set(59, 59, 1, 0.5f, true);
	}
	else if (b_IsFacingRight) {
		m_Anim->Set(81, 83, 1, 0.5f, true);
		rotation.y = 0;
	}
	else {
		m_Anim->Set(81, 83, 1, 0.5f, true);
		rotation.y = 180;
	}
	m_AnimAllyWarrior->Update(m_Anim, m_scene->m_dt);


	glDisable(GL_CULL_FACE);
	GameObject::Render();

	glDisable(GL_DEPTH_TEST);


	m_scene->modelStack.PushMatrix();
	if (!b_IsFacingRight)
	{
		m_scene->modelStack.Translate(pos.x - 1.5f, pos.y, currentIndex * 0.1f);
		m_scene->modelStack.Rotate(180, 0, 1, 0);

		if (m_CurrentState == ATTACK) {
			m_scene->modelStack.Rotate(-m_weaponRotation + 180, 0.f, 0.f, 1.f);
		}
	}
	else if (b_IsFacingRight)
	{
		m_scene->modelStack.Translate(pos.x + 1.5f, pos.y, currentIndex * 0.1f);

		if (m_CurrentState == ATTACK)
			m_scene->modelStack.Rotate(m_weaponRotation, 0.f, 0.f, 1.f);
	}


	m_scene->modelStack.Scale(2.5f, 5.f, 1.f);
	m_Anim_CurrentFrame = m_Anim->m_currentFrame;
	m_scene->RenderMesh(m_scene->meshList[Scene::GEO_TRANQGUN], false);

	m_scene->modelStack.PopMatrix();

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	// Render Health bar
	/*Vector3 barPos = pos;
	barPos.y += collider.bboxSize.y;
	Mesh** meshList = m_scene->meshList;
	UI_BAR::RenderBar(m_scene, meshList[Scene::GEO_HP_BACKGROUND], meshList[Scene::GEO_CAP_FOREGROUND], meshList[Scene::GEO_HP_FRAME], barPos, Vector3(4, 0.5f), (float)GetCurrentHealth() / m_DefaultHP);*/
}

void Ally_Archer::OnCollisionHit(GameObject * other)
{
	AI_Base::OnCollisionHit(other);

	AI_Base* enemy = dynamic_cast<AI_Base*>(other);

	if (enemy != nullptr && enemy->IsAlly() == false && m_scene->m_elapsedTime >= m_NextDamageTime) {

		ReduceHealth(4);
		m_NextDamageTime = m_scene->m_elapsedTime + 0.5f;
	}

	if ((this->collider.manifold.normal.x == -1 || this->collider.manifold.normal.x == 1) && other->type == Scene::GEO_PLAYER)
	{

		if (m_scene->m_elapsedTime > m_attackTimer)
		{
			is_attacking = true;
			m_attackTimer = m_scene->m_elapsedTime + 1.f;
		}
	}
	else if ((this->collider.manifold.normal.y == -1 || this->collider.manifold.normal.y == 1) && other->type == Scene::GEO_PLAYER)
	{
		//If AI is facing right, AI will move right , else AI will move left
		this->b_IsFacingRight ? m_force.x = 55 : m_force.x = -55;
	}
}



