#ifndef SCENE_LEVEL02_H
#define SCENE_LEVEL02_H

#include "SceneBase.h"
#include <vector>
#include "GameObject.h"

class AI_Base;
class CapturePoint;
class Brick;
class Portal;
class Enemy_Golem;

class SceneLevel02 : public SceneBase 
{

public:
	SceneLevel02();
	~SceneLevel02();

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();


private:

	Brick* m_Gate1 = nullptr;
	Brick* m_Gate2 = nullptr;
	Brick* m_Gate3 = nullptr;
	CapturePoint* m_Point1 = nullptr;
	CapturePoint* m_Point2 = nullptr;
	Portal* m_Core = nullptr;
	Enemy_Golem* m_Golem = nullptr;



protected:
	AI_Base* Test = nullptr;



};




#endif // !SCENE_PAUSE_MENU_H

