#include "DemonFireball.h"

#include "SceneBase.h"
#include "Player.h"

#include "UI_Bar.h"
#include "ColliderFactory.h"


DemonFireball::DemonFireball(Scene * scene, Player* player, Vector3 direction) : AI_Base(scene, Scene::GEO_ENEMYDEMON_FIREBALL, player, 25, 40) {
	b_EnableLighting = false;
	b_EnablePhysics = true;
	b_EnableTrigger = true;
	b_EnableGravity = false;

	active = true;

	color.Set(1, 1, 1);

	scale.Set(5, 3, 1);

	bounciness = 0;
	
	collider = ColliderFactory::CreateOBBCollider(this, scale);

	if (direction.IsZero() == false)
		direction.Normalize();

	m_Direction = direction;
}

bool DemonFireball::Update() {
	
	vel = m_Direction * m_DefaultSpeed;
	dir = m_Direction;


	return true;
}

void DemonFireball::Render() {
	GameObject::Render();
}

void DemonFireball::OnCollisionHit(GameObject * other) {

	if (other->type == Scene::GEO_PLAYER) {
		m_scene->playerManager.ReduceHealth(40);
		m_scene->goManager.DestroyObjectQueue(this);
	}

}
