#include "Application.h"
#include "FurnitureMan.h"
#include "Scene.h"

#include "ColliderFactory.h"
#include "SpriteAnimation.h"

#include "GL\glew.h"



FurnitureMan::FurnitureMan(Scene * scene) : GameObject(scene, Scene::GEO_PLAYER) {
	b_EnableLighting = false;
	b_EnableGravity = false;
	b_EnablePhysics = true;
	b_EnablePhysicsResponse = false;

	Vector3 size(5, 5, 1);
	scale = size;

	collider = ColliderFactory::CreateOBBCollider(this, size);

	// Animation 
	m_AnimPlayer = static_cast<SpriteAnimation*>(scene->meshList[Scene::GEO_PLAYER]);
	m_Anim = new Animation();
	m_Anim->Set(0, 0, 1, 1.f, true);

}

bool FurnitureMan::Update() {

	if (Application::IsKeyPressed(VK_LEFT))
	{
		m_Anim->Set(33, 35, 1, 0.5f, true);
		rotation.y = 180;
	}
	else if (Application::IsKeyPressed(VK_RIGHT))
	{
		m_Anim->Set(33, 35, 1, 0.5f, true);
		rotation.y = 0;
	}
	else {
		m_Anim->Set(28, 28, 1, 0.5f, true);
	}

	if (b_EnableGravity) {
		MAX_VELOCITY.Set(20, -100, 0);
	}
	else {
		MAX_VELOCITY.SetZero();
	}

	return true;
}

void FurnitureMan::Render()
{
	m_AnimPlayer->Update(m_Anim, m_scene->m_dt);

	glDisable(GL_CULL_FACE);
	m_scene->modelStack.PushMatrix();
	m_scene->modelStack.Translate(pos.x, pos.y, 10); // Prevent Z-Fighting
	m_scene->modelStack.Rotate(rotation.y, 0, 1, 0);
	m_scene->modelStack.Rotate(rotation.z, 0, 0, 1);
	m_scene->modelStack.Rotate(rotation.x, 1, 0, 0);
	m_scene->modelStack.Scale(scale.x, scale.y, scale.z);
	m_scene->meshList[type]->color = color;
	m_scene->meshList[type]->alpha = alpha;
	m_Anim_CurrentFrame = m_Anim->m_currentFrame;
	m_scene->RenderMesh(m_scene->meshList[type], b_EnableLighting);
	m_scene->modelStack.PopMatrix();
	glEnable(GL_CULL_FACE);

}
