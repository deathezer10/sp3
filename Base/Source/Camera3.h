#ifndef CAMERA_3_H
#define CAMERA_3_H

#include "Camera.h"

class Camera3 : public Camera
{
public:
	Vector3 defaultPosition;
	Vector3 defaultTarget;
	Vector3 defaultUp;
	Vector3 defaultView;
	Vector3 defaultRight;

	Vector3 view;
	Vector3 right;
	
	Camera3();
	~Camera3();
	virtual void Init(const Vector3& pos, const Vector3& target, const Vector3& up);
	virtual void Update(double dt);
	virtual void Reset();

	// Offset the Camera's height by this value
	float terrainOffset = 0;

private:
	// Current angle of camera rotation
	float pitch = 0;
	float yaw = 0;

	// Mouse direction moved, possible values: -1, 0, 1
	double mouseMovedX;
	double mouseMovedY;

	// Distance the Cursor moved from the current and last frame. Used to determine mouse sensitivity
	float mouseMovedDistanceX;
	float mouseMovedDistanceY;

	// Updates the direction in which the cursor has moved
	void updateCursor();

	void ResetCursorVariables();

	bool isMouseEnabled = true;

	// Maximum & Minimum height that the User can look
	float MinPitchAngle = -70.0f;
	float MaxPitchAngle = 70.0f;

	// Cursor's previous position
	double lastX = 0;
	double lastY = 0;

	// Clamp the maximum distance that the Camera go
	const float MaxXDistance = 500.0f;
	const float MaxZDistance = 500.0f;


};

#endif