#ifndef ENEMY_ARCHER_H
#define ENEMY_ARCHER_H

#include "AI_Base.h"


class SpriteAnimation;
struct Animation;


class Enemy_Archer : public AI_Base
{
public:
	Enemy_Archer(Scene* scene, Player*player);
	~Enemy_Archer();

	bool Update();

	void AI_Chase(Player* player, double dt);
	void AI_Attack(Player* player, double dt);

	void Render();

private:
	float m_time = 5.f;
	float m_firingAngle = 0.0f;
	float m_attackCoolDown = 1.5f;

	SpriteAnimation * m_AnimEnemyArcher;
	Animation* m_Anim;

};
#endif // ! ENEMY_ARCHER_H

