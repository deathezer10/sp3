#include "Armour_Kit.h"
#include "Scene.h"
#include "ColliderFactory.h"
#include "PlayerManager.h"

Armour_Kit::Armour_Kit(Scene * scene) : GameObject(scene,Scene::GEO_ARMOUR)
{
	m_armourValue = 10;

	b_EnableGravity = true;
	b_EnablePhysics = true;
	b_EnableLighting = false;

	active = true;
	scale.Set(5, 5, 1);
	this->collider = ColliderFactory::CreateOBBCollider(this, Vector3(4.f, 4.f, 4.f));

}

Armour_Kit::~Armour_Kit()
{
}

void Armour_Kit::Render()
{

	m_scene->modelStack.PushMatrix();
	m_OffsetPos.y = cos(m_scene->m_elapsedTime);
	m_scene->modelStack.Translate(pos.x + m_OffsetPos.x, pos.y + m_OffsetPos.y, m_OffsetPos.z + (currentIndex * 0.01f)); // Prevent Z-Fighting
	m_scene->modelStack.Rotate(rotation.y + m_OffsetRot.y, 0, 1, 0);
	m_scene->modelStack.Rotate(rotation.z + m_OffsetRot.z, 0, 0, 1);
	m_scene->modelStack.Rotate(rotation.x + m_OffsetRot.x, 1, 0, 0);
	m_scene->modelStack.Scale(scale.x + m_OffsetScale.x, scale.y + m_OffsetScale.y, scale.z + m_OffsetScale.z);
	m_scene->RenderMesh(m_scene->meshList[type], b_EnableLighting, m_Anim_CurrentFrame);
	m_scene->modelStack.PopMatrix();
}

void Armour_Kit::OnCollisionHit(GameObject * other)
{
	if (other->type == Scene::GEO_PLAYER)
	{
		m_scene->playerManager.IncreaseArmor(m_armourValue);
		m_scene->goManager.DestroyObjectQueue(this);
	}
}


bool Armour_Kit::Update()
{
	return true;
}




