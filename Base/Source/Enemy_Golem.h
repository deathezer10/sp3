#pragma once
#include"AI_Base.h"


class SpriteAnimation;
struct Animation;


class Enemy_Golem :public AI_Base
{
public:
	Enemy_Golem(Scene* scene, Player* player);
	~Enemy_Golem();

	//void update(Player* player , double dt);

	bool Update();

	void AI_Chase();
	void AI_Attack();
	void AI_Stun();
	void Render();

	void OnCollisionHit(GameObject* obj);

	float ROTATION_SPEED;

private:
	Vector3 m_offsetPosition;
	float m_offset;
	float m_launchAngle;

	Vector3 distanceTraveled;

	SpriteAnimation* m_AnimatedGolem;
	Animation* m_Anim;

};