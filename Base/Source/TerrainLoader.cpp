#include "TerrainLoader.h"

#include "Player.h"
#include "Brick.h"
#include "FurnitureMan.h"

#include "Scene.h"
#include "SceneManager.h"
#include "ColliderFactory.h"
#include "GameObjectManager.h"

#include <sstream>
#include <fstream>
#include <iostream>


using namespace std;


bool TerrainLoader::IsTerrain(unsigned geoType) {
	return (geoType >= Scene::GEO_TERRAIN_GRASS && geoType <= Scene::GEO_TERRAIN_INVISIBLE_WALL);
}

bool TerrainLoader::LoadTerrain(string filePath, GameObjectManager* goManager) {

	int count = 0;

	Scene* m_scene = SceneManager::getInstance()->getCurrentScene();

	std::ifstream file(filePath, std::ios::in);
	std::string line;

	// Failed to open file
	if (file.is_open() == false) {
		cout << "Failed to open " << filePath << ".." << endl;
		return false;
	}

	// Read line by line while outputting the data into vector<Monster>
	while (std::getline(file, line)) {

		std::stringstream linestream(line);
		std::string data;

		unsigned type;
		Vector3 pos;
		Vector3 scale;
		Vector3 dir;
		Vector3 boxSize;

		unsigned index = 0;

		// Delimittedly read from line and output the data into appropriate stats
		while (getline(linestream, data, ',')) {
			switch (index) {
			case 0:
				type = stoi(data);
				break;
			case 1:
				pos.x = stof(data);
				break;
			case 2:
				pos.y = stof(data);
				break;
			case 3:
				pos.z = stof(data);
				break;
			case 4:
				scale.x = stof(data);
				break;
			case 5:
				scale.y = stof(data);
				break;
			case 6:
				scale.z = stof(data);
				break;
			case 7:
				dir.x = stof(data);
				break;
			case 8:
				dir.y = stof(data);
				break;
			case 9:
				dir.z = stof(data);
				break;
			case 10:
				boxSize.x = stof(data);
				break;
			case 11:
				boxSize.y = stof(data);
				break;
			case 12:
				boxSize.z = stof(data);
				break;
			}
			index++;
		}

		// Create the new GameObject
		Brick* terrain = new Brick(m_scene);
		terrain->active = true;
		terrain->type = type;
		terrain->dir = dir;
		terrain->bounciness = 0;
		terrain->mass = 0;
		terrain->scale = scale;
		terrain->collider = ColliderFactory::CreateOBBCollider(terrain, boxSize);
		terrain->pos = pos;

		goManager->CreateObject(terrain);

		count++;
	}

	file.close();
	cout << "Level loaded successfully. Created " << count << " GameObject(s)" << endl;

	return true;
}


void TerrainLoader::SaveTerrain(GameObjectManager * goManager) {

	const string filename = "level00.txt";

	ofstream myfile;
	myfile.open(filename, ios::out | ios::trunc);

	int count = 0;

	for (auto &it : goManager->m_goList) {

		// Don't save the player
		if (dynamic_cast<FurnitureMan*>(it) == nullptr && dynamic_cast<Player*>(it) == nullptr) {

			myfile << it->type << ',';

			myfile << it->pos.x << ',';
			myfile << it->pos.y << ',';
			myfile << it->pos.z << ',';

			myfile << it->scale.x << ',';
			myfile << it->scale.y << ',';
			myfile << it->scale.z << ',';

			myfile << it->dir.x << ',';
			myfile << it->dir.y << ',';
			myfile << it->dir.z << ',';

			myfile << it->collider.bboxSize.x << ',';
			myfile << it->collider.bboxSize.y << ',';
			myfile << it->collider.bboxSize.z << endl;

			count++;
		}

	}

	myfile.close();

	cout << "Level saved successfully, written " << count << " GameObject(s) to file." << " Filename: " << filename << endl;
}
