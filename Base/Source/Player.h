#pragma once

#include <vector>
#include "GameObject.h"


class Camera;
class WeaponInfo;
class SpriteAnimation;
struct Animation;

namespace irrklang {
	class ISound;
}

class Player : public GameObject {

public:
	Player(Scene* scene);
	virtual ~Player();

	virtual bool Update();
	virtual void Render();
	virtual void OnCollisionHit(GameObject* other);

	void useDash();

	bool b_isRealPlayer = true;
	
	bool b_isJumping;

	bool b_isAttacking;

	bool b_isFacingRight;

	int m_weaponIndex;

	WeaponInfo* m_weaponManager[2];

	bool m_InfiniteSpeed = false;
	
private:
	irrklang::ISound* m_WalkSound;

	SpriteAnimation* m_AnimPlayer;
	Animation* m_Anim;

	float m_DashDuration;
	float m_DashCooldown;
};