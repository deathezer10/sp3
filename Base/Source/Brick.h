#pragma once


#include "GameObject.h"


class Brick : public GameObject{


public:
	Brick(Scene* scene);
	~Brick() {};

	virtual bool Update();
	virtual void OnCollisionHit(GameObject* other);

private:
	// Lava Damage
	float m_Lava_NextDamageTime = 0;
	const float m_Lava_DamageInterval = 0.5f;
	const float m_Lava_Damage = 5.f;


};