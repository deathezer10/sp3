#pragma once


#include"AI_Base.h"



class SpriteAnimation;
class DemonCrystal;
class DemonLightning;
class Portal;

struct Animation;


class Enemy_Demon : public AI_Base {

public:
	Enemy_Demon(Scene* scene, Player* player, Portal* deathThrone);
	~Enemy_Demon();

	bool Update();
	void Render();

	void OnDestroy();
	void OnReduceHealth(float damage);
	void OnCollisionHit(GameObject * other);
	
	// Start combat manuvers
	void ActivateDemon(DemonCrystal* crystal1, DemonCrystal* crystal2, DemonCrystal* crystal3, DemonCrystal* crystal4);

	bool IsDemonActive() { return b_IsDemonActive; }

private:
	enum DEMON_STATE {

		IDLE = 0,
		STUN,
		FIREBALL,
		SPEAR,
		LASER

	};

	SpriteAnimation * m_AnimEnemyDemon;
	Animation* m_Anim;

	Portal* m_DeathThrone = nullptr;

	// 4 Crystals protecting the demon
	DemonCrystal* m_Crystal1;
	DemonCrystal* m_Crystal2;
	DemonCrystal* m_Crystal3;
	DemonCrystal* m_Crystal4;

	DEMON_STATE m_CurrentDemonState = IDLE;

	int m_CurrentCrystal;
	int m_CurrentSkill;
	
	bool b_IsDemonAppearing = false;
	bool b_IsDemonActive = false;

	float m_DemonNextSkillTime = 0;
	
	// Forcefield
	bool b_IsForcefieldActive = true;
	float m_DemonNextStunThreshold; // If HP drops below this, stuns the boss
	const float m_DemonStunTime = 10;
	float m_ForcefieldAlpha = 0;

	// Fireball
	float m_DemonFireballInterval = 0.75f; // Interval between each fireball
	float m_DemonNextFireballTime = 0;
	int m_DemonFireballCount = 0; // Stop shooting when reached FireballMax
	const int m_DemonFireballMax = 5; // How many fireballs to shoot?

	// Spear
	const int m_DemonSpearCount = 6;

	// Laser
	DemonLightning* m_Laser1 = nullptr;
	DemonLightning* m_Laser2 = nullptr;
	float m_CurrentLaserScale = 1;
	float m_CurrentLaserTime = 0;


	void StunDemon();
	void Stunned();
	void SkillFireball();
	void SkillSpear();
	void SkillLaser();

};
