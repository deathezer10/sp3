#pragma once

#include "Vector3.h"
#include "Vertex.h"

class Scene;

// Flexible Particle Class, could be further extended for added functionality
class Particle {

public:
	Particle(Scene* scene, unsigned meshType);
	~Particle() {};
	
	// Toggle gravity
	bool b_EnableGravity = false;

	Vector3 pos;
	Vector3 vel;
	Color color;
	float scale = 1;

	// In Degrees per second
	float rotationSpeed = 0;

	void SetLifeTime(float time);
	float GetLifeTime() { return lifetime; }
	
	virtual void UpdateMovement(const Vector3 &emitterPos);
	virtual void Render(const Vector3 &emitterPos);

private:
	Scene* m_scene;
	unsigned type;
	
	const float gravity = 9.8f;

	// Duration of this particle in seconds
	float lifetime = 0;

	float currentLifetime = 0;
	float currentRotation = 0;



};