#ifndef HEALTH_WELL_H
#define HEALTH_WELL_H
#include "GameObject.h"

class Health_Well :public GameObject
{
public:
	Health_Well(Scene*scene);
	~Health_Well();

	int getHealthValue() { return m_healthValue; }

	void Render();
	void OnCollisionHit(GameObject*other);
	bool Update();
private:
	int m_healthValue = 0;// healing value
	bool b_translation = true;
	float m_CurrentHealthCharge =100;
};
#endif 