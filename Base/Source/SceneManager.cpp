#include "SceneManager.h"
#include "Application.h"
#include "Scene.h"

#include "Main_Menu.h"
#include "SceneEditor.h"
#include "SceneLevel01.h"
#include "SceneLevel02.h"
#include "SceneLevel03.h"
#include "PlayerData.h"

#include <GLFW\glfw3.h>

#include  <iomanip>

SceneManager *SceneManager::_instance = nullptr;


SceneManager* SceneManager::getInstance() {
	if (!_instance) {
		_instance = new SceneManager();
	}
	return _instance;
}

Scene * SceneManager::CreateScene(unsigned type)
{
	switch (type)
	{
	case Scene::SCENE_MAINMENU:
		return new Main_Menu();
	case Scene::SCENE_EDITOR:
		return new SceneEditor();
	case Scene::SCENE_LEVEL01:
		return new SceneLevel01();
	case Scene::SCENE_LEVEL02:
		return new SceneLevel02();
	case Scene::SCENE_LEVEL03:
		return new SceneLevel03();
	}

	return nullptr;
}

void SceneManager::Exit() {
	if (_instance) {
		delete _instance; // Delete the singleton instance on exit
		_instance = nullptr;
	}

	PlayerDataManager::getInstance()->Exit();
}

void SceneManager::Update() {

	m_timer.startTimer(); // Start timer to calculate how long it takes to render this frame

	GLFWwindow* window = glfwGetCurrentContext();

	// For synchronizing of Update() loop
	DWORD simulationTime = timeGetTime();

	while (!glfwWindowShouldClose(window))
	{
		// If there is any pending Scene, delete the current one and switch Scene
		if (_hasPendingScene) {
			StopWatch time = StopWatch();
			time.startTimer();
			std::cout << "Switching Scene.." << std::endl;
			currentScene->Exit();
			delete currentScene; // free memory
			currentScene = pendingScene;
			currentScene->Init();
			_hasPendingScene = false;
			pendingScene = nullptr;
			std::cout << "Loaded Scene - Time taken: " << std::setprecision(2) << time.getDeltaTime() << "s" << std::endl;
		}

		glfwPollEvents();

		// To simulate physics 60 times a second
		while (!glfwWindowShouldClose(window) && simulationTime <= timeGetTime()) {
			Application::UpdateInput();
			simulationTime += Application::frameTime;
			currentScene->Update(Application::deltaTime);
			Application::EndFrameUpdate();

			if (_hasPendingScene)
				break;
		}

		currentScene->Render();
		//Swap buffers
		glfwSwapBuffers(window);
		//Get and organize events, like keyboard and mouse input, window resizing, etc...
		m_timer.waitUntil(Application::frameTime);       // Frame rate limiter. Limits each frame to a specified time in ms.  


		Application::m_FPS = 1.f / (float)m_timer.getDeltaTime();
	}

	currentScene->Exit();
	delete currentScene;

}

void SceneManager::changeScene(Scene* scene) {
	if (!currentScene) {
		currentScene = scene;
		scene->Init();
	}
	else {
		if (pendingScene)
			delete pendingScene;

		pendingScene = scene;
		_hasPendingScene = true;
	}
}