#pragma once


#include "AI_Base.h"

class Player;


class DemonCrystal : public AI_Base {


public:
	DemonCrystal(Scene* scene, Player* player);
	~DemonCrystal() {};

	virtual bool Update();
	virtual void Render();
	virtual void OnCollisionHit(GameObject* other);

	bool b_IsCrystalActive = false;
	bool b_IsSecretCrystal = false;

private:


};