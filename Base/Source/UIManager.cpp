#include "Application.h"

#include "GL\glew.h"
#include "GLFW\glfw3.h"

#include "Mesh.h"
#include "UI_Bar.h"
#include "SceneBase.h"

#include "Player.h"
#include "UIManager.h"
#include "SceneManager.h"
#include "PlayerManager.h"
#include "PlayerData.h"

#include <fstream>
#include <sstream>


using std::ostringstream;



UIManager::UIManager(Scene* scene) {

	m_scene = scene;

	textMesh = &(m_scene->meshList[Scene::GEO_TEXT]);

	anchor_offset[ANCHOR_BOT_LEFT] = 0;
	anchor_offset[ANCHOR_BOT_RIGHT] = 0;
	anchor_offset[ANCHOR_TOP_LEFT] = (Application::windowHeight() / 10) - 2;
	anchor_offset[ANCHOR_TOP_RIGHT] = (Application::windowHeight() / 10) - 2;
	anchor_offset[ANCHOR_BOT_CENTER] = 0;
	anchor_offset[ANCHOR_TOP_CENTER] = (Application::windowHeight() / 10) - 2;
	anchor_offset[ANCHOR_CENTER_CENTER] = 0;
}

bool UIManager::LoadFontWidth(std::string fontPath) {

	// Clear vector if it's loaded
	if (currentFontWidth.empty())
		currentFontWidth.clear();

	std::ifstream fileStream(fontPath);

	if (!fileStream.is_open()) {
		std::cout << "Impossible to open " << fontPath << ". Are you in the right directory ?\n";
		return false;
	}

	std::string data;
	while (std::getline(fileStream, data, '\n')) // read every line
	{
		currentFontWidth.push_back(std::stoul(data));
	}

	return true;
}

//Render the mesh onto the world
void UIManager::queueRenderMesh(MeshQueue meshQueue) {
	currentMeshQueue.push(meshQueue);
}

void UIManager::queueRenderText(Text text) {
	currentTextQueue.push(text);
}

void UIManager::dequeueMesh() {

	// Render all queued meshes
	while (!currentMeshQueue.empty()) {

		MeshQueue mesh = currentMeshQueue.front();

		m_scene->modelStack.PushMatrix();
		m_scene->modelStack.Translate(mesh.position.x, mesh.position.y, mesh.position.z);
		m_scene->modelStack.Rotate(mesh.rotation.y, 0, 1, 0);
		m_scene->modelStack.Rotate(mesh.rotation.z, 0, 0, 1);
		m_scene->modelStack.Rotate(mesh.rotation.x, 1, 0, 0);
		m_scene->modelStack.Scale(mesh.scaling.x, mesh.scaling.y, mesh.scaling.z);

		if (mesh.wireframe == true) {
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); //default fill mode
			m_scene->RenderMesh(mesh.mesh, mesh.lighting);
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); //default fill mode
		}
		else {
			m_scene->RenderMesh(mesh.mesh, mesh.lighting);

		}

		m_scene->modelStack.PopMatrix();

		currentMeshQueue.pop();
	}
}

void UIManager::dequeueText() {
	// Print all the queued Texts
	while (!currentTextQueue.empty()) {
		renderTextOnScreen(currentTextQueue.front());
		currentTextQueue.pop();
	}
}

void UIManager::renderTextOnScreen(Text text) {

	if (!textMesh || (*textMesh)->textureArray[0] <= 0) //Proper error check
		return;

	glDisable(GL_DEPTH_TEST);

	Mtx44 ortho;
	ortho.SetToOrtho(0, Application::windowWidth() / 10, 0, Application::windowHeight() / 10, -10, 10); //size of screen UI
	m_scene->projectionStack.PushMatrix();
	m_scene->projectionStack.LoadMatrix(ortho);
	m_scene->viewStack.PushMatrix();
	m_scene->viewStack.LoadIdentity(); //No need camera for ortho mode
	m_scene->modelStack.PushMatrix();
	m_scene->modelStack.LoadIdentity(); //Reset modelStack

	glUniform1i(m_scene->m_parameters[Scene::U_TEXT_ENABLED], 1);
	glUniform3fv(m_scene->m_parameters[Scene::U_TEXT_COLOR], 1, &text.color.r);
	glUniform1i(m_scene->m_parameters[Scene::U_LIGHTENABLED], 0);
	glUniform1i(m_scene->m_parameters[Scene::U_COLOR_TEXTURE_ENABLED], 1);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, (*textMesh)->textureArray[0]);
	glUniform1i(m_scene->m_parameters[Scene::U_COLOR_TEXTURE], 0);

	// Width of the entire string
	float totalWidth = 0;

	// Calculate total width of the string, used for offsetting the anchors
	for (unsigned i = 0; i < text.value.length(); ++i) {
		totalWidth += currentFontWidth[text.value[i]] / 32.0f;
	}

	float tX = 0, tY = 0;

	// Move the text position according to the given anchor
	switch (text.anchor) {

	case ANCHOR_BOT_LEFT:
		tY = (float)anchor_offset[ANCHOR_BOT_LEFT];
		anchor_offset[ANCHOR_BOT_LEFT] += 2;
		break;

	case ANCHOR_BOT_RIGHT:
		tX = (Application::windowWidth() / 10) - totalWidth;
		tY = (float)anchor_offset[ANCHOR_BOT_RIGHT];
		anchor_offset[ANCHOR_BOT_RIGHT] += 2;
		break;

	case ANCHOR_TOP_LEFT:
		tY = (float)anchor_offset[ANCHOR_TOP_LEFT];
		anchor_offset[ANCHOR_TOP_LEFT] -= 2;
		break;

	case ANCHOR_TOP_RIGHT:
		tX = (Application::windowWidth() / 10) - totalWidth;
		tY = (float)anchor_offset[ANCHOR_TOP_RIGHT];
		anchor_offset[ANCHOR_TOP_RIGHT] -= 2;
		break;

	case ANCHOR_BOT_CENTER:
		tX = (Application::windowWidth() / 20) - (totalWidth / 2);
		tY = (float)anchor_offset[ANCHOR_BOT_CENTER];
		anchor_offset[ANCHOR_BOT_CENTER] += 2;
		break;

	case ANCHOR_TOP_CENTER:
		tX = (Application::windowWidth() / 20) - (totalWidth / 2);
		tY = (float)anchor_offset[ANCHOR_TOP_CENTER];
		anchor_offset[ANCHOR_TOP_CENTER] -= 2;
		break;

	case ANCHOR_CENTER_CENTER:
		tX = (Application::windowWidth() / 20) - (totalWidth / 2);
		tY = (Application::windowHeight() / 20) - (float)anchor_offset[ANCHOR_CENTER_CENTER];
		anchor_offset[ANCHOR_CENTER_CENTER] += 2;
		break;

	default:
		break;

	}

	m_scene->modelStack.Translate(tX + 1, tY + 1, 0); // minor offset to display properly

	MS textStack;
	unsigned stackCount = 0;

	float prevWidth = 0; // the previous printed character width

	for (unsigned i = 0; i < text.value.length(); ++i, ++stackCount) {

		textStack.PushMatrix();
		textStack.Translate(prevWidth, 0, 0); // spacing of each character

		prevWidth = currentFontWidth[text.value[i]] / 32.0f;

		Mtx44 MVP = m_scene->projectionStack.Top() * m_scene->viewStack.Top() * m_scene->modelStack.Top() * textStack.Top();
		glUniformMatrix4fv(m_scene->m_parameters[Scene::U_MVP], 1, GL_FALSE, &MVP.a[0]);

		(*textMesh)->Render((unsigned)text.value[i] * 6, 6);
	}

	glBindTexture(GL_TEXTURE_2D, 0);
	glUniform1i(m_scene->m_parameters[Scene::U_TEXT_ENABLED], 0);

	for (unsigned i = 0; i < stackCount; ++i) {
		textStack.PopMatrix();
	}

	m_scene->projectionStack.PopMatrix();
	m_scene->viewStack.PopMatrix();
	m_scene->modelStack.PopMatrix();

	glEnable(GL_DEPTH_TEST);
}

void UIManager::addTimedMeshToScreen(MeshQueue mesh, float duration) {
	currentTimeMeshes[mesh] = duration;
}

//void UIManager::addMeshQueue(MeshQueue meshQueue)
//{
//	vectorMeshQueue.push_back(meshQueue);
//}
//
//void UIManager::RenderVectorMeshQueue()
//{
//	Mtx44 ortho;
//	ortho.SetToOrtho(0, Application::windowWidth() / 10, 0, Application::windowHeight() / 10, -100, 100); //size of screen UI
//
//	for (size_t i = 0; i < vectorMeshQueue.size(); ++i)
//	{
//
//		_scene->projectionStack.PushMatrix();
//		_scene->projectionStack.LoadMatrix(ortho);
//		_scene->viewStack.PushMatrix();
//		_scene->viewStack.LoadIdentity(); //No need camera for ortho mode
//		_scene->modelStack.PushMatrix();
//		_scene->modelStack.LoadIdentity();
//		_scene->modelStack.Translate(vectorMeshQueue[i].position.x, vectorMeshQueue[i].position.y, 1);
//		_scene->modelStack.Rotate(vectorMeshQueue[i].rotation.y, 0, 1, 0);
//		_scene->modelStack.Rotate(vectorMeshQueue[i].rotation.z, 0, 0, 1);
//		_scene->modelStack.Rotate(vectorMeshQueue[i].rotation.x, 1, 0, 0);
//		_scene->modelStack.Scale(vectorMeshQueue[i].scaling.x, vectorMeshQueue[i].scaling.y, vectorMeshQueue[i].scaling.z);
//		_scene->RenderMesh(vectorMeshQueue[i].mesh, false); //UI should not have light
//		_scene->projectionStack.PopMatrix();
//		_scene->viewStack.PopMatrix();
//		_scene->modelStack.PopMatrix();
//	}
//}

//void UIManager::updateSelectionMesh(double dt, MeshQueue meshQueue)
//{
//	meshQueue.position.x += vectorMeshQueue[1].position.x + vectorMeshQueue[1].scaling.x + meshQueue.scaling.x;
//	if (Application::IsKeyPressed('S'))
//	{
//		meshQueue.position.Set(10, 10, 0);
//	}
//}
//
//UIManager::MeshQueue UIManager::getMeshQueue(int index)
//{
//	return vectorMeshQueue[index];
//}

void UIManager::RenderTimedMeshOnScreen() {


	for (auto &it = currentTimeMeshes.begin(); it != currentTimeMeshes.end();) {

		MeshQueue mq = it->first;

		if (it->second > 0) {
			RenderMeshOnScreen(mq.mesh, mq.position.x, mq.position.y, mq.rotation, mq.scaling);
			it->second -= m_scene->m_dt;
			++it;
		}
		else {
			it = currentTimeMeshes.erase(it);
		}
	}

}

void UIManager::RenderMeshOnScreen(Mesh* mesh, float x, float y, Vector3 rotate, Vector3 scale) {
	// glDisable(GL_DEPTH_TEST);
	Mtx44 ortho;
	ortho.SetToOrtho(0, Application::windowWidth() / 10, 0, Application::windowHeight() / 10, -100, 100); //size of screen UI
	m_scene->projectionStack.PushMatrix();
	m_scene->projectionStack.LoadMatrix(ortho);
	m_scene->viewStack.PushMatrix();
	m_scene->viewStack.LoadIdentity(); //No need camera for ortho mode
	m_scene->modelStack.PushMatrix();
	m_scene->modelStack.LoadIdentity();
	m_scene->modelStack.Translate(x, y, 1);
	m_scene->modelStack.Rotate(rotate.y, 0, 1, 0);
	m_scene->modelStack.Rotate(rotate.z, 0, 0, 1);
	m_scene->modelStack.Rotate(rotate.x, 1, 0, 0);
	m_scene->modelStack.Scale(scale.x, scale.y, scale.z);
	m_scene->RenderMesh(mesh, false); //UI should not have light
	m_scene->projectionStack.PopMatrix();
	m_scene->viewStack.PopMatrix();
	m_scene->modelStack.PopMatrix();
	// glEnable(GL_DEPTH_TEST);
}

void UIManager::RenderMeshOnScreen(Mesh* mesh, float x, float y, float z, Vector3 rotate, Vector3 scale) {
	// glDisable(GL_DEPTH_TEST);
	glEnable(GL_SAMPLE_ALPHA_TO_COVERAGE); // ADDED THIS NEW STUFF
	Mtx44 ortho;
	ortho.SetToOrtho(0, Application::windowWidth() / 10, 0, Application::windowHeight() / 10, -100, 100); //size of screen UI
	m_scene->projectionStack.PushMatrix();
	m_scene->projectionStack.LoadMatrix(ortho);
	m_scene->viewStack.PushMatrix();
	m_scene->viewStack.LoadIdentity(); //No need camera for ortho mode
	m_scene->modelStack.PushMatrix();
	m_scene->modelStack.LoadIdentity();
	m_scene->modelStack.Translate(x, y, z);
	m_scene->modelStack.Rotate(rotate.y, 0, 1, 0);
	m_scene->modelStack.Rotate(rotate.z, 0, 0, 1);
	m_scene->modelStack.Rotate(rotate.x, 1, 0, 0);
	m_scene->modelStack.Scale(scale.x, scale.y, scale.z);
	m_scene->RenderMesh(mesh, false); //UI should not have light
	m_scene->projectionStack.PopMatrix();
	m_scene->viewStack.PopMatrix();
	m_scene->modelStack.PopMatrix();
	glDisable(GL_SAMPLE_ALPHA_TO_COVERAGE);
	// glEnable(GL_DEPTH_TEST);
}

void UIManager::RenderTextOnScreen(Mesh * mesh, std::string text, Color color, float size, float x, float y)
{

	if (!textMesh || (*textMesh)->textureArray[0] <= 0) //Proper error check
		return;

	glDisable(GL_DEPTH_TEST);
	Mtx44 ortho;
	ortho.SetToOrtho(0, 80, 0, 60, -10, 10);
	m_scene->projectionStack.PushMatrix();
	m_scene->projectionStack.LoadMatrix(ortho);
	m_scene->viewStack.PushMatrix();
	m_scene->viewStack.LoadIdentity();
	m_scene->modelStack.PushMatrix();
	m_scene->modelStack.LoadIdentity();
	m_scene->modelStack.Translate(x, y, 0);
	m_scene->modelStack.Scale(size, size, size);


	glUniform1i(m_scene->m_parameters[Scene::U_TEXT_ENABLED], 1);
	glUniform3fv(m_scene->m_parameters[Scene::U_TEXT_COLOR], 1, &color.r);
	glUniform1i(m_scene->m_parameters[Scene::U_LIGHTENABLED], 0);
	glUniform1i(m_scene->m_parameters[Scene::U_COLOR_TEXTURE_ENABLED], 1);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, (*textMesh)->textureArray[0]);
	glUniform1i(m_scene->m_parameters[Scene::U_COLOR_TEXTURE], 0);


	for (unsigned i = 0; i < text.length(); ++i)
	{
		Mtx44 characterSpacing;
		characterSpacing.SetToTranslation(i * 1.0f + 0.5f, 0.5f, 0); //1.0f is the spacing of each character, you may change this value
		Mtx44 MVP = m_scene->projectionStack.Top() * m_scene->viewStack.Top() * m_scene->modelStack.Top() * characterSpacing;
		glUniformMatrix4fv(m_scene->m_parameters[Scene::U_MVP], 1, GL_FALSE, &MVP.a[0]);

		mesh->Render((unsigned)text[i] * 6, 6);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	glUniform1i(m_scene->m_parameters[Scene::U_TEXT_ENABLED], 0);
	m_scene->modelStack.PopMatrix();
	m_scene->viewStack.PopMatrix();
	m_scene->projectionStack.PopMatrix();
	glEnable(GL_DEPTH_TEST);

}

void UIManager::RenderPlayerHud()
{
	glDisable(GL_DEPTH_TEST);

	SceneBase* _scene = static_cast<SceneBase*>(m_scene);
	Mesh** meshList = m_scene->meshList; // Shortcut access to meshList[GEO_TYPE]

	// Brown background frame
	RenderMeshOnScreen(m_scene->meshList[Scene::GEO_BAR_BG], Application::windowWidth()*0.008f, Application::windowHeight()*0.094f, Vector3(0.f, 0.f, 0.f), Vector3(18.f * Application::windowWidth()*0.001f, 4.f *Application::windowHeight()*0.001f, 1.f));

	// Heart & Power Icon
	RenderMeshOnScreen(m_scene->meshList[Scene::GEO_HEART], Application::windowWidth()*0.0095f, Application::windowHeight()*0.096f, 1.f, Vector3(0.f, 0.f, 0.f), Vector3(1.5f* Application::windowWidth()*0.001f, 1.5f *Application::windowHeight()*0.001f, 1.f));
	RenderMeshOnScreen(m_scene->meshList[Scene::GEO_POWER], Application::windowWidth()*0.0095f, Application::windowHeight()*0.092f, 1.f, Vector3(0.f, 0.f, 0.f), Vector3(1.5f* Application::windowWidth()*0.001f, 1.5f *Application::windowHeight()*0.001f, 1.f));

	// Scaling of current HP/Armor/Power
	float healthScale = (float)_scene->playerManager.GetHealth() / 100;
	float armorScale = (float)_scene->playerManager.GetArmor() / 100;
	float powerScale = (float)_scene->playerManager.GetUltimateCharge() / 100;

	// On-screen HP Bar
	RenderMeshOnScreen(m_scene->meshList[Scene::GEO_HP_FRAME], Application::windowWidth()*0.0129f, Application::windowHeight()*0.096f, 1.f, Vector3(0.f, 0.f, 0.f), Vector3(15.1f * Application::windowWidth()*0.001f, 1.3f*Application::windowHeight()*0.001f, 1.f));
	RenderMeshOnScreen(m_scene->meshList[Scene::GEO_HP_BACKGROUND], Application::windowWidth()*0.013f, Application::windowHeight()*0.096f, 1.f, Vector3(0.f, 0.f, 0.f), Vector3(15.f * Application::windowWidth()*0.001f, 1.f*Application::windowHeight()*0.001f, 1.f));
	RenderMeshOnScreen(m_scene->meshList[Scene::GEO_HP_FOREGROUND], Application::windowWidth()*0.013f, Application::windowHeight() *0.096f, 1.f, Vector3(0.f, 0.f, 0.f), Vector3(15.f* healthScale* Application::windowWidth()*0.001f, 1.f*Application::windowHeight()*0.001f, 1.f));

	// On-screen Power Bar
	RenderMeshOnScreen(m_scene->meshList[Scene::GEO_HP_FRAME], Application::windowWidth()*0.0129f, Application::windowHeight()*0.092f, 1.f, Vector3(0.f, 0.f, 0.f), Vector3(15.1f * Application::windowWidth()*0.001f, 1.3f*Application::windowHeight()*0.001f, 1.f));
	RenderMeshOnScreen(m_scene->meshList[Scene::GEO_HP_BACKGROUND], Application::windowWidth()*0.013f, Application::windowHeight()*0.092f, 1.f, Vector3(0.f, 0.f, 0.f), Vector3(15.f * Application::windowWidth()*0.001f, 1.f*Application::windowHeight()*0.001f, 1.f));
	RenderMeshOnScreen(m_scene->meshList[Scene::GEO_POW_FOREGROUND], Application::windowWidth()*0.013f, Application::windowHeight() *0.092f, 1.f, Vector3(0.f, 0.f, 0.f), Vector3(15.f* powerScale * Application::windowWidth()*0.001f, 1.f*Application::windowHeight()*0.001f, 1.f));

	// On-screen Armor Bar
	RenderMeshOnScreen(m_scene->meshList[Scene::GEO_ARMOR_FOREGROUND], Application::windowWidth()*0.013f, Application::windowHeight() *0.096f, 1.f, Vector3(0.f, 0.f, 0.f), Vector3(15.f* armorScale* Application::windowWidth()*0.001f, 1.f*Application::windowHeight()*0.001f, 1));

	// Character Portrait
	RenderMeshOnScreen(m_scene->meshList[Scene::GEO_PORTRAIT], Application::windowWidth()*0.0001f, Application::windowHeight()*0.094f, Vector3(0.f, 0.f, 0.f), Vector3(4.f* Application::windowWidth()*0.001f, 4.f *Application::windowHeight()*0.001f, 1.f));

	//to do: include check weapon lock
	int weapontype = _scene->playerManager.GetPlayer()->m_weaponIndex;
	if (weapontype == 0)
		RenderMeshOnScreen(m_scene->meshList[Scene::GEO_W_FRAME], Application::windowWidth()*0.0015f, Application::windowHeight()*0.0855f, 1.f, Vector3(0.f, 0.f, 0.f), Vector3(6.f* Application::windowWidth()*0.001f, 6.f *Application::windowHeight()*0.001f, 1.f));
	else if (weapontype == 1)
		RenderMeshOnScreen(m_scene->meshList[Scene::GEO_W_FRAME], Application::windowWidth()*0.0015f, Application::windowHeight()*0.0755f, 1.f, Vector3(0.f, 0.f, 0.f), Vector3(6.f* Application::windowWidth()*0.001f, 6.f *Application::windowHeight()*0.001f, 1.f));
	else if (weapontype == 2 && PlayerDataManager::getInstance()->m_data.b_isWeapon3Unlocked == true)
		RenderMeshOnScreen(m_scene->meshList[Scene::GEO_W_FRAME], Application::windowWidth()*0.0015f, Application::windowHeight()*0.0655f, 1.f, Vector3(0.f, 0.f, 0.f), Vector3(6.f* Application::windowWidth()*0.001f, 6.f *Application::windowHeight()*0.001f, 1.f));


	m_scene->meshList[Scene::GEO_SHEARS]->color.Set(1, 1, 1); // Reset Colors
	m_scene->meshList[Scene::GEO_TRANQGUN]->color.Set(1, 1, 1); // Reset Colors
	m_scene->meshList[Scene::GEO_SPEAR]->color.Set(1, 1, 1); // Reset Colors

	// Weapon Icons
	RenderMeshOnScreen(m_scene->meshList[Scene::GEO_SHEARS], Application::windowWidth()*0.0075f, Application::windowHeight()*0.085f, 1.f, Vector3(0.f, 0.f, 0.f), Vector3(5.f* Application::windowWidth()*0.001f, 5.f *Application::windowHeight()*0.001f, 1.f));

	RenderMeshOnScreen(m_scene->meshList[Scene::GEO_TRANQGUN], Application::windowWidth()*0.0075f, Application::windowHeight()*0.075f, 1.f, Vector3(0.f, 0.f, 0.f), Vector3(5.f* Application::windowWidth()*0.001f, 5.f *Application::windowHeight()*0.001f, 1.f));




	/*if (PlayerDataManager::getInstance()->m_data.b_isWeapon3Unlocked == true)
		RenderMeshOnScreen(m_scene->meshList[Scene::GEO_SPEAR], Application::windowWidth()*0.0075f, Application::windowHeight()*0.065f, 1.f, Vector3(0.f, 0.f, 0.f), Vector3(5.f* Application::windowWidth()*0.001f, 9.f *Application::windowHeight()*0.001f, 1.f));
	else
	{
		RenderMeshOnScreen(m_scene->meshList[Scene::GEO_SPEAR], Application::windowWidth()*0.0075f, Application::windowHeight()*0.065f, 1.f, Vector3(0.f, 0.f, 0.f), Vector3(5.f* Application::windowWidth()*0.001f, 9.f *Application::windowHeight()*0.001f, 1.f));
		RenderMeshOnScreen(m_scene->meshList[Scene::GEO_WLOCK], Application::windowWidth()*0.0048f, Application::windowHeight()*0.065f, 1.f, Vector3(0.f, 0.f, 0.f), Vector3(2.5f* Application::windowWidth()*0.001f, 2.5f *Application::windowHeight()*0.001f, 1.f));
	}*/


	// Player current Position
	Vector3 playerPos = m_scene->playerManager.GetPlayer()->pos;
	playerPos.y += 4;


	// Power Bar in world
	UI_BAR::RenderBar(m_scene, meshList[Scene::GEO_HP_BACKGROUND], meshList[Scene::GEO_POW_FOREGROUND], meshList[Scene::GEO_HP_FRAME], playerPos, Vector3(4.0f, 0.5f, 1.0f), powerScale);

	// Health Bar in World
	playerPos.y += 1;
	UI_BAR::RenderBar(m_scene, meshList[Scene::GEO_HP_BACKGROUND], meshList[Scene::GEO_HP_FOREGROUND], meshList[Scene::GEO_HP_FRAME], playerPos, Vector3(4.0f, 0.5f, 1.0f), healthScale);

	// Armor Bar in world
	UI_BAR::RenderBar(m_scene, nullptr, meshList[Scene::GEO_ARMOR_FOREGROUND], nullptr, playerPos, Vector3(4.0f, 0.5f, 1.0f), armorScale);


	if (m_scene->playerManager.GetUltimateCharge() < 50)
	{
		RenderMeshOnScreen(m_scene->meshList[Scene::GEO_W_FRAME_ERROR], Application::windowWidth()* -0.001f, Application::windowHeight()*0.011f, 1.f, Vector3(0.f, 0.f, 0.f), Vector3(5.f* Application::windowWidth()*0.001f, 5.f *Application::windowHeight()*0.001f, 1.f));
	}
	//friendly warrior icon on hud
	RenderMeshOnScreen(m_scene->meshList[Scene::GEO_STAMPEDE], Application::windowWidth()*0.002f, Application::windowHeight()*0.0105f, 1, Vector3(0.f, 0.f, 0.f), Vector3(2.f* Application::windowWidth()*0.001f, 2.f *Application::windowHeight()*0.001f, 1.f));


	//friendly warrior
	std::stringstream text1;
	text1 << " Q";
	RenderTextOnScreen(meshList[Scene::GEO_TEXT], text1.str(), Color(1, 1, 1), 1, 6, 6);

	if (m_scene->playerManager.GetUltimateCharge() < 50)
	{
		RenderMeshOnScreen(m_scene->meshList[Scene::GEO_W_FRAME_ERROR], Application::windowWidth()* -0.001f, Application::windowHeight()*0.004f, 1.f, Vector3(0.f, 0.f, 0.f), Vector3(5.f* Application::windowWidth()*0.001f, 5.f *Application::windowHeight()*0.001f, 1.f));
	}
	//friendly archer icon on hud
	RenderMeshOnScreen(m_scene->meshList[Scene::GEO_ARCHER_ICON], Application::windowWidth()*0.002f, Application::windowHeight()*0.0035f, 1, Vector3(0.f, 0.f, 0.f), Vector3(2.f* Application::windowWidth()*0.001f, 2.f *Application::windowHeight()*0.001f, 1.f));


	//friendly archer
	std::stringstream text2;
	text2 << " W";
	RenderTextOnScreen(meshList[Scene::GEO_TEXT], text2.str(), Color(1, 1, 1), 1, 6, 2);

	//attract
	RenderMeshOnScreen(m_scene->meshList[Scene::GEO_ATTRACT], Application::windowWidth()*0.0022f, Application::windowHeight()*0.0245f, 1, Vector3(0.f, 0.f, 0.f), Vector3(2.f* Application::windowWidth()*0.001f, 2.f *Application::windowHeight()*0.001f, 1.f));

	if (m_scene->playerManager.GetUltimateCharge() < 25)
	{
		RenderMeshOnScreen(m_scene->meshList[Scene::GEO_W_FRAME_ERROR], Application::windowWidth()* -0.001f, Application::windowHeight()*0.025f, 1.f, Vector3(0.f, 0.f, 0.f), Vector3(5.f* Application::windowWidth()*0.001f, 5.f *Application::windowHeight()*0.001f, 1.f));
	}
	std::stringstream text3;
	text3 << " S";
	RenderTextOnScreen(meshList[Scene::GEO_TEXT], text3.str(), Color(1, 1, 1), 1, 6, 10);

	if (m_scene->playerManager.GetUltimateCharge() < 25)
	{
		RenderMeshOnScreen(m_scene->meshList[Scene::GEO_W_FRAME_ERROR], Application::windowWidth()* -0.001f, Application::windowHeight()*0.0175f, 1.f, Vector3(0.f, 0.f, 0.f), Vector3(5.f* Application::windowWidth()*0.001f, 5.f *Application::windowHeight()*0.001f, 1.f));
	}
	//pipe bomb
	RenderMeshOnScreen(m_scene->meshList[Scene::GEO_PIPEBOMB], Application::windowWidth()*0.004f, Application::windowHeight()*0.017f, 1, Vector3(0.f, 0.f, 0.f), Vector3(3.f* Application::windowWidth()*0.001f, 3.f *Application::windowHeight()*0.001f, 1.f));

	std::stringstream text4;
	text4 << " A";
	RenderTextOnScreen(meshList[Scene::GEO_TEXT], text4.str(), Color(1, 1, 1), 1, 6, 14);

	glEnable(GL_DEPTH_TEST);



	// Player Current Score
	std::stringstream text;
	text.str("");
	text << "Wool: " << m_scene->playerManager.GetCurrentScore();
	renderTextOnScreen(UIManager::Text{ text.str(), Color(1,1,0), UIManager::ANCHOR_BOT_CENTER });

	// Cheat key
	if (m_scene->playerManager.IsCheatEnabled()) {
		text.str("");
		text << "Cheat Enabled";
		renderTextOnScreen(UIManager::Text{ text.str(), Color(1,1,0), UIManager::ANCHOR_BOT_RIGHT });
	}

	if (Scene::ShowDebugInfo) {

		text.str("");
		text << "Player Velocity: " << m_scene->playerManager.GetPlayer()->vel;
		m_scene->textManager.renderTextOnScreen(UIManager::Text{ text.str(), Color(1,1,1), UIManager::ANCHOR_BOT_CENTER });

		text.str("");
		text << "Player Manifold: " << m_scene->playerManager.GetPlayer()->collider.manifold.normal;
		m_scene->textManager.renderTextOnScreen(UIManager::Text{ text.str(), Color(1,1,1), UIManager::ANCHOR_BOT_CENTER });

		text.str("");
		text << "Position: " << m_scene->playerManager.GetPlayer()->pos;
		m_scene->textManager.renderTextOnScreen(UIManager::Text{ text.str(), Color(1,1,1), UIManager::ANCHOR_BOT_CENTER });

	}





}

void UIManager::reset() {

	anchor_offset[ANCHOR_BOT_LEFT] = 0;
	anchor_offset[ANCHOR_BOT_RIGHT] = 0;
	anchor_offset[ANCHOR_TOP_LEFT] = (Application::windowHeight() / 10) - 2;
	anchor_offset[ANCHOR_TOP_RIGHT] = (Application::windowHeight() / 10) - 2;
	anchor_offset[ANCHOR_BOT_CENTER] = 0;
	anchor_offset[ANCHOR_TOP_CENTER] = (Application::windowHeight() / 10) - 2;
	anchor_offset[ANCHOR_CENTER_CENTER] = 0;

}


