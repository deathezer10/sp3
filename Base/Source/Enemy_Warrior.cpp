#include "Enemy_Warrior.h"
#include "Player.h"
#include "Scene.h"
#include "GameObject.h"
#include "PlayerManager.h"
#include "SpriteAnimation.h"
#include "ColliderFactory.h"
#include "UI_Bar.h"

Enemy_Warrior::Enemy_Warrior(Scene* scene, Player* player) :AI_Base(scene, Scene::GEO_ENEMYWARRIOR, player, 100, 10)
{
	b_EnableGravity = true;
	b_EnablePhysics = true;
	b_EnableLighting = false;
	
	scale.Set(6.f, 6.f, 1.f);
	this->collider = ColliderFactory::CreateOBBCollider(this, Vector3(6.0f, 6.0f, 0.0f));

	m_AnimEnemyWarrior = static_cast<SpriteAnimation*>(scene->meshList[Scene::GEO_ENEMYWARRIOR]);
	m_Anim = new Animation();
	m_Anim->Set(0, 0, 1, 1.f, true);

	m_stunThreshold = 500.f;
	m_stunDuration = 1.f;
}

Enemy_Warrior::~Enemy_Warrior() {
	if (m_Anim)
		delete m_Anim;
}

bool Enemy_Warrior::Update()
{
	if (m_CurrentState == CHASE)
		AI_Chase(thePlayer, m_scene->m_dt);
	if (m_CurrentState == JUMP)
		AI_Jump(m_scene->m_dt);
	if (m_CurrentState == ATTACK)
		AI_Attack(thePlayer, m_scene->m_dt);
	if (m_CurrentState == STUN)
		AI_Stun();
	if (m_CurrentState == FLEE)
		AI_Flee(thePlayer, m_scene->m_dt);

	

	if (!b_IsFacingRight)
	{
		m_offsetPosition.x = -3.f;
		m_offsetPosition.y = sin(Math::DegreeToRadian(m_weaponRotation));
	}
	else
	{
		m_offsetPosition.x = 3.f;
		m_offsetPosition.y = sin(Math::DegreeToRadian(m_weaponRotation));
	}

	return true;
}

void Enemy_Warrior::AI_Chase(Player*player, double dt)
{
	if (pos.x < player->pos.x && (player->pos.x - pos.x) >= player->scale.x)
	{
		m_force.x = 90;
		b_IsFacingRight = true;
		if (collider.manifold.normal.x != 0 && !is_jumping)
		{
			m_CurrentState = JUMP;
			is_jumping = true;
		}
	}
	else if (pos.x > player->pos.x && (player->pos.x - pos.x) <= -player->scale.x)
	{
		m_force.x = -90;
		b_IsFacingRight = false;
		if (collider.manifold.normal.x != 0 && !is_jumping)
		{
			m_CurrentState = JUMP;
			is_jumping = true;
		}
	}
	if (is_attacking)
		m_CurrentState = ATTACK;
}

void Enemy_Warrior::AI_Attack(Player * player, double dt)
{
	if (!b_completeLoop)
	{
		m_weaponRotation -= 400 * m_scene->m_dt;
	}
	else
		m_weaponRotation += 400 * m_scene->m_dt;

	if (m_weaponRotation < -55)
	{

		if (thePlayer->b_isRealPlayer)
			m_scene->playerManager.ReduceHealth(5);

		b_completeLoop = true;
	}
	else if (m_weaponRotation > 25)
	{
		b_completeLoop = false;
		m_CurrentState = CHASE;
		is_attacking = false;
	}

}


void Enemy_Warrior::Render()
{
	if (vel.x < 1 && vel.x > -1) {
		m_Anim->Set(24, 24, 1, 0.5f, true);
		
	}
	else if (b_IsFacingRight) {
		m_Anim->Set(24, 26, 1, 0.5f, true);
		
		rotation.y = 0;

	}
	else {
		m_Anim->Set(24, 26, 1, 0.5f, true);
		
		rotation.y = 180;
	}
	m_AnimEnemyWarrior->Update(m_Anim, m_scene->m_dt);
	m_Anim_CurrentFrame = m_Anim->m_currentFrame;

	glDisable(GL_CULL_FACE);
	GameObject::Render();

	glDisable(GL_DEPTH_TEST);
	// Render Weapon
	m_scene->modelStack.PushMatrix();
	if (!b_IsFacingRight)
	{
		m_scene->modelStack.Translate(pos.x + m_offsetPosition.x, pos.y + m_offsetPosition.y, currentIndex * 0.01f);
		m_scene->modelStack.Rotate(180, 0, 1, 0);
	}
	else if (b_IsFacingRight)
	{
		m_scene->modelStack.Translate(pos.x + m_offsetPosition.x, pos.y + m_offsetPosition.y, currentIndex * 0.01f);
	}
	if (is_attacking)
		m_scene->modelStack.Rotate(m_weaponRotation, 0.f, 0.f, 1.f);
	m_scene->modelStack.Scale(5, 5, 1);
	m_scene->meshList[Scene::GEO_SWORD]->color = Color(1.f, 0.5f, 0.f);
	m_scene->RenderMesh(m_scene->meshList[Scene::GEO_SWORD], false);

	m_scene->modelStack.PopMatrix();
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	// Render Health bar
	Vector3 barPos = pos;
	barPos.y += collider.bboxSize.y;
	Mesh** meshList = m_scene->meshList;
	UI_BAR::RenderBar(m_scene, meshList[Scene::GEO_HP_BACKGROUND], meshList[Scene::GEO_HP_FOREGROUND], meshList[Scene::GEO_HP_FRAME], barPos, Vector3(4, 0.5f), (float)GetCurrentHealth() / m_DefaultHP);

}

void Enemy_Warrior::OnCollisionHit(GameObject * other)
{
	AI_Base::OnCollisionHit(other);
	if ((this->collider.manifold.normal.x == -1 || this->collider.manifold.normal.x == 1) && other->type == Scene::GEO_PLAYER)
	{
		if (m_scene->m_elapsedTime > m_attackTimer)
		{
			is_attacking = true;
			m_attackTimer = m_scene->m_elapsedTime + 1.f;
		}
	}
	// Checking if AI is under or above player
	else if ((this->collider.manifold.normal.y == -1 || this->collider.manifold.normal.y == 1) && other->type == Scene::GEO_PLAYER)
	{
		// If AI is facing right, AI will move right , else AI will move left
		this->b_IsFacingRight ? m_force.x = 55 : m_force.x = -55;
	}
}



