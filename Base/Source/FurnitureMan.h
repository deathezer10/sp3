#pragma once


#include "GameObject.h"


class SpriteAnimation;
struct Animation;

class FurnitureMan : public GameObject {


public:
	FurnitureMan(Scene* scene);
	~FurnitureMan() {};

	virtual bool Update();
	virtual void Render();


private:
	SpriteAnimation* m_AnimPlayer;
	Animation* m_Anim;

};