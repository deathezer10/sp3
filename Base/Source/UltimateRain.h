#pragma once

#include <vector>
#include "GameObject.h"


class UltimateRain : public GameObject {

public:
	UltimateRain(Scene* scene);
	~UltimateRain() {};

	virtual bool Update();
	virtual void Render();
	virtual void OnCollisionHit(GameObject* other);

private:
	float m_offset;
	float m_spawnTime;
	float m_lifeTime;
	bool b_HitSomething;
};