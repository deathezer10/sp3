#pragma once

#include <queue>

using std::vector;

class Player;
class Scene;
class ParticleEmitter;

class PlayerManager {

public:
	PlayerManager(Scene* scene);
	~PlayerManager();

	void Update();

	void SetPlayer(Player* player);

	// Returns the pointer to the Player GameObject
	Player* GetPlayer() { return m_player; }

	int GetCurrentScore() { return m_Score; }
	void IncreaseScore(int value) { m_Score += abs(value); }

	// Returns the current ultimate charge, value range: 0-100
	float GetHealth() { return m_Health; }

	// Returns the current ultimate charge, value range: 0-100
	float GetUltimateCharge() { return m_Ultimate_Charge; }

	// Returns the current amount of Armor, value range: 0-100
	float GetArmor() { return m_Armor; }

	// Returns the state of the player, if they are shielding
	bool GetShielding() { return b_IsShielding; }

	// Increases the player's Armor, amount is converted to positive value before increasing
	void IncreaseArmor(float amount);

	// Reduces the player's Armor, damage is converted to positive value before reducing
	void ReduceArmor(float damage);

	// Increases the player's Health, amount is converted to positive value before increasing
	void IncreaseHealth(float amount);

	// Increase the player's ultimate charge, amount is converted to positive value before increasing
	void IncreaseUltimateCharge(float amount);

	// Reduces the player's Health, damage is converted to positive value before reducing
	// Applies Armor reduction first before damage
	// Does not deal damage if player is currently shielding
	void ReduceHealth(float damage);

	// Is the player currently dashing
	bool IsDashing();

	// Is the cheat key activated?
	bool IsCheatEnabled() { return b_IsCheatEnabled; }

	void Init();

	ParticleEmitter* m_FurEmitter;

private:
	bool b_attackButton = false;
	bool b_IsShielding = false;
	bool b_IsGodmode = false;
	bool b_IsCheatEnabled = false;

	Player* m_player;
	Scene* m_scene;

	float m_Health = 100;
	float m_Armor = 0;
	float m_Ultimate_Charge = 0;

	int m_Score = 0;
	
	float m_dashTimer = 0.f;

	char m_PreviousDashKey;

	// Directional arrow turning rate
	const float ARROW_TURNING_SPEED = 180.f;

	// GameJam
	bool b_IsAttracting = false;
	float m_CurrentAttractTimer = 0;


};