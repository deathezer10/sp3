#include "UI_Bar.h"

#include "GL\glew.h"
#include "GLFW\glfw3.h"
#include "Mtx44.h"
#include "shader.hpp"

#include "MeshBuilder.h"
#include "Utility.h"

#include "Scene.h"


UI_BAR::UI_BAR() {
}

UI_BAR::~UI_BAR() {
}

void UI_BAR::RenderBar(Scene* m_scene, Mesh * BGMesh, Mesh * FGMesh, Mesh* FRMesh, Vector3 pos, Vector3 Scale, float Scale_factor)
{
	Scale_factor = Math::Clamp(Scale_factor, 0.0f, 1.0f);

	glDisable(GL_DEPTH_TEST);

	// check if got frame and background texture
	if (BGMesh != nullptr&& FRMesh != nullptr)
	{
		m_scene->modelStack.PushMatrix();
		//pos
		m_scene->modelStack.Translate(pos.x - Scale.x, pos.y, pos.z);
		m_scene->modelStack.Scale(Scale.x, Scale.y, Scale.z);

		{
			//BG rendering
			m_scene->modelStack.PushMatrix();
			m_scene->RenderMesh(BGMesh, false);
			m_scene->modelStack.PopMatrix();

			//FG rendering
			m_scene->modelStack.PushMatrix();
			m_scene->modelStack.Scale(Scale_factor, 1.0f, 1.0f);
			m_scene->RenderMesh(FGMesh, false);
			m_scene->modelStack.PopMatrix();

			//Frame rendering
			m_scene->modelStack.PushMatrix();
			//m_scene->modelStack.Scale(0.99, 1.0f, 1.0f);
			m_scene->RenderMesh(FRMesh, false);
			m_scene->modelStack.PopMatrix();

		}

		m_scene->modelStack.PopMatrix();
	}

	else
	{
		m_scene->modelStack.PushMatrix();
		m_scene->modelStack.Translate(pos.x - Scale.x, pos.y, pos.z);
		m_scene->modelStack.Scale(Scale.x, Scale.y, Scale.z);
		//FG rendering
		m_scene->modelStack.PushMatrix();
		m_scene->modelStack.Scale(Scale_factor, 1.0f, 1.0f);
		m_scene->RenderMesh(FGMesh, false);
		m_scene->modelStack.PopMatrix();
		m_scene->modelStack.PopMatrix();
	}

	glEnable(GL_DEPTH_TEST);
}

