#include "UltimateWave.h"
#include "Scene.h"
#include "Mesh.h"
#include "ColliderFactory.h"
#include "AI_Base.h"
#include "Player.h"

UltimateWave::UltimateWave(Scene * scene) : GameObject(scene, Scene::GEO_ULTIMATE_WAVE)
{
	b_EnableLighting = false;
	b_EnablePhysics = true;
	b_EnablePhysicsResponse = false;
	b_EnableGravity = false;
	b_EnableTrigger = true;

	bounciness = 0;

	if (m_scene->playerManager.GetPlayer()->b_isFacingRight)
		vel.Set(50.f, 0.f, 0.f);
	else
		vel.Set(-50.f, 0.f, 0.f);

	vel.x += m_scene->playerManager.GetPlayer()->vel.x;

	mass = 100.f;

	active = true;
	scale.Set(7.f, 10.f, 1);
	collider = ColliderFactory::CreateOBBCollider(this, Vector3(7.f, 10.f, 1.f));
}

bool UltimateWave::Update()
{
	m_TraveledDistance += abs(vel.x) * m_scene->m_dt;

	if (m_TraveledDistance >= m_MaxDistance - 20) {
		alpha -= m_scene->m_dt * 2;
	}

	if (m_TraveledDistance >= m_MaxDistance) {
		m_scene->goManager.DestroyObject(this);
		return false;
	}

	return true;
}

void UltimateWave::Render()
{
	rotation.z = Math::RadianToDegree(atan2(vel.y, vel.x));
	m_OffsetPos.z = 5;
	GameObject::Render();
}

void UltimateWave::OnCollisionHit(GameObject * other)
{
	AI_Base* Enemy = dynamic_cast<AI_Base*>(other);
	if (Enemy && !Enemy->IsAlly())
	{
		std::vector<GameObject*>::iterator it;
		for (it = m_EnemyHit.begin(); it != m_EnemyHit.end(); ++it)
		{
			if (*it == Enemy)
				break;
		}

		if (it == m_EnemyHit.end())
		{
			m_EnemyHit.push_back(Enemy);

			if (vel.x > 0)
				Enemy->m_force.x += Math::RandFloatMinMax(1000, 1500);
			else
				Enemy->m_force.x += Math::RandFloatMinMax(-1500, -1000);

			Enemy->ReduceHealth(40);


		}
	}

}
