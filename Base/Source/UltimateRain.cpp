#include "UltimateRain.h"
#include "Scene.h"
#include "Mesh.h"
#include "TerrainLoader.h"
#include "ColliderFactory.h"
#include "Projectile.h"
#include "AI_Base.h"
#include "SoundManager.h"



UltimateRain::UltimateRain(Scene * scene) : GameObject(scene, Scene::GEO_ULTIMATE_ARROW)
{
	b_EnableGravity = true;
	b_EnableLighting = false;
	b_EnablePhysics = true;
	b_EnablePhysicsResponse = false;
	b_EnableTrigger = true;

	bounciness = 0;

	alpha = 0.8f;

	active = true;

	scale.Set(7, 2, 1);
	collider = ColliderFactory::CreateOBBCollider(this, Vector3(7, 2, 1));

	m_spawnTime = 0.f;

	b_HitSomething = false;
}

bool UltimateRain::Update()
{
	if (b_HitSomething)
	{
		b_EnablePhysics = false;
		if (m_scene->m_elapsedTime > m_spawnTime)
		{
			SoundManager::getInstance().Play2DSound("Ultimate3", false, true, false);
			m_spawnTime = m_scene->m_elapsedTime + 1.f;
			for (int i = 0; i < 7; ++i)
			{
				Projectile* obj = new Projectile(m_scene);
				obj->b_EnableTrigger = true; // Disable response
				obj->pos = pos;
				Mtx44 rotation;
				rotation.SetToRotation(25.7f + (i + 1) * 15.f, 0, 0, 1);
				obj->vel = rotation * Vector3(-1.f, 0.f, 0) * 75.f;
				obj->vel.z = 0.f;
				obj->collider = ColliderFactory::CreateOBBCollider(obj, Vector3(7.5f, 2.f, 1.f));
				obj->scale.Set(4.f, 4.f, 1.f);
				Vector3 offset;
				offset = obj->vel.Normalized() * (obj->collider.bboxSize);
				obj->pos += offset;
				obj->b_EnablePhysicsResponse = false;
				m_scene->goManager.CreateObjectQueue(obj);
			}
		}
		else if (m_scene->m_elapsedTime > m_lifeTime)
		{
			m_scene->goManager.DestroyObject(this);
			return false;
		}
	}
	else if (!vel.IsZero())
		dir = vel.Normalized();
	else if (vel.y <= 0)
	{
		m_lifeTime = m_scene->m_elapsedTime + 7.5f;
		b_HitSomething = true;
	}

	return true;
}

void UltimateRain::Render()
{
	rotation.z = Math::RadianToDegree(atan2(vel.y, vel.x));
	GameObject::Render();
}

void UltimateRain::OnCollisionHit(GameObject * other)
{
}
