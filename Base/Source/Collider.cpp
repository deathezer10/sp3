#include "Collider.h"
#include "GameObject.h"

#include <map>
#include <vector>
#include <algorithm>

using std::pair;
using std::make_pair;



void Collider::operator=(const Collider & other) {
	m_ColliderType = other.m_ColliderType;
	m_Obj = other.m_Obj;
	bboxSize = other.bboxSize;
	radius = other.radius;
}

Collider::Collider(GameObject * obj, TYPE type) : m_ColliderType(type) {
	m_Obj = obj;
}

Collider::Collider() : m_ColliderType(NONE) {
	m_Obj = nullptr;
}

void Collider::CalculateVertices(float angleInRadian) {

	manifold.box_vertices[0] = m_Obj->pos; // Top left
	manifold.box_vertices[0].x -= bboxSize.x / 2;
	manifold.box_vertices[0].y += bboxSize.y / 2;
	manifold.box_vertices[0].RotateAround(m_Obj->pos, angleInRadian);

	manifold.box_vertices[1] = m_Obj->pos; // Top Right
	manifold.box_vertices[1].x += bboxSize.x / 2;
	manifold.box_vertices[1].y += bboxSize.y / 2;
	manifold.box_vertices[1].RotateAround(m_Obj->pos, angleInRadian);

	manifold.box_vertices[2] = m_Obj->pos; // Bot left
	manifold.box_vertices[2].x -= bboxSize.x / 2;
	manifold.box_vertices[2].y -= bboxSize.y / 2;
	manifold.box_vertices[2].RotateAround(m_Obj->pos, angleInRadian);

	manifold.box_vertices[3] = m_Obj->pos; // Bot Right
	manifold.box_vertices[3].x += bboxSize.x / 2;
	manifold.box_vertices[3].y -= bboxSize.y / 2;
	manifold.box_vertices[3].RotateAround(m_Obj->pos, angleInRadian);

}

bool Collider::CheckCollision(Collider & other) {

	Collider* col1 = this;
	Collider* col2 = &other;

	GameObject* go1 = col1->m_Obj;
	GameObject* go2 = col2->m_Obj;

	// OBB vs OBB
	if (col1->m_ColliderType == OBB && col2->m_ColliderType == OBB) {

		// Re-calculate vertices position oriented to the rotation
		col1->CalculateVertices(atan2(go1->dir.y, go1->dir.x));
		col2->CalculateVertices(atan2(go2->dir.y, go2->dir.x));

		// Rotates the direction 90 degrees clockwise
		Vector3 sideDir1 = go1->dir;
		std::swap(sideDir1.x, sideDir1.y);
		sideDir1.x = -sideDir1.x;

		Vector3 sideDir2 = go2->dir;
		std::swap(sideDir2.x, sideDir2.y);
		sideDir2.x = -sideDir2.x;

		const Vector3 axisVectors[4] = {
			go1->dir,
			go2->dir,
			sideDir1,
			sideDir2
		};

		// Use lowest minimum translation distance to determine how far to push the object out
		float lowestMTD = 10000.f;
		Vector3 MTDnormal;

		for (int i = 0; i < 4; ++i) {

			// Minimum & Maximum Dot product
			float min1;
			float max1;

			float min2;
			float max2;

			int index1 = 0;
			int index2 = 0;

			for (auto &vertice : col1->manifold.box_vertices) {

				float result = vertice.Dot(axisVectors[i]);

				if (index1 == 0) {
					min1 = result;
					max1 = result;
				}
				else {
					if (result < min1)
						min1 = result;
					else if (result > max1)
						max1 = result;
				}
				index1++;
			}

			for (auto &vertice : col2->manifold.box_vertices) {

				float result = vertice.Dot(axisVectors[i]);

				if (index2 == 0) {
					min2 = result;
					max2 = result;
				}
				else {
					if (result < min2)
						min2 = result;
					else if (result > max2)
						max2 = result;
				}
				index2++;
			}



			// Minimum translation distance
			float mtd = 0;

			// Is Projection A higher than B?
			bool isAHigher = false;

			// No collision
			if (CheckOverlap(min1, max1, min2, max2, &mtd, &isAHigher) == false) {
				// col1->manifold.normal.SetZero();
				col1->manifold.penetration = 0;

				// col2->manifold.normal.SetZero();
				col2->manifold.penetration = 0;
				return false;
			}
			else if (mtd < lowestMTD) {
				lowestMTD = mtd;
				MTDnormal = axisVectors[i];

				if (isAHigher)
					MTDnormal = -MTDnormal;
			}
		}

		col1->manifold.normal = MTDnormal;
		col2->manifold.normal = -col1->manifold.normal;
		col1->manifold.penetration = lowestMTD;
		col2->manifold.penetration = lowestMTD;

		return true;
	}

	// Circle vs Circle
	else if (col1->m_ColliderType == CIRCLE && col2->m_ColliderType == CIRCLE) {
		float radius = col1->radius + col2->radius;
		radius *= radius; // square it

		float distX = col1->m_Obj->pos.x + col2->m_Obj->pos.x;
		distX *= distX;

		float distY = col1->m_Obj->pos.y + col2->m_Obj->pos.y;
		distY *= distY;

		return (radius < (distX + distY));
	}

	return false;
}

void Collider::ResolveCollision(Collider & other, float dt) {

	Collider* col1 = this;
	Collider* col2 = &other;

	// go1 = Current Object
	// go2 = Other Object
	GameObject* go1 = col1->m_Obj;
	GameObject* go2 = col2->m_Obj;

	// Calculate relative velocity
	Vector3 rv = go2->vel - go1->vel;

	// Calculate relative velocity in terms of the normal direction
	float velAlongNormal = rv.Dot(manifold.normal);

	// Do not resolve if velocities are separating
	if (velAlongNormal > 0)
		return;

	// Get the max friction to apply
	float mu;

	if (go1->b_ForceFriction)
		mu = go1->friction;
	else if (go2->b_ForceFriction)
		mu = go2->friction;
	else
		mu = std::min(go1->friction, go2->friction);

	// Get the minimum bounciness
	float e;
	if (go1->b_ForceBounciness)
		e = go1->bounciness;
	else if (go2->b_ForceBounciness)
		e = go2->bounciness;
	else
		e = std::min(go1->bounciness, go2->bounciness);

	// Calculate impulse scalar
	float j = 0;
	j = -(1 + e) * velAlongNormal;
	float m1_inv = (go1->mass == 0) ? 0 : (1 / go1->mass);
	float m2_inv = (go2->mass == 0) ? 0 : (1 / go2->mass);

	if (m1_inv != 0 && m2_inv != 0)
		j /= (m1_inv + m2_inv);

	Vector3 totalgo1vel;
	Vector3 totalgo1pos;

	Vector3 totalgo2vel;
	Vector3 totalgo2pos;

	// Apply impulse
	Vector3 impulse = j * manifold.normal;
	totalgo1vel -= m1_inv * impulse;
	totalgo2vel += m2_inv * impulse;

	// Prevent friction when brushing against wall on the left/right
	if ((go1->vel.y == 0 || col1->manifold.normal.y < 0) && go1->vel.IsZero() == false)
		totalgo1vel -= go1->vel.Normalized() * mu;

	if ((go2->vel.y == 0 || col2->manifold.normal.y < 0) && go2->vel.IsZero() == false)
		totalgo2vel -= go2->vel.Normalized() * mu;

	// Object sinking fix
	if (m1_inv != 0 || m2_inv != 0) {

		// Only sink if mass is not infinite
		const float percent = 0.8f; // usually 20% to 80%
		const float slop = 0.01f; // usually 0.01 to 0.1
		Vector3 correction = std::max<float>(manifold.penetration - slop, 0) / (m1_inv + m2_inv) * percent * manifold.normal;
		totalgo1pos -= m1_inv * correction;
		totalgo2pos += m2_inv * correction;
	}

	// Only offset if physics is enabled
	if (go1->b_EnablePhysicsResponse) {
		go1->vel += totalgo1vel;
		go1->pos += totalgo1pos;
	}

	// Only offset if physics is enabled
	if (go2->b_EnablePhysicsResponse) {
		go2->vel += totalgo2vel;
		go2->pos += totalgo2pos;
	}

}
