#ifndef SCENE_LEVEL01_H
#define SCENE_LEVEL01_H

#include "SceneBase.h"
#include "GameObject.h"
#include "UI_Bar.h"

#include <vector>


class Brick;
class Portal;
class CapturePoint;
class Health_Well;



class SceneLevel01 : public SceneBase {

public:
	SceneLevel01();
	~SceneLevel01();

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();


private:
	Brick* m_Gate1 = nullptr;
	CapturePoint* m_Point1 = nullptr;
	Portal* m_GatePortal = nullptr;
	Portal* m_Core = nullptr;
	Health_Well* m_HealthWell = nullptr;

protected:


};
#endif

