#pragma once


#include "Particle.h"
#include "Vector3.h"
#include <vector>


// Emits added Particles from Emitter's position
class ParticleEmitter {

	friend class ParticleManager;

public:
	ParticleEmitter();
	~ParticleEmitter();
	
	Vector3 pos;

	bool isActive() { return m_isActive; };

	// Toggle Updating & Rendering of this Emitter
	void setActive(bool toggle) { m_isActive = toggle; }

	// Limits the amount of Particles that spawns from the Emitter
	void setMaxParticles(unsigned max) { m_MaxParticle = max; }

	// Adds a Particle into collection allowing it to be Processed & Rendered
	void AddParticle(const Particle& particle);

	// Resets all Particles position to the origin
	void ResetParticles(const Vector3& pos);

	// Updates all Particles in the Emitter
	void Update();

	// Render all Particles in the Emitter
	void Render();

	// Current amount of Particles in the collection
	unsigned Size() { return particleList.size(); };

private:
	std::vector<Particle> particleList;

	bool m_isActive = true;
	unsigned m_MaxParticle = 1000;

};