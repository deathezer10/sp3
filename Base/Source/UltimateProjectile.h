#pragma once

#include <vector>
#include "GameObject.h"


class UltimateProjectile : public GameObject {

public:
	UltimateProjectile(Scene* scene);
	~UltimateProjectile() {};

	virtual bool Update();
	virtual void Render();
	virtual void OnCollisionHit(GameObject* other);

private:
	bool b_HitSomething;
	float m_lifetime;
	float m_DamageTimer;
};