#include "Application.h"
#include "Camera.h"
#include "Mtx44.h"

#include "Player.h"
#include "FurnitureMan.h"
#include "PlayerManager.h"

#include "GameObject.h"
#include "SceneBase.h"
#include "SceneEditor.h"
#include "SceneManager.h"



Camera::Camera()
{
}

Camera::~Camera()
{
}

void Camera::Init(const Vector3& pos, const Vector3& target, const Vector3& up)
{
	this->position = pos;
	this->target = target;
	this->up = up;

}

void Camera::Update(double dt)
{
	SceneBase* _scene = (SceneBase*)SceneManager::getInstance()->getCurrentScene();

	// Default initialization
	if (m_player == nullptr) {

		if (_scene->sceneType == Scene::SCENE_EDITOR) {

			m_player = ((SceneEditor*)_scene)->m_FurnitureMan;
		}
		else {
			m_player = _scene->playerManager.GetPlayer();
		}

		position = m_player->pos;
		target = m_player->pos;
		target.z = -1;

		prevX = m_player->pos.x;
		prevY = m_player->pos.y;

		target.x -= _scene->m_worldWidth * 0.5f;
		target.y -= _scene->m_worldHeight * 0.2f;

		position.x -= _scene->m_worldWidth * 0.5f;
		position.y -= _scene->m_worldHeight * 0.2f;
	}


	// X Scroll
	const float Mag_X = 25;
	const float MAX_X = Mag_X; // Left/Right limit
	const float MIN_X = -Mag_X;
	float deltaX = m_player->pos.x - prevX;

	currentScrollX += deltaX;
	currentScrollX = Math::Clamp<float>(currentScrollX, MIN_X, MAX_X);

	if (currentScrollX == MIN_X) {
		position.x += deltaX;
		target.x += deltaX;
	}
	else if (currentScrollX == MAX_X) {
		position.x += deltaX;
		target.x += deltaX;
	}

	// Y Scroll
	const float MAX_Y = 45; // Up/Down limit
	const float MIN_Y = -5;
	float deltaY = m_player->pos.y - prevY;

	currentScrollY += deltaY;
	currentScrollY = Math::Clamp<float>(currentScrollY, MIN_Y, MAX_Y);

	if (currentScrollY == MIN_Y) {
		position.y += deltaY;
		target.y += deltaY;
	}
	else if (currentScrollY == MAX_Y) {
		position.y += deltaY;
		target.y += deltaY;
	}

	prevX = m_player->pos.x;
	prevY = m_player->pos.y;
}

void Camera::SetPrev(Vector3 pos) {
	prevX = pos.x;
	prevY = pos.y;
}

void Camera::SetZoomMutiplier(float percentage)
{
	currentZoomMultiplier = percentage;
}
