#include "Dart.h"
#include "Scene.h"
#include "Mesh.h"
#include "AI_Base.h"
#include "TerrainLoader.h"


Dart::Dart(Scene * scene) : GameObject(scene, Scene::GEO_DART)
{
	b_EnableGravity = true;
	b_EnableLighting = false;
	b_EnablePhysics = true;
	b_EnableTrigger = true;

	active = true;

	mass = 5.f;
	scale.Set(2.5f, 2.5f, 1);
}

void Dart::fireDart(float angle, Vector3 AIpos, Collider AIcolliderSize)
{
	Vector3 offset;

	this->pos.Set(AIpos.x, AIpos.y, 0.f);
	this->vel.Set(100.f * cos(angle), 90.f*sin(angle), 0.f);

	offset = this->vel.Normalized() * (AIcolliderSize.bboxSize * 1.5f);
	this->pos += offset;
}

bool Dart::Update()
{
	if (vel.IsZero() == false)
		dir = vel.Normalized();

	return true;
}

void Dart::Render()
{
	rotation.z = Math::RadianToDegree(atan2(vel.y, vel.x));
	GameObject::Render();
}

void Dart::OnCollisionHit(GameObject * other)
{
	if ((other->type != Scene::GEO_ARROW && other->type != Scene::GEO_PLAYER))
	{
		if (TerrainLoader::IsTerrain(other->type)) {
			m_scene->goManager.DestroyObjectQueue(this);
		}
		else {

			AI_Base* Enemy = dynamic_cast<AI_Base*>(other);
			if (Enemy && !Enemy->IsAlly() && Enemy->type != Scene::GEO_ENEMYGIANT && Enemy->type != Scene::GEO_ENEMYDEMON && Enemy->type != Scene::GEO_ENEMYGOLEM)
			{
				Enemy->StunAI(60);
				m_scene->goManager.DestroyObjectQueue(this);
			}
		}
	}
}
