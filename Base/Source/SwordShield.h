#pragma once

#include "GameObject.h"
#include "WeaponInfo.h"

class Player;

class SwordShield : public WeaponInfo, public GameObject
{
public:
	SwordShield(Scene* scene, Player* player);
	~SwordShield() {};

	bool Update();

	void Render();

	bool checkRange(GameObject* obj);

	void checkTimer();

	bool useUltimate();

	int getAttack();

	float getUltimateDuration();

private:
	Player* m_player;
	float m_offsetRotation;
	float m_offset;
	Vector3 m_offsetPosition;
	bool b_completeLoop;
	bool b_isUsingUltimate;
	float m_UltimateDuration;
};