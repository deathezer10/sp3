#include "SceneLevel01.h"
#include "GL\glew.h"
#include "Application.h"
#include <sstream>
#include "Player.h"

#include "MeshBuilder.h"
#include "LoadTGA.h"

#include "ColliderFactory.h"

#include "Brick.h"
#include "Enemy_Giant.h"
#include "Portal.h"
#include "CapturePoint.h"
#include "Ally_Warrior.h"
#include "Health_Well.h"
#include "ParticleEmitter.h"

#include "SceneManager.h"
#include "SceneGameOver.h"
#include "TerrainLoader.h"



SceneLevel01::SceneLevel01() :SceneBase(SCENE_LEVEL01)
{
}

SceneLevel01::~SceneLevel01()
{
}

void SceneLevel01::Init()
{
	SceneBase::Init();
	//Calculating aspect ratio
	m_worldHeight = 100.f;
	m_worldWidth = m_worldHeight * (float)Application::windowWidth() / Application::windowHeight();

	TerrainLoader::LoadTerrain("level01.txt", &goManager);
	 

	// Player
	Player* player = new Player(this);
	player->active = true;
	player->pos.Set(m_worldWidth_default * 0.5f, m_worldHeight_default * .5f, 0.f);
	goManager.CreateObject(player);
	playerManager.SetPlayer(player);


	m_HealthWell = new Health_Well(this);
	m_HealthWell->active = true;
	m_HealthWell->pos.Set(405.f, 56.f, 0.f);
	goManager.CreateObject(m_HealthWell);

	Health_Well* HealthWell2 = new Health_Well(this);
	HealthWell2->active = true;
	HealthWell2->pos.Set(940.f, 37.f, 0.f);
	goManager.CreateObject(HealthWell2);

	Health_Well* HealthWell3 = new Health_Well(this);
	HealthWell3->active = true;
	HealthWell3->pos.Set(1240.f, 30.f, 0.f);
	goManager.CreateObject(HealthWell3);

	// Portal
	Portal* portal1 = new Portal(this, player, Portal::E_PORTAL_TYPE::WARRIOR);
	portal1->active = true;
	portal1->pos.Set(490, 50, 0.f);
	goManager.CreateObject(portal1);

	Portal* portal2 = new Portal(this, player, Portal::E_PORTAL_TYPE::WARRIOR);
	portal2->active = true;
	portal2->pos.Set(840, 27.5f, 0.f);
	goManager.CreateObject(portal2);

	Portal* portal3 = new Portal(this, player, Portal::E_PORTAL_TYPE::ARCHER);
	portal3->active = true;
	portal3->pos.Set(1351, 64, 0.f);
	goManager.CreateObject(portal3);

	Portal* portal4 = new Portal(this, player, Portal::E_PORTAL_TYPE::NINJA);
	portal4->active = true;
	portal4->pos.Set(1489, 68, 0.f);
	goManager.CreateObject(portal4);

	Portal* portal5 = new Portal(this, player, Portal::E_PORTAL_TYPE::WARRIOR);
	portal5->active = true;
	portal5->pos.Set(220.f, 34.2f, 0.f);
	goManager.CreateObject(portal5);

	Portal* portal6 = new Portal(this, player, Portal::E_PORTAL_TYPE::ARCHER);
	portal6->active = true;
	portal6->pos.Set(350.f, 50.7f, 0.f);
	goManager.CreateObject(portal6);

	Portal* portal7 = new Portal(this, player, Portal::E_PORTAL_TYPE::ARCHER);
	portal7->active = true;
	portal7->pos.Set(730.f, 34.9f, 0.f);
	goManager.CreateObject(portal7);

	Portal* portal8 = new Portal(this, player, Portal::E_PORTAL_TYPE::ARCHER);
	portal8->active = true;
	portal8->pos.Set(1641.f, 92.9f, 0.f);
	goManager.CreateObject(portal8);

	m_Core = new Portal(this, player, Portal::E_PORTAL_TYPE::WARRIOR_AND_ARCHER, true);
	m_Core->active = true;
	m_Core->SetHealth(250);
	m_Core->m_PActivateThreshold = 250;
	m_Core->pos.Set(1770, 111, 0.f);
	goManager.CreateObject(m_Core);

	m_GatePortal = new Portal(this, player, Portal::E_PORTAL_TYPE::WARRIOR);
	m_GatePortal->active = true;
	m_GatePortal->pos.Set(1060, 95, 0.f);
	goManager.CreateObject(m_GatePortal);

	m_Gate1 = new Brick(this);
	m_Gate1->active = true;
	m_Gate1->type = GEO_TERRAIN_BRICK;
	m_Gate1->mass = 0;
	m_Gate1->dir.Set(0, 1, 0);
	m_Gate1->pos.Set(1050, 65, 0);
	m_Gate1->scale.x = 48;
	goManager.CreateObject(m_Gate1);

	m_Point1 = new CapturePoint(this);
	m_Point1->active = true;
	m_Point1->pos.Set(1015, 52.5f, 0);
	goManager.CreateObject(m_Point1);


}

void SceneLevel01::Update(double dt)
{

	//pause menu part
	//update pausemenu function	
	pauseManager.UpdatePauseMenu(m_dt);

	//check if game is paused, freeze all stuff that is updating
	if (pauseManager.isPaused()) {
		return;
	}

	SceneBase::Update(dt);

	if (Application::IsKeyPressedAndReleased('C')) {
		ShowDebugInfo = !ShowDebugInfo;
	}

	// Gameplay Logic

	// Unlock gate once captured
	if (m_Gate1 != nullptr && m_Point1->GetCaptureProgress() >= 100) {
		goManager.DestroyObject(m_GatePortal);
		m_GatePortal = nullptr;

		goManager.DestroyObject(m_Gate1);
		m_Gate1 = nullptr;
	}

	if (m_Core->GetCurrentHealth() <= 0) {
		playerManager.IncreaseScore(100);
		SceneManager::getInstance()->changeScene(new SceneGameover("Level 01 Victory! You destroyed the Castle's Core!", SceneGameover::MENU_VICTORY, SceneBase::SCENE_LEVEL01, playerManager.GetCurrentScore()));
		return;
	}


	playerManager.Update();
	goManager.DequeueObjects();
	goManager.UpdateObjects(m_dt);

	m_BGScroller.Update();
}

void SceneLevel01::Render()
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	float m_newWorldHeight = 100 * (float)Application::windowHeight() / Application::windowDefaultHeight()  * camera.GetZoomMultiplier();
	float m_newWorldWidth = m_newWorldHeight * (float)Application::windowWidth() / Application::windowHeight();

	float offset = -((m_newWorldWidth - m_worldWidth) / 2);

	// Projection matrix : Orthographic Projection
	Mtx44 projection;
	projection.SetToOrtho(offset, m_newWorldWidth + offset, 0, m_newWorldHeight, -100, 100);
	projectionStack.LoadMatrix(projection);


	// Camera matrix
	viewStack.LoadIdentity();
	viewStack.LookAt(
		camera.position.x, camera.position.y, camera.position.z,
		camera.target.x, camera.target.y, camera.target.z,
		camera.up.x, camera.up.y, camera.up.z
	);
	// Model matrix : an identity matrix (model will be at the origin)
	modelStack.LoadIdentity();

	// Render background
	m_BGScroller.RenderBackground();

	goManager.RenderObjects();
	textManager.dequeueMesh();

	playerManager.m_FurEmitter->Render();

	// Render Player UI
	textManager.RenderPlayerHud();

	if (pauseManager.isPaused()) {
		pauseManager.RenderPauseMenu();
		return;
	}

	// HUDs
	std::stringstream text;
	text << "FPS: " << Application::GetFPS();
	textManager.renderTextOnScreen(UIManager::Text{ text.str(), Color(1,1,1), UIManager::ANCHOR_TOP_RIGHT });

	textManager.dequeueText();
	textManager.reset();
}

void SceneLevel01::Exit()
{
	SceneBase::Exit();
}


