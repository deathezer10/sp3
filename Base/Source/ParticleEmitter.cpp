#include "ParticleEmitter.h"
#include "Particle.h"


ParticleEmitter::ParticleEmitter() {

}

ParticleEmitter::~ParticleEmitter() {
}

void ParticleEmitter::AddParticle(const Particle& particle) {
	particleList.push_back(particle);
}

void ParticleEmitter::ResetParticles(const Vector3 & pos)
{
	for (auto &it : particleList) {
		it.pos = pos;
	}
}

void ParticleEmitter::Update() {

	unsigned counter = 0;

	std::vector<Particle>::iterator it = particleList.begin();
	for (; it != particleList.end(); ++it) {

		// Exit upon hitting the max particle count
		if (counter >= m_MaxParticle)
			return;

		(*it).UpdateMovement(pos);
		++counter;
	}

}

void ParticleEmitter::Render() {

	if (m_isActive == false)
		return;

	unsigned counter = 0;

	std::vector<Particle>::iterator it = particleList.begin();
	for (; it != particleList.end(); ++it) {

		// Exit upon hitting the max particle count
		if (counter >= m_MaxParticle)
			return;

		(*it).Render(pos);
		++counter;
	}

}