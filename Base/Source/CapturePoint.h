#pragma once


#include "GameObject.h"


class CapturePoint : public GameObject {


public:
	CapturePoint(Scene* scene);
	~CapturePoint() {};

	virtual bool Update();
	virtual void Render();
	virtual void OnCollisionHit(GameObject* other);

	float GetCaptureProgress() { return m_CurrentCaptureProgress; }

private:
	float m_CurrentCaptureProgress = 0;


};