#include "UltimateProjectile.h"
#include "Scene.h"
#include "Mesh.h"
#include "ColliderFactory.h"
#include "AI_Base.h"

UltimateProjectile::UltimateProjectile(Scene * scene) : GameObject(scene, Scene::GEO_ULTIMATE_BALL)
{
	b_EnableGravity = true;
	b_EnableLighting = false;
	b_EnablePhysics = true;

	bounciness = 0;

	active = true;

	b_HitSomething = false;

	scale.Set(6, 6, 1);
	collider = ColliderFactory::CreateOBBCollider(this, scale);

	m_DamageTimer = 0.f;
	m_lifetime = 0.f;

	MAX_VELOCITY.x = 50.f;
}

bool UltimateProjectile::Update()
{
	dir = vel.Normalized();

	if (b_HitSomething) {

		if (m_scene->m_elapsedTime > m_lifetime)
		{
			m_scene->goManager.DestroyObject(this);
			return false;
		}
		m_OffsetRot.z -= 90.f * m_scene->m_dt;

		if (m_scene->m_elapsedTime > m_DamageTimer) {

			m_DamageTimer = m_scene->m_elapsedTime + 0.2f;
			for (std::vector<GameObject*>::iterator it = m_scene->goManager.m_goList.begin(); it != m_scene->goManager.m_goList.end(); ++it)
			{
				GameObject* obj = *it;
				Collider temp = ColliderFactory::CreateOBBCollider(this, Vector3(scale.x * 7.5f, scale.y * 7.5f, scale.z));
				AI_Base* Enemy = dynamic_cast<AI_Base*>(obj);
				if (Enemy && !Enemy->IsAlly())
				{
					if (temp.CheckCollision(obj->collider))
					{
						obj->m_force = (pos - obj->pos).Normalized() * 2500.f;
						Enemy->ReduceHealth(6);
						obj->m_force.z = 0;
					}
				}
			}
		}
	}
	else {
		m_OffsetRot.z += ((vel.x > 0) ? -1 : 1) * 360.f * m_scene->m_dt;
	}

	return true;
}

void UltimateProjectile::Render()
{
	if (b_HitSomething == true) {
		type = Scene::GEO_BLACKHOLE;
		m_OffsetScale.Set(scale.x * 7.5f, scale.y * 7.5f, 0);
		alpha = 0.75f;
	}

	glEnable(GL_SAMPLE_ALPHA_TO_COVERAGE);
	GameObject::Render();
	glDisable(GL_SAMPLE_ALPHA_TO_COVERAGE);
}

void UltimateProjectile::OnCollisionHit(GameObject * other)
{
	AI_Base* Enemy = dynamic_cast<AI_Base*>(other);
	if (Enemy && !Enemy->IsAlly())
	{
		Enemy->ReduceHealth(25);
	}
	m_lifetime = m_scene->m_elapsedTime + 8.f;
	collider.bboxSize.SetZero();
	b_HitSomething = true;
	b_EnableGravity = false;
	b_EnablePhysics = false;
	b_EnablePhysicsResponse = false;
}
