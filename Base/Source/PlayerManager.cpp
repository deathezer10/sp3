#include "Application.h"
#include "Player.h"
#include "Scene.h"
#include "WeaponInfo.h"
#include "ParticleEmitter.h"
#include "PlayerManager.h"
#include "PlayerData.h"
#include "AI_Base.h"
#include "SoundManager.h"
#include "SceneManager.h"
#include "SceneGameOver.h"

#include "Ally_Warrior.h"
#include "Ally_Archer.h"

#include "Pipebomb.h"

float range = 5.f;

PlayerManager::PlayerManager(Scene* scene)
{
	m_scene = scene;
}

PlayerManager::~PlayerManager()
{
}

void PlayerManager::Init() {

	m_FurEmitter = new ParticleEmitter();

	for (int i = 0; i < 100; ++i) {
		Particle fur(m_scene, Scene::GEO_FUR);
		fur.SetLifeTime(Math::RandFloatMinMax(1, 2));
		fur.vel.Set(Math::RandFloatMinMax(-20, 20), Math::RandFloatMinMax(-20, 20), 0);
		fur.pos = fur.vel * fur.GetLifeTime();
		fur.rotationSpeed = Math::RandFloatMinMax(-720, 720);
		m_FurEmitter->AddParticle(fur);
	}

	m_FurEmitter->setActive(false);
}


void PlayerManager::Update()
{

	if (Application::IsKeyPressedAndReleased(VK_F6))
		b_IsCheatEnabled = !b_IsCheatEnabled;

	// Cheat keys
	if (b_IsCheatEnabled) {

		if (Application::IsKeyPressedOnce('J')) {
			m_Ultimate_Charge = 100;
		}
		if (Application::IsKeyPressedOnce('K')) {
			b_IsGodmode = !b_IsGodmode;

			if (b_IsGodmode)
				m_Health = 100;
		}

		if (b_IsGodmode)
			m_player->color = Color(0, 0, 1);
		else
			m_player->color = Color(1, 1, 1);

		if (Application::IsKeyPressed(VK_LSHIFT))
			m_player->m_InfiniteSpeed = true;
		else
			m_player->m_InfiniteSpeed = false;

		// Temporary for debugging
		if (Application::IsKeyPressed('F')) {
			m_player->m_force.y = 100;
		}
		if (Application::IsKeyPressed('G')) {
			m_player->m_force.y = -100;
		}

	}

	if (Application::IsKeyPressed(VK_LEFT))
	{
		if (m_player->b_isFacingRight)
		{
			m_player->m_weaponManager[m_player->m_weaponIndex]->m_angle = 180 - m_player->m_weaponManager[m_player->m_weaponIndex]->m_angle;
		}
		m_player->b_isFacingRight = false;
		m_player->m_force.x = -100;
	}
	else if (Application::IsKeyPressed(VK_RIGHT))
	{
		if (!m_player->b_isFacingRight)
		{
			m_player->m_weaponManager[m_player->m_weaponIndex]->m_angle = 180 - m_player->m_weaponManager[m_player->m_weaponIndex]->m_angle;
		}
		m_player->b_isFacingRight = true;
		m_player->m_force.x = 100;
	}

	if (Application::IsKeyPressedOnce(VK_LEFT))
	{
		if (m_PreviousDashKey == VK_LEFT) {
			if (m_scene->m_elapsedTime < m_dashTimer && !b_IsShielding)
			{
				m_player->m_force.x = -3000.f;
				m_player->useDash();
			}

			m_dashTimer = m_scene->m_elapsedTime + 0.5f;
		}
		m_PreviousDashKey = VK_LEFT;
	}
	else if (Application::IsKeyPressedOnce(VK_RIGHT))
	{
		if (m_PreviousDashKey == VK_RIGHT) {
			if (m_scene->m_elapsedTime < m_dashTimer && !b_IsShielding)
			{
				m_player->m_force.x = 3000.f;
				m_player->useDash();
			}

			m_dashTimer = m_scene->m_elapsedTime + 0.5f;
		}
		m_PreviousDashKey = VK_RIGHT;
	}

	if (Application::IsKeyPressed(VK_UP))
	{
		if (m_player->m_weaponIndex == 1)
		{
			if (!m_player->b_isFacingRight)
				m_player->m_weaponManager[m_player->m_weaponIndex]->m_angle -= ARROW_TURNING_SPEED * m_scene->m_dt;
			else
				m_player->m_weaponManager[m_player->m_weaponIndex]->m_angle += ARROW_TURNING_SPEED * m_scene->m_dt;
		}

	}
	else if (Application::IsKeyPressed(VK_DOWN))
	{
		if (m_player->m_weaponIndex == 1)
		{
			if (!m_player->b_isFacingRight)
				m_player->m_weaponManager[m_player->m_weaponIndex]->m_angle += ARROW_TURNING_SPEED * m_scene->m_dt;
			else
				m_player->m_weaponManager[m_player->m_weaponIndex]->m_angle -= ARROW_TURNING_SPEED * m_scene->m_dt;
		}

	}

	if (Application::IsKeyPressed(VK_SPACE) && !m_player->b_isJumping)
	{
		m_player->m_force.y = 2000;
		m_player->b_isJumping = true;
	}

	const int m_Skill1Cost = 25;
	const float m_Skill1Duration = 10;
	const float m_Skill1MaxSpeed = 30;

	if (Application::IsKeyPressedAndReleased('A')) {
		if (m_Ultimate_Charge >= m_Skill1Cost && b_IsAttracting == false) { // Skill cost
			m_Ultimate_Charge -= m_Skill1Cost;
			

			b_IsAttracting = true;

			m_CurrentAttractTimer = m_scene->m_elapsedTime + m_Skill1Duration;

			m_FurEmitter->setActive(true);
			m_FurEmitter->ResetParticles(m_player->pos);
		}

	}

	// Attract all sheeps
	if (b_IsAttracting) {
		// Draw texture here?
		m_FurEmitter->pos = m_player->pos;
		m_FurEmitter->Update();

		auto list = m_scene->goManager.m_goList;

		// Increase all speed now
		for (auto it : list) {
			AI_Base* ai = dynamic_cast<AI_Base*>(it);
			if (ai && ai->IsAlly() == false) {
				ai->MAX_VELOCITY.x = m_Skill1MaxSpeed;
				ai->color.Set(1, 0, 0);
			}
		}

		// Finish Attracting
		if (m_scene->m_elapsedTime >= m_CurrentAttractTimer) {
			b_IsAttracting = false;
			m_FurEmitter->setActive(false);

			auto list = m_scene->goManager.m_goList;

			// Reset all speed now
			for (auto it : list) {
				AI_Base* ai = dynamic_cast<AI_Base*>(it);
				if (ai && ai->IsAlly() == false) {
					ai->MAX_VELOCITY.x = ai->GetDefaultSpeed();
					ai->color.Set(1, 1, 1);
				}
			}
		}
	}

	const int m_Skill2Cost = 25;

	// Pipebomb logic
	if (Application::IsKeyPressedAndReleased('S')) {

		if (m_Ultimate_Charge >= m_Skill2Cost) {
			m_Ultimate_Charge -= m_Skill2Cost;

			PipeBomb* bomb = new PipeBomb(m_scene);
			bomb->active = true;
			bomb->pos = m_player->pos;
			bomb->pos.y += bomb->scale.y;
			bomb->m_force.Set(((m_player->b_isFacingRight) ? 10000 : -10000), 100000, 0);
			m_scene->goManager.CreateObjectQueue(bomb);
		}

	}

	if (Application::IsKeyPressed('Z') && !b_attackButton)
	{
		b_attackButton = true;
		m_player->m_weaponManager[m_player->m_weaponIndex]->checkTimer();
	}
	else if (Application::IsKeyPressed('Z') && b_attackButton && m_player->m_weaponIndex == 2)
		m_player->m_weaponManager[m_player->m_weaponIndex]->b_canAttack = true;
	else if (!Application::IsKeyPressed('Z') && b_attackButton)
	{
		b_attackButton = false;
		m_player->m_weaponManager[m_player->m_weaponIndex]->b_canAttack = false;
	}

	const float m_Skill3Cost = 50;

	if (Application::IsKeyPressedOnce('Q'))// && PlayerDataManager::getInstance()->m_data.m_amountofUnit1 > 0)
	{
		if (m_Ultimate_Charge >= m_Skill3Cost) {
			m_Ultimate_Charge -= m_Skill3Cost;

			for (int i = 0; i < 7; ++i)
			{
				AI_Base* Test = new Ally_Warrior(m_scene, GetPlayer());
				Test->active = true;
				//Test->pos.Set(GetPlayer()->pos.x, GetPlayer()->pos.y + 10.f, 0);
				if (GetPlayer()->b_isFacingRight)
					Test->pos.Set(GetPlayer()->pos.x - (Math::RandFloatMinMax(50.f, 60.f) *(float)Application::windowWidth() / Application::windowHeight()), GetPlayer()->pos.y + Math::RandFloatMinMax(-range, range), 0.f);
				else
					Test->pos.Set(GetPlayer()->pos.x + (Math::RandFloatMinMax(50.f, 60.f) *(float)Application::windowWidth() / Application::windowHeight()), GetPlayer()->pos.y + Math::RandFloatMinMax(-range, range), 0.f);
				m_scene->goManager.CreateObject(Test);


				PlayerDataManager::getInstance()->m_data.m_amountofUnit1 -= 1;
			}
		}
	}

	if (Application::IsKeyPressedOnce('W'))// && PlayerDataManager::getInstance()->m_data.m_amountofUnit2 > 0)
	{
		if (m_Ultimate_Charge >= m_Skill3Cost) {
			m_Ultimate_Charge -= m_Skill3Cost;

			AI_Base* Test = new Ally_Archer(m_scene, GetPlayer());
			Test->active = true;
			//Change difrection where tranq shooter come out from depnding on player direction
			if (GetPlayer()->b_isFacingRight)
				Test->pos.Set(GetPlayer()->pos.x - 5.f, GetPlayer()->pos.y, 0);
			else
				Test->pos.Set(GetPlayer()->pos.x + 5.f, GetPlayer()->pos.y, 0);

			m_scene->goManager.CreateObject(Test);

			PlayerDataManager::getInstance()->m_data.m_amountofUnit2 -= 1;
		}
	}

	if (Application::IsKeyPressed('X'))
	{
		if (m_player->m_weaponIndex == 0)
			b_IsShielding = true;
	}
	else
		b_IsShielding = false;

	//m_Ultimate_Charge = Math::Clamp<float>(m_Ultimate_Charge, 0, 100);
	//if (Application::IsKeyPressed('A') && m_Ultimate_Charge == 100)
	//{
	//	if (m_player->m_weaponManager[m_player->m_weaponIndex]->useUltimate())
	//		m_Ultimate_Charge = 0;
	//}
	if (Application::IsKeyPressed('1'))
	{
		m_player->m_weaponIndex = 0;
	}
	if (Application::IsKeyPressed('2')/* && PlayerDataManager::getInstance()->getInstance()->m_data.b_isWeapon2Unlocked*/)
	{
		m_player->m_weaponIndex = 1;
		if (!m_player->b_isFacingRight)
		{
			m_player->m_weaponManager[m_player->m_weaponIndex]->m_angle = 180.f;
		}
		else
		{
			m_player->m_weaponManager[m_player->m_weaponIndex]->m_angle = 0.f;
		}
	}
	//if (Application::IsKeyPressed('3') && PlayerDataManager::getInstance()->getInstance()->m_data.b_isWeapon3Unlocked)
	//{
	//	m_player->m_weaponIndex = 2;
	//	m_player->m_weaponManager[m_player->m_weaponIndex]->b_canAttack = false;
	//}

	if (m_player->m_weaponIndex == 1)
	{
		if (m_player->b_isFacingRight)
		{
			m_player->m_weaponManager[m_player->m_weaponIndex]->m_angle = Math::Clamp(m_player->m_weaponManager[m_player->m_weaponIndex]->m_angle, -45.f, 45.f);
		}
		else
		{
			m_player->m_weaponManager[m_player->m_weaponIndex]->m_angle = Math::Clamp(m_player->m_weaponManager[m_player->m_weaponIndex]->m_angle, 135.f, 225.f);
		}
	}

	if (m_player->m_weaponManager[m_player->m_weaponIndex]->b_canAttack)
	{
		for (std::vector<GameObject*>::iterator it = m_scene->goManager.m_goList.begin(); it != m_scene->goManager.m_goList.end(); ++it)
		{
			GameObject* obj = *it;
			if (obj->active)
			{
				if (obj == m_player)
					continue;
				AI_Base* Enemy = dynamic_cast<AI_Base*>(obj);
				if (Enemy && !Enemy->IsAlly())
				{
					if (m_player->m_weaponManager[m_player->m_weaponIndex]->checkRange(Enemy))
					{
						Enemy->ReduceHealth((float)m_player->m_weaponManager[m_player->m_weaponIndex]->getAttack());
					}
				}
			}
		}
		if (m_player->m_weaponIndex == 0)
			m_player->m_weaponManager[m_player->m_weaponIndex]->b_canAttack = false;
	}
}

void PlayerManager::SetPlayer(Player * player)
{
	m_player = player;
}

void PlayerManager::IncreaseArmor(float amount)
{
	m_Armor += std::abs(amount);
	m_Armor = Math::Clamp<float>(m_Armor, 0, 100);
}

void PlayerManager::ReduceArmor(float damage)
{
	m_Armor -= std::abs(damage);
	m_Armor = Math::Clamp<float>(m_Armor, 0, 100);
}

void PlayerManager::IncreaseHealth(float amount)
{
	m_Health += std::abs(amount);
	m_Health = Math::Clamp<float>(m_Health, 0, 100);

	// Play sound effect?
}

void PlayerManager::IncreaseUltimateCharge(float amount)
{
	m_Ultimate_Charge += abs(amount);
	m_Ultimate_Charge = Math::Clamp<float>(m_Ultimate_Charge, 0, 100);
}

void PlayerManager::ReduceHealth(float damage)
{
	if (b_IsGodmode)
		return;

	if (b_IsShielding) {
		SoundManager::getInstance().Play2DSound("Block", false, true, false);
		damage *= 0.5f;
	}
	if (m_Armor > 0) {
		SoundManager::getInstance().Play2DSound("Armor", false, true, false);
		damage /= 2; // Halve the damage
		ReduceArmor(damage);
	}
	else
	{
		SoundManager::getInstance().Play2DSound("Hit", false, true, false);
		m_Health -= std::abs(damage);
		m_Health = Math::Clamp<float>(m_Health, 0, 100);

		if (m_Health == 0) {
			SceneManager::getInstance()->changeScene(new SceneGameover("Game Over: You are DEAD!", SceneGameover::TYPE_MENU::MENU_GAMEOVER, m_scene->sceneType, m_Score / 2));
			return;
		}
	}
}

bool PlayerManager::IsDashing() {
	return (m_scene->m_elapsedTime < m_dashTimer);
}
