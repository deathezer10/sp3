#include "ParticleManager.h"
#include "ParticleEmitter.h"
#include "Particle.h"

ParticleManager::ParticleManager() {
}

ParticleManager::~ParticleManager() {
	if (m_pEmitterList.size()) {
		for (auto &i : m_pEmitterList)
			delete i;
	}
	m_pEmitterList.clear();
}

void ParticleManager::AddEmitter(ParticleEmitter * emitter) {
	m_pEmitterList.push_back(emitter);
}

void ParticleManager::UpdateParticles() {
	for (auto &i : m_pEmitterList) {
		if (i->isActive())
			i->Update();
	}
}

void ParticleManager::RenderParticles() {
	for (auto &i : m_pEmitterList) {
		if (i->isActive())
			i->Render();
	}
}

void ParticleManager::Exit(){
	for (auto &it : m_pEmitterList)
		delete it;

	m_pEmitterList.clear();
}
