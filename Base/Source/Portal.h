#pragma once

#include <vector>
#include "AI_Base.h"


class Portal : public AI_Base {

public:
	enum E_PORTAL_TYPE {

		WARRIOR = 0,
		ARCHER,
		GOLEM,
		NINJA,
		WARRIOR_AND_ARCHER,
		GOLEM_AND_WARRIOR,
		NINJA_AND_ARCHER

	};

	Portal(Scene* scene, Player* player, E_PORTAL_TYPE portalType, bool isCore = false);
	~Portal() {};

	virtual bool Update();
	virtual void Render();
	virtual void OnCollisionHit(GameObject* other);

	// Minimum distance to activate this portal
	float m_PActivateThreshold;

private:
	// Used to determine what monster the portal is currently spawning
	enum E_PORTAL_MONSTERS {
		M_WARRIOR = 0,
		M_ARCHER,
		M_GOLEM,
		M_NINJA
	};

	E_PORTAL_TYPE m_PortalType;

	// Is this portal a core?
	bool b_PIsCore = false;

	// Is this portal currently spawning anything?
	bool b_PIsActive = false;

	// Helper, to determine next spawn timing
	float m_PNextSpawnTime = 0;

	// Cooldown between each spawn
	float m_PSpawnCooldown;
	
	// What monster it is currently spawning
	unsigned m_PCurrentSpawnIndex = 0;

	// List of what to spawn
	std::vector<E_PORTAL_MONSTERS> v_MonstersToSpawn;

	void SpawnMonster(E_PORTAL_MONSTERS monsterType);

};