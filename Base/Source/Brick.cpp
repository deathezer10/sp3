#include "Brick.h"
#include "Scene.h"
#include "Player.h"
#include "AI_Base.h"
#include "ColliderFactory.h"



Brick::Brick(Scene * scene) : GameObject(scene, Scene::GEO_TERRAIN_GRASS) {
	b_EnableLighting = false;
	b_EnablePhysics = true;

	color.Set(1, 1, 1);

	Vector3 size(100, 5, 1);
	scale = size;

	collider = ColliderFactory::CreateOBBCollider(this, size);
}

bool Brick::Update() {

	collider.bboxSize = scale;

	alpha = 1;

	Scene::GEOMETRY_TYPE terrainType = (Scene::GEOMETRY_TYPE)type;

	switch (terrainType) {

	case Scene::GEO_TERRAIN_GRASS:
		break;
	case Scene::GEO_TERRAIN_MUDDY:
		break;
	case Scene::GEO_TERRAIN_LAVA:
		break;
	case Scene::GEO_TERRAIN_ICE:
		friction = 0.025f;
		break;
	case Scene::GEO_TERRAIN_BRICK:
		break;
	case Scene::GEO_TERRAIN_CRATE:
		b_EnablePhysics = false;
		break;
	case Scene::GEO_TERRAIN_INVISIBLE_WALL:
		alpha = 0;
		break;

	}

	return true;
}

void Brick::OnCollisionHit(GameObject * other) {

	if (other->type == Scene::GEO_PLAYER || dynamic_cast<AI_Base*>(other) != nullptr) {

		Scene::GEOMETRY_TYPE terrainType = (Scene::GEOMETRY_TYPE)type;

		switch (terrainType) {

		case Scene::GEO_TERRAIN_GRASS:
			break;

		case Scene::GEO_TERRAIN_MUDDY:
		{
			if (other->type == Scene::GEO_PLAYER) {
				if (m_scene->playerManager.IsDashing() == false)
					other->vel.x = Math::Clamp<float>(other->vel.x, -10, 10); // Limit velocity if not dashing

			}
			else { // Enemy Slow down

				AI_Base* ai = dynamic_cast<AI_Base*>(other);

				if (ai && ai->IsAlly() == false) {

					float maxSpeed = static_cast<AI_Base*>(other)->GetDefaultSpeed();
					other->vel.x = Math::Clamp<float>(other->vel.x, -(maxSpeed / 2), maxSpeed / 2); // Limit velocity temporarily

				}
			}
		}
		break;

		case Scene::GEO_TERRAIN_LAVA:
			if (other == m_scene->playerManager.GetPlayer()) { // Lava only damages player

				if (m_scene->playerManager.IsDashing() == false)
					other->vel.x = Math::Clamp<float>(other->vel.x, -15, 15); // Limit velocity if not dashing

				if (m_scene->m_elapsedTime >= m_Lava_NextDamageTime) {

					m_scene->playerManager.ReduceHealth(m_Lava_Damage);

					m_Lava_NextDamageTime = m_scene->m_elapsedTime + m_Lava_DamageInterval;
				}
			}
			break;

		case Scene::GEO_TERRAIN_ICE:
			break;

		case Scene::GEO_TERRAIN_BRICK:
			break;

		case Scene::GEO_TERRAIN_CRATE:
			break;

		case Scene::GEO_TERRAIN_INVISIBLE_WALL:
			break;

		}

	}

}
