#pragma once


#include <string>

using std::string;

class GameObjectManager;

class TerrainLoader {
		
public:
	// Returns TRUE if the geometry enum is a Terrain
	static bool IsTerrain(unsigned geoType);
	
	// Read the txt file and load all GameObjects into goManager
	// Returns FALSE if the txt file failed to open
	static bool LoadTerrain(string filePath, GameObjectManager* goManager);

	// Writes all GameObject in goManager into a txt file
	static void SaveTerrain(GameObjectManager* goManager);


private:


};