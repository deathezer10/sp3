#ifndef APPLICATION_H
#define APPLICATION_H

#include "timer.h"
#include <bitset>

class Application {

	friend class SceneManager;

public:
	Application();
	~Application();

	// Max FPS of this game, remember to disable V-Sync using glfwSwapInterval(0)
	const static unsigned char FPS = 60;

	// Time in between each frame in milliseconds
	const static unsigned int frameTime = 1000 / FPS;

	// Time in between rendering this and the previous frame in millisconds 
	// (Using a constant because Update() now runs at a fixed time slice even in order to synchronize physics behaviour)
	const static double deltaTime;

	void Init();
	void Run();
	void Exit();

	// Returns the current FPS of the application
	static float GetFPS() { return m_FPS; };

	// TRUE if Key is currently held down (Same as the old function)
	static bool IsKeyPressed(unsigned char virtualKey);

	// TRUE if Key was pressed, but will not fire again until the User let go of the Key (Useful for Combo-ing Keys)
	static bool IsKeyPressedOnce(unsigned char virtualKey);

	// TRUE if Key was pressed and then let go (Useful for UI controls)
	static bool IsKeyPressedAndReleased(unsigned char virtualKey);

	static void GetCursorPos(double *xpos, double *ypos);

	// Returns the current window Width
	static int windowWidth() { return m_window_width; };

	// Returns the current window Height
	static int windowHeight() { return m_window_height; };

	// Returns the default window Width
	static int windowDefaultWidth() { return WINDOW_DEFAULT_WIDTH; };

	// Returns the default window Height
	static int windowDefaultHeight() { return WINDOW_DEFAULT_HEIGHT; };

	// Is the application currently in Fullscreen?
	static bool IsFullScreenMode() { return _isFullScreen; };

	// Switch between Fullscreen and Window mode
	static void ToggleFullscreen();

	// Was window resized recently?
	static bool IsWindowValid() { return _isValid; };

	// Flag window as clean
	static void ValidateWindow() { _isValid = true; };

private:
	const static int WINDOW_DEFAULT_WIDTH = 1024;
	const static int WINDOW_DEFAULT_HEIGHT = 720;

	// Declare the window width and height as constant integer
	static int m_window_width;
	static int m_window_height;

	static bool _isFullScreen;
	static bool _isValid; // Used to prevent cursor from moving the camera when switching from window to fullscreen mode

	static float m_FPS;
	
	// Keyboard Controller
	const static int MAX_KEYS = 256;

	static std::bitset<MAX_KEYS> currStatus, prevStatus;

	static void EndFrameUpdate();

	static void UpdateInput();
	static void UpdateKeyboardStatus(unsigned char _slot, bool _isPressed);


};

#endif