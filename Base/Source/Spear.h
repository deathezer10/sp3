#pragma once

#include "GameObject.h"
#include "WeaponInfo.h"

class Player;

class Spear : public WeaponInfo, public GameObject
{
public:
	Spear(Scene* scene, Player* player);
	~Spear() {};

	bool Update();

	void Render();

	bool checkRange(GameObject* obj);

	void checkTimer();

	bool useUltimate();

	int getAttack();


	float getUltimateDuration();

private:
	Player* m_player;
	float m_offsetPos;
	Vector3 m_offset;
	bool b_completeLoop;
	bool b_usingUlt;
	bool b_atTip;
	bool b_attackAnimation;
};