#include "Application.h"

#include "GL\glew.h"
#include "GLFW\glfw3.h"
#include "Mtx44.h"
#include "shader.hpp"




#include "SceneManager.h"
#include "SceneGameover.h"
#include "MeshBuilder.h"
#include "Utility.h"
#include "LoadTGA.h"
//include the scenes and player saving data header 
#include "PlayerData.h"
#include "Main_Menu.h"
#include "SceneLevel01.h"
#include "SceneLevel02.h"
#include "SceneLevel03.h"


#include <sstream>

SceneGameover::SceneGameover(const string & title, TYPE_MENU type, TYPE_SCENE previousScene, int currencyEarned) : SceneBase(SCENE_GAMEOVER_VIC)
{
	_previousScene = previousScene;
	_menuSelected = 0;
	_menuType = type;
	_title = title;
	//to do link to the score
	_currencyEarned = currencyEarned;
}

SceneGameover::~SceneGameover()
{
}

void SceneGameover::Init()
{
	SceneBase::Init();

	if (_menuType == MENU_VICTORY)
	{
		glClearColor(0.4f, 0.4f, 0.4f, 0.0f);
	}
	else
	{
		glClearColor(0.5f, 0.0f, 0.0f, 0.0f);

	}

	//Calculating aspect ratio
	m_worldHeight = 100.f;
	m_worldWidth = m_worldHeight * (float)Application::windowWidth() / Application::windowHeight();

	meshList[GEO_UI_DEFEAT] = MeshBuilder::GenerateQuad("UI_D", Color(1, 1, 1), 1.f);
	meshList[GEO_UI_DEFEAT]->textureArray[0] = LoadTGA("Image/defeat_ui.tga");

	meshList[GEO_UI_VICTORY] = MeshBuilder::GenerateQuad("UI_V", Color(1, 1, 1), 1.f);
	meshList[GEO_UI_VICTORY]->textureArray[0] = LoadTGA("Image/victory_ui.tga");

	meshList[GEO_VIC_TEXT] = MeshBuilder::GenerateQuad("UI_VT", Color(1, 1, 1), 1.f);
	meshList[GEO_VIC_TEXT]->textureArray[0] = LoadTGA("Image/vic_text.tga");

	meshList[GEO_DE_TEXT] = MeshBuilder::GenerateQuad("UI_DT", Color(1, 1, 1), 1.f);
	meshList[GEO_DE_TEXT]->textureArray[0] = LoadTGA("Image/de_text.tga");

	if (_menuType == MENU_VICTORY &&_previousScene == SCENE_LEVEL01)
	{
		PlayerDataManager::getInstance()->m_data.b_isLevel02Unlocked = true;
	}
	if (_menuType == MENU_VICTORY&&_previousScene == SCENE_LEVEL02)
	{
		PlayerDataManager::getInstance()->m_data.b_isLevel03Unlocked = true;
	}



	PlayerDataManager::getInstance()->m_data.m_currency += _currencyEarned;
	PlayerDataManager::getInstance()->SaveData();
}

void SceneGameover::Update(double dt)
{


	GLFWwindow* window = glfwGetCurrentContext();

	if (!glfwGetKey(window, GLFW_KEY_UP) && !glfwGetKey(window, GLFW_KEY_DOWN)) {
		canPressUpDown = true;
	}

	///////////////////////////////////////////////vvvvvvvvvvvvvvvvvvARROW CONTROL FOR VICTORY MENUvvvvvvvvvvvvvvvvvvvvvvv/////////////////////////////////////////
	if (_menuType == MENU_VICTORY&&_previousScene != SCENE_LEVEL03) {
		if (glfwGetKey(window, GLFW_KEY_UP) && canPressUpDown) {
			if (_menuSelected >= 1) {
				_menuSelected -= 1;
				canPressUpDown = false;
			}
		}
		if (glfwGetKey(window, GLFW_KEY_DOWN) && canPressUpDown) {
			if (_menuSelected <= 1) {
				_menuSelected += 1;
				canPressUpDown = false;
			}
		}
	}

	if (_menuType == MENU_VICTORY&&_previousScene == SCENE_LEVEL03) {
		if (glfwGetKey(window, GLFW_KEY_UP) && canPressUpDown) {
			if (_menuSelected >= 1) {
				_menuSelected -= 1;
				canPressUpDown = false;
			}
		}
		if (glfwGetKey(window, GLFW_KEY_DOWN) && canPressUpDown) {
			if (_menuSelected <= 1) {
				_menuSelected = 1;
				canPressUpDown = false;
			}
		}
	}
	///////////////////////////////////////////^^^^^^^^^^^^^^^^^^^^^ARROW CONTROL FOR VICTORY MENU^^^^^^^^^^^^^^^^^^^/////////////////////////////////////////////

	////////////////////////////////////////////////////////vvvvvvvvvvvvARROWS CONTROL FOR GAME OVER MENUvvvvvvvvvvvvvvvvvvvvvv//////////////////////////////
	if (_menuType == MENU_GAMEOVER) {
		if (glfwGetKey(window, GLFW_KEY_UP) && canPressUpDown) {
			if (_menuSelected >= 1) {
				_menuSelected -= 1;
				canPressUpDown = false;
			}
		}
		if (glfwGetKey(window, GLFW_KEY_DOWN) && canPressUpDown) {
			if (_menuSelected <= 1) {
				_menuSelected += 1;
				canPressUpDown = false;
			}
		}
	}
	/////////////////////////////////////////////////////////^^^^^^^^^^^^^^^^ARROWS CONTROL FOR GAME OVER MENU^^^^^^^^^^^^^^//////////////////////////////


	if (!glfwGetKey(window, GLFW_KEY_ENTER)) {
		canPressEnter = true;
	}

	//////////////////////////////////////////////////////////vvvvvvvvvvvvvCHANGE GAME STATEvvvvvvvvvvvvvvvvvv//////////////////////////////////////////////
	if (glfwGetKey(window, GLFW_KEY_ENTER) && canPressEnter && _menuType == MENU_GAMEOVER) {
		switch (_menuSelected) {
		case 0:
			SceneManager::getInstance()->changeScene(SceneManager::getInstance()->CreateScene(_previousScene)); // Change to previous Scene
			return;
		case 1:
			SceneManager::getInstance()->changeScene(new Main_Menu());
			return;
		case 2:
			glfwSetWindowShouldClose(glfwGetCurrentContext(), true); // Flag application to quit
			break;
		}

		canPressEnter = false;

	}
	if (glfwGetKey(window, GLFW_KEY_ENTER) && canPressEnter && _menuType == MENU_VICTORY&&_previousScene == Scene::SCENE_LEVEL03) {
		switch (_menuSelected) {
		case 0:
			SceneManager::getInstance()->changeScene(new Main_Menu());
			return;
		case 1:
			glfwSetWindowShouldClose(glfwGetCurrentContext(), true); // Flag application to quit
			break;
		}
		canPressEnter = false;
	}

	if (glfwGetKey(window, GLFW_KEY_ENTER) && canPressEnter && _menuType == MENU_VICTORY) {
		switch (_menuSelected) {
		case 0:
			switch (_previousScene) {
			case Scene::SCENE_LEVEL01:
				SceneManager::getInstance()->changeScene(new SceneLevel02()); // Change to previous Scene
				return;
			case Scene::SCENE_LEVEL02:
				SceneManager::getInstance()->changeScene(new SceneLevel03()); // Change to previous Scene
				return;
			case Scene::SCENE_LEVEL03:
				break;
			default:
				break;
			}
			break;
		case 1:
			SceneManager::getInstance()->changeScene(SceneManager::getInstance()->CreateScene(Scene::SCENE_MAINMENU));
			return;
		case 2:
			glfwSetWindowShouldClose(glfwGetCurrentContext(), true); // Flag application to quit
			break;
		}
		canPressEnter = false;

	}
	/////////////////////////////////////////////////////////////////^^^^^^^^^^^^^^CHANGE GAME STATE^^^^^^^^^^^^^^^^^^//////////////////////////////////////////////


}

void SceneGameover::Render()
{

	// Render VBO here
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);



	float winWidth = (float)Application::windowWidth() / 20;
	float winHeight = (float)Application::windowHeight() / 20;
	Vector3 tileScale(65, 40, 23);/*
								 glDisable(GL_DEPTH_TEST);
								 textManager.RenderMeshOnScreen(meshList[GEO_UI], winWidth * 0.50f, winHeight * 0.28f, Vector3(90, 0, 0), tileScale);
								 glEnable(GL_DEPTH_TEST);
								 */

	std::string newLine = "";
	std::string title = _title;

	std::string option1 = "Option 1";
	std::string option2 = "Option 2";

	switch (_menuType) {

	case TYPE_MENU::MENU_GAMEOVER:
		glDisable(GL_DEPTH_TEST);

		textManager.RenderMeshOnScreen(meshList[GEO_UI_DEFEAT], winWidth, winHeight - 8, Vector3(0, 0, 0), tileScale);
		textManager.RenderMeshOnScreen(meshList[GEO_DE_TEXT], winWidth, winHeight + 24, Vector3(0, 0, 0), Vector3(65, 10, 23));
		option1 = (_menuSelected == 0) ? ">Restart<" : "Restart";
		option2 = (_menuSelected == 2) ? ">Quit<" : "Quit";
		break;
	case TYPE_MENU::MENU_VICTORY:
		switch (_previousScene) {
		case Scene::SCENE_LEVEL03:
			glDisable(GL_DEPTH_TEST);


			textManager.RenderMeshOnScreen(meshList[GEO_UI_VICTORY], winWidth, winHeight - 8, Vector3(0, 0, 0), tileScale);
			textManager.RenderMeshOnScreen(meshList[GEO_VIC_TEXT], winWidth, winHeight + 24, Vector3(0, 0, 0), Vector3(65, 10, 23));
			option1 = (_menuSelected == 0) ? ">Main Menu<" : "Main Menu";
			option2 = (_menuSelected == 1) ? ">Quit<" : "Quit";
			break;
		default:
			glDisable(GL_DEPTH_TEST);

			textManager.RenderMeshOnScreen(meshList[GEO_UI_VICTORY], winWidth, winHeight - 8, Vector3(0, 0, 0), tileScale);
			textManager.RenderMeshOnScreen(meshList[GEO_VIC_TEXT], winWidth, winHeight + 24, Vector3(0, 0, 0), Vector3(65, 10, 23));
			option1 = (_menuSelected == 0) ? ">Next level<" : "Next level";
			option2 = (_menuSelected == 2) ? ">Quit<" : "Quit";
			break;
		}
	}




	//std::string option4 = "Currency earned: ";
	//option4.append(std::to_string(_currencyEarned));
	//textManager.renderTextOnScreen(UIManager::Text(option4, Color(1, 1, 1), UIManager::ANCHOR_CENTER_CENTER));
	//textManager.renderTextOnScreen(UIManager::Text(newLine, Color(1, 1, 1), UIManager::ANCHOR_CENTER_CENTER));

	textManager.renderTextOnScreen(UIManager::Text(title, Color(1, 1, 1), UIManager::ANCHOR_CENTER_CENTER));
	textManager.renderTextOnScreen(UIManager::Text(newLine, Color(1, 1, 1), UIManager::ANCHOR_CENTER_CENTER));
	textManager.renderTextOnScreen(UIManager::Text(newLine, Color(1, 1, 1), UIManager::ANCHOR_CENTER_CENTER));
	textManager.renderTextOnScreen(UIManager::Text(option1, Color(1, 1, 1), UIManager::ANCHOR_CENTER_CENTER));
	textManager.renderTextOnScreen(UIManager::Text(newLine, Color(1, 1, 1), UIManager::ANCHOR_CENTER_CENTER));



	if (_menuType == MENU_VICTORY&&_previousScene == SCENE_LEVEL03) {
		textManager.renderTextOnScreen(UIManager::Text(newLine, Color(1, 1, 1), UIManager::ANCHOR_CENTER_CENTER));
		textManager.renderTextOnScreen(UIManager::Text(option2, Color(1, 1, 1), UIManager::ANCHOR_CENTER_CENTER));
	}

	if (_previousScene == Scene::SCENE_LEVEL03&&_menuType == MENU_GAMEOVER) {
		std::string option3 = "Option 3";
		option3 = (_menuSelected == 1) ? ">Main Menu<" : "Main Menu";
		textManager.renderTextOnScreen(UIManager::Text(option3, Color(1, 1, 1), UIManager::ANCHOR_CENTER_CENTER));
		textManager.renderTextOnScreen(UIManager::Text(newLine, Color(1, 1, 1), UIManager::ANCHOR_CENTER_CENTER));
		textManager.renderTextOnScreen(UIManager::Text(option2, Color(1, 1, 1), UIManager::ANCHOR_CENTER_CENTER));
		textManager.renderTextOnScreen(UIManager::Text(newLine, Color(1, 1, 1), UIManager::ANCHOR_CENTER_CENTER));
	}
	if (_previousScene != Scene::SCENE_LEVEL03&&_menuType == MENU_VICTORY) {

		std::string option3 = "Option 3";
		option3 = (_menuSelected == 1) ? ">Main Menu<" : "Main Menu";
		textManager.renderTextOnScreen(UIManager::Text(option3, Color(1, 1, 1), UIManager::ANCHOR_CENTER_CENTER));
		textManager.renderTextOnScreen(UIManager::Text(newLine, Color(1, 1, 1), UIManager::ANCHOR_CENTER_CENTER));
		textManager.renderTextOnScreen(UIManager::Text(option2, Color(1, 1, 1), UIManager::ANCHOR_CENTER_CENTER));
	}

	if (_menuType == MENU_GAMEOVER&&_previousScene != SCENE_LEVEL03) {
		std::string option3 = "Option 3";
		option3 = (_menuSelected == 1) ? ">Main Menu<" : "Main Menu";
		textManager.renderTextOnScreen(UIManager::Text(option3, Color(1, 1, 1), UIManager::ANCHOR_CENTER_CENTER));
		textManager.renderTextOnScreen(UIManager::Text(newLine, Color(1, 1, 1), UIManager::ANCHOR_CENTER_CENTER));
		textManager.renderTextOnScreen(UIManager::Text(option2, Color(1, 1, 1), UIManager::ANCHOR_CENTER_CENTER));
	}



	textManager.reset();

}

void SceneGameover::Exit()
{
	SceneBase::Exit();
}
