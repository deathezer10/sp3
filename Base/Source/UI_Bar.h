#pragma once

#include "Vector3.h"


// Forward declaration
class Mesh;
class Scene;


class UI_BAR {

public:
	UI_BAR();

	// Render the bar for hp and capture point
	// m_scene takes in current scene
	// BGMesh takes in the meshlist[GEOMETRY_TYPE] for background texture
	// FGMesh takes in the meshlist[GEOMETRY_TYPE] for foreground texture
	// FRMesh takes in the meshlist[GEOMETRY_TYPE] for bar frame texture
	// pos takes in Vector3 position
	// Scale takes in Vector3 Scale
	// Scale_factor takes in value that affects or updates the scaling of the bar clamp at 0 to 1
	static void RenderBar(Scene* m_scene, Mesh* BGMesh, Mesh* FGMesh, Mesh* FRMMesh, Vector3 pos, Vector3 Scale, float Scale_factor);

private:
	virtual ~UI_BAR() = 0;


};