#include "PlayerData.h"

PlayerDataManager* PlayerDataManager::m_instance = nullptr;

PlayerDataManager * PlayerDataManager::getInstance()
{
	if (!m_instance)
		m_instance = new PlayerDataManager();
	return m_instance;
}

void PlayerDataManager::Exit()
{
	if (m_instance) {
		delete m_instance;
		m_instance = nullptr;
	}
}

void PlayerDataManager::SetFile(int FileIndex)
{
	FileName = "Data" + std::to_string(FileIndex) + ".txt";
}

void PlayerDataManager::SaveData()
{
	std::ofstream OutputFile;
	OutputFile.open(FileName);

	if (OutputFile.is_open())
	{
		//Writing into text file here
		OutputFile << "<Level 2>=" << m_data.b_isLevel02Unlocked << std::endl;
		OutputFile << "<Level 3>=" << m_data.b_isLevel03Unlocked << std::endl;
		OutputFile << "<Weapon 2>=" << m_data.b_isWeapon2Unlocked << std::endl;
		OutputFile << "<Weapon 3>=" << m_data.b_isWeapon3Unlocked << std::endl;
		OutputFile << "<Currency>=" << m_data.m_currency << std::endl;
		OutputFile << "<Unit 1>=" << m_data.m_amountofUnit1 << std::endl;
		OutputFile << "<Unit 2>=" << m_data.m_amountofUnit2 << std::endl;
	}

}

void PlayerDataManager::LoadData()
{
	std::ifstream InputFile;
	std::string tempString;

	InputFile.open(FileName);

	if (InputFile.is_open())
	{
		while (InputFile.good())
		{
			//Loading from file
			std::getline(InputFile, tempString);
			std::string::size_type begin = tempString.find_first_not_of(" \f\t\v");

			if (begin == std::string::npos)
				continue;
			else if (std::string("#").find(tempString[begin]) != std::string::npos)
				continue;
			else if (std::string("<").find(tempString[begin]) != std::string::npos)
			{
				for (unsigned i = 0; i < tempString.size(); ++i)
				{
					if (tempString[i] == '=')
					{
						if (tempString.substr(0, i) == "<Level 2>")
							m_data.b_isLevel02Unlocked = (std::stoi(tempString.substr(i + 1)) > 0);
						else if (tempString.substr(0, i) == "<Level 3>")
							m_data.b_isLevel03Unlocked = (std::stoi(tempString.substr(i + 1)) > 0);
						else if (tempString.substr(0, i) == "<Weapon 2>")
							m_data.b_isWeapon2Unlocked = (std::stoi(tempString.substr(i + 1)) > 0);
						else if (tempString.substr(0, i) == "<Weapon 3>")
							m_data.b_isWeapon3Unlocked = (std::stoi(tempString.substr(i + 1)) > 0);
						else if (tempString.substr(0, i) == "<Currency>")
							m_data.m_currency = std::stoi(tempString.substr(i + 1));
						else if (tempString.substr(0, i) == "<Unit 1>")
							m_data.m_amountofUnit1 = std::stoi(tempString.substr(i + 1));
						else if (tempString.substr(0, i) == "<Unit 2>")
							m_data.m_amountofUnit2 = std::stoi(tempString.substr(i + 1));
					}
				}
			}
		}
	}
}

