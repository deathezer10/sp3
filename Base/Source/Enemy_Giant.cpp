#include "Enemy_Giant.h"
#include "Player.h"
#include "Scene.h"
#include "GameObject.h"
#include "PlayerManager.h"
#include "SpriteAnimation.h"
#include "ColliderFactory.h"
#include "UI_Bar.h"

Enemy_Giant::Enemy_Giant(Scene* scene, Player* player) :AI_Base(scene, Scene::GEO_ENEMYGIANT, player, 500, 5)
{
	b_EnableGravity = true;
	b_EnablePhysics = true;
	b_EnableLighting = false;

	scale.Set(15, 20.f, 1.f);
	this->collider = ColliderFactory::CreateOBBCollider(this, Vector3(8.f, 17.5f, 1));

	m_AnimEnemyWarrior = static_cast<SpriteAnimation*>(scene->meshList[Scene::GEO_ENEMYGIANT]);
	m_Anim = new Animation();
	m_Anim->Set(0, 0, 1, 1.f, true);

	m_OffsetPos.y -= 1.f; // align with the ground

	m_stunThreshold = 2000;
	m_stunDuration = 0.25f;

	m_WeaponRange = 15;

}

Enemy_Giant::~Enemy_Giant() {
	if (m_Anim)
		delete m_Anim;
}

bool Enemy_Giant::Update()
{
	if (b_IsGiantActivated == false) {
		m_Anim->Set(0, 0, 1, 1, true);
		vel.x = 0;
	}
	else {

		if (m_CurrentState == CHASE)
			AI_Chase(thePlayer, m_scene->m_dt);
		if (m_CurrentState == JUMP)
			AI_Jump(m_scene->m_dt);
		if (m_CurrentState == ATTACK)
			AI_Attack(thePlayer, m_scene->m_dt);
		if (m_CurrentState == STUN)
			AI_Stun();

		if (!b_IsFacingRight) {
			m_WeaponOffset.x = -cos(Math::DegreeToRadian(m_weaponRotation));
			m_WeaponOffset.y = sin(Math::DegreeToRadian(m_weaponRotation));
		}
		else {
			m_WeaponOffset.x = cos(Math::DegreeToRadian(m_weaponRotation));
			m_WeaponOffset.y = sin(Math::DegreeToRadian(m_weaponRotation));
		}

		// Offset so that the club touches the hand
		m_WeaponOffset *= 5;
		m_WeaponOffset.x -= 1;
		m_WeaponOffset.y -= 1;

	}

	return true;
}

void Enemy_Giant::AI_Chase(Player*player, double dt)
{
	if (pos.x < player->pos.x && (player->pos.x - pos.x) >= player->scale.x * 2)
	{
		m_force.x = 90;
		b_IsFacingRight = true;
		if (collider.manifold.normal.x != 0 && !is_jumping)
		{
			m_CurrentState = JUMP;
			is_jumping = true;
		}
	}
	else if (pos.x > player->pos.x && (player->pos.x - pos.x) <= -player->scale.x * 2)
	{
		m_force.x = -90;
		b_IsFacingRight = false;
		if (collider.manifold.normal.x != 0 && !is_jumping)
		{
			m_CurrentState = JUMP;
			is_jumping = true;
		}
	}
	if ((player->pos - pos).Length() < m_WeaponRange) {
		if (player->pos.x > pos.x)
			b_IsFacingRight = true;
		else if (player->pos.x < pos.x)
			b_IsFacingRight = false;

		m_CurrentState = ATTACK;
	}
}

void Enemy_Giant::AI_Attack(Player * player, double dt)
{
	m_force.SetZero();

	// Swing Up first
	if (b_completeLoop == false && b_IsLoopResetting == false) {
		if (m_weaponRotation < 90)
			m_weaponRotation += 90 * m_scene->m_dt;
		else {
			if (m_WeaponColor.g > 0) {
				m_WeaponColor.g -= m_scene->m_dt / 1.5f;
				m_WeaponColor.b -= m_scene->m_dt / 1.5f;
			}
			else {
				b_completeLoop = true;
			}
		}
	}

	// Swing down
	if (b_completeLoop == true && b_IsLoopResetting == false) {
		if (m_weaponRotation > -90)
			m_weaponRotation -= 720 * m_scene->m_dt;
		else {

			// Direction that the giant is attacking
			Vector3 direction;

			if (b_IsFacingRight)
				direction = Vector3::Right;
			else
				direction = Vector3::Left;

			// Check if player is within a cone of 45 degrees
			if (abs(player->pos.x - pos.x) < m_WeaponRange && pos.IsFacingVector(player->pos, direction, 45) && thePlayer->b_isRealPlayer)
				m_scene->playerManager.ReduceHealth(100); // Damage player

			b_IsLoopResetting = true;
		}
	}

	// Reset club to default rotation
	if (b_IsLoopResetting) {

		if (m_weaponRotation < 0)
			m_weaponRotation += 60 * m_scene->m_dt;

		if (m_WeaponColor.g < 1) {
			m_WeaponColor.g += m_scene->m_dt;
			m_WeaponColor.b += m_scene->m_dt;
		}

		if (m_weaponRotation >= 0) {
			m_weaponRotation = 0;
			m_WeaponColor.Set(1, 1, 1);

			b_IsLoopResetting = false;
			b_completeLoop = false;

			m_CurrentState = CHASE;
		}
	}

}


void Enemy_Giant::Render()
{
	if (b_IsGiantActivated == true) {

		if (vel.x < 1 && vel.x > -1) {
			m_Anim->Set(5, 5, 1, 1, true);

			if (b_IsFacingRight) {
				rotation.y = 180;
			}
			else {
				rotation.y = 0;
			}
		}
		else if (b_IsFacingRight) { // Walk Right
			m_Anim->Set(4, 7, 1, 1, true);
			rotation.y = 180;

		}
		else { // Walk Left
			m_Anim->Set(4, 7, 1, 1, true);
			rotation.y = 0;
		}

		if (m_CurrentState != ATTACK)
			m_AnimEnemyWarrior->Update(m_Anim, m_scene->m_dt);

		m_Anim_CurrentFrame = m_Anim->m_currentFrame;
	}

	glDisable(GL_CULL_FACE);
	GameObject::Render();

	glDisable(GL_DEPTH_TEST);

	if (b_IsGiantActivated) {
		// Render Weapon
		m_scene->modelStack.PushMatrix();

		if (!b_IsFacingRight) {
			m_scene->modelStack.Translate(pos.x + m_OffsetPos.x + m_WeaponOffset.x, pos.y + m_OffsetPos.y + m_WeaponOffset.y, currentIndex * 0.01f);
			m_scene->modelStack.Rotate(180, 0, 1, 0);
		}
		else if (b_IsFacingRight) {
			m_scene->modelStack.Translate(pos.x + m_OffsetPos.x + m_WeaponOffset.x, pos.y + m_OffsetPos.y + m_WeaponOffset.y, currentIndex * 0.01f);
		}

		if (m_CurrentState == ATTACK || m_CurrentState == STUN)
			m_scene->modelStack.Rotate(m_weaponRotation, 0.f, 0.f, 1.f);

		m_scene->modelStack.Scale(10, 5, 1);
		m_scene->meshList[Scene::GEO_ENEMYGIANT_CLUB]->color = m_WeaponColor; // Redness of the club
		m_scene->RenderMesh(m_scene->meshList[Scene::GEO_ENEMYGIANT_CLUB], false);
		m_scene->meshList[Scene::GEO_ENEMYGIANT_CLUB]->color = Color(1, 1, 1); // Reset color so it doesnt affect other giants

		m_scene->modelStack.PopMatrix();
		glEnable(GL_DEPTH_TEST);
	}

	glEnable(GL_CULL_FACE);

	// Render Health bar
	Vector3 barPos = pos;
	barPos.y += collider.bboxSize.y * 0.8f;
	Mesh** meshList = m_scene->meshList;
	UI_BAR::RenderBar(m_scene, meshList[Scene::GEO_HP_BACKGROUND], meshList[Scene::GEO_HP_FOREGROUND], meshList[Scene::GEO_HP_FRAME], barPos, Vector3(5, 1), (float)GetCurrentHealth() / m_DefaultHP);


}

void Enemy_Giant::OnCollisionHit(GameObject * other)
{
	AI_Base::OnCollisionHit(other);
	if ((this->collider.manifold.normal.x == -1 || this->collider.manifold.normal.x == 1) && other->type == Scene::GEO_PLAYER)
	{
		if (m_scene->m_elapsedTime > m_attackTimer)
		{
			is_attacking = true;
			m_attackTimer = m_scene->m_elapsedTime + 1.f;
		}
	}
	// Checking if AI is under or above player
	else if ((this->collider.manifold.normal.y == -1 || this->collider.manifold.normal.y == 1) && other->type == Scene::GEO_PLAYER)
	{
		// If AI is facing right, AI will move right , else AI will move left
		this->b_IsFacingRight ? m_force.x = 55 : m_force.x = -55;
	}
}



