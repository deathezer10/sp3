#include"Enemy_arrow.h"
#include"Scene.h"
#include"ColliderFactory.h"
#include"PlayerManager.h"
#include"Player.h"
#include"TerrainLoader.h"
#include"AI_Base.h"


Enemy_arrow::Enemy_arrow(Scene* scene) : GameObject(scene,Scene::GEO_ARROW)
{
	b_EnableGravity = true;
	b_EnableLighting = false;
	b_EnablePhysics = true;
	active = true;
	b_EnableTrigger = true;

	mass = 1.f;
	scale.Set(7.5f, 7.5f, 1.f);

	this->collider = ColliderFactory::CreateOBBCollider(this, Vector3(7.5f, 1.f, 0.f));
	MAX_VELOCITY.y = 100.f;
	MAX_VELOCITY.x = 100.f;
}


Enemy_arrow::~Enemy_arrow()
{
}

bool Enemy_arrow::Update()
{
	if (vel.IsZero() == false)
		dir = vel.Normalized();
	return true;
}

void Enemy_arrow::fireArrow(float angle,Vector3 AIpos, Collider AIcolliderSize)
{
	Vector3 offset;
	
	this->pos.Set(AIpos.x, AIpos.y, 0.f);
	this->vel.Set(100.f * cos(angle), 90.f*sin(angle), 0.f);
	
	offset = this->vel.Normalized() * (AIcolliderSize.bboxSize * 1.5f);
	this->pos += offset;
}

void Enemy_arrow::OnCollisionHit(GameObject* other)
{
	AI_Base* Ally = dynamic_cast<AI_Base*>(other);
	if (other->type != Scene::GEO_ARROW && (other->type == Scene::GEO_PLAYER ||TerrainLoader::IsTerrain(other->type)))
	{
		if (other == m_scene->playerManager.GetPlayer())
		{
			m_scene->playerManager.ReduceHealth(10);
			collider.ResolveCollision(other->collider, m_scene->m_dt);
		}
		m_scene->goManager.DestroyObjectQueue(this);
	}
	else if (Ally && Ally->IsAlly() && Ally->type != Scene::GEO_ALLYWARRIOR)
	{
		Ally->ReduceHealth(10);
		collider.ResolveCollision(other->collider, m_scene->m_dt);
		m_scene->goManager.DestroyObjectQueue(this);
	}
}

void Enemy_arrow::Render()
{
	rotation.z = Math::RadianToDegree(atan2f(vel.y, vel.x));
	GameObject::Render();
}
