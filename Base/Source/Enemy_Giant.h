#pragma once

#include"AI_Base.h"


class SwordShield;
class SpriteAnimation;
struct Animation;


class Enemy_Giant :public AI_Base {

public:
	Enemy_Giant(Scene* scene, Player* player);
	~Enemy_Giant();

	bool Update();

	void AI_Chase(Player*player, double dt);
	void AI_Attack(Player* player, double dt);
	void Render();
	void OnCollisionHit(GameObject * other);

	// By default, giant is inactive until you call this function, then it will start attacking
	void ActivateGiant() { b_IsGiantActivated = true; }

private:
	SpriteAnimation * m_AnimEnemyWarrior;
	Animation* m_Anim;

	float m_WeaponRange; // Only start attacking when within this range
	Vector3 m_WeaponOffset;
	float m_WeaponOffsetLength;
	Color m_WeaponColor;

	bool b_IsLoopResetting = false;
	bool b_IsGiantActivated = false;


};
