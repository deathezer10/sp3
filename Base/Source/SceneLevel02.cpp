#include "SceneLevel02.h"
#include "GL\glew.h"
#include "Application.h"
#include <sstream>
#include "Player.h"

#include "ParticleEmitter.h"
#include "SpriteAnimation.h"
#include "MeshBuilder.h"
#include "LoadTGA.h"

#include "TerrainLoader.h"
#include "ColliderFactory.h"

#include "Brick.h"
#include "Portal.h"
#include "Enemy_Ninja.h"
#include "Enemy_Warrior.h"
#include "Enemy_Golem.h"
#include "SceneManager.h"
#include "SceneGameOver.h"
#include "Enemy_Archer.h"
#include "Ally_Warrior.h"
#include "CapturePoint.h"
#include "Health_Well.h"


SceneLevel02::SceneLevel02() :SceneBase(SCENE_LEVEL02)
{
}

SceneLevel02::~SceneLevel02()
{
}

void SceneLevel02::Init()
{
	SceneBase::Init();
	//Calculating aspect ratio
	m_worldHeight = 100.f;
	m_worldWidth = m_worldHeight * (float)Application::windowWidth() / Application::windowHeight();

	TerrainLoader::LoadTerrain("level02.txt", &goManager);

	// Player
	Player* player = new Player(this);
	player->active = true;
	player->pos.Set(m_worldWidth_default * 0.f, m_worldHeight_default * .5f, 0.f);
	goManager.CreateObject(player);
	playerManager.SetPlayer(player);

	// Portal
	Portal* portal1 = new Portal(this, player, Portal::E_PORTAL_TYPE::WARRIOR_AND_ARCHER);
	portal1->active = true;
	portal1->pos.Set(m_worldWidth_default * 0.75f, m_worldHeight_default * .8f, 0.f);
	goManager.CreateObject(portal1);

	Portal* portal2 = new Portal(this, player, Portal::E_PORTAL_TYPE::NINJA_AND_ARCHER);
	portal2->active = true;
	portal2->pos.Set(260.f, 79, 0.f);
	goManager.CreateObject(portal2);

	Portal* portal3 = new Portal(this, player, Portal::E_PORTAL_TYPE::WARRIOR_AND_ARCHER);
	portal3->active = true;
	portal3->pos.Set(432.f, 76.f, 0.f);
	goManager.CreateObject(portal3);

	Portal* portal4 = new Portal(this, player, Portal::E_PORTAL_TYPE::ARCHER);
	portal4->active = true;
	portal4->pos.Set(638.5f, 62.f, 0.f);
	goManager.CreateObject(portal4);

	Portal* portal5 = new Portal(this, player, Portal::E_PORTAL_TYPE::WARRIOR);
	portal5->active = true;
	portal5->pos.Set(638.5f, 34.f, 0.f);
	goManager.CreateObject(portal5);

	Health_Well* HealthWell1 = new Health_Well(this);
	HealthWell1->active = true;
	HealthWell1->pos.Set(182.f, 78.5f, 0.f);
	goManager.CreateObject(HealthWell1);

	Health_Well* HealthWell2 = new Health_Well(this);
	HealthWell2->active = true;
	HealthWell2->pos.Set(520.f, 61.5f, 0.f);
	goManager.CreateObject(HealthWell2);

	Health_Well* HealthWell3 = new Health_Well(this);
	HealthWell3->active = true;
	HealthWell3->pos.Set(1135.f, 92.5f, 0.f);
	goManager.CreateObject(HealthWell3);

	Enemy_Golem* Golem1 = new Enemy_Golem(this, player);
	Golem1->active = true;
	Golem1->pos.Set(830.f, 42.f, 0.f);
	goManager.CreateObject(Golem1);

	m_Golem = new Enemy_Golem(this, player);
	m_Golem->active = false;
	m_Golem->pos.Set(1100.f, 90.f, 0.f);
	goManager.CreateObject(m_Golem);

	m_Gate1 = new Brick(this);
	m_Gate1->active = true;
	m_Gate1->type = GEO_TERRAIN_BRICK;
	m_Gate1->mass = 0;
	m_Gate1->dir.Set(0, 1, 0);
	m_Gate1->pos.Set(443.f, 102.f, 0);
	m_Gate1->scale.x = 66;
	goManager.CreateObject(m_Gate1);

	m_Gate2 = new Brick(this);
	m_Gate2->active = false;
	m_Gate2->b_EnablePhysics = false;
	m_Gate2->type = GEO_TERRAIN_BRICK;
	m_Gate2->mass = 0;
	m_Gate2->dir.Set(0, 1, 0);
	m_Gate2->pos.Set(950.f, 120.f, 0);
	m_Gate2->scale.x = 66;
	goManager.CreateObject(m_Gate2);

	m_Gate3 = new Brick(this);
	m_Gate3->active = false;
	m_Gate3->type = GEO_TERRAIN_BRICK;
	m_Gate3->mass = 0;
	m_Gate3->dir.Set(0, 1, 0);
	m_Gate3->pos.Set(1110.f, 120.f, 0);
	m_Gate3->scale.x = 66;
	goManager.CreateObject(m_Gate3);

	m_Point1 = new CapturePoint(this);
	m_Point1->active = true;
	m_Point1->pos.Set(350.f, 38.f, 0);
	goManager.CreateObject(m_Point1);

	m_Point2 = new CapturePoint(this);
	m_Point2->active = true;
	m_Point2->pos.Set(1030.f, 98.f, 0);
	goManager.CreateObject(m_Point2);

	m_Core = new Portal(this, player, Portal::E_PORTAL_TYPE::NINJA_AND_ARCHER, true);
	m_Core->active = true;
	m_Core->SetHealth(250);
	m_Core->m_PActivateThreshold = 250;
	m_Core->pos.Set(1315.f, 135.f, 0.f);
	goManager.CreateObject(m_Core);
}

void SceneLevel02::Update(double dt)
{

	//pause menu part
	//update pausemenu function
	pauseManager.UpdatePauseMenu(m_dt);
	//Test->update(playerManager.GetPlayer(),dt);
	//Test->AI_Chase(playerManager.GetPlayer(), dt);
	//Test->AI_Jump();
	//check if game is paused, freeze all stuff that is updating
	if (pauseManager.isPaused()) {
		return;
	}

	SceneBase::Update(dt);

	if (Application::IsKeyPressedAndReleased('C')) {
		ShowDebugInfo = !ShowDebugInfo;
	}

	if (m_Gate1 != nullptr && m_Point1->GetCaptureProgress() >= 100) {
		goManager.DestroyObject(m_Gate1);
		m_Gate1 = nullptr;
	}

	if (m_Gate2 != nullptr && m_Gate3 != nullptr && playerManager.GetPlayer()->pos.x > 960.f)
	{
		m_Gate2->active = true;
		m_Gate2->b_EnablePhysics = true;
		m_Gate3->active = true;
		m_Golem->active = true;
	}
	if (m_Gate2 != nullptr && m_Gate3 != nullptr && m_Point2->GetCaptureProgress() >= 100) {
		goManager.DestroyObject(m_Gate2);
		goManager.DestroyObject(m_Gate3);
		m_Gate2 = nullptr;
		m_Gate3 = nullptr;
	}

	if (m_Core->GetCurrentHealth() <= 0) {
		playerManager.IncreaseScore(100);
		SceneManager::getInstance()->changeScene(new SceneGameover("Level 02 Victory! You destroyed the Castle's Core!", SceneGameover::MENU_VICTORY, SceneBase::SCENE_LEVEL02, playerManager.GetCurrentScore()));
		return;
	}

	playerManager.Update();
	goManager.DequeueObjects();
	goManager.UpdateObjects(m_dt);

	m_BGScroller.Update();

}

void SceneLevel02::Render()
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	float m_newWorldHeight = 100 * (float)Application::windowHeight() / Application::windowDefaultHeight()  * camera.GetZoomMultiplier();
	float m_newWorldWidth = m_newWorldHeight * (float)Application::windowWidth() / Application::windowHeight();

	float offset = -((m_newWorldWidth - m_worldWidth) / 2);

	// Projection matrix : Orthographic Projection
	Mtx44 projection;
	projection.SetToOrtho(offset, m_newWorldWidth + offset, 0, m_newWorldHeight, -100, 100);
	projectionStack.LoadMatrix(projection);


	// Camera matrix
	viewStack.LoadIdentity();
	viewStack.LookAt(
		camera.position.x, camera.position.y, camera.position.z,
		camera.target.x, camera.target.y, camera.target.z,
		camera.up.x, camera.up.y, camera.up.z
	);
	// Model matrix : an identity matrix (model will be at the origin)
	modelStack.LoadIdentity();

	// Render background
	m_BGScroller.RenderBackground();


	goManager.RenderObjects();
	textManager.dequeueMesh();

	playerManager.m_FurEmitter->Render();

	// Render Player UI
	textManager.RenderPlayerHud();

	if (pauseManager.isPaused()) {
		pauseManager.RenderPauseMenu();
		return;
	}

	// HUDs
	std::stringstream text;
	text << "FPS: " << Application::GetFPS();
	textManager.renderTextOnScreen(UIManager::Text{ text.str(), Color(1,1,1), UIManager::ANCHOR_TOP_RIGHT });

	textManager.dequeueText();
	textManager.reset();
}

void SceneLevel02::Exit()
{
	SceneBase::Exit();
}


