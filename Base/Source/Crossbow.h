#pragma once

#include "GameObject.h"
#include "WeaponInfo.h"

class Player;

class Crossbow : public WeaponInfo, public GameObject
{
public:
	Crossbow(Scene* scene, Player* player);
	~Crossbow() {};

	bool Update();

	void Render();

	bool checkRange(GameObject* obj);

	void checkTimer();
	
	bool useUltimate();

	int getAttack();

	float getUltimateDuration();

private:
	Player* m_player;
	Vector3 m_offset;
};