#include "Health_Well.h"

#include "SceneBase.h"

#include "UI_Bar.h"
#include "ColliderFactory.h"
#include "PlayerManager.h"
#include "AI_Base.h"

Health_Well::Health_Well(Scene * scene) : GameObject(scene,Scene::GEO_HEALTHWELL)
{
	m_healthValue = 10;

	b_EnablePhysics = true;
	b_EnableLighting = false;
	b_EnableTrigger = true;

	active = true;
	scale.Set(6, 5, 1);
	this->collider = ColliderFactory::CreateOBBCollider(this, Vector3(40.f, 8.f, 4.f));

	m_OffsetPos.y -= 2.5f;
}

Health_Well::~Health_Well()
{
}

void Health_Well::Render()
{

	if (m_CurrentHealthCharge<100)
	{
		m_CurrentHealthCharge += 5 * m_scene->m_dt;
	}



	Vector3 barPos = pos;
	barPos.y += collider.bboxSize.y * 0.75f;

	Mesh** meshList = m_scene->meshList;

	UI_BAR::RenderBar(m_scene, meshList[Scene::GEO_HP_BACKGROUND], meshList[Scene::GEO_POW_FOREGROUND], meshList[Scene::GEO_HP_FRAME], barPos, Vector3(10.0f, 1.0f, 1.0f), m_CurrentHealthCharge / 100);

	GameObject::Render();

}

void Health_Well::OnCollisionHit(GameObject * other)
{
	AI_Base* AI = dynamic_cast<AI_Base*>(other);
	if (m_CurrentHealthCharge>0)
	{
		if (other->type == Scene::GEO_PLAYER && m_scene->playerManager.GetHealth() != 100)
		{
			m_scene->playerManager.IncreaseHealth(m_healthValue*m_scene->m_dt);
			m_CurrentHealthCharge -= 10 * m_scene->m_dt;
		}
		if (AI != nullptr&&AI->GetCurrentHealth() != AI->GetDefaultHealth())
		{
			AI->SetHealth(AI->GetCurrentHealth() + 10.f*m_scene->m_dt);
			m_CurrentHealthCharge -= 10 * m_scene->m_dt;

		}
	}
    
}


bool Health_Well::Update()
{
	return true;
}




