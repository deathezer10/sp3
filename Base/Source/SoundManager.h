#pragma once
#pragma comment(lib, "irrKlang.lib") // link with irrKlang.dll


#include "irrKlang.h"
#include <map>
#include <string>
#include "Vector3.h"


using std::string;


class SoundManager {

public:
	static SoundManager& getInstance() {
		static SoundManager instance;
		return instance;
	}

	// Maps the filePath onto the given soundName to allow playback using Play2DSound()
	void AddSound(string soundName, string filePath);

	// Stops all currently playing sounds
	void StopAllSound();
	
	// Param:
	// soundName: Name of the Sound added by AddSound()
	// loop: Repeats the sound after playback finishes
	// ignorePlayback: If TRUE, plays sound even if it is currently playing
	// track: If TRUE, returns the pointer to the sound file
	irrklang::ISound* Play2DSound(string soundName, bool loop = false, bool ignorePlayback = false, bool track = false);

	// Param:
	// TODO:
	void Play3DSound(string soundName, const Vector3 &position, bool loop = false, bool ignorePlayback = false);

private:
	SoundManager();
	~SoundManager();

	irrklang::ISoundEngine* m_SoundEngine;

	// Key: Name of Sound
	// Data: Location of Soundfile
	std::map<string, string> m_SoundMap;

};