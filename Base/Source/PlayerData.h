#pragma once
#include <fstream>
#include <sstream>

struct PlayerData
{
	bool b_isLevel02Unlocked = false;
	bool b_isLevel03Unlocked = false;

	int m_currency = 0;

	bool b_isWeapon2Unlocked = true;
	bool b_isWeapon3Unlocked = false;

	int m_amountofUnit1 = 0;
	int m_amountofUnit2 = 0;
};

class PlayerDataManager
{
private:
	static PlayerDataManager* m_instance;

	std::string FileName = "Data1.txt";
	

public:
	static PlayerDataManager* getInstance();

	~PlayerDataManager() {}; // Call Exit() to destroy Singleton

	void Exit();

	void SetFile(int FileIndex);

	void SaveData();
	void LoadData();

	PlayerData m_data;

};
