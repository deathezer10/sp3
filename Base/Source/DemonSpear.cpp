#include "DemonSpear.h"

#include "SceneBase.h"
#include "Player.h"

#include "UI_Bar.h"
#include "ColliderFactory.h"


DemonSpear::DemonSpear(Scene * scene, Vector3 startPos) : GameObject(scene, Scene::GEO_ENEMYDEMON_FIERYSPEAR) {
	b_EnableLighting = false;
	b_EnablePhysics = true;
	b_EnableTrigger = true;
	b_EnableGravity = false;

	active = true;

	color.Set(1, 1, 1);

	scale.Set(10, 4, 1);

	bounciness = 0;

	collider = ColliderFactory::CreateOBBCollider(this, scale);

	MAX_VELOCITY.y = 0;

	m_InitialPos = startPos;
	m_InitialPos.y += 35;

	pos = m_InitialPos;
}

bool DemonSpear::Update() {

	dir.Set(0, -1, 0);

	if (m_IndicatorAlpha < 1) {
		m_IndicatorAlpha += m_scene->m_dt / 4;
	}
	else {
		b_IsLaunched = true;
		vel.y = -40;
	}

	if (pos.y < -100)
		m_scene->goManager.DestroyObjectQueue(this);

	return true;
}

void DemonSpear::Render() {

	if (b_IsLaunched)
		GameObject::Render();
	else {

		glDisable(GL_DEPTH_TEST);
		m_scene->modelStack.PushMatrix();
		m_scene->modelStack.Translate(pos.x, m_InitialPos.y, pos.z);
		m_scene->modelStack.Scale(scale.y, 500, 1);
		m_scene->meshList[Scene::GEO_ENEMYDEMON_FIERYSPEAR_INDICATOR]->alpha = m_IndicatorAlpha;
		m_scene->RenderMesh(m_scene->meshList[Scene::GEO_ENEMYDEMON_FIERYSPEAR_INDICATOR], false);
		m_scene->modelStack.PopMatrix();
		glEnable(GL_DEPTH_TEST);

	}

}

void DemonSpear::OnCollisionHit(GameObject * other) {

	if (other->type == Scene::GEO_PLAYER) {
		m_scene->playerManager.ReduceHealth(75);
		m_scene->goManager.DestroyObjectQueue(this);
	}

}
