#pragma once


#include "Vector3.h"
#include "Collider.h"


class GameObject;


class ColliderFactory {

public:
	static Collider CreateCircleCollider(GameObject* obj, float radius);
	static Collider CreateOBBCollider(GameObject* obj, Vector3 bboxSize);

private:
	ColliderFactory();
	~ColliderFactory() {};

};