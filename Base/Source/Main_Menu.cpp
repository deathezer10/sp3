#include "Main_Menu.h"
#include "GL\glew.h"
#include "Application.h"
#include <sstream>
#include "Player.h"

#include "MeshBuilder.h"
#include "LoadTGA.h"

#include <GLFW/glfw3.h>

#include "ColliderFactory.h"
#include "Brick.h"
#include "PlayerData.h"
#include "SceneManager.h"
#include "SceneLevel01.h"
#include "SceneLevel02.h"
#include "SceneLevel03.h"
#include "SceneEditor.h"


Main_Menu::Main_Menu() :SceneBase(SCENE_MAINMENU)
{
}

Main_Menu::~Main_Menu()
{
}

void Main_Menu::Init()
{
	SceneBase::Init();
	//Calculating aspect ratio
	m_worldHeight = 100.f;
	m_worldWidth = m_worldHeight * (float)Application::windowWidth() / Application::windowHeight();

	meshList[GEO_MAINMENU] = MeshBuilder::GenerateQuad("Main_menu_bg", Color(1.f, 1.f, 1.f));
	meshList[GEO_MAINMENU]->textureArray[0] = LoadTGA("Image//MainMenu//mm.tga");

	meshList[GEO_POINTER] = MeshBuilder::GenerateQuad("Pointer", Color(1.f, 1.f, 1.f), 0.25f);
	meshList[GEO_POINTER]->textureArray[0] = LoadTGA("Image//MainMenu//Pointer.tga");

	meshList[GEO_LEVEL] = MeshBuilder::GenerateQuad("LevelMenu", Color(1.f, 1.f, 1.f));
	meshList[GEO_LEVEL]->textureArray[0] = LoadTGA("Image//MainMenu//LevelSelect.tga");

	//meshList[GEO_SHOP] = MeshBuilder::GenerateQuad("ShopMenu", Color(1.f, 1.f, 1.f));
	//meshList[GEO_SHOP]->textureArray[0] = LoadTGA("Image//MainMenu//shop.tga");

	meshList[GEO_TICK] = MeshBuilder::GenerateQuad("GreenTick", Color(1.f, 1.f, 1.f));
	meshList[GEO_TICK]->textureArray[0] = LoadTGA("Image//MainMenu//green_tick.tga");

	meshList[GEO_LOCK] = MeshBuilder::GenerateQuad("Lock", Color(1.f, 1.f, 1.f));
	meshList[GEO_LOCK]->textureArray[0] = LoadTGA("Image//MainMenu//lock.tga");


	camera.Init(Vector3(0, 0, 0), Vector3(0, 0, -1), Vector3(0, 1, 0));

	Math::InitRNG();

	//Init values
	{
		mainChoice = 4;
		//shopChoice = 5;
		levelChoice = 4;

		Dkeypressed = false;
		Ukeypressed = false;
		Lkeypressed = false;
		Rkeypressed = false;
		EnterKeypressed = true;

		mainMenu = true;
		levelMenu = false;
		//shopMenu = false;
		Credits = false;
		instructions = false;

		//translateShopMenu = false;
		translateLevelMenu = false;
		translateCreditsMenu = false;
		translateInstruction = false;
	}

	PlayerDataManager::getInstance()->LoadData();
}

void Main_Menu::Update(double dt)
{
	SceneBase::Update(dt);

	GLFWwindow * window = glfwGetCurrentContext();

	//Checking up down keypresses
	{
		//Check up down keypress at main menu
		if (/*mainMenu && !shopMenu && !levelMenu && !Credits && !translateShopMenu &&*/ !translateLevelMenu && !translateCreditsMenu && !levelMenu)
		{
			if (glfwGetKey(window, GLFW_KEY_DOWN) && !Dkeypressed)
			{
				mainChoice--;
				if (mainChoice < 1) // Prevent mainChoice from going out of bounds
					mainChoice = 1;
				Dkeypressed = true;
			}

			else if (!glfwGetKey(window, GLFW_KEY_DOWN) && Dkeypressed)
			{
				Dkeypressed = false;
			}

			if (glfwGetKey(window, GLFW_KEY_UP) && !Ukeypressed)
			{
				mainChoice++;
				if (mainChoice > 4)
					mainChoice = 4; // Prevent mainChoice from going out of bounds
				Ukeypressed = true;
			}

			else if (!glfwGetKey(window, GLFW_KEY_UP) && Ukeypressed)
			{
				Ukeypressed = false;
			}
		}

		//Check up down keypress at shop menu
		//else if (shopMenu && !translateShopMenu)
		//{

		//	if (glfwGetKey(window, GLFW_KEY_DOWN) && !Dkeypressed)
		//	{
		//		shopChoice = 5;
		//		Dkeypressed = true;
		//	}
		//	else if (!glfwGetKey(window, GLFW_KEY_DOWN) && Dkeypressed)
		//	{
		//		Dkeypressed = false;
		//	}

		//	if (glfwGetKey(window, GLFW_KEY_UP) && !Ukeypressed)
		//	{
		//		shopChoice = 1;
		//		Dkeypressed = true;
		//	}
		//	else if (!glfwGetKey(window, GLFW_KEY_UP) && Ukeypressed)
		//	{
		//		Ukeypressed = false;
		//	}
		//}

		//Check up down keypress at level menu
		else if (levelMenu && !translateLevelMenu)
		{
			if (glfwGetKey(window, GLFW_KEY_DOWN) && !Dkeypressed)
			{
				levelChoice = 5;
				Dkeypressed = true;
			}

			else if (!glfwGetKey(window, GLFW_KEY_DOWN) && Dkeypressed)
			{
				Dkeypressed = false;
			}

			if (glfwGetKey(window, GLFW_KEY_UP) && !Ukeypressed)
			{
				levelChoice = 1;
				Ukeypressed = true;
			}

			else if (!glfwGetKey(window, GLFW_KEY_UP) && Ukeypressed)
			{
				Ukeypressed = false;
			}
		}

	}

	//Checking Left Right keypresses 
	{
		//Checking Left Right during shop menu
		//if (shopMenu && !translateShopMenu)
		//{
		//	if (glfwGetKey(window, GLFW_KEY_LEFT) && !Lkeypressed)
		//	{
		//		shopChoice++;
		//		if (shopChoice > 5) // Prevent mainChoice from going out of bounds
		//			shopChoice = 5;
		//		Lkeypressed = true;
		//	}
		//	else if (!glfwGetKey(window, GLFW_KEY_LEFT) && Lkeypressed)
		//	{
		//		Lkeypressed = false;
		//	}

		//	if (glfwGetKey(window, GLFW_KEY_RIGHT) && !Rkeypressed)
		//	{
		//		shopChoice--;
		//		if (shopChoice < 1)
		//			shopChoice = 1; // Prevent mainChoice from going out of bounds
		//		Rkeypressed = true;
		//	}
		//	else if (!glfwGetKey(window, GLFW_KEY_RIGHT) && Rkeypressed)
		//	{
		//		Rkeypressed = false;
		//	}
		//}

		//Checking Left Right During level menu
		/*else*/ if (levelMenu && !translateLevelMenu)
		{
			if (glfwGetKey(window, GLFW_KEY_LEFT) && !Lkeypressed)
			{
				levelChoice++;
				if (levelChoice > 5) // Prevent mainChoice from going out of bounds
					levelChoice = 5;
				Lkeypressed = true;
			}
			else if (!glfwGetKey(window, GLFW_KEY_LEFT) && Lkeypressed)
			{
				Lkeypressed = false;
			}

			if (glfwGetKey(window, GLFW_KEY_RIGHT) && !Rkeypressed)
			{
				levelChoice--;
				if (levelChoice < 1)
					levelChoice = 1; // Prevent mainChoice from going out of bounds
				Rkeypressed = true;
			}
			else if (!glfwGetKey(window, GLFW_KEY_RIGHT) && Rkeypressed)
			{
				Rkeypressed = false;
			}
		}
	}

	// Checking 'Enter' keypress
	{
		if (glfwGetKey(window, GLFW_KEY_ENTER) && !EnterKeypressed)
		{
			EnterKeypressed = true;
			//Checking of choice at main menu
			if (mainMenu)
			{
				switch (mainChoice)
				{
				//case 5:	//Go to level select
				//	levelMenu = true;
				//	translateLevelMenu = true;
				//	mainMenu = false;
				//	break;
				//case 4:	//Go to shop
				//	shopMenu = true;
				//	translateShopMenu = true;
				//	mainMenu = false;
				//	break;
				//case 3:	//Go to controls
				//	instructions = true;
				//	translateInstruction = true;
				//	mainMenu = false;
				//	break;
				//case 2:	//Go to credits
				//	Credits = true;
				//	translateCreditsMenu = true;
				//	mainMenu = false;
				//	break;

				case 4:
					levelMenu = true;
					translateLevelMenu = true;
					mainMenu = false;
					break;
					case 3:	//Go to controls
						instructions = true;
						translateInstruction = true;
						mainMenu = false;
						break;
					case 2:	//Go to credits
						Credits = true;
						translateCreditsMenu = true;
						mainMenu = false;
						break;
				case 1:	//Exit
					glfwSetWindowShouldClose(glfwGetCurrentContext(), true);
					break;

				default:
					break;
				}
			}

			//Checking of choices at shop menu
			//else if (shopMenu && !translateShopMenu)
			//{
			//	switch (shopChoice)
			//	{
			//	case 5:	//buying of spear
			//		if (!PlayerDataManager::getInstance()->m_data.b_isWeapon3Unlocked && PlayerDataManager::getInstance()->m_data.m_currency >= 300)
			//		{
			//			PlayerDataManager::getInstance()->m_data.b_isWeapon3Unlocked = true;
			//			PlayerDataManager::getInstance()->m_data.m_currency -= 300;
			//			PlayerDataManager::getInstance()->SaveData();

			//		}
			//		break;
			//	case 4:
			//		if (!PlayerDataManager::getInstance()->m_data.b_isWeapon2Unlocked && PlayerDataManager::getInstance()->m_data.m_currency >= 300)
			//		{
			//			PlayerDataManager::getInstance()->m_data.b_isWeapon2Unlocked = true;
			//			PlayerDataManager::getInstance()->m_data.m_currency -= 300;
			//			PlayerDataManager::getInstance()->SaveData();

			//		}
			//		break;
			//	case 3:
			//		if (PlayerDataManager::getInstance()->m_data.m_currency >= 100)
			//		{
			//			PlayerDataManager::getInstance()->m_data.m_currency -= 100;
			//			PlayerDataManager::getInstance()->m_data.m_amountofUnit1++;
			//		}
			//		break;
			//	case 2:
			//		if (PlayerDataManager::getInstance()->m_data.m_currency >= 150)
			//		{
			//			PlayerDataManager::getInstance()->m_data.m_currency -= 150;
			//			PlayerDataManager::getInstance()->m_data.m_amountofUnit2++;

			//		}
			//		break;
			//	case 1:
			//		shopMenu = false;
			//		mainMenu = true;
			//		translateShopMenu = true;
			//		shopChoice = 6;
			//		break;
			//	default:
			//		break;
			//	}
			//}

			//Checking of choices at level menu
			else if (levelMenu && !translateLevelMenu)
			{
				switch (levelChoice)
				{
				case 5:
					SceneManager::getInstance()->changeScene(new SceneEditor);
					return;
				case 4:
					SceneManager::getInstance()->changeScene(new SceneLevel01);
					return;
				case 3:
					if (PlayerDataManager::getInstance()->m_data.b_isLevel02Unlocked) {
						SceneManager::getInstance()->changeScene(new SceneLevel02);
						return;
					}
					break;
				case 2:
					if (PlayerDataManager::getInstance()->m_data.b_isLevel03Unlocked) {
						SceneManager::getInstance()->changeScene(new SceneLevel03);
						return;
					}
					break;
				case 1:
					levelMenu = false;
					mainMenu = true;
					translateLevelMenu = true;
					levelChoice = 4;
					break;
				default:
					break;
				}
			}

			//Checking if 'Enter' is pressed during credits, to cut credits early
			else if (Credits)
			{
				camera.position.Set(0, 0, 0);
				camera.target.Set(0, 0, -1);
				mainMenu = true;
				Credits = false;
				translateCreditsMenu = false;
			}

			//Checking if 'Enter' is pressed during credits, to cut credits early
			else if (instructions)
			{
				camera.position.Set(0, 0, 0);
				camera.target.Set(0, 0, -1);
				mainMenu = true;
				instructions = false;
				translateInstruction = false;
			}
		}

		else if (!Application::IsKeyPressed(VK_RETURN) && EnterKeypressed)
		{
			EnterKeypressed = false;
		}
	}

	//Translating level menu and shop menu up and down
	{
		//Shop Menu
		//{
		//	if (shopMenu && camera.position.x > -m_worldWidth && translateShopMenu)
		//	{
		//		camera.position.x -= 100.f * m_dt;
		//		camera.target.x -= 100.f * m_dt;
		//	}
		//	else if (shopMenu && camera.position.x <= -m_worldWidth && translateShopMenu)
		//	{
		//		translateShopMenu = false;
		//	}
		//	if (!shopMenu && camera.position.x < 0 && translateShopMenu)
		//	{
		//		camera.position.x += 100.f* m_dt;
		//		camera.target.x += 100.f * m_dt;
		//	}
		//	else if (!shopMenu && camera.position.x >= 0 && translateShopMenu)
		//	{
		//		translateShopMenu = false;
		//	}
		//}

		//Level Menu

		{
			if (levelMenu && camera.position.x < m_worldWidth && translateLevelMenu)
			{
				camera.position.x += 100.f * m_dt;
				camera.target.x += 100.f * m_dt;
			}
			else if (levelMenu && camera.position.x >= m_worldWidth && translateLevelMenu)
			{
				translateLevelMenu = false;
			}
			if (!levelMenu && camera.position.x > 0 && translateLevelMenu)
			{
				camera.position.x -= 100.f * m_dt;
				camera.target.x -= 100.f * m_dt;
			}
			else if (!levelMenu && camera.position.x <= 0 && translateLevelMenu)
			{
				translateLevelMenu = false;
			}
		}

		//Credits Menu
		{
			if (Credits && camera.position.y > -m_worldHeight && translateCreditsMenu)
			{
				camera.position.y -= 70.f * m_dt;
				camera.target.y -= 70.f * m_dt;
			}
			else if (Credits && camera.position.y <= -m_worldHeight && translateCreditsMenu)
			{
				translateCreditsMenu = false;
			}
			if (!Credits && camera.position.y < 0 && translateCreditsMenu)
			{
				camera.position.y += 70.f * m_dt;
				camera.target.y += 70.f * m_dt;
			}
			else if (!Credits && camera.position.y >= 0 && translateCreditsMenu)
			{
				translateCreditsMenu = false;
			}
		}

		//Instructions
		{
			if (instructions && camera.position.y < m_worldHeight && translateInstruction)
			{
				camera.position.y += 100.f * m_dt;
				camera.target.y += 100.f * m_dt;
			}
			else if (instructions && camera.position.y >= m_worldWidth && translateInstruction)
			{
				translateInstruction = false;
			}
			if (!instructions && camera.position.y > 0 && translateInstruction)
			{
				camera.position.y -= 100.f * m_dt;
				camera.target.y -= 100.f * m_dt;
			}
			else if (!instructions && camera.position.y <= 0 && translateInstruction)
			{
				translateInstruction = false;
			}
		}
	}
}

void Main_Menu::Render()
{
	// Default things
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Projection matrix : Orthographic Projection
		Mtx44 projection;
		projection.SetToOrtho(0, m_worldWidth, 0, m_worldHeight, -100, 100);
		projectionStack.LoadMatrix(projection);

		// Camera matrix
		viewStack.LoadIdentity();
		viewStack.LookAt(
			camera.position.x, camera.position.y, camera.position.z,
			camera.target.x, camera.target.y, camera.target.z,
			camera.up.x, camera.up.y, camera.up.z
		);

		// Model matrix : an identity matrix (model will be at the origin)
		modelStack.LoadIdentity();
	}
	textManager.dequeueMesh();

	//Render of menu screens
	{
		// Main menu
		modelStack.PushMatrix();
		modelStack.Translate(m_worldWidth * 0.5f, m_worldHeight * 0.5f, -1.0f);
		modelStack.Scale(m_worldWidth, m_worldHeight, 50);
		RenderMesh(meshList[GEO_MAINMENU], false);
		modelStack.PopMatrix();

		// Level menu
		modelStack.PushMatrix();
		modelStack.Translate(m_worldWidth * 1.5f, m_worldHeight * 0.5f, -1.0f);
		modelStack.Scale(m_worldWidth, m_worldHeight, 50);
		RenderMesh(meshList[GEO_LEVEL], false);
		modelStack.PopMatrix();

		// Shop menu
		//modelStack.PushMatrix();
		//modelStack.Translate(-m_worldWidth * 0.5f, m_worldHeight * 0.5f, -1.0f);
		//modelStack.Scale(m_worldWidth, m_worldHeight, 50);
		//RenderMesh(meshList[GEO_SHOP], false);
		//modelStack.PopMatrix();

		// Controls
		modelStack.PushMatrix();
		modelStack.Translate(m_worldWidth * 0.5f, m_worldHeight * 1.5f, -1.0f);
		modelStack.Scale(m_worldWidth, m_worldHeight, 50);
		RenderMesh(meshList[GEO_CONTROLS], false);
		modelStack.PopMatrix();
	}

	//Rendering of main menu choices 
	switch (mainChoice)
	{
	//case 5:
	//	modelStack.PushMatrix();
	//	modelStack.Translate(m_worldWidth* 0.33f, m_worldHeight * 0.51f, 1.f);
	//	modelStack.Scale(35.f, 35.f, 1.f);
	//	RenderMesh(meshList[GEO_POINTER], false);
	//	modelStack.PopMatrix();
	//	break;
	//case 4:
	//	modelStack.PushMatrix();
	//	modelStack.Translate(m_worldWidth* 0.33f, m_worldHeight * 0.40f, 1.f);
	//	modelStack.Scale(35.f, 35.f, 1.f);
	//	RenderMesh(meshList[GEO_POINTER], false);
	//	modelStack.PopMatrix();
	//	break;
	//case 3:
	//	modelStack.PushMatrix();
	//	modelStack.Translate(m_worldWidth* 0.33f, m_worldHeight * 0.29f, 1.f);
	//	modelStack.Scale(35.f, 35.f, 1.f);
	//	RenderMesh(meshList[GEO_POINTER], false);
	//	modelStack.PopMatrix();
	//	break;
	//case 2:
	//	modelStack.PushMatrix();
	//	modelStack.Translate(m_worldWidth* 0.33f, m_worldHeight * 0.18f, 1.f);
	//	modelStack.Scale(35.f, 35.f, 1.f);
	//	RenderMesh(meshList[GEO_POINTER], false);
	//	modelStack.PopMatrix();
	//	break;
	//case 1:
	//	modelStack.PushMatrix();
	//	modelStack.Translate(m_worldWidth* 0.33f, m_worldHeight * 0.07f, 1.f);
	//	modelStack.Scale(35.f, 35.f, 1.f);
	//	RenderMesh(meshList[GEO_POINTER], false);
	//	modelStack.PopMatrix();
	//	break;
	case 4:
		modelStack.PushMatrix();
		modelStack.Translate(m_worldWidth* 0.33f, m_worldHeight * 0.51f, 1.f);
		modelStack.Scale(35.f, 35.f, 1.f);
		RenderMesh(meshList[GEO_POINTER], false);
		modelStack.PopMatrix();
		break;
	case 3:
		modelStack.PushMatrix();
		modelStack.Translate(m_worldWidth* 0.33f, m_worldHeight * 0.40f, 1.f);
		modelStack.Scale(35.f, 35.f, 1.f);
		RenderMesh(meshList[GEO_POINTER], false);
		modelStack.PopMatrix();
		break;
	case 2:
		modelStack.PushMatrix();
		modelStack.Translate(m_worldWidth* 0.33f, m_worldHeight * 0.29f, 1.f);
		modelStack.Scale(35.f, 35.f, 1.f);
		RenderMesh(meshList[GEO_POINTER], false);
		modelStack.PopMatrix();
		break;
	case 1:
		modelStack.PushMatrix();
		modelStack.Translate(m_worldWidth* 0.33f, m_worldHeight * 0.18f, 1.f);
		modelStack.Scale(35.f, 35.f, 1.f);
		RenderMesh(meshList[GEO_POINTER], false);
		modelStack.PopMatrix();
		break;
	default:
		break;
	}

	//Rendering of shop menu choices after translateShopMenu is done (false)
	//if (shopMenu && !translateShopMenu)
	//{
	//	switch (shopChoice)
	//	{
	//	case 5:
	//		modelStack.PushMatrix();
	//		modelStack.Translate(m_worldWidth* -0.97f, m_worldHeight * 0.63f, 1.f);
	//		modelStack.Scale(33.f, 33.f, 1.f);
	//		RenderMesh(meshList[GEO_POINTER], false);
	//		modelStack.PopMatrix();
	//		break;
	//	case 4:
	//		modelStack.PushMatrix();
	//		modelStack.Translate(m_worldWidth* -0.72f, m_worldHeight * 0.63f, 1.f);
	//		modelStack.Scale(33.f, 33.f, 1.f);
	//		RenderMesh(meshList[GEO_POINTER], false);
	//		modelStack.PopMatrix();
	//		break;
	//	case 3:
	//		modelStack.PushMatrix();
	//		modelStack.Translate(m_worldWidth* -0.47f, m_worldHeight * 0.63f, 1.f);
	//		modelStack.Scale(33.f, 33.f, 1.f);
	//		RenderMesh(meshList[GEO_POINTER], false);
	//		modelStack.PopMatrix();
	//		break;
	//	case 2:
	//		modelStack.PushMatrix();
	//		modelStack.Translate(m_worldWidth* -0.22f, m_worldHeight * 0.63f, 1.f);
	//		modelStack.Scale(33.f, 33.f, 1.f);
	//		RenderMesh(meshList[GEO_POINTER], false);
	//		modelStack.PopMatrix();
	//		break;
	//	case 1:
	//		modelStack.PushMatrix();
	//		modelStack.Translate(m_worldWidth* -0.33f, m_worldHeight *  0.9f, 1.f);
	//		modelStack.Scale(33.f, 33.f, 1.f);
	//		RenderMesh(meshList[GEO_POINTER], false);
	//		modelStack.PopMatrix();
	//		break;
	//	default:
	//		break;
	//	}
	//}

	//Rendering of level menu choices after translateLevelMenu is done (false)
	if (levelMenu && !translateLevelMenu)
	{
		switch (levelChoice)
		{
		case 5:
			modelStack.PushMatrix();
			modelStack.Translate(m_worldWidth* 1.22f, m_worldHeight * 0.63f, 1.f);
			modelStack.Rotate(180.f, 0.f, 0.f, 1.f);
			modelStack.Scale(33.f, 33.f, 1.f);
			RenderMesh(meshList[GEO_POINTER], false);
			modelStack.PopMatrix();
			break;
		case 4:
			modelStack.PushMatrix();
			modelStack.Translate(m_worldWidth* 1.47f, m_worldHeight * 0.63f, 1.f);
			modelStack.Rotate(180.f, 0.f, 0.f, 1.f);
			modelStack.Scale(33.f, 33.f, 1.f);
			RenderMesh(meshList[GEO_POINTER], false);
			modelStack.PopMatrix();
			break;
		case 3:
			modelStack.PushMatrix();
			modelStack.Translate(m_worldWidth* 1.72f, m_worldHeight * 0.63f, 1.f);
			modelStack.Rotate(180.f, 0.f, 0.f, 1.f);
			modelStack.Scale(33.f, 33.f, 1.f);
			RenderMesh(meshList[GEO_POINTER], false);
			modelStack.PopMatrix();
			break;
		case 2:
			modelStack.PushMatrix();
			modelStack.Translate(m_worldWidth* 1.97f, m_worldHeight * 0.63f, 1.f);
			modelStack.Rotate(180.f, 0.f, 0.f, 1.f);
			modelStack.Scale(33.f, 33.f, 1.f);
			RenderMesh(meshList[GEO_POINTER], false);
			modelStack.PopMatrix();
			break;
		case 1:
			modelStack.PushMatrix();
			modelStack.Translate(m_worldWidth* 1.35f, m_worldHeight *  0.87f, 1.f);
			modelStack.Rotate(180.f, 0.f, 0.f, 1.f);
			modelStack.Scale(33.f, 33.f, 1.f);
			RenderMesh(meshList[GEO_POINTER], false);
			modelStack.PopMatrix();
			break;
		default:
			break;
		}
	}

	// HUDs
	std::stringstream text;

	text << "FPS: " << static_cast<int>(1.f / m_dt);
	textManager.renderTextOnScreen(UIManager::Text{ text.str(), Color(1,1,1), UIManager::ANCHOR_TOP_RIGHT });

	// Rendering text information on screen at shop
	//if (shopMenu && !translateShopMenu)
	//{
	//	textManager.RenderTextOnScreen(meshList[GEO_TEXT], std::to_string(PlayerDataManager::getInstance()->m_data.m_currency), Color(0.3f, 0.2f, 0.1f), 2.f, m_worldWidth_default * 0.1f, m_worldHeight_default * 0.215f);
	//	if(!PlayerDataManager::getInstance()->m_data.b_isWeapon3Unlocked)
	//	textManager.RenderTextOnScreen(meshList[GEO_TEXT], "$" + std::to_string(300), Color(0.3f, 0.2f, 0.1f), 2.f, m_worldWidth_default * 0.051f, m_worldHeight_default * 0.42f);

	//	if (!PlayerDataManager::getInstance()->m_data.b_isWeapon2Unlocked)
	//		textManager.RenderTextOnScreen(meshList[GEO_TEXT], "$" + std::to_string(300), Color(0.3f, 0.2f, 0.1f), 2.f, m_worldWidth_default * 0.19f, m_worldHeight_default * 0.42f);

	//	// Warrior Ally price
	//	textManager.RenderTextOnScreen(meshList[GEO_TEXT], "$" + std::to_string(100), Color(0.3f, 0.2f, 0.1f), 2.f, m_worldWidth_default * 0.33f, m_worldHeight_default * 0.42f);

	//	// Archer Ally price
	//	textManager.RenderTextOnScreen(meshList[GEO_TEXT], "$" + std::to_string(150), Color(0.3f, 0.2f, 0.1f), 2.f, m_worldWidth_default * 0.47f, m_worldHeight * 0.42f);

	//	// Warrior Ally Amount
	//	textManager.RenderTextOnScreen(meshList[GEO_TEXT], std::to_string(PlayerDataManager::getInstance()->m_data.m_amountofUnit1), Color(0.3f, 0.2f, 0.1f), 2.f, m_worldWidth_default * 0.33f, m_worldHeight * 0.30f);

	//	// Archer Ally Amount
	//	textManager.RenderTextOnScreen(meshList[GEO_TEXT], std::to_string(PlayerDataManager::getInstance()->m_data.m_amountofUnit2), Color(0.3f, 0.2f, 0.1f), 2.f, m_worldWidth_default * 0.47f, m_worldHeight * 0.30f);

	//	if (PlayerDataManager::getInstance()->m_data.b_isWeapon3Unlocked)
	//	{
	//		modelStack.PushMatrix();
	//		modelStack.Translate(-m_worldWidth * 0.83f, m_worldHeight * 0.57f, 1.0f);
	//		modelStack.Scale(7.f, 7.f, 1.f);
	//		RenderMesh(meshList[GEO_TICK], false);
	//		modelStack.PopMatrix();
	//	}
	//	if (PlayerDataManager::getInstance()->m_data.b_isWeapon2Unlocked)
	//	{
	//		modelStack.PushMatrix();
	//		modelStack.Translate(-m_worldWidth * 0.58f, m_worldHeight * 0.57f, 1.0f);
	//		modelStack.Scale(7.f, 7.f, 1.f);
	//		RenderMesh(meshList[GEO_TICK], false);
	//		modelStack.PopMatrix();
	//	}
	//}

	// Rendering locks on locked levels
	{
		if (!PlayerDataManager::getInstance()->m_data.b_isLevel02Unlocked)
		{
			modelStack.PushMatrix();
			modelStack.Translate(m_worldWidth* 1.67f, m_worldHeight * 0.6f, 5.f);
			modelStack.Scale(7.f, 7.f, 1.f);
			RenderMesh(meshList[GEO_LOCK], false);
			modelStack.PopMatrix();
		}
		if (!PlayerDataManager::getInstance()->m_data.b_isLevel03Unlocked)
		{
			modelStack.PushMatrix();
			modelStack.Translate(m_worldWidth* 1.92f, m_worldHeight * 0.6f, 5.f);
			modelStack.Scale(7.f, 7.f, 1.f);
			RenderMesh(meshList[GEO_LOCK], false);
			modelStack.PopMatrix();
		}
	}

	// Credits
	if (Credits && !translateCreditsMenu)
	{
		textManager.renderTextOnScreen(UIManager::Text("Hui Sheng (Programmer)" , Color(1.f, 1.f, 1.f), UIManager::ANCHOR_TOP_CENTER));
		textManager.renderTextOnScreen(UIManager::Text("Kevin (Programmer)", Color(1.f, 1.f, 1.f), UIManager::ANCHOR_TOP_CENTER));
		textManager.renderTextOnScreen(UIManager::Text("Stanley (Programmer)", Color(1.f, 1.f, 1.f), UIManager::ANCHOR_TOP_CENTER));
		textManager.renderTextOnScreen(UIManager::Text("Chuan Xu (Programmer)", Color(1.f, 1.f, 1.f), UIManager::ANCHOR_TOP_CENTER));

		textManager.renderTextOnScreen(UIManager::Text(" ", Color(1.f, 1.f, 1.f), UIManager::ANCHOR_TOP_CENTER));
		textManager.renderTextOnScreen(UIManager::Text("Images used", Color(1.f, 1.f, 1.f), UIManager::ANCHOR_TOP_CENTER));

		//textManager.renderTextOnScreen(UIManager::Text("Background and terrain textures are taken from opengameart.org", Color(1.f, 1.f, 1.f), UIManager::ANCHOR_CENTER_CENTER));
		//textManager.renderTextOnScreen(UIManager::Text("Sprites used are from RPG Maker", Color(1.f, 1.f, 1.f), UIManager::ANCHOR_CENTER_CENTER));
		//textManager.renderTextOnScreen(UIManager::Text("Sounds used are taken from the game 'Smite' by Hirez Studios ", Color(1.f, 1.f, 1.f), UIManager::ANCHOR_CENTER_CENTER));
		textManager.renderTextOnScreen(UIManager::Text("Legend of zelda hyrule castle by MinionSlayer on devianart.com", Color(1.f, 1.f, 1.f), UIManager::ANCHOR_TOP_CENTER));
		textManager.renderTextOnScreen(UIManager::Text("Old paper scroll from bgfons.com", Color(1.f, 1.f, 1.f), UIManager::ANCHOR_TOP_CENTER));
		textManager.renderTextOnScreen(UIManager::Text("Player sprite from www.rpgmakercentral.com/topic/17481-simple-guide-to-sprite-styles/ ", Color(1.f, 1.f, 1.f), UIManager::ANCHOR_TOP_CENTER));
		textManager.renderTextOnScreen(UIManager::Text("Giant sprite from rpgconspiracy.wordpress.com/2011/06/23/monsters/ogre/ ", Color(1.f, 1.f, 1.f), UIManager::ANCHOR_TOP_CENTER));
		textManager.renderTextOnScreen(UIManager::Text("Terrain taken from opengameart.org/content/100-seamless-textures ", Color(1.f, 1.f, 1.f), UIManager::ANCHOR_TOP_CENTER));
		textManager.renderTextOnScreen(UIManager::Text("Background Parallex image from opengameart.org/content/hd-multi-layer-parallex-background-samples-of-glitch-game-assets ", Color(1.f, 1.f, 1.f), UIManager::ANCHOR_TOP_CENTER));
		textManager.renderTextOnScreen(UIManager::Text("Sheep sprites from forums.rpgmakerweb.com/index.php?threads/whtdragons-animals-and-running-horses-now-with-more-dragons.53552/", Color(1.f, 1.f, 1.f), UIManager::ANCHOR_TOP_CENTER));
		textManager.renderTextOnScreen(UIManager::Text("and", Color(1.f, 1.f, 1.f), UIManager::ANCHOR_TOP_CENTER));
		textManager.renderTextOnScreen(UIManager::Text("research.clps.brown.edu/mkho/gardenpath/img/sheepSpriteSheet.png", Color(1.f, 1.f, 1.f), UIManager::ANCHOR_TOP_CENTER));
		textManager.renderTextOnScreen(UIManager::Text("Demon sprite from www.neoseeker.com/forums/22121/t639522-need-this-sprite-editted/ ", Color(1.f, 1.f, 1.f), UIManager::ANCHOR_TOP_CENTER));
		textManager.renderTextOnScreen(UIManager::Text("Gun from www.pinterest.com/pin/295830269247326557/?autologin=true ", Color(1.f, 1.f, 1.f), UIManager::ANCHOR_TOP_CENTER));
		textManager.renderTextOnScreen(UIManager::Text("Tranquilizer bolt zelda.wikia.com/wiki/Silver_Arrow ", Color(1.f, 1.f, 1.f), UIManager::ANCHOR_TOP_CENTER));
		textManager.renderTextOnScreen(UIManager::Text("Boss crystal and shrine image from vxresource.wordpress.com/2010/02/13/gorgeous-statues/", Color(1.f, 1.f, 1.f), UIManager::ANCHOR_TOP_CENTER));
		textManager.renderTextOnScreen(UIManager::Text(" ", Color(1.f, 1.f, 1.f), UIManager::ANCHOR_TOP_CENTER));
		textManager.renderTextOnScreen(UIManager::Text("Music", Color(1.f, 1.f, 1.f), UIManager::ANCHOR_TOP_CENTER));
		textManager.renderTextOnScreen(UIManager::Text("All SFX are from Smite", Color(1.f, 1.f, 1.f), UIManager::ANCHOR_TOP_CENTER));






	}
	textManager.dequeueText();
	textManager.reset();
}

void Main_Menu::Exit()
{
	PlayerDataManager::getInstance()->SaveData();
	SceneBase::Exit();
}