#pragma once

#include "Vector3.h"


class GameObject;
class ColliderFactory;

struct Manifold {

	Manifold() {};

	// Normal contact point of collision
	Vector3 normal;

	// How deep the objects penetrated
	float penetration = 0;

	// Box's vertices position
	Vector3 box_vertices[4];

};

class Collider {

	friend ColliderFactory;
	friend GameObject;

public:
	enum TYPE {
		NONE = 0,
		OBB,
		CIRCLE,
	};

	bool CheckCollision(Collider &other);
	void ResolveCollision(Collider &other, float dt);

	TYPE m_ColliderType;

	void operator=(const Collider & other);

	// Circle
	float radius = 0;

	// AABB
	Vector3 bboxSize = Vector3(0, 0, 0);

	// Physics component for a collision hit, used for calculating collision response
	Manifold manifold;

private:
	Collider(GameObject* obj, TYPE type);
	Collider();

	GameObject* m_Obj = nullptr;

	// Oriented Bounding-Box Helpers

	// Calculate the 4 points of the rectangle after applying rotation
	void CalculateVertices(float angleInRadian);

	// Returns true if point A and B intercepts each other
	// out overlapDistance: how far the point has overlapped
	// out isAHigher: Whether point A is higher than point B upon overlapping
	bool CheckOverlap(float aMin, float aMax, float bMin, float bMax, float* overlapDistance = nullptr , bool* isAHigher = false) {
		if (aMax > bMin && aMin < bMax) {
			if (aMax > bMax) {
				*overlapDistance = bMax - aMin;
				*isAHigher = true;
			}
			else {
				*overlapDistance = aMax - bMin;
			}
			return true;
		}
		return false;
	}
};