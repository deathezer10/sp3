#pragma once

#include "Player.h"


class PipeBomb : public Player {

public:
	PipeBomb(Scene* scene);
	virtual ~PipeBomb() {};

	virtual bool Update();
	virtual void Render();
	virtual void OnCollisionHit(GameObject* other);
	virtual void OnDestroy();

private:
	float m_NextVanishTime = 0;


};