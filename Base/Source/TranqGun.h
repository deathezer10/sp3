#pragma once

#include "GameObject.h"
#include "WeaponInfo.h"

class Player;

class TranqGun : public WeaponInfo, public GameObject
{
public:
	TranqGun(Scene* scene, Player* player);
	~TranqGun() {};

	bool Update();

	void Render();

	bool checkRange(GameObject* obj);

	void checkTimer();
	
	bool useUltimate();

	int getAttack();

	float getUltimateDuration();

private:
	Player* m_player;
	Vector3 m_offset;
};