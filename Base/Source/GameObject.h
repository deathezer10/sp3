#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H


#include "Vertex.h"
#include "Vector3.h"
#include "Collider.h"


class Scene;

class GameObject {

	friend class GameObjectManager;

public:
	bool active = false;
	unsigned type; // Geometry mesh index

	Vector3 pos;
	Vector3 scale;
	Vector3 rotation;

	// Current velocity of this object
	Vector3 vel;

	// Heavier mass = more force to push an object
	float mass = 1;

	// Percentage of velocity lost upon collision with another object
	float bounciness = 0.5f;

	// Percentage of velocity lost when moving on top of colliding object, 1 being total energy lost
	float friction = 0.5f;

	Color color;
	float alpha = 1;

	Vector3 dir; // direction/orientation
	float momentOfInertia = 0;
	float angularVelocity = 0; // in radians

	Vector3 m_force;
	Vector3 m_torque;

	// Max velocity of this object, 
	Vector3 MAX_VELOCITY;
	float MAX_ANGULAR_VELOCITY = 0;

	Collider collider;

	// Allow Object to wrap around the Scene
	bool b_EnableWrapping = false;

	// Allow this Object to have Physics & Collision between other GameObjects
	bool b_EnablePhysics = false;

	// Allow this Object to resolve collision with other GameObjects
	bool b_EnablePhysicsResponse = true;

	// Allow any Objects that collided to not resolve collision
	bool b_EnableTrigger = false;

	// Force the Physics engine to use this GameObject's bounciness instead of the min value
	bool b_ForceBounciness = false;

	// Force the Physics engine to use this GameObject's friction instead of the min value
	bool b_ForceFriction = false;

	// Allow Object to rebound from the edge of Scene
	bool b_EnableRebound = false;

	// Enable lighting for this Object
	bool b_EnableLighting = true;

	// Enable gravity, b_EnablePhysics must be enabled in order to work
	bool b_EnableGravity = false;

	GameObject(Scene* scene, unsigned typeValue);
	virtual ~GameObject();

	// Process the Object's logic, return false if Object is flagged for deletion
	virtual bool Update() = 0;

	// Calculate velocity, angular velocity, direction
	virtual void UpdatePhysics();

	// Render the GameObject onto the Scene
	virtual void Render();

	// Called only when there is a Collision with another Object
	virtual void OnCollisionHit(GameObject* other) {};

	// Called when this game object is about to get removed from collection
	virtual void OnDestroy();

	// Wrap the Object, preventing it from going out of screen
	void WrapObject();

	// Rebound the Object, allowing it to bounce back from screen edge
	void ReboundObject();

	const Vector3& GetMomentum() {
		return momentum;
	}

	const void SetMomentum(const Vector3& value) {
		momentum = value;
		vel = momentum / mass;
	}
	
protected:
	Scene* m_scene;
	
	// For sprite animation frame playback
	int m_Anim_CurrentFrame = 0;

	// Assign a different index for all GameObjects to prevent Z-Order rendering issue
	int currentIndex;

	// (Don't use this variable for rendering)
	// To keep track of the currentIndex to assign
	static int index;

	Vector3 momentum;
	float kineticEnergy;

	// Offset position, does not affect physics
	Vector3 m_OffsetPos;

	// Offset rotation, does not affect physics
	Vector3 m_OffsetRot;

	// Offset scale, does not affect physics/collider
	Vector3 m_OffsetScale;


};

#endif