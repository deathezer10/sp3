#ifndef SCENE_BASE_H
#define SCENE_BASE_H

#include "Scene.h"
#include "Mtx44.h"
#include "Camera.h"
#include "Mesh.h"
#include "MatrixStack.h"
#include "Light.h"
#include "GameObject.h"
#include "PauseManager.h"
#include "ParallaxScroller.h"

#include <vector>


class SceneBase : public Scene
{
public:
	SceneBase(TYPE_SCENE type);
	~SceneBase();

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();
	void UpdateLightUniforms();

	//Physics
	float fps;
	float m_speed;
	float m_worldWidth;
	float m_worldHeight;
	float m_worldWidth_default;
	float m_worldHeight_default;

	Camera camera;
	ParallaxScroller m_BGScroller;

protected:

	Light lights[2];

	bool bLightEnabled;

	bool win = 0;
	bool lost = 0;


};

#endif