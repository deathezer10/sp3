
#include "Application.h"
#include "AI_Base.h"
#include "Scene.h"
#include "ColliderFactory.h"
#include "GL\glew.h"
#include"SceneLevel02.h"
#include"Player.h"
#include "Enemy_Warrior.h"
#include "Armour_Kit.h"
#include "Wool.h"

#include "TerrainLoader.h"

AI_Base::AI_Base(Scene* scene, unsigned type, Player* player, float defaultHP, float defaultSpeed, int defaultStunCounter) :GameObject(scene, type)
{
	b_EnablePhysics = true;
	b_EnableGravity = true;

	thePlayer = player;

	m_Damage = 1;

	m_CurrentHP = defaultHP;
	m_DefaultHP = defaultHP;

	m_stunCounter = defaultStunCounter;
	m_defaultStunCounter = defaultStunCounter;

	m_DefaultSpeed = defaultSpeed;
	MAX_VELOCITY.x = m_DefaultSpeed;
};

AI_Base::~AI_Base()
{
};

void AI_Base::SetHealth(float value) {
	m_CurrentHP = Math::Clamp<float>(value, 0, m_DefaultHP);
}

void AI_Base::SetStunCounter(int value)
{
	m_stunCounter = value;
}


void AI_Base::AI_Chase(Player * player, double dt)
{

	if (m_CurrentState == CHASE)
	{
		if (pos.x < player->pos.x && (player->pos.x - pos.x) > player->scale.x)
		{
			m_force.x = 55;
			b_IsFacingRight = true;
			if (collider.manifold.normal.x != 0 && !is_jumping)
			{
				m_CurrentState = JUMP;
				is_jumping = true;
			}
		}
		else if (pos.x > player->pos.x && (player->pos.x - pos.x) < -player->scale.x)
		{
			m_force.x = -55;
			b_IsFacingRight = false;
			if (collider.manifold.normal.x != 0 && !is_jumping)
			{
				m_CurrentState = JUMP;
				is_jumping = true;
			}
		}


	}
}

void AI_Base::AI_Attack(Player * player, double dt)
{


}

void AI_Base::AI_Jump(double dt)
{

	if (m_CurrentState == JUMP && is_jumping)
	{
		m_force.y = 1750;
		if (b_IsFacingRight)
		{
			m_force.x = 55;
		}
		else
		{
			m_force.x = -55;
		}


		if (m_PreviousState == FLEE)
		{
			std::cout << "back to flee" << std::endl;
			m_CurrentState = m_PreviousState;
		}
		else
		{
			m_CurrentState = CHASE;
		}

	}
}

void AI_Base::AI_Stun()
{
	color.Set(1, 0.8f, 0);

	if (m_scene->m_elapsedTime > m_stunTimer)
	{
		m_CurrentState = m_PreviousStunState;
		MAX_VELOCITY.x = m_DefaultSpeed;
		m_stunCounter = m_defaultStunCounter;
		color.Set(1, 1, 1);
	}
}

void AI_Base::AI_Flee(Player* player, double dt)
{
	b_EnableTrigger = true;
	_dt = (float)dt;
	_elapsedTime += _dt;
	color.Set(1, 1, 1);

	if (_elapsedTime >= 5)
	{
		m_scene->goManager.DestroyObjectQueue(this);
		active = false;
		_elapsedTime = 0;
	}
	// go right if player is left of sheep
	if (pos.x > player->pos.x && (player->pos.x - pos.x) < -player->scale.x)
	{
		m_force.x = 50;
		b_IsFacingRight = true;






		//if (collider.manifold.normal.x != 0 && !is_jumping)
		//{
		//	m_PreviousState = FLEE;
		//	m_CurrentState = JUMP;
		//	is_jumping = true;
		//}

	}
	// go left if player is right of sheep
	else if (pos.x < player->pos.x && (player->pos.x - pos.x) > player->scale.x)
	{
		m_force.x = -50;
		b_IsFacingRight = false;



		//if (collider.manifold.normal.x != 0 && !is_jumping)
		//{
		//	m_PreviousState = FLEE;
		//	m_CurrentState = JUMP;
		//	is_jumping = true;
		//}

	}
}




void AI_Base::StunAI(float duration) {
	MAX_VELOCITY.x = 0;

	if (m_CurrentState != STUN) {
		m_PreviousStunState = m_CurrentState;
	}

	m_CurrentState = STUN;
	m_stunTimer = m_scene->m_elapsedTime + duration;
}

void AI_Base::ReduceStunCounter(int stunRemove)
{
	if (m_scene->m_elapsedTime > m_stunTimer)
	{
		m_stunCounter -= stunRemove;
		if (m_stunCounter <= 0)
			StunAI(m_stunDuration);
	}
}

void AI_Base::ReduceHealth(float damage, bool spawnWool) {
	if (m_CurrentState == FLEE)
		return;
	if (b_IsInvulnerable == false)
	{
		if ((type == Scene::GEO_ENEMYNINJA || type == Scene::GEO_ENEMYWARRIOR || type == Scene::GEO_ENEMYARCHER || type == Scene::GEO_ENEMYGOLEM) && spawnWool)
		{
			Wool* wool = new Wool(m_scene);
			wool->pos = pos;
			wool->m_force.Set(Math::RandFloatMinMax(-1000, 1000), 1000, 0);
			wool->active = true;
			m_scene->goManager.CreateObjectQueue(wool);
		}

		m_CurrentHP -= damage;
	}
	if (abs(m_force.x) > m_stunThreshold)
	{
		StunAI(m_stunDuration);
	}

	OnReduceHealth(damage);

	if (m_CurrentHP <= 0 && active)
	{
		int roll = Math::RandIntMinMax(1, 99);
		if (!b_isAlly && roll < m_DropChance )
		{
			Armour_Kit* ArmourKit = new Armour_Kit(m_scene);
			ArmourKit->pos = pos;
			ArmourKit->active = true;
			m_scene->goManager.CreateObjectQueue(ArmourKit);
		}

		//m_scene->playerManager.IncreaseScore(10);
		//m_scene->playerManager.IncreaseUltimateCharge(10);

		//to do: switch to flee state 
		if (type != Scene::GEO_ENEMYARCHER|| type != Scene::GEO_ENEMYWARRIOR|| type != Scene::GEO_ENEMYNINJA)
		{
			m_scene->goManager.DestroyObjectQueue(this);
		}
		else
		{
			m_CurrentState = FLEE;
		}
		
			
	}
}

bool AI_Base::Update()
{
	return true;
}



void AI_Base::OnCollisionHit(GameObject * other) {
	// Allow player to jump again if landed on top of a terrain
	if (TerrainLoader::IsTerrain(other->type) && collider.manifold.normal.y < 0 && collider.manifold.normal.x == 0) {
		is_jumping = false;
	}

	if (m_CurrentState == FLEE && TerrainLoader::IsTerrain(other->type)) {

		collider.ResolveCollision(other->collider, m_scene->m_dt);

	}

}
