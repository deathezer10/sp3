#pragma once

#include <vector>
#include "GameObject.h"


class Dart : public GameObject {

public:
	Dart(Scene* scene);
	~Dart() {};

	void fireDart(float angle, Vector3 AIpos, Collider AIcolliderSize);
	virtual bool Update();
	virtual void Render();
	virtual void OnCollisionHit(GameObject* other);

};