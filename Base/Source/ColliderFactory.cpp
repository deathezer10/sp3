#include "ColliderFactory.h"


Collider ColliderFactory::CreateCircleCollider(GameObject * obj, float radius) {
	Collider col(obj, Collider::CIRCLE);
	col.radius = radius;
	return col;
}

Collider ColliderFactory::CreateOBBCollider(GameObject * obj, Vector3 bboxSize) {
	Collider col(obj, Collider::OBB);
	col.bboxSize = bboxSize;
	return col;
}
