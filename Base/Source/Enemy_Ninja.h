#pragma once


#include"AI_Base.h"



class SpriteAnimation;
struct Animation;


class Enemy_Ninja : public AI_Base {

public:
	Enemy_Ninja(Scene* scene, Player* player);
	~Enemy_Ninja();


	bool Update();
	void Render();

	void OnReduceHealth(float damage);
	void OnCollisionHit(GameObject * other);

	void AI_Chase(Player*player, double dt);
	void AI_Attack(Player* player, double dt);	

private:
	SpriteAnimation * m_AnimEnemyNinja;
	Animation* m_Anim;

	float m_NextMeleeTime = 0;
	float m_NextTeleportTime = 0;
	bool b_IsAttacking = false;

	const float m_TeleportChargeTime = 3;
	const float m_TeleportCooldownTime = 3;

	const float m_MeleeCooldown = 0.5f;

};
