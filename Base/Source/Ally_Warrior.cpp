#include "Ally_Warrior.h"
#include "Player.h"
#include "Scene.h"
#include "GameObject.h"
#include "PlayerManager.h"
#include "SpriteAnimation.h"
#include "ColliderFactory.h"
#include "UI_Bar.h"

Ally_Warrior::Ally_Warrior(Scene* scene, Player* player) :AI_Base(scene, Scene::GEO_ALLYWARRIOR, player, 100, 20)
{
	b_EnableGravity = false;
	b_EnablePhysics = true;
	b_EnableLighting = false;
	b_EnableTrigger = true;

	if (player->b_isFacingRight)
		this->m_force.Set(Math::RandFloatMinMax(2000.f, 3000.f), 0, 0);
	else
	{
		this->m_force.Set(Math::RandFloatMinMax(-2000.f, -3000.f), 0, 0);
		b_IsFacingRight = false;
	}
	scale.Set(10.f, 10.f, 1.f);
	this->collider = ColliderFactory::CreateOBBCollider(this, Vector3(10.0f, 10.0f, 0.0f));

	color.Set(Math::RandFloatMinMax(0, 1), Math::RandFloatMinMax(0, 1), Math::RandFloatMinMax(0, 1));

	m_AnimAllyWarrior = static_cast<SpriteAnimation*>(scene->meshList[Scene::GEO_ALLYWARRIOR]);
	m_Anim = new Animation();
	m_Anim->Set(0, 0, 1, 1.f, true);

	b_isAlly = true;

	MAX_VELOCITY.x = 1000;


}

Ally_Warrior::~Ally_Warrior() {
	if (m_Anim)
		delete m_Anim;
}

bool Ally_Warrior::Update()
{
	//Old warrior ally code
	{
		/*
		float lowestdistance = 999999;
		bool assigned = false;

		for (std::vector<GameObject*>::iterator it = m_scene->goManager.m_goList.begin(); it != m_scene->goManager.m_goList.end(); ++it)
		{

			GameObject* obj = *it;
			if (obj->active)
			{
				AI_Base* Enemy = dynamic_cast<AI_Base*>(obj);

				if (Enemy != nullptr && Enemy != this)
				{
					float newEnemyLength = (Enemy->pos - pos).Length();

					if (Enemy->IsAlly() == false && newEnemyLength < threshold) {

						if (newEnemyLength <= lowestdistance) {
							lowestdistance = newEnemyLength;
							m_CurrentTarget = Enemy;
							assigned = true;
						}

					}
				}

			}

			if (assigned == false && it == m_scene->goManager.m_goList.end()) {
				m_CurrentTarget = nullptr;
			}
		}

		if (m_CurrentState == CHASE)
			AI_Chase(thePlayer, m_scene->m_dt);
		if (m_CurrentState == JUMP)
			AI_Jump(m_scene->m_dt);
		if (m_CurrentState == INTERCEPT)
			Intercept(m_CurrentTarget, m_scene->m_dt);
		if (m_CurrentState == ATTACK)
			Engage(m_CurrentTarget, m_scene->m_dt);


		if (!b_IsFacingRight)
		{
			m_offsetPosition.x = -3.f;
			m_offsetPosition.y = sin(Math::DegreeToRadian(m_weaponRotation));
		}
		else
		{
			m_offsetPosition.x = 3.f;
			m_offsetPosition.y = sin(Math::DegreeToRadian(m_weaponRotation));
		}

		return true;
		*/
	}

	m_countDown += m_scene->m_dt;
	if (m_countDown >= 9)
	{
		m_scene->goManager.DestroyObjectQueue(this);
		return false;
	}

	return true;
}

void Ally_Warrior::AI_Jump(double dt)
{
	if (is_jumping) {
		m_CurrentState = m_PreviousState;
		return;
	}


	m_force.y = 1700;

	if (b_IsFacingRight)
	{
		m_force.x = 55;

	}
	else
	{
		m_force.x = -55;

	}
	m_CurrentState = m_PreviousState;


}

void Ally_Warrior::Engage(AI_Base * AI, double dt)
{
	if (m_CurrentTarget == nullptr) {
		m_CurrentState = CHASE;
		return;
	}

	if (pos.x < thePlayer->pos.x - 60.f && (thePlayer->pos.x - pos.x - 60.f) > thePlayer->scale.x || pos.x > thePlayer->pos.x + 60.f && (thePlayer->pos.x - pos.x + 60.f) < -thePlayer->scale.x || m_CurrentTarget == nullptr)
	{
		m_CurrentState = CHASE;
	}


	else if (pos.x < m_CurrentTarget->pos.x && (m_CurrentTarget->pos.x - pos.x) >m_CurrentTarget->scale.x || pos.x > m_CurrentTarget->pos.x && (m_CurrentTarget->pos.x - pos.x) < -m_CurrentTarget->scale.x)
	{
		m_CurrentState = INTERCEPT;
	}
	else
	{
		if (!b_completeLoop)
		{
			m_weaponRotation -= 400 * m_scene->m_dt;
			/*
						if (m_CurrentTarget != nullptr)
							m_CurrentTarget->ReduceHealth(1.f);*/
		}
		else
			m_weaponRotation += 400 * m_scene->m_dt;

		if (m_weaponRotation < -55)
		{

			b_completeLoop = true;
		}
		else if (m_weaponRotation > 25)
		{
			b_completeLoop = false;
			m_CurrentState = CHASE;
			//is_attacking = false;
		}


	}
}

void Ally_Warrior::Intercept(AI_Base * AI, double dt)
{
	if (m_CurrentTarget == nullptr) {
		m_CurrentState = CHASE;
		return;
	}

	if (pos.x < thePlayer->pos.x - 60.f && (thePlayer->pos.x - pos.x - 60.f) > thePlayer->scale.x || pos.x > thePlayer->pos.x + 60.f && (thePlayer->pos.x - pos.x + 60.f) < -thePlayer->scale.x)
	{
		m_CurrentState = CHASE;
	}
	else if (m_CurrentTarget->pos.x > 100000.f || m_CurrentTarget->pos.x < -100000.f)
	{
		m_CurrentTarget = nullptr;
		m_CurrentState = CHASE;
	}


	if (m_CurrentTarget != nullptr)
	{
		if (pos.x <  m_CurrentTarget->pos.x && (m_CurrentTarget->pos.x - pos.x) >  m_CurrentTarget->scale.x / 2) // Right
		{
			m_force.x += 100;
			b_IsFacingRight = true;
			if (collider.manifold.normal.x != 0 && !is_jumping)
			{
				m_PreviousState = m_CurrentState;
				m_CurrentState = JUMP;
				is_jumping = true;
			}
		}
		else if (pos.x > m_CurrentTarget->pos.x && (m_CurrentTarget->pos.x - pos.x) < -m_CurrentTarget->scale.x / 2) // Left
		{
			m_force.x += -100;
			b_IsFacingRight = false;
			if (collider.manifold.normal.x != 0 && !is_jumping)
			{
				m_PreviousState = m_CurrentState;
				m_CurrentState = JUMP;
				is_jumping = true;
			}
		}
	}



}

void Ally_Warrior::AI_Chase(Player * player, double dt)
{

	if (pos.x < player->pos.x - 30.f && (player->pos.x - pos.x - 30.f) > player->scale.x)
	{
		m_force.x += 100;
		b_IsFacingRight = true;
		if (collider.manifold.normal.x != 0 && !is_jumping)
		{
			m_PreviousState = m_CurrentState;
			m_CurrentState = JUMP;
			is_jumping = true;
		}
	}
	else if (pos.x > player->pos.x + 30.f && (player->pos.x - pos.x + 30.f) < -player->scale.x)
	{
		m_force.x -= 100;
		b_IsFacingRight = false;
		if (collider.manifold.normal.x != 0 && !is_jumping)
		{
			m_PreviousState = m_CurrentState;
			m_CurrentState = JUMP;
			is_jumping = true;
		}
	}
	else
	{
		if (m_CurrentTarget != nullptr)
			m_CurrentState = INTERCEPT;

	}


}

void Ally_Warrior::Render()
{
	if (vel.x < 1 && vel.x > -1) {
		m_Anim->Set(24, 24, 1, 0.5f, true);
	}
	else if (b_IsFacingRight) {
		m_Anim->Set(24, 26, 1, 0.5f, true);
		rotation.y = 0;
	}
	else {
		m_Anim->Set(24, 26, 1, 0.5f, true);
		rotation.y = 180;
	}
	m_AnimAllyWarrior->Update(m_Anim, m_scene->m_dt);


	glDisable(GL_CULL_FACE);
	GameObject::Render();

	glDisable(GL_DEPTH_TEST);
	//m_scene->modelStack.PushMatrix();
	//if (!b_IsFacingRight)
	//{
	//	m_scene->modelStack.Translate(pos.x + m_offsetPosition.x, pos.y + m_offsetPosition.y, currentIndex * 0.1f);
	//	m_scene->modelStack.Rotate(180, 0, 1, 0);
	//}
	//else if (b_IsFacingRight)
	//{
	//	m_scene->modelStack.Translate(pos.x + m_offsetPosition.x, pos.y + m_offsetPosition.y, currentIndex * 0.1f);
	//}

	//if (is_attacking)
	//	m_scene->modelStack.Rotate(m_weaponRotation, 0.f, 0.f, 1.f);
	//m_scene->modelStack.Scale(5, 5, 1);
	//m_scene->meshList[Scene::GEO_SWORD]->color = Color(0.f, 0.5f, 0.5f);
	m_Anim_CurrentFrame = m_Anim->m_currentFrame;
	//m_scene->RenderMesh(m_scene->meshList[Scene::GEO_SWORD], false);
	//m_scene->modelStack.PopMatrix();

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);


	// Render Health bar
	/*Vector3 barPos = pos;
	barPos.y += collider.bboxSize.y;
	Mesh** meshList = m_scene->meshList;
	UI_BAR::RenderBar(m_scene, meshList[Scene::GEO_HP_BACKGROUND], meshList[Scene::GEO_CAP_FOREGROUND], meshList[Scene::GEO_HP_FRAME], barPos, Vector3(4, 0.5f), (float)GetCurrentHealth() / m_DefaultHP);*/
}

void Ally_Warrior::OnCollisionHit(GameObject * other)
{
	//AI_Base::OnCollisionHit(other);
	AI_Base* enemy = dynamic_cast<AI_Base*>(other);

	//Old ally warrior code
	{
		//AI_Base* enemy = dynamic_cast<AI_Base*>(other);

		//if (enemy != nullptr && enemy->IsAlly() == false) {

		//	if (enemy == m_CurrentTarget && m_scene->m_elapsedTime >= m_NextAttackDamageTime) {
		//		m_CurrentState = ATTACK;
		//		enemy->ReduceHealth(8);
		//		m_NextAttackDamageTime = m_scene->m_elapsedTime + 0.5f;
		//	}

		//	// Damage myself every .5 seconds
		//	if (m_scene->m_elapsedTime >= m_NextDamageTime) {
		//		ReduceHealth(4);
		//		m_NextDamageTime = m_scene->m_elapsedTime + 1;
		//	}

		//}

		//if ((this->collider.manifold.normal.x == -1 || this->collider.manifold.normal.x == 1) && other->type == Scene::GEO_PLAYER)
		//{

		//	if (m_scene->m_elapsedTime > m_attackTimer)
		//	{
		//		is_attacking = true;
		//		m_attackTimer = m_scene->m_elapsedTime + 1.f;
		//	}
		//}
		//else if ((this->collider.manifold.normal.y == -1 || this->collider.manifold.normal.y == 1) && other->type == Scene::GEO_PLAYER)
		//{
		//	//If AI is facing right, AI will move right , else AI will move left
		//	this->b_IsFacingRight ? m_force.x = 55 : m_force.x = -55;
		//}
	}
	float stunduration = 2;

	if ((this->collider.manifold.normal.x == -1 || this->collider.manifold.normal.x == 1) && (other->type == Scene::GEO_ENEMYWARRIOR || other->type == Scene::GEO_ENEMYARCHER || other->type == Scene::GEO_ENEMYGOLEM || other->type == Scene::GEO_ENEMYNINJA || other->type == Scene::GEO_DEMONSHEEPBOSS || other->type == Scene::GEO_ENEMYGIANT))
	{
		enemy->StunAI(stunduration);
		enemy->MAX_VELOCITY.y = 50.f;
		enemy->ReduceHealth(6, false);
		other->m_force.y = 5000.f;
		other->m_force.x = Math::RandFloatMinMax(-50.f, 50.f);
	}
}



