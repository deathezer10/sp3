#include "Application.h"

//Include GLEW
#include <GL/glew.h>

//Include GLFW
#include <GLFW/glfw3.h>

//Include the standard C++ headers
#include <stdio.h>
#include <stdlib.h>

#include "SceneManager.h"
#include "SceneEditor.h"
#include "SceneLevel01.h"
#include "SceneLevel02.h"
#include "SceneLevel03.h"
#include "Main_Menu.h"


int Application::m_window_width = WINDOW_DEFAULT_WIDTH;
int Application::m_window_height = WINDOW_DEFAULT_HEIGHT;

const double Application::deltaTime = (float)1 / FPS;
float Application::m_FPS = 0;

GLFWwindow* m_window;

bool Application::_isFullScreen = false;
bool Application::_isValid = true;

// For keyboard controller
std::bitset<Application::MAX_KEYS> Application::currStatus;
std::bitset<Application::MAX_KEYS> Application::prevStatus;


Application::Application()
{
}

Application::~Application()
{
}

//Define an error callback
static void error_callback(int error, const char* description)
{
	fputs(description, stderr);
	_fgetchar();
}

//Define the key input callback
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_F5 && action == GLFW_RELEASE)
		Application::ToggleFullscreen();
}

void resize_callback(GLFWwindow* window, int w, int h)
{
	glViewport(0, 0, w, h);
}

void Application::UpdateKeyboardStatus(unsigned char _slot, bool _isPressed)
{
	currStatus.set(_slot, _isPressed);
}

void Application::EndFrameUpdate()
{
	prevStatus = currStatus;
}

bool Application::IsKeyPressed(unsigned char virtualKey)
{
	return currStatus.test(virtualKey);
}

bool Application::IsKeyPressedOnce(unsigned char virtualKey)
{
	return IsKeyPressed(virtualKey) && !prevStatus.test(virtualKey);
}

bool Application::IsKeyPressedAndReleased(unsigned char virtualKey)
{
	return !IsKeyPressed(virtualKey) && prevStatus.test(virtualKey);
}

bool TestKeyPressed(unsigned short key)
{
	return ((GetAsyncKeyState(key) & 0x8001) != 0);
}

void Application::UpdateInput()
{
	// Update Keyboard Input
	for (int i = 0; i < MAX_KEYS; ++i)
		UpdateKeyboardStatus(i, TestKeyPressed(i));
}

void Application::GetCursorPos(double *xpos, double *ypos)
{
	glfwGetCursorPos(m_window, xpos, ypos);
}

void Application::ToggleFullscreen() {

	const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());

	if (_isFullScreen == false) { // Switch to Borderless Fullscreen
		m_window_width = mode->width;
		m_window_height = mode->height;
		glfwSetWindowMonitor(m_window, glfwGetPrimaryMonitor(), 0, 0, mode->width, mode->height, mode->refreshRate);
	}
	else { // Switch to Windowed
		m_window_width = WINDOW_DEFAULT_WIDTH;
		m_window_height = WINDOW_DEFAULT_HEIGHT;
		glfwSetWindowMonitor(m_window, NULL, mode->width / 5, mode->height / 5, WINDOW_DEFAULT_WIDTH, WINDOW_DEFAULT_HEIGHT, GL_DONT_CARE);
	}

	_isValid = false; // Window was resized

	_isFullScreen = !_isFullScreen;
}

void Application::Init()
{
	//Set the error callback
	glfwSetErrorCallback(error_callback);

	//Initialize GLFW
	if (!glfwInit())
	{
		exit(EXIT_FAILURE);
	}

	//Current monitor's video mode
	const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());

	//Set the GLFW window creation hints - these are optional
	glfwWindowHint(GLFW_SAMPLES, 4); //Request 4x antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //Request a specific OpenGL version
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); //Request a specific OpenGL version
	//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL 

	//Set the Video mode to same as the current one
	glfwWindowHint(GLFW_RED_BITS, mode->redBits);
	glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
	glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
	glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

	//Create a window and create its OpenGL context
	m_window = glfwCreateWindow(m_window_width, m_window_height, "Attack on Castle", NULL, NULL);

	//Center the window
	glfwSetWindowPos(m_window, mode->width / 5, mode->height / 5);

	//If the window couldn't be created
	if (!m_window)
	{
		fprintf(stderr, "Failed to open GLFW window.\n");
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	// Disable V-Sync
	// glfwSwapInterval(0);

	//This function makes the context of the specified window current on the calling thread. 
	glfwMakeContextCurrent(m_window);

	//Sets the key callback
	glfwSetKeyCallback(m_window, key_callback);
	glfwSetWindowSizeCallback(m_window, resize_callback);

	glewExperimental = true; // Needed for core profile
	//Initialize GLEW
	GLenum err = glewInit();

	//If GLEW hasn't initialized
	if (err != GLEW_OK)
	{
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
		//return -1;
	}

	// Hide the cursor
	// glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

void Application::Run()
{
	SceneManager* scene_manager = SceneManager::getInstance();

	scene_manager->changeScene(new Main_Menu());

	scene_manager->Update();
	scene_manager->Exit(); // Cleaning up, chances are that the Application is terminating after this	
}

void Application::Exit()
{
	//Close OpenGL window and terminate GLFW
	glfwDestroyWindow(m_window);
	//Finalize and clean up GLFW
	glfwTerminate();
}
