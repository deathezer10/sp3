#include "Portal.h"
#include "Scene.h"
#include "Mesh.h"
#include "UI_Bar.h"
#include "Player.h"

#include "Enemy_Warrior.h"
#include "Enemy_Archer.h"
#include "Enemy_Ninja.h"
#include "Enemy_Golem.h"

#include "ColliderFactory.h"


Portal::Portal(Scene * scene, Player* player, E_PORTAL_TYPE portalType, bool isCore) : AI_Base(scene, ((isCore) ? Scene::GEO_CORE : Scene::GEO_PORTAL), player, 200) {

	b_EnableGravity = false;
	b_EnableLighting = false;
	b_EnablePhysics = true;
	b_EnablePhysicsResponse = false;

	active = true;

	if (isCore)
		scale.Set(20, 20, 1);
	else
		scale.Set(10, 10, 1);

	collider = ColliderFactory::CreateOBBCollider(this, scale);

	color.Set(.3f, .3f, .3f); // Initially inactive, set color to grey

	m_PortalType = portalType;

	b_PIsCore = isCore;

	// Portal Cooldown
	m_PActivateThreshold = 100;
	m_PSpawnCooldown = 5;

	// Set objects to spawn
	switch (m_PortalType) {

	case WARRIOR:
		v_MonstersToSpawn.push_back(M_WARRIOR);
		break;

	case ARCHER:
		v_MonstersToSpawn.push_back(M_ARCHER);
		break;

	case GOLEM:
		v_MonstersToSpawn.push_back(M_GOLEM);
		break;

	case NINJA:
		v_MonstersToSpawn.push_back(M_NINJA);
		break;

	case WARRIOR_AND_ARCHER:
		v_MonstersToSpawn.push_back(M_WARRIOR);
		v_MonstersToSpawn.push_back(M_ARCHER);
		break;

	case GOLEM_AND_WARRIOR:
		v_MonstersToSpawn.push_back(M_WARRIOR);
		v_MonstersToSpawn.push_back(M_GOLEM);
		break;

	case NINJA_AND_ARCHER:
		v_MonstersToSpawn.push_back(M_ARCHER);
		v_MonstersToSpawn.push_back(M_NINJA);
		break;

	default:
		throw 0;
	}

}

bool Portal::Update() {

	// make this portal immovable
	m_force.SetZero();

	// Return if this Portal is not supposed to spawn enemies
	if (m_PActivateThreshold == 0) {

		if (alpha < 1)
			alpha += m_scene->m_dt / 3;
		else
			b_IsInvulnerable = false;

		return true;
	}

	if (b_PIsActive) {

		if (m_scene->m_elapsedTime >= m_PNextSpawnTime) {

			SpawnMonster(v_MonstersToSpawn[m_PCurrentSpawnIndex]); // Spawn monster

			// Increment index, wrap around from 0 to max range
			m_PCurrentSpawnIndex = Math::Wrap<unsigned>(++m_PCurrentSpawnIndex, 0, v_MonstersToSpawn.size() - 1);

			m_PNextSpawnTime = m_scene->m_elapsedTime + Math::RandFloatMinMax(m_PSpawnCooldown, m_PSpawnCooldown + 2); // Add cooldown
		}

	}
	else {
		float hDist = (thePlayer->pos - pos).LengthSquared();

		if (hDist < m_PActivateThreshold * m_PActivateThreshold) {
			b_PIsActive = true;
		}
	}

	return true;
}

void Portal::Render() {
	if (b_PIsActive) {
		color.Set(1, 1, 1);

		if (b_PIsCore == false)
			m_OffsetRot.z -= 90 * m_scene->m_dt;
	}

	if (b_IsInvulnerable == false) {
		Vector3 barPos = pos;
		barPos.y += collider.bboxSize.y;

		Mesh** meshList = m_scene->meshList;

		UI_BAR::RenderBar(m_scene, meshList[Scene::GEO_HP_BACKGROUND], meshList[Scene::GEO_HP_FOREGROUND], meshList[Scene::GEO_HP_FRAME], barPos, Vector3(7, 1), (float)GetCurrentHealth() / m_DefaultHP);
	}

	AI_Base::Render();
}

void Portal::OnCollisionHit(GameObject * other) {
}

void Portal::SpawnMonster(E_PORTAL_MONSTERS monsterType) {

	AI_Base* monster;

	switch (monsterType) {

	case M_WARRIOR:
		monster = new Enemy_Warrior(m_scene, thePlayer);
		break;

	case M_ARCHER:
		monster = new Enemy_Archer(m_scene, thePlayer);
		break;

	case M_GOLEM:
		monster = new Enemy_Golem(m_scene, thePlayer);
		break;

	case M_NINJA:
		monster = new Enemy_Ninja(m_scene, thePlayer);
		break;

	default:
		throw 0;
	}

	monster->active = true;
	monster->pos = pos;
	monster->pos.x -= monster->collider.bboxSize.x / 2 + collider.bboxSize.x / 2;

	m_scene->goManager.CreateObjectQueue(monster);

}
