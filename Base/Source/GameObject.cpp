#include "Application.h"
#include "GameObject.h"
#include "Scene.h"
#include "SceneBase.h"

int GameObject::index = 0;

GameObject::GameObject(Scene* scene, unsigned typeValue)
	:
	m_scene(scene),
	type(typeValue),
	scale(1, 1, 1),
	rotation(0, 0, 0)
{
	currentIndex = index;
	++index;
	color.Set(1, 1, 1);
	dir.Set(1, 0, 0);
}

GameObject::~GameObject()
{
}

void GameObject::UpdatePhysics() {

	if (mass == 0) {
		rotation.z = Math::RadianToDegree(atan2(dir.y, dir.x));
		return;
	}

	momentOfInertia = mass * scale.x * scale.x;

	// Update Object's velocity based on m_force
	Vector3 a = m_force / mass;
	vel += a * m_scene->m_dt;

	// Apply gravity
	if (b_EnableGravity)
		vel.y -= 50.f * m_scene->m_dt;

	if (MAX_VELOCITY.IsZero() == false) {
		// Handles negative max velocity as well
		Vector3 maxVel = MAX_VELOCITY;

		if (maxVel.x != 0) {
			maxVel.x = abs(maxVel.x);
			vel.x = Math::Clamp<float>(vel.x, -maxVel.x, maxVel.x);
		}

		if (maxVel.y != 0) {
			maxVel.y = abs(maxVel.y);
			vel.y = Math::Clamp<float>(vel.y, -maxVel.y, maxVel.y);
		}

		if (maxVel.z != 0) {
			maxVel.z = abs(maxVel.z);
			vel.z = Math::Clamp<float>(vel.z, -maxVel.z, maxVel.z);
		}
	}

	// Modify Object position
	pos += vel * m_scene->m_dt;

	if (angularVelocity != 0 || m_torque.IsZero() == false) {
		// Angular movement
		float alpha = m_torque.z / momentOfInertia;
		angularVelocity += alpha * m_scene->m_dt;

		// Only clamp omega if MAX_ANGULAR_VELOCITY is set
		if (MAX_ANGULAR_VELOCITY != 0)
			angularVelocity = Math::Clamp<float>(angularVelocity, -MAX_ANGULAR_VELOCITY, MAX_ANGULAR_VELOCITY);

		float theta = atan2(dir.y, dir.x);
		theta += angularVelocity * m_scene->m_dt;
		dir.Set(cos(theta), sin(theta), 0);
	}

	m_force.SetZero();
	m_torque.SetZero();

	rotation.z = Math::RadianToDegree(atan2(dir.y, dir.x));

	momentum = mass * vel;
	kineticEnergy = 0.5f * mass * vel.LengthSquared();

}

void GameObject::Render() {

	m_scene->modelStack.PushMatrix();
	m_scene->modelStack.Translate(pos.x + m_OffsetPos.x, pos.y + m_OffsetPos.y, m_OffsetPos.z + (currentIndex * 0.01f)); // Prevent Z-Fighting
	m_scene->modelStack.Rotate(rotation.y + m_OffsetRot.y, 0, 1, 0);
	m_scene->modelStack.Rotate(rotation.z + m_OffsetRot.z, 0, 0, 1);
	m_scene->modelStack.Rotate(rotation.x + m_OffsetRot.x, 1, 0, 0);
	m_scene->modelStack.Scale(scale.x + m_OffsetScale.x, scale.y + m_OffsetScale.y, scale.z + m_OffsetScale.z);
	m_scene->meshList[type]->color = color;
	m_scene->meshList[type]->alpha = alpha;
	m_scene->RenderMesh(m_scene->meshList[type], b_EnableLighting, m_Anim_CurrentFrame);
	m_scene->modelStack.PopMatrix();
}

void GameObject::OnDestroy()
{
}

void GameObject::WrapObject() {

	if (b_EnableWrapping == false)
		return;

	SceneBase* _scene = static_cast<SceneBase*>(m_scene);

	// Wrap position if it leaves screen
	if (pos.x > _scene->m_worldWidth + (scale.x * 0.5f))
		pos.x = -(scale.x * 0.5f);
	else if (pos.x < (scale.x * -0.5f))
		pos.x = _scene->m_worldWidth + (scale.x * 0.5f);
	else if (pos.y > _scene->m_worldHeight + (scale.y * 0.5f))
		pos.y = -(scale.y * 0.5f);
	else if (pos.y < (scale.y * -0.5f))
		pos.y = _scene->m_worldHeight + (scale.y * 0.5f);

}

void GameObject::ReboundObject() {

	if (b_EnableRebound == false)
		return;

	SceneBase* _scene = static_cast<SceneBase*>(m_scene);

	// Rebound position if it leaves screen
	if ((pos.x > _scene->m_worldWidth - scale.x) && vel.x > 0) {
		// pos.x = Math::Clamp<float>(pos.x, (scale.x / 2), _scene->m_worldWidth - (scale.x / 2));
		vel.x = -vel.x;
	}
	else if (pos.x < scale.x && vel.x < 0) {
		vel.x = -vel.x;
	}
	if ((pos.y > _scene->m_worldHeight - scale.y) && vel.y > 0) { // y
		// pos.y = Math::Clamp<float>(pos.y, (scale.y / 2), _scene->m_worldHeight - (scale.y / 2));
		vel.y = -vel.y;
	}
	else if (pos.y < scale.y && vel.y < 0) {
		vel.y = -vel.y;
	}

}