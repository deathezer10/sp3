#include "TranqGun.h"
#include "Scene.h"
#include "Player.h"
#include "Dart.h"
#include "ColliderFactory.h"
#include "GL\glew.h"
#include "Mesh.h"
#include "UltimateRain.h"
#include "SoundManager.h"

TranqGun::TranqGun(Scene * scene, Player* player) : GameObject(scene, Scene::GEO_TRANQGUN)
{
	b_EnableLighting = false;
	b_EnablePhysicsResponse = false;

	scale.Set(5, 5, 1);

	m_player = player;

	active = true;

	m_angle = 0.f;

	m_attackTimer = 0.f;

	m_damage = 45;
}

bool TranqGun::Update()
{
	pos = m_player->pos;

	rotation.z = m_angle;
	
	if (m_player->b_isFacingRight)
	{
		m_offset.x = 2.f;
		rotation.y = 0.f;
	}
	else
	{
		m_offset.x = -2.f;
		rotation.z = -(m_angle - 180.f);
		rotation.y = 180.f;
	}
	return true;
}

void TranqGun::Render()
{
	glDisable(GL_CULL_FACE);
	m_scene->modelStack.PushMatrix();
	m_scene->modelStack.Translate(pos.x + m_offset.x, pos.y + m_offset.y, currentIndex * 0.01f);
	m_scene->modelStack.Rotate(rotation.y, 0, 1, 0);
	m_scene->modelStack.Rotate(rotation.z, 0, 0, 1);
	m_scene->modelStack.Rotate(rotation.x, 1, 0, 0);
	m_scene->modelStack.Scale(scale.x, scale.y, scale.z);
	m_scene->meshList[type]->color = color;
	m_scene->meshList[type]->alpha = alpha;
	m_scene->RenderMesh(m_scene->meshList[type], b_EnableLighting);
	m_scene->modelStack.PopMatrix();

	glEnable(GL_CULL_FACE);
}

bool TranqGun::checkRange(GameObject * obj)
{
	return false;
}

void TranqGun::checkTimer()
{
	if (m_scene->m_elapsedTime > m_attackTimer)
	{
		m_attackTimer = m_scene->m_elapsedTime + 1.5f;
		Dart *bolt = new Dart(m_scene);
		bolt->collider = ColliderFactory::CreateOBBCollider(bolt, Vector3(7.5f, 2, 1));
		bolt->pos.Set(pos.x + m_offset.x, pos.y + m_offset.y, 0);
		float angle = Math::DegreeToRadian(m_angle);
		bolt->vel.Set(150.f * cos(angle), 150.f * sin(angle), 0.f);
		bolt->vel.x += m_player->vel.x;
		Vector3 offset;
		offset = bolt->vel.Normalized() * (m_player->collider.bboxSize);
		bolt->pos += offset;
		m_scene->goManager.CreateObject(bolt);
		m_attackTimer = m_scene->m_elapsedTime + 0.75f;
		b_canAttack = false;
		SoundManager::getInstance().Play2DSound("ArrowFire", false, true, false);
	}
}

bool TranqGun::useUltimate()
{
	if (m_scene->m_elapsedTime > m_attackTimer)
	{
		m_attackTimer = m_scene->m_elapsedTime + 1.5f;
		UltimateRain *bolt = new UltimateRain(m_scene);
		bolt->pos.Set(pos.x, pos.y, 0);
		float angle = Math::DegreeToRadian(m_angle);
		bolt->vel.Set(0.f, 37.5f, 0.f);
		Vector3 offset;
		offset.y = bolt->vel.Normalized().y * (m_player->collider.bboxSize.y + bolt->collider.bboxSize.y);
		bolt->pos += offset;
		m_scene->goManager.CreateObject(bolt);

		SoundManager::getInstance().Play2DSound("Ultimate3");

		return true;
	}

	return false;
}

int TranqGun::getAttack()
{
	return m_damage;
}

float TranqGun::getUltimateDuration()
{
	return 0.0f;
}
