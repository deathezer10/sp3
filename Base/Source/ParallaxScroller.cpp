#include "ParallaxScroller.h"
#include "SceneBase.h"
#include "Camera.h"


ParallaxScroller::ParallaxScroller(SceneBase * scene, unsigned type) {
	m_scene = scene;
	m_Type = type;
	m_Camera = &scene->camera;
}

void ParallaxScroller::Update() {
}

void ParallaxScroller::RenderBackground() {

	m_scene->modelStack.PushMatrix();
	m_scene->modelStack.Translate(m_Camera->position.x + (m_scene->m_worldWidth / 2), m_scene->m_worldHeight * 0.8f, -0.5f);
	m_scene->modelStack.Scale(300.0f, 200.0f, 1.0f);
	m_scene->RenderMesh(m_scene->meshList[Scene::GEO_Parallax_L1_3], false);
	m_scene->modelStack.PopMatrix();

	m_scene->modelStack.PushMatrix();
	m_scene->modelStack.Translate(m_Camera->position.x * 0.7f + (m_scene->m_worldWidth), m_scene->m_worldHeight * 0.45f, -0.2f);
	m_scene->modelStack.Scale(300.0f, 200.0f, 1.0f);
	m_scene->RenderMesh(m_scene->meshList[Scene::GEO_Parallax_L1_2], false);
	m_scene->modelStack.PopMatrix();

	m_scene->modelStack.PushMatrix();
	m_scene->modelStack.Translate(m_Camera->position.x * 0.8f + (m_scene->m_worldWidth / 2), m_scene->m_worldHeight * 0.8f, -0.1f);
	m_scene->modelStack.Scale(300.0f, 200.0f, 1.0f);
	m_scene->RenderMesh(m_scene->meshList[Scene::GEO_Parallax_L1_1], false);
	m_scene->modelStack.PopMatrix();

}
