#include "Camera3.h"
#include "Application.h"
#include "Mtx44.h"
#include "GLFW\glfw3.h"


Vector3 Camera::position;


Camera3::Camera3()
{
}

Camera3::~Camera3()
{
}

void cbMouseEvent(GLFWwindow* window, int button, int action, int mods);

void Camera3::Init(const Vector3& pos, const Vector3& target, const Vector3& up)
{
	this->position = defaultPosition = pos;
	this->view = defaultView = (target - position).Normalized();
	this->target = defaultTarget = target;

	this->up = defaultUp = up;

	this->right = defaultRight = view.Cross(up);
	this->right.Normalize();

	mouseMovedX = 0;
	mouseMovedY = 0;

	glfwSetMouseButtonCallback(glfwGetCurrentContext(), cbMouseEvent);
}

void cbMouseEvent(GLFWwindow* window, int button, int action, int mods) {

	// Toggle cursor on middle click
	if (button == GLFW_MOUSE_BUTTON_MIDDLE && action == GLFW_RELEASE) {
		if (glfwGetInputMode(window, GLFW_CURSOR) == GLFW_CURSOR_NORMAL) {
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED); // Hide cursor
		}
		else {
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL); // Show Cursor

																	   // Reset the cursor to middle
			int w, h;
			glfwGetWindowSize(window, &w, &h);
			glfwSetCursorPos(window, (double)w / 2, (double)h / 2);
		}
	}

}

void Camera3::updateCursor() {

	// Prevent the cursor from moving when window is resized
	if (Application::IsWindowValid() == false) {
		Application::ValidateWindow();
		return;
	}

	double currentX = 0;
	double currentY = 0;

	glfwGetCursorPos(glfwGetCurrentContext(), &currentX, &currentY);

	if (currentX != lastX) {
		mouseMovedX = (currentX > lastX) ? 1 : -1; // 1 for left, -1 for right
		mouseMovedDistanceX = std::abs((float)(currentX - lastX)); // distance the cursor moved and return a positive value of it
	}
	else {
		mouseMovedX = 0;
	}

	if (currentY != lastY) {
		mouseMovedY = (currentY < lastY) ? 1 : -1; // 1 for up, -1 for down
		mouseMovedDistanceY = std::abs((float)(currentY - lastY)); // distance the cursor moved and return a positive value of it
	}
	else {
		mouseMovedY = 0;
	}

	lastX = currentX;
	lastY = currentY;

}

void Camera3::Update(double dt) {

	// Cursor is shown, stop rotating the camera
	if (isMouseEnabled && glfwGetInputMode(glfwGetCurrentContext(), GLFW_CURSOR) == GLFW_CURSOR_DISABLED && !Application::IsKeyPressed(MK_RBUTTON)) {
		updateCursor();
	}
	else {
		ResetCursorVariables();
	}

	float CAMERA_SPEED = 200.0f * (float)dt;
	float rotationSpeed = 5.0f * (float)dt;

	// "Sprint"
	if (Application::IsKeyPressed(VK_LSHIFT)) {
		CAMERA_SPEED *= 2.0f;
	}

	// position.x = Math::Clamp<float>(position.x, -MaxXDistance, MaxXDistance);
	// position.z = Math::Clamp<float>(position.z, -MaxZDistance, MaxZDistance);
	
	// Camera Forward / Backward / Left / Right
	if (Application::IsKeyPressed('W')) { // Forward
		position.y = terrainOffset;
		target.y = terrainOffset;
		position += view * CAMERA_SPEED;
		target = position + view;
	}
	else if (Application::IsKeyPressed('S')) { // Backward
		position.y = terrainOffset;
		target.y = terrainOffset;
		position -= view * CAMERA_SPEED;
		target = position + view;
	}
	if (Application::IsKeyPressed('A')) { // Left
		position.y = terrainOffset;
		target.y = terrainOffset;
		position = position - (right * CAMERA_SPEED);
		target = position + view;
	}
	else if (Application::IsKeyPressed('D')) { // Right
		position.y = terrainOffset;
		target.y = terrainOffset;
		position = position + right * CAMERA_SPEED;
		target = position + view;
	}

	if (mouseMovedX < 0) { // Mouse Left
		float angle = rotationSpeed * mouseMovedDistanceX;
		yaw -= angle;

		Mtx44 rotation;
		rotation.SetToRotation(angle, 0, 1, 0);
		view = (target - position).Normalized();
		view = rotation * view;
		target = position + view;
		up = rotation * up;
	}
	else if (mouseMovedX > 0) { // Mouse Right
		float angle = rotationSpeed * mouseMovedDistanceX;
		yaw += angle;

		Mtx44 rotation;
		rotation.SetToRotation(-angle, 0, 1, 0);
		view = (target - position).Normalized();
		view = rotation * view;
		target = position + view;
		up = rotation * up;
	}

	// Re-calculate view and right vectors
	view = (target - position).Normalized();
	right = view.Cross(up).Normalized();
	
	if (mouseMovedY > 0 && pitch <= MaxPitchAngle) { // Mouse Up
		float angle = rotationSpeed * mouseMovedDistanceY;
		pitch += angle;

		Mtx44 rotation;
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();
		rotation.SetToRotation(angle, right.x, right.y, right.z);
		view = rotation * view;
		target = position + view;
	}
	else if (mouseMovedY < 0 && pitch >= MinPitchAngle) { // Mouse Down
		float angle = -rotationSpeed * mouseMovedDistanceY;
		pitch += angle;

		Mtx44 rotation;
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();
		rotation.SetToRotation(angle, right.x, right.y, right.z);
		view = rotation * view;
		target = position + view;
	}



}

void Camera3::ResetCursorVariables() {
	mouseMovedX = 0;
	mouseMovedY = 0;
	mouseMovedDistanceX = 0;
	mouseMovedDistanceY = 0;
	glfwGetCursorPos(glfwGetCurrentContext(), &lastX, &lastY);
}

void Camera3::Reset() {
	view = defaultView;
	position = defaultPosition;
	up = defaultUp;
	right = defaultRight;
}