#pragma once


#include "AI_Base.h"

class Player;


class DemonFireball : public AI_Base {


public:
	DemonFireball(Scene* scene, Player* player, Vector3 direction);
	~DemonFireball() {};

	virtual bool Update();
	virtual void Render();
	virtual void OnCollisionHit(GameObject* other);
	
private:
	Vector3 m_Direction;

};