#include "Enemy_Golem.h"
#include "Player.h"
#include "GameObject.h"
#include "SceneBase.h"
#include "Mesh.h"
#include "SpriteAnimation.h"
#include "TerrainLoader.h"
#include "UI_Bar.h"

#include "ColliderFactory.h"


Enemy_Golem::Enemy_Golem(Scene* scene, Player* player) :AI_Base(scene, Scene::GEO_ENEMYGOLEM, player, 300, 50)
{
	b_EnableGravity = true;
	b_EnablePhysics = true;
	b_EnableLighting = false;
	b_EnablePhysicsResponse = true;

	m_AnimatedGolem = static_cast<SpriteAnimation*>(scene->meshList[Scene::GEO_ENEMYGOLEM]);
	m_Anim = new Animation();
	m_Anim->Set(67, 67, 1, 1.f, true);

	scale.Set(10.f, 10.f, 1.f);

	collider = ColliderFactory::CreateOBBCollider(this, scale);

	m_offset = 0.f;
	m_launchAngle = 200.f;

	ROTATION_SPEED = 20.f;

	m_stunDuration = 2.f;
	m_stunThreshold = 1000.f;
}

Enemy_Golem::~Enemy_Golem() {
	if (m_Anim)
		delete m_Anim;
}


bool Enemy_Golem::Update()
{
	SceneBase* _scene = (SceneBase*)m_scene;

	switch (m_CurrentState)
	{
	case CHASE:
		AI_Chase();
		break;
	case STUN:
		AI_Stun();
		break;
	case ATTACK:
		AI_Attack();
	}

	return true;
}



void Enemy_Golem::AI_Chase()
{
	ROTATION_SPEED += 20.f * m_scene->m_dt;
	m_offset += ROTATION_SPEED * m_scene->m_dt;

	if (pos.x <= thePlayer->pos.x)
		b_IsFacingRight = true;
	else if (pos.x >= thePlayer->pos.x)
		b_IsFacingRight = false;

	if (m_offset > m_launchAngle)
		m_CurrentState = ATTACK;
}

void Enemy_Golem::AI_Attack()
{
	if (b_IsFacingRight)
	{
		m_force.x = 100;
	}
	else
	{
		m_force.x = -100;
	}

	ROTATION_SPEED += 20.f * m_scene->m_dt;
	m_offset += ROTATION_SPEED * m_scene->m_dt;

	distanceTraveled += vel * m_scene->m_dt;

	if (abs(distanceTraveled.x) > 125.f)
	{
		m_CurrentState = STUN;
		m_stunTimer = m_scene->m_elapsedTime + 4.f;
	}
}

void Enemy_Golem::AI_Stun()
{
	m_force.SetZero();
	m_offset = 0.f;
	if (m_scene->m_elapsedTime > m_stunTimer)
	{
		distanceTraveled.SetZero();
		m_CurrentState = CHASE;
		ROTATION_SPEED = 20.f;
	}
}

void Enemy_Golem::Render()
{

	glDisable(GL_CULL_FACE);
	if (m_CurrentState == STUN)
	{
		type = Scene::GEO_ENEMYGOLEM;
		m_offsetPosition.y = 0;
	}
	else
	{
		type = Scene::GEO_ENEMYGOLEM_BOULDER;
		m_offsetPosition.y = 0;
	}

	m_AnimatedGolem->Update(m_Anim, m_scene->m_dt);

	m_scene->modelStack.PushMatrix();
	m_scene->modelStack.Translate(pos.x + m_offsetPosition.x, pos.y + m_offsetPosition.y, currentIndex * 0.01f);
	if (b_IsFacingRight)
		m_scene->modelStack.Rotate(rotation.y + 180.f, 0, 1, 0);
	else
		m_scene->modelStack.Rotate(rotation.y, 0, 1, 0);

	m_scene->modelStack.Rotate(rotation.z + m_offset, 0, 0, 1);
	m_scene->modelStack.Rotate(rotation.x, 1, 0, 0);
	m_scene->modelStack.Scale(scale.x, scale.y, scale.z);
	m_scene->meshList[type]->color = color;
	m_scene->meshList[type]->alpha = alpha;
	m_scene->RenderMesh(m_scene->meshList[type], b_EnableLighting, m_Anim->m_currentFrame);
	m_scene->modelStack.PopMatrix();
	glEnable(GL_CULL_FACE);

	// Render Health bar
	Vector3 barPos = pos;
	barPos.y += collider.bboxSize.y;
	Mesh** meshList = m_scene->meshList;
	UI_BAR::RenderBar(m_scene, meshList[Scene::GEO_HP_BACKGROUND], meshList[Scene::GEO_HP_FOREGROUND], meshList[Scene::GEO_HP_FRAME], barPos, Vector3(4, 0.5f), (float)GetCurrentHealth() / m_DefaultHP);
}

void Enemy_Golem::OnCollisionHit(GameObject * obj)
{
	if (m_CurrentState != STUN && collider.manifold.normal.x != 0 && abs(vel.x) > 1.f)
	{
		if (TerrainLoader::IsTerrain(obj->type))
		{
			if ((b_IsFacingRight && collider.manifold.normal.x == 1) || (!b_IsFacingRight && collider.manifold.normal.x == -1))
			{
				m_CurrentState = STUN;
				m_stunTimer = m_scene->m_elapsedTime + 4.f;
			}
		}
		else if (obj == thePlayer)
		{
			std::cout << vel << std::endl;

			m_CurrentState = STUN;
			m_stunTimer = m_scene->m_elapsedTime + 4.f;

			if (thePlayer->b_isRealPlayer)
				m_scene->playerManager.ReduceHealth(60);
			// collider.ResolveCollision(thePlayer->collider, m_scene->m_dt);
		}
	}
}
