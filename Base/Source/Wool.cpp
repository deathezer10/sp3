#include "Wool.h"
#include "Scene.h"
#include "ColliderFactory.h"
#include "PlayerManager.h"

Wool::Wool(Scene * scene, int woolCounter) : GameObject(scene, Scene::GEO_FUR)
{
	m_woolCount = woolCounter;
	b_EnableGravity = true;
	b_EnablePhysics = true;
	b_EnableTrigger = true;
	b_EnableLighting = false;

	active = true;
	float rng = Math::RandFloatMinMax(2, 4);
	scale.Set(rng, rng, 1);
	this->collider = ColliderFactory::CreateOBBCollider(this, Vector3(scale.x - 0.5f, scale.y - 0.5f, 1));

}

Wool::~Wool()
{
}

void Wool::Render()
{

	m_scene->modelStack.PushMatrix();
	m_OffsetPos.y = cos(m_scene->m_elapsedTime * 4) / 2;
	m_scene->modelStack.Translate(pos.x + m_OffsetPos.x, pos.y + m_OffsetPos.y, m_OffsetPos.z + (currentIndex * 0.01f)); // Prevent Z-Fighting
	m_scene->modelStack.Rotate(rotation.y + m_OffsetRot.y, 0, 1, 0);
	m_scene->modelStack.Rotate(rotation.z + m_OffsetRot.z, 0, 0, 1);
	m_scene->modelStack.Rotate(rotation.x + m_OffsetRot.x, 1, 0, 0);
	m_scene->modelStack.Scale(scale.x + m_OffsetScale.x, scale.y + m_OffsetScale.y, scale.z + m_OffsetScale.z);
	m_scene->RenderMesh(m_scene->meshList[type], b_EnableLighting, m_Anim_CurrentFrame);
	m_scene->modelStack.PopMatrix();
}

void Wool::OnCollisionHit(GameObject * other)
{
	if (other->type == Scene::GEO_PLAYER)
	{
		m_scene->playerManager.IncreaseScore((int)scale.x * 10);
		m_scene->playerManager.IncreaseUltimateCharge((int)scale.x * 2.5f);
		m_scene->goManager.DestroyObjectQueue(this);
	}
	else if (other->type >= Scene::GEO_TERRAIN_GRASS && other->type <= Scene::GEO_TERRAIN_INVISIBLE_WALL) {
		collider.ResolveCollision(other->collider, m_scene->m_dt);
	}
}


bool Wool::Update()
{
	return true;
}