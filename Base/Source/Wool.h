#ifndef WOOL_H
#define WOOL_H
#include "GameObject.h"

class Wool :public GameObject
{
public:
	Wool(Scene*scene, int woolCount = 1);
	~Wool();

	int getWoolScore() { return m_woolCount; }

	void Render();
	void OnCollisionHit(GameObject*other);
	bool Update();
private:
	int m_woolCount;
	bool b_translation = true;;
};
#endif 