#include "SceneLevel03.h"
#include "GL\glew.h"
#include "Application.h"
#include <sstream>
#include "Player.h"

#include "MeshBuilder.h"
#include "LoadTGA.h"

#include "ParticleEmitter.h"
#include "TerrainLoader.h"
#include "ColliderFactory.h"

#include "Brick.h"
#include "Portal.h"
#include "CapturePoint.h"
#include "Ally_Warrior.h"
#include "Health_Well.h"
#include "DemonCrystal.h"

#include "Enemy_Ninja.h"
#include "Enemy_Warrior.h"
#include "Enemy_Golem.h"
#include "Enemy_Archer.h"
#include "Enemy_Demon.h"
#include "Enemy_Giant.h"

#include "SceneManager.h"
#include "SceneGameOver.h"




SceneLevel03::SceneLevel03() :SceneBase(SCENE_LEVEL03)
{
}

SceneLevel03::~SceneLevel03()
{
}

void SceneLevel03::Init()
{
	SceneBase::Init();
	//Calculating aspect ratio
	m_worldHeight = 100.f;
	m_worldWidth = m_worldHeight * (float)Application::windowWidth() / Application::windowHeight();

	TerrainLoader::LoadTerrain("level03.txt", &goManager);

	// Player
	Player* player = new Player(this);
	player->active = true;
	player->pos.Set(m_worldWidth_default * 0.3f, m_worldHeight_default * .5f, 0.f);
	// player->pos.Set(615, 55, 0.f);
	goManager.CreateObject(player);
	playerManager.SetPlayer(player);

	//Portal
	Portal* portal1 = new Portal(this, player, Portal::E_PORTAL_TYPE::WARRIOR);
	portal1->active = true;
	portal1->pos.Set(160.f, 37.f, 0.f);
	goManager.CreateObject(portal1);

	Portal* portal2 = new Portal(this, player, Portal::E_PORTAL_TYPE::ARCHER);
	portal2->active = true;
	portal2->pos.Set(176.6f, 52.8f, 0.f);
	goManager.CreateObject(portal2);

	m_Golem = new Enemy_Golem(this, player);
	m_Golem->active = false;
	m_Golem->pos.Set(251.6f, 24.4f, 0.f);
	goManager.CreateObject(m_Golem);

	m_Point1 = new CapturePoint(this);
	m_Point1->active = true;
	m_Point1->pos.Set(225.f, 31.f, 0.f);
	goManager.CreateObject(m_Point1);

	m_Point2 = new CapturePoint(this);
	m_Point2->active = true;
	m_Point2->pos.Set(445.f, 1.8f, 0.f);
	goManager.CreateObject(m_Point2);

	m_Gate1 = new Brick(this);
	m_Gate1->active = true;
	m_Gate1->type = GEO_TERRAIN_BRICK;
	m_Gate1->mass = 0;
	m_Gate1->dir.Set(0, 1, 0);
	m_Gate1->pos.Set(273.7f, 37.f, 0);
	m_Gate1->scale.x = 22;
	goManager.CreateObject(m_Gate1);

	m_Gate2 = new Brick(this);
	m_Gate2->active = true;
	m_Gate2->type = GEO_TERRAIN_BRICK;
	m_Gate2->mass = 0;
	m_Gate2->dir.Set(0, 1, 0);
	m_Gate2->pos.Set(515.f, 27.f, 0);
	m_Gate2->scale.x = 24;
	goManager.CreateObject(m_Gate2);

	m_Giant = new Enemy_Giant(this, player);
	m_Giant->active = true;
	m_Giant->pos.Set(465.f, -5.8f, 0.f);
	goManager.CreateObject(m_Giant);


	m_Core = new Portal(this, player, Portal::E_PORTAL_TYPE::WARRIOR, true);
	m_Core->active = false;
	m_Core->alpha = 0;
	m_Core->b_IsInvulnerable = true;
	m_Core->SetHealth(250);
	m_Core->m_PActivateThreshold = 0;
	m_Core->pos.Set(616.f, -23.5f, 0);
	m_Core->color = Color(1, 1, 1);
	goManager.CreateObject(m_Core);

	m_Demon = new Enemy_Demon(this, player, m_Core);
	m_Demon->active = true;
	m_Demon->pos.Set(616.f, 0, 0.f);
	goManager.CreateObject(m_Demon);

	m_DemonCrystal1 = new DemonCrystal(this, player);
	m_DemonCrystal1->active = true;
	m_DemonCrystal1->pos.Set(569.f, -29.5f, 0);

	m_DemonCrystal2 = new DemonCrystal(this, player);
	m_DemonCrystal2->active = true;
	m_DemonCrystal2->pos.Set(670.f, -29.5f, 0);

	m_DemonCrystal3 = new DemonCrystal(this, player);
	m_DemonCrystal3->active = true;
	m_DemonCrystal3->pos.Set(671.9f, 2.6f, 0);

	m_DemonCrystal4 = new DemonCrystal(this, player);
	m_DemonCrystal4->active = true;
	m_DemonCrystal4->pos.Set(567.6f, 9.2f, 0);

	goManager.CreateObject(m_DemonCrystal1);
	goManager.CreateObject(m_DemonCrystal2);
	goManager.CreateObject(m_DemonCrystal3);
	goManager.CreateObject(m_DemonCrystal4);

	Health_Well* HealthWell1 = new Health_Well(this);
	HealthWell1->active = true;
	HealthWell1->pos.Set(585.f, 68.6f, 0.f);
	goManager.CreateObject(HealthWell1);

}

void SceneLevel03::Update(double dt)
{

	//pause menu part
	//update pausemenu function
	pauseManager.UpdatePauseMenu(m_dt);

	//check if game is paused, freeze all stuff that is updating
	if (pauseManager.isPaused()) {
		return;
	}

	SceneBase::Update(dt);

	if (Application::IsKeyPressedAndReleased('C')) {
		ShowDebugInfo = !ShowDebugInfo;
	}

	if (m_Gate1 != nullptr && m_Point1->GetCaptureProgress() >= 100) {
		goManager.DestroyObject(m_Gate1);
		m_Gate1 = nullptr;
	}

	if (m_Gate2 != nullptr && m_Point2->GetCaptureProgress() >= 100) {
		goManager.DestroyObject(m_Gate2);
		m_Gate2 = nullptr;
	}
	if (playerManager.GetPlayer()->pos.x > 190.f)
		m_Golem->active = true;
	if (playerManager.GetPlayer()->pos.x > 420.f) {
		m_Giant->ActivateGiant();
	}
	if (playerManager.GetPlayer()->pos.x > 613 && playerManager.GetPlayer()->pos.y <= 35 && m_Demon->IsDemonActive() == false) {
		m_Demon->ActivateDemon(m_DemonCrystal1, m_DemonCrystal2, m_DemonCrystal3, m_DemonCrystal4);
	}

	if (m_Core->GetCurrentHealth() <= 0) {
		playerManager.IncreaseScore(200);
		SceneManager::getInstance()->changeScene(new SceneGameover("Level 03 Victory! You killed the Demon and the Core!", SceneGameover::MENU_VICTORY, SceneBase::SCENE_LEVEL03, playerManager.GetCurrentScore()));
		return;
	}

	if (Application::IsKeyPressedAndReleased('0')) {
		m_Demon->b_IsInvulnerable = false;
		m_Demon->ReduceHealth(3000);
	}

	playerManager.Update();
	goManager.DequeueObjects();
	goManager.UpdateObjects(m_dt);

	m_BGScroller.Update();
}

void SceneLevel03::Render()
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	float m_newWorldHeight = 100 * (float)Application::windowHeight() / Application::windowDefaultHeight()  * camera.GetZoomMultiplier();
	float m_newWorldWidth = m_newWorldHeight * (float)Application::windowWidth() / Application::windowHeight();

	float offset = -((m_newWorldWidth - m_worldWidth) / 2);

	// Projection matrix : Orthographic Projection
	Mtx44 projection;
	projection.SetToOrtho(offset, m_newWorldWidth + offset, 0, m_newWorldHeight, -100, 100);
	projectionStack.LoadMatrix(projection);


	// Camera matrix
	viewStack.LoadIdentity();
	viewStack.LookAt(
		camera.position.x, camera.position.y, camera.position.z,
		camera.target.x, camera.target.y, camera.target.z,
		camera.up.x, camera.up.y, camera.up.z
	);
	// Model matrix : an identity matrix (model will be at the origin)
	modelStack.LoadIdentity();

	// Render background
	m_BGScroller.RenderBackground();


	goManager.RenderObjects();
	textManager.dequeueMesh();

	playerManager.m_FurEmitter->Render();

	// Render Player UI
	textManager.RenderPlayerHud();

	if (pauseManager.isPaused()) {
		pauseManager.RenderPauseMenu();
		return;
	}

	// HUDs
	std::stringstream text;
	text << "FPS: " << Application::GetFPS();
	textManager.renderTextOnScreen(UIManager::Text{ text.str(), Color(1,1,1), UIManager::ANCHOR_TOP_RIGHT });

	textManager.dequeueText();
	textManager.reset();
}

void SceneLevel03::Exit()
{
	SceneBase::Exit();
}