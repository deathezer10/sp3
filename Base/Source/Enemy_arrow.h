#ifndef ENEMY_ARROW_H
#define ENEMY_ARROW_H
#include "GameObject.h"

class Enemy_arrow :public GameObject
{
public:
	Enemy_arrow(Scene* scene);
	~Enemy_arrow();

	bool Update();

	void fireArrow(float angle, Vector3 AIpos, Collider AIcolliderSize);
	void OnCollisionHit(GameObject* other);
	void Render();

private:
	Vector3 firingForce;

};
#endif // !ENEMY_ARROW_H

