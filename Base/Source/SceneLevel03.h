#ifndef SCENE_LEVEL03_H
#define SCENE_LEVEL03_H

#include "SceneBase.h"
#include <vector>
#include "GameObject.h"

class AI_Base;
class CapturePoint;
class Brick;
class Portal;
class Enemy_Golem;
class Enemy_Giant;
class Enemy_Demon;
class DemonCrystal;

class SceneLevel03 : public SceneBase 
{

public:
	SceneLevel03();
	~SceneLevel03();

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();


private:
	Brick* m_Gate1 = nullptr;
	Brick* m_Gate2 = nullptr;
	Brick* m_Gate3 = nullptr;
	CapturePoint* m_Point1 = nullptr;
	CapturePoint* m_Point2 = nullptr;
	Enemy_Golem* m_Golem = nullptr;
	Enemy_Giant* m_Giant = nullptr;

	Enemy_Demon* m_Demon = nullptr;
	DemonCrystal* m_DemonCrystal1 = nullptr;
	DemonCrystal* m_DemonCrystal2 = nullptr;
	DemonCrystal* m_DemonCrystal3 = nullptr;
	DemonCrystal* m_DemonCrystal4 = nullptr;

	Portal* m_Core = nullptr;

protected:



};




#endif // !SCENE_PAUSE_MENU_H

