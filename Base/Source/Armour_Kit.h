#ifndef ARMOUR_KIT_H
#define ARMOUR_KIT_H
#include "GameObject.h"

class Armour_Kit :public GameObject
{
public:
	Armour_Kit(Scene*scene);
	~Armour_Kit();

	float getArmourValue() { return m_armourValue; }

	void Render();
	void OnCollisionHit(GameObject*other);
	bool Update();
private:
	float m_armourValue = 0;
	bool b_translation = true;;
};
#endif 