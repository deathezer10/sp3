#include "Enemy_Ninja.h"
#include "Player.h"
#include "Scene.h"
#include "UI_Bar.h"
#include "GameObject.h"
#include "PlayerManager.h"
#include "SpriteAnimation.h"
#include "ColliderFactory.h"


Enemy_Ninja::Enemy_Ninja(Scene* scene, Player* player) :AI_Base(scene, Scene::GEO_ENEMYNINJA, player, 100, 10)
{
	b_EnableGravity = true;
	b_EnablePhysics = true;
	b_EnableLighting = false;

	scale.Set(9, 9, 1.f);
	collider = ColliderFactory::CreateOBBCollider(this, Vector3(3.f, 7, 1));

	m_AnimEnemyNinja = static_cast<SpriteAnimation*>(scene->meshList[Scene::GEO_ENEMYNINJA]);
	m_Anim = new Animation();
	m_Anim->Set(0, 0, 1, 1.f, true);

}

Enemy_Ninja::~Enemy_Ninja() {
	if (m_Anim)
		delete m_Anim;
}

bool Enemy_Ninja::Update() {

	if (thePlayer->pos.x > pos.x)
		b_IsFacingRight = true;
	else
		b_IsFacingRight = false;

	if (m_CurrentState == CHASE)
		AI_Chase(thePlayer, m_scene->m_dt);
	if (m_CurrentState == JUMP)
		AI_Jump(m_scene->m_dt);
	if (m_CurrentState == ATTACK)
		AI_Attack(thePlayer, m_scene->m_dt);
	if (m_CurrentState == STUN)
		AI_Stun();
	if (m_CurrentState == FLEE)
		AI_Flee(thePlayer, m_scene->m_dt);


	return true;
}

void Enemy_Ninja::AI_Chase(Player* player, double dt) {

	b_IsAttacking = false;

	float playerDistance = (pos - player->pos).Length();

	// Reached the player, attack him
	if (playerDistance <= 30 && m_scene->m_elapsedTime >= m_NextTeleportTime) { // Modify teleport range here
		m_CurrentState = ATTACK;
		m_NextTeleportTime = m_scene->m_elapsedTime + m_TeleportChargeTime;
	}
	else { // Chase player
		if (pos.x < player->pos.x && (player->pos.x - pos.x) > player->scale.x) // Right
		{
			m_force.x += 100;

			if (collider.manifold.normal.x != 0 && !is_jumping)
			{
				m_CurrentState = JUMP;
				is_jumping = true;
			}
		}
		else if (pos.x > player->pos.x && (player->pos.x - pos.x) < -player->scale.x) // Left
		{
			m_force.x += -100;
			if (collider.manifold.normal.x != 0 && !is_jumping)
			{
				m_CurrentState = JUMP;
				is_jumping = true;
			}
		}
	}

}

void Enemy_Ninja::AI_Attack(Player * player, double dt) {

	m_Anim->Set(21, 21, 1, 0.5f, true);
	m_AnimEnemyNinja->Update(m_Anim, m_scene->m_dt);

	alpha -= (float)dt / 3;

	// Charge up before teleporting
	if (m_scene->m_elapsedTime >= m_NextTeleportTime) {
		pos = player->pos;
		pos.y += player->collider.bboxSize.y * 5;

		m_CurrentState = CHASE;
		alpha = 1;

		m_NextTeleportTime = m_scene->m_elapsedTime + m_TeleportCooldownTime; // Add cooldown
	}
	else {
		b_IsAttacking = true;
	}

}

void Enemy_Ninja::Render() {

	m_OffsetPos.y = 1;

	// Idle/Run animation
	if (b_IsAttacking == false || m_CurrentState == FLEE) {
		if (vel.x < 1 && vel.x > -1) { // Standing
			m_Anim->Set(21, 21, 1, 0.5f, true);
		}
		else if (!b_IsFacingRight || m_CurrentState == FLEE) { // left
			m_Anim->Set(21, 23, 1, 0.5f, true);
		}
		else if( m_CurrentState == FLEE) { // Right
			m_Anim->Set(21, 23, 1, 0.5f, true);
		}

		m_AnimEnemyNinja->Update(m_Anim, m_scene->m_dt);
	}

	rotation.y = 180.f * b_IsFacingRight;

	glDisable(GL_CULL_FACE);

	m_Anim_CurrentFrame = m_Anim->m_currentFrame;
	GameObject::Render();

	glDisable(GL_DEPTH_TEST);
	// Render Weapon if attacking
	if (b_IsAttacking) {

		m_scene->modelStack.PushMatrix();

		if (!b_IsFacingRight)
		{
			m_scene->modelStack.Translate(pos.x - 3.f, pos.y - 0.5f, currentIndex * 0.01f);
			m_scene->modelStack.Rotate(180, 0, 1, 0);
		}
		else if (b_IsFacingRight)
		{
			m_scene->modelStack.Translate(pos.x + 3.f, pos.y - 0.5f, currentIndex * 0.01f);
		}

		if (is_attacking)
			m_scene->modelStack.Rotate(m_weaponRotation, 0.f, 0.f, 1.f);

		m_scene->modelStack.Scale(4, 4, 1);
		m_scene->meshList[Scene::GEO_SWORD]->color = color;
		m_scene->meshList[Scene::GEO_SWORD]->alpha = alpha;
		m_scene->RenderMesh(m_scene->meshList[Scene::GEO_SWORD], false);
		m_scene->meshList[Scene::GEO_SWORD]->alpha = 1;
		m_scene->modelStack.PopMatrix();
	}
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	// Render Health bar
	Vector3 barPos = pos;
	barPos.y += collider.bboxSize.y;
	Mesh** meshList = m_scene->meshList;
	UI_BAR::RenderBar(m_scene, meshList[Scene::GEO_HP_BACKGROUND], meshList[Scene::GEO_HP_FOREGROUND], meshList[Scene::GEO_HP_FRAME], barPos, Vector3(4, 0.5f), (float)GetCurrentHealth() / m_DefaultHP);

}

void Enemy_Ninja::OnReduceHealth(float damage) {
	alpha = 1;
}

void Enemy_Ninja::OnCollisionHit(GameObject * other)
{
	AI_Base::OnCollisionHit(other);

	if (m_CurrentState == FLEE)
		return;

	if (m_scene->m_elapsedTime >= m_NextMeleeTime && dynamic_cast<Player*>(other) != nullptr && thePlayer->b_isRealPlayer) {
		m_scene->playerManager.ReduceHealth(10);
		m_NextMeleeTime = m_scene->m_elapsedTime + m_MeleeCooldown;
	}

}

