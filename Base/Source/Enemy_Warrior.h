#ifndef ENEMY_WARRIOR_H
#define ENEMY_WARRIOR_H
#include"AI_Base.h"


class SwordShield;
class SpriteAnimation;
struct Animation;


class Enemy_Warrior :public AI_Base
{
public:
	Enemy_Warrior(Scene* scene, Player* player);
	~Enemy_Warrior();

	bool Update();

	void AI_Chase(Player*player, double dt);
	void AI_Attack(Player* player, double dt);

	void Render();
	void OnCollisionHit(GameObject * other);

private:
	SpriteAnimation * m_AnimEnemyWarrior;
	Animation* m_Anim;

	Vector3 m_offsetPosition;
	float m_offset;
};

#endif
