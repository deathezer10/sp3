#include "SwordShield.h"
#include "Scene.h"
#include "Player.h"
#include "Mesh.h"
#include "GL\glew.h"
#include "ColliderFactory.h"
#include "UltimateWave.h"
#include "SoundManager.h"

SwordShield::SwordShield(Scene * scene, Player* player) : GameObject(scene, Scene::GEO_SWORD)
{
	b_EnableLighting = false;
	b_isUsingUltimate = false;
	b_EnablePhysicsResponse = false;
	b_EnablePhysics = false;

	scale.Set(5, 5, 1);

	m_player = player;

	active = true;

	m_attackTimer = 0.f;

	m_damage = 25;
}

bool SwordShield::Update()
{
	pos = m_player->pos;

	collider.bboxSize.SetZero();

	if (m_player->b_isFacingRight)
	{
		rotation.y = 0;
		rotation.z = 0;
		m_offsetRotation = -90.f;
	}
	else
	{
		rotation.z = 90;
		m_offsetRotation = 180.f;
	}
	if (m_scene->m_elapsedTime > m_UltimateDuration)
		b_isUsingUltimate = false;
	

	if (m_scene->m_elapsedTime < m_attackTimer)
	{
		if (m_player->b_isFacingRight)
		{
			if (!b_completeLoop)
				m_offset -= 700 * m_scene->m_dt;
			else
				m_offset += 700 * m_scene->m_dt;

			if (m_offsetRotation > rotation.z + m_offset)
			{
				b_completeLoop = true;
			}
			else if (rotation.z + m_offset > 0.f)
				m_offset = 0.f;
		}
		else
		{
			if (!b_completeLoop)
				m_offset += 700 * m_scene->m_dt;
			else
				m_offset -= 700 * m_scene->m_dt;

			if (m_offsetRotation < rotation.z + m_offset)
			{
				b_completeLoop = true;
			}
			else if (rotation.z + m_offset < 90.f)
				m_offset = 0.f;
		}
	}
	else
	{
		m_offset = 0.f;
		b_canAttack = false;
	}
	if (m_scene->playerManager.GetShielding())
		m_offset = m_offsetRotation - rotation.z;

	if (m_scene->playerManager.GetShielding())
	{
		if (m_player->b_isFacingRight)
		{
			m_offsetPosition.x = -2.f;
			m_offsetPosition.y = sin(Math::DegreeToRadian(rotation.z + m_offset)) * 3.f + 1.f;
			rotation.y = 180.f;
		}
		else
		{
			m_offsetPosition.x = 2.f;
			m_offsetPosition.y = sin(Math::DegreeToRadian(rotation.z + m_offset + 90.f)) * 3.f + 1.f;
			rotation.y = 180.f;
		}
	}
	else
	{
		if (!m_player->b_isFacingRight)
		{
			//offsetPosition.x = cos(Math::DegreeToRadian(rotation.z + offset + 90.f)) * 3.f;
			m_offsetPosition.x = -3.f;
			m_offsetPosition.y = sin(Math::DegreeToRadian(rotation.z + m_offset + 90.f)) * 3.f + 1.f;
			rotation.y = 0.f;
		}
		else
		{
			//offsetPosition.x = cos(Math::DegreeToRadian(rotation.z + offset)) * 3.f;
			m_offsetPosition.x = 3.f;
			m_offsetPosition.y = sin(Math::DegreeToRadian(rotation.z + m_offset)) * 3.f + 1.f;
			rotation.y = 0.f;
		}
	}

	return true;
}

void SwordShield::Render()
{
	glDisable(GL_CULL_FACE);
	m_scene->modelStack.PushMatrix();
	m_scene->modelStack.Translate(pos.x + m_offsetPosition.x, pos.y + m_offsetPosition.y, currentIndex * 0.01f);
	if (m_scene->playerManager.GetShielding())
		m_scene->modelStack.Translate(0.f, -1.f, 0.f);

	if (b_isUsingUltimate)
		m_scene->meshList[type]->color = Color(0.f, 1.f, 1.f);
	else
		m_scene->meshList[type]->color = color;

	m_scene->modelStack.Rotate(rotation.y, 0, 1, 0);

	m_scene->modelStack.Rotate(rotation.z + m_offset, 0, 0, 1);
	m_scene->modelStack.Rotate(rotation.x, 1, 0, 0);
	m_scene->modelStack.Scale(scale.x, scale.y, scale.z);
	m_scene->meshList[type]->alpha = alpha;
	m_scene->RenderMesh(m_scene->meshList[type], b_EnableLighting);
	m_scene->modelStack.PopMatrix();

	m_scene->modelStack.PushMatrix();
	if (m_player->b_isFacingRight)
		m_scene->modelStack.Translate(pos.x - m_offsetPosition.x + 1.f, pos.y - 0.5f, currentIndex * 0.01f);
	else
		m_scene->modelStack.Translate(pos.x - m_offsetPosition.x - 1.f, pos.y - 0.5f, currentIndex * 0.01f);
	m_scene->modelStack.Rotate(rotation.y, 0, 1, 0);
	if (m_player->b_isFacingRight)
		m_scene->modelStack.Rotate(180, 0, 1, 0);

	if (m_scene->playerManager.GetShielding())
	{
		m_scene->meshList[m_scene->GEO_SHIELD]->color = Color(1.f, 1.f, 0.f);

		if (m_player->b_isFacingRight)
			m_scene->modelStack.Translate(m_offsetPosition.x + 1.f, 1.f, 0.f);
		else
			m_scene->modelStack.Translate(-m_offsetPosition.x + 1.f, 1.f, 0.f);
	}
	else
		m_scene->meshList[m_scene->GEO_SHIELD]->color = color;

	m_scene->modelStack.Rotate(rotation.x, 1, 0, 0);
	m_scene->modelStack.Scale(scale.x, scale.y, scale.z);
	m_scene->meshList[type]->alpha = alpha;
	m_scene->RenderMesh(m_scene->meshList[m_scene->GEO_SHIELD], b_EnableLighting);
	m_scene->modelStack.PopMatrix();
	glEnable(GL_CULL_FACE);
}

bool SwordShield::checkRange(GameObject * obj)
{
	if (b_isUsingUltimate)
		return false;
	
	this->collider = ColliderFactory::CreateOBBCollider(this, Vector3(scale.x * 2.5f, scale.y * 0.8f, 1.f));

	Vector3 direction;
	if (m_player->b_isFacingRight)
	{
		if (pos.x > obj->pos.x)
			return false;
	}
	else
	{
		if (pos.x < obj->pos.x)
			return false;
	}
	if (collider.CheckCollision(obj->collider))
	{
		if (m_player->b_isFacingRight)
		{
			obj->m_force.x = Math::RandFloatMinMax(250,1250);
		}
		else
		{
			obj->m_force.x = Math::RandFloatMinMax(-1250, -250);
		}
		
		obj->m_force.y = 150.f;

		return true;
	}
	else
		return false;
}

void SwordShield::checkTimer()
{
	if (m_scene->playerManager.GetShielding())
		return;

	if (m_scene->m_elapsedTime > m_attackTimer)
	{
		SoundManager::getInstance().Play2DSound("Swing", false, true, false);

		if (b_isUsingUltimate)
		{
			m_attackTimer = m_scene->m_elapsedTime + 1.f;
			UltimateWave *wave = new UltimateWave(m_scene);
			wave->pos.Set(pos.x + m_offsetPosition.x, pos.y + m_offsetPosition.y + 2.f, 0);
			m_scene->goManager.CreateObject(wave);

			b_completeLoop = false;
			b_canAttack = true;
		}
		else
		{
			m_attackTimer = m_scene->m_elapsedTime + 0.15f;
			b_completeLoop = false;
			b_canAttack = true;
		}
	}
	else
		b_canAttack = false;
}

bool SwordShield::useUltimate()
{
	b_isUsingUltimate = true;
 	m_UltimateDuration = m_scene->m_elapsedTime + 10.f;

	SoundManager::getInstance().Play2DSound("Ultimate2", false, true, false);

	return true;
}

int SwordShield::getAttack()
{
	if (b_canAttack)
		return m_damage;
	else
		return 0;
}

float SwordShield::getUltimateDuration()
{
	if (b_canAttack)
		return m_UltimateDuration - m_scene->m_elapsedTime;
	else
		return 0.f;
}
