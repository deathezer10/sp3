#pragma once

#include <vector>
#include "GameObject.h"


class Projectile : public GameObject {

public:
	Projectile(Scene* scene);
	~Projectile() {};

	void fireArrow(float angle, Vector3 AIpos, Collider AIcolliderSize);
	virtual bool Update();
	virtual void Render();
	virtual void OnCollisionHit(GameObject* other);

};