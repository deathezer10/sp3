#ifndef CAMERA_H
#define CAMERA_H

#include "Vector3.h"

class GameObject;

class Camera
{
public:
	static Vector3 position;
	Vector3 target;
	Vector3 up;

	Camera();
	~Camera();
	virtual void Init(const Vector3& pos, const Vector3& target, const Vector3& up);
	virtual void Update(double dt);

	GameObject* m_player = nullptr;

	float GetZoomMultiplier() { return currentZoomMultiplier; }
	void SetPrev(Vector3 pos);
	void SetZoomMutiplier(float percentage);

	float currentScrollX = 0;
	float currentScrollY = 0;

private:
	float currentZoomMultiplier = 1;

	float prevX;
	float prevY;

};

#endif