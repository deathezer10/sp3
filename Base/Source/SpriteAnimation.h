#pragma once

#include "Mesh.h"
#include <vector>



struct Animation {

	Animation() {
		this->m_currentFrame = 0;
		this->m_currentTime = 0;
	}

	void Set(int startFrame, int endFrame, int repeat, float time, bool active)
	{
		this->startFrame = startFrame;
		this->endFrame = endFrame;
		this->repeatCount = repeat;
		this->animTime = time;
		this->animActive = active;

	}

	int startFrame;
	int endFrame;
	int repeatCount;
	float animTime;
	bool ended;
	bool animActive;

	int m_currentFrame;
	float m_currentTime;

};

class SpriteAnimation : public Mesh {

public:
	SpriteAnimation(const std::string &meshName, int row, int col);
	virtual ~SpriteAnimation();

	void Update(Animation* m_anim, double dt);
	void Render(int currentFrame);

	int m_row;
	int m_col;
	
};