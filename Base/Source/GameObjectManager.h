#pragma once

#include <vector>
#include <queue>


using std::vector;

class GameObject;

class GameObjectManager {

public:

	GameObjectManager() {};
	~GameObjectManager();

	std::vector<GameObject*>::iterator GetIterator() { return m_goListIterator; }
		
	// Adds the Object into queue so that it gets added to the vector next frame
	// Used when adding Objects while iterating through m_goList
	void CreateObjectQueue(GameObject * obj);

	// Stores the Object into collection to allow interaction
	GameObject* CreateObject(GameObject* obj);

	// Destroys the Object after this frame finishes
	void DestroyObjectQueue(GameObject * obj);

	// Remove the given Object from the vector
	void DestroyObject(GameObject* obj);

	// Render all Objects in the collection into the Scene
	void RenderObjects();
	
	// Render all Objects in the collection into the Scene from last Object to the first
	void RenderObjectsReversed();
	
	// Process interactions for all Object, should be called during Update()
	void UpdateObjects(float dt);

	// Updates the current iterator, used when an element is deleted from the container to prevent other iterators from being invalidated
	void ValidateIterator(std::vector<GameObject*>::iterator it);

	// Push all pending Objects into m_goList
	void DequeueObjects();

	// Do neccessary cleaning up of memory allocated
	void Exit();

	std::vector<GameObject*> m_goList;


private:
	// Was the iterator updated?
	bool _iteratorUpdated = false;

	std::vector<GameObject*>::iterator m_goListIterator;
	std::queue<GameObject*> m_goQueue;
	std::queue<GameObject*> m_destroyQueue;

};