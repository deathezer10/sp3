#pragma once


#include "AI_Base.h"

class Player;
class SpriteAnimation;
struct Animation;


class DemonLightning : public GameObject {


public:
	DemonLightning(Scene* scene);
	~DemonLightning();

	virtual bool Update();
	virtual void Render();
	virtual void OnCollisionHit(GameObject* other);

private:
	SpriteAnimation * m_AnimLightning;
	Animation* m_Anim;

	float m_NextDamageTime = 0;

};