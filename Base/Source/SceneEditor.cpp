#include "SceneEditor.h"
#include "GL\glew.h"
#include "Application.h"
#include <sstream>
#include "FurnitureMan.h"

#include "LoadTGA.h"
#include "MeshBuilder.h"
#include "TerrainLoader.h"

#include "ColliderFactory.h"

#include "Brick.h"
#include "SceneManager.h"
#include "SceneGameOver.h"


const Vector3 m_TerrainDefaultSize(5, 5, 1);



SceneEditor::SceneEditor() :SceneBase(SCENE_EDITOR), m_BGScroller(this, SCENE_EDITOR)
{
}

SceneEditor::~SceneEditor()
{
}

void SceneEditor::Init()
{
	SceneBase::Init();
	//Calculating aspect ratio
	m_worldHeight = 100.f;
	m_worldWidth = m_worldHeight * (float)Application::windowWidth() / Application::windowHeight();

	m_FurnitureMan = new FurnitureMan(this);
	m_FurnitureMan->active = true;
	m_FurnitureMan->pos.Set(m_worldWidth * 0.5f, m_worldHeight * .5f, 0.f);
	goManager.CreateObject(m_FurnitureMan);

}

void SceneEditor::Update(double dt)
{

	// Update pausemenu function
	pauseManager.UpdatePauseMenu(m_dt);

	// Check if game is paused, freeze all stuff that is updating
	if (pauseManager.isPaused()) {
		return;
	}

	SceneBase::Update(dt);

	if (Application::IsKeyPressedAndReleased('V')) {
		ShowDebugInfo = !ShowDebugInfo;
	}

	if (Application::IsKeyPressedAndReleased(VK_F1)) {
		TerrainLoader::SaveTerrain(&goManager);
	}

	if (Application::IsKeyPressedAndReleased(VK_F2)) {
		TerrainLoader::LoadTerrain("level00.txt", &goManager);
	}

	// Camera Zoom Controls
	if (Application::IsKeyPressed(VK_SHIFT))
		m_CurrentZoom += m_dt;
	if (Application::IsKeyPressed(VK_CONTROL))
		m_CurrentZoom -= m_dt;
	if (Application::IsKeyPressedAndReleased(VK_MENU))
		m_CurrentZoom = 1;

	camera.SetZoomMutiplier(m_CurrentZoom);

	// Move speed increaser
	if (Application::IsKeyPressed('M'))
		m_MoveSpeedMultiplier += 0.10f;
	else if (Application::IsKeyPressed('N'))
		m_MoveSpeedMultiplier -= 0.10f;
	else if (Application::IsKeyPressedAndReleased('B'))
		m_MoveSpeedMultiplier = 1;

	m_MoveSpeedMultiplier = Math::Max<float>(0.05f, m_MoveSpeedMultiplier); // Prevent from going below 0.10f

	// Movement controls
	const float m_MoveSpeed = 50 * m_MoveSpeedMultiplier;

	if (Application::IsKeyPressed(VK_LEFT))
		m_FurnitureMan->vel.x = -m_MoveSpeed;
	else if (Application::IsKeyPressed(VK_RIGHT))
		m_FurnitureMan->vel.x = m_MoveSpeed;
	else if (m_FurnitureMan->b_EnableGravity == false)
		m_FurnitureMan->vel.x = 0;

	if (m_FurnitureMan->b_EnableGravity) {
		if (Application::IsKeyPressedAndReleased(VK_UP))
			m_FurnitureMan->m_force.y = 2000;
	}
	else {
		if (Application::IsKeyPressed(VK_UP))
			m_FurnitureMan->vel.y = m_MoveSpeed;
		else if (Application::IsKeyPressed(VK_DOWN))
			m_FurnitureMan->vel.y = -m_MoveSpeed;
		else if (m_FurnitureMan->b_EnableGravity == false) // Disable gravity while in editing mode
			m_FurnitureMan->vel.y = 0;
	}

	// Enable gravity for play testing?
	if (Application::IsKeyPressedAndReleased(VK_TAB) && m_SelectedObject == nullptr) {
		m_FurnitureMan->b_EnableGravity = !m_FurnitureMan->b_EnableGravity;
		m_FurnitureMan->b_EnablePhysicsResponse = !m_FurnitureMan->b_EnablePhysicsResponse;
	}

	// Invoking new objects out
	if (m_SelectedObject == nullptr && m_FurnitureMan->b_EnableGravity == false) {

		if (Application::IsKeyPressedAndReleased('1')) {
			m_SelectedObject = CreateNewObject(GEO_TERRAIN_GRASS);
		}

		if (Application::IsKeyPressedAndReleased('2')) {
			m_SelectedObject = CreateNewObject(GEO_TERRAIN_MUDDY);
		}

		if (Application::IsKeyPressedAndReleased('3')) {
			m_SelectedObject = CreateNewObject(GEO_TERRAIN_ICE);
		}

		if (Application::IsKeyPressedAndReleased('4')) {
			m_SelectedObject = CreateNewObject(GEO_TERRAIN_LAVA);
		}

		if (Application::IsKeyPressedAndReleased('5')) {
			m_SelectedObject = CreateNewObject(GEO_TERRAIN_BRICK);
		}

		if (Application::IsKeyPressedAndReleased('6')) {
			m_SelectedObject = CreateNewObject(GEO_TERRAIN_CRATE);
		}

		if (Application::IsKeyPressedAndReleased('7')) {
			m_SelectedObject = CreateNewObject(GEO_TERRAIN_INVISIBLE_WALL);
		}

	}

	if (m_SelectedObject != nullptr) {

		m_SelectedObject->pos = m_FurnitureMan->pos;

		// Switching Terrain Type
		if (Application::IsKeyPressedAndReleased('1'))
			m_SelectedObject->type = GEO_TERRAIN_GRASS;
		if (Application::IsKeyPressedAndReleased('2'))
			m_SelectedObject->type = GEO_TERRAIN_MUDDY;
		if (Application::IsKeyPressedAndReleased('3'))
			m_SelectedObject->type = GEO_TERRAIN_ICE;
		if (Application::IsKeyPressedAndReleased('4'))
			m_SelectedObject->type = GEO_TERRAIN_LAVA;
		if (Application::IsKeyPressedAndReleased('5'))
			m_SelectedObject->type = GEO_TERRAIN_BRICK;
		if (Application::IsKeyPressedAndReleased('6'))
			m_SelectedObject->type = GEO_TERRAIN_CRATE;
		if (Application::IsKeyPressedAndReleased('7'))
			m_SelectedObject->type = GEO_TERRAIN_INVISIBLE_WALL;

		// Object Scaling
		const float scaleSpeed = 30; // Speed

		if (Application::IsKeyPressed('Z')) {
			m_SelectedObject->scale.x -= scaleSpeed * m_dt;
		}
		if (Application::IsKeyPressed('C')) {
			m_SelectedObject->scale.x += scaleSpeed * m_dt;
		}
		if (Application::IsKeyPressed('S')) {
			m_SelectedObject->scale.y += scaleSpeed * m_dt;
		}
		if (Application::IsKeyPressed('X')) {
			m_SelectedObject->scale.y -= scaleSpeed * m_dt;
		}

		// Clamp the scale size
		m_SelectedObject->scale.x = Math::Max(m_SelectedObject->scale.x, 0.1f);
		m_SelectedObject->scale.y = Math::Max(m_SelectedObject->scale.y, 0.1f);

		// Object Rotation
		const float rotateSpeed = 90; // Speed

		if (Application::IsKeyPressed('A')) {
			Mtx44 rotation;
			rotation.SetToRotation(rotateSpeed, 0, 0, 1);
			m_SelectedObject->dir += rotation * m_SelectedObject->dir * m_dt;
		}
		if (Application::IsKeyPressed('D')) {
			Mtx44 rotation;
			rotation.SetToRotation(-rotateSpeed, 0, 0, 1);
			m_SelectedObject->dir += rotation * m_SelectedObject->dir * m_dt;
		}
		if (Application::IsKeyPressedAndReleased('Q')) {
			Mtx44 rotation;
			rotation.SetToRotation(15, 0, 0, 1);
			m_SelectedObject->dir += rotation * m_SelectedObject->dir;

			if (m_SelectedObject->dir.IsZero() == false)
				m_SelectedObject->dir.Normalize();
		}
		if (Application::IsKeyPressedAndReleased('E')) {
			Mtx44 rotation;
			rotation.SetToRotation(-15, 0, 0, 1);
			m_SelectedObject->dir += rotation * m_SelectedObject->dir;

			if (m_SelectedObject->dir.IsZero() == false)
				m_SelectedObject->dir.Normalize();
		}

		// Reset current Object
		if (Application::IsKeyPressed('R'))
			ResetSelectedObject();

		// Reset current Object rotation
		if (Application::IsKeyPressed('F'))
			m_SelectedObject->dir.Set(1, 0, 0);

		// Backspace to delete selected object
		if (Application::IsKeyPressedAndReleased(VK_BACK)) {
			goManager.DestroyObject(m_SelectedObject);
			m_SelectedObject = nullptr;
		}
	}

	// Select/De-select object
	if (m_FurnitureMan->b_EnableGravity == false && Application::IsKeyPressedAndReleased(VK_SPACE)) {
		if (m_SelectedObject != nullptr) { // Place Object down
			m_SelectedObject = nullptr;
		}
		else { // Find and select the object that collided
			for (auto it = goManager.m_goList.begin(); it != goManager.m_goList.end(); ++it) {
				if (*it != m_FurnitureMan && (*it)->collider.CheckCollision(m_FurnitureMan->collider)) { // Anything hit the FurnitureMan's collider?
					m_SelectedObject = *it;
					break;
				}
			}
		}
	}

	goManager.DequeueObjects();
	goManager.UpdateObjects(m_dt);


	// Show Invisible wall for now
	if (m_SelectedObject != nullptr &&m_SelectedObject->type == GEO_TERRAIN_INVISIBLE_WALL)
		m_SelectedObject->alpha = 0.5f;

	m_BGScroller.Update();

}

void SceneEditor::Render()
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	float m_newWorldHeight = 100 * (float)Application::windowHeight() / Application::windowDefaultHeight()  * camera.GetZoomMultiplier();
	float m_newWorldWidth = m_newWorldHeight * (float)Application::windowWidth() / Application::windowHeight();

	float offset = -((m_newWorldWidth - m_worldWidth) / 2);

	// Projection matrix : Orthographic Projection
	Mtx44 projection;
	projection.SetToOrtho(offset, m_newWorldWidth + offset, 0, m_newWorldHeight, -100, 100);
	projectionStack.LoadMatrix(projection);


	// Camera matrix
	viewStack.LoadIdentity();
	viewStack.LookAt(
		camera.position.x, camera.position.y, camera.position.z,
		camera.target.x, camera.target.y, camera.target.z,
		camera.up.x, camera.up.y, camera.up.z
	);
	// Model matrix : an identity matrix (model will be at the origin)
	modelStack.LoadIdentity();

	// Render background
	m_BGScroller.RenderBackground();

	goManager.RenderObjectsReversed();
	textManager.dequeueMesh();


	if (pauseManager.isPaused()) {
		pauseManager.RenderPauseMenu();
		return;
	}

	// HUDs
	std::stringstream text;
	text << "FPS: " << Application::GetFPS();
	textManager.renderTextOnScreen(UIManager::Text{ text.str(), Color(1,1,1), UIManager::ANCHOR_TOP_RIGHT });

	// Player Position
	text.str("");
	text << "Current Position: " << m_FurnitureMan->pos;
	textManager.renderTextOnScreen(UIManager::Text{ text.str(), Color(0,1,0), UIManager::ANCHOR_TOP_CENTER });

	text.str("");
	text << "Terrain Amount: " << goManager.m_goList.size() - 1;
	textManager.renderTextOnScreen(UIManager::Text{ text.str(), Color(0,1,0), UIManager::ANCHOR_BOT_RIGHT });

	text.str("");
	text << "Gravity: " << ((m_FurnitureMan->b_EnableGravity) ? "On" : "Off");
	textManager.renderTextOnScreen(UIManager::Text{ text.str(), ((m_FurnitureMan->b_EnableGravity) ? Color(0,1,0) : Color(1,0,0)), UIManager::ANCHOR_BOT_RIGHT });

	// FurnitureMan move speed
	text.str("");
	text << "Move Speed: " << m_MoveSpeedMultiplier;
	textManager.renderTextOnScreen(UIManager::Text{ text.str(), Color(0,1,0), UIManager::ANCHOR_BOT_RIGHT });

	// Selected Object
	Color objColor;
	text.str("");
	text << "Selected Object: ";
	if (m_SelectedObject == nullptr) {
		text << "Nothing";
		objColor.Set(1, 1, 1);
	}
	else {
		text << GetTerrainString(m_SelectedObject->type);
		objColor.Set(0, 1, 0);
	}
	textManager.renderTextOnScreen(UIManager::Text{ text.str(), objColor, UIManager::ANCHOR_BOT_CENTER });

	if (m_SelectedObject != nullptr) {
		text.str("");
		text << "Object Dir: " << m_SelectedObject->dir;
		textManager.renderTextOnScreen(UIManager::Text{ text.str(), Color(0,1,0), UIManager::ANCHOR_BOT_CENTER });
	}

	textManager.dequeueText();
	textManager.reset();
}

void SceneEditor::Exit()
{
	SceneBase::Exit();
}

std::string SceneEditor::GetTerrainString(unsigned type) {
	switch (m_SelectedObject->type) {
	case GEO_TERRAIN_GRASS:
		return "Grass";
	case GEO_TERRAIN_MUDDY:
		return "Muddy";
	case GEO_TERRAIN_ICE:
		return "Ice";
	case GEO_TERRAIN_LAVA:
		return "Lava";
	case GEO_TERRAIN_BRICK:
		return "Brick";
	case GEO_TERRAIN_CRATE:
		return "Crate";
	case GEO_TERRAIN_INVISIBLE_WALL:
		return "Invisible Wall";
	default:
		return "INVALID TERRAIN ENUM";
	}
}

GameObject * SceneEditor::CreateNewObject(Scene::GEOMETRY_TYPE type) {


	Brick* obj = new Brick(this);
	obj->active = true;
	obj->type = type;
	obj->bounciness = 0;
	obj->mass = 0;
	obj->scale = m_TerrainDefaultSize;
	obj->collider = ColliderFactory::CreateOBBCollider(obj, m_TerrainDefaultSize);
	obj->pos = m_FurnitureMan->pos;
	goManager.CreateObject(obj);

	return obj;
}

void SceneEditor::ResetSelectedObject() {

	if (m_SelectedObject != nullptr) {

		m_SelectedObject->scale = m_TerrainDefaultSize;
		m_SelectedObject->collider.bboxSize = m_TerrainDefaultSize;
		m_SelectedObject->dir = Vector3(1, 0, 0);

	}

}


