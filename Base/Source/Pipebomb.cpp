#include "GL\glew.h"
#include "PipeBomb.h"

#include "SceneBase.h"
#include "SceneManager.h"
#include "Application.h"

#include "ColliderFactory.h"
#include "AI_Base.h"


PipeBomb::PipeBomb(Scene * scene) : Player(scene)
{
	b_EnableGravity = true;
	b_EnableLighting = false;
	b_EnablePhysics = true;

	b_isRealPlayer = false;

	type = Scene::GEO_PIPEBOMB;

	MAX_VELOCITY.Set(40, 40, 0);

	scale.Set(3.5f, 3.5f, 1.f);

	b_ForceBounciness = true;
	b_ForceFriction = true;

	bounciness = 0.6f;
	friction = 1;

	collider = ColliderFactory::CreateOBBCollider(this, Vector3(scale.x, scale.y, 1));

	m_NextVanishTime = m_scene->m_elapsedTime + 7.5f;

}

bool PipeBomb::Update()
{
	//Calculating aspect ratio
	SceneBase* _scene = (SceneBase*)m_scene;

	if (m_scene->m_elapsedTime >= m_NextVanishTime) {
		m_scene->goManager.DestroyObjectQueue(this);
		return false;
	}

	for (auto it : _scene->goManager.m_goList) {

		AI_Base* ai = dynamic_cast<AI_Base*>(it);			

		if (ai && ai->IsAlly() == false) {
			ai->thePlayer = this;
			ai->MAX_VELOCITY.x = 30;
			ai->color.Set(1, 0, 0);
		}
	}

	return true;
}

void PipeBomb::Render() {

	glDisable(GL_CULL_FACE);
	GameObject::Render();
	glEnable(GL_CULL_FACE);

}

void PipeBomb::OnCollisionHit(GameObject * other) {
}

void PipeBomb::OnDestroy(){

	for (auto it : m_scene->goManager.m_goList) {

		AI_Base* ai = dynamic_cast<AI_Base*>(it);

		if (ai && ai->IsAlly() == false) {
			ai->thePlayer = m_scene->playerManager.GetPlayer();
			ai->MAX_VELOCITY.x = ai->GetDefaultSpeed();
			ai->color.Set(1, 1, 1);
		}
	}

}
