#include "Application.h"
#include "GameObject.h"
#include "GameObjectManager.h"

#include "Scene.h"
#include "Mesh.h"
#include "UIManager.h"
#include "SceneManager.h"


GameObjectManager::~GameObjectManager() {
	Exit();
}

void GameObjectManager::CreateObjectQueue(GameObject * obj) {
	m_goQueue.push(obj);
}

GameObject* GameObjectManager::CreateObject(GameObject * obj) {
	m_goList.push_back(obj);
	return obj;
}

void GameObjectManager::DestroyObjectQueue(GameObject * obj) {
	m_destroyQueue.push(obj);
}

void GameObjectManager::DestroyObject(GameObject * obj) {
	// Find the GameObject to delete
	for (auto &it = m_goList.begin(); it != m_goList.end();) {
		if (*it == obj) {
			(*it)->OnDestroy(); // Invoke OnDestroy before deleting
			it = m_goList.erase(it);
			ValidateIterator(it); // return the updated iterator
			delete obj; // de-allocate memory
			return;
		}
		else {
			++it;
		}
	}
}

void GameObjectManager::UpdateObjects(float dt) {

	GameObject* obj;
	m_goListIterator = m_goList.begin();

	for (m_goListIterator; m_goListIterator != m_goList.end();) {

		obj = *m_goListIterator;

		if (obj->active) {

			// Update() return false if the Object gets deleted
			if (obj->Update() == true) {

				// Update physics only when enabled
				if (obj->b_EnablePhysics) {

					// Apply velocity, angular acceleration, momentum, etc
					obj->UpdatePhysics();

					// If Physics is enabled, check for collision and then resolve it
					for (auto &it = m_goListIterator + 1; it != m_goList.end(); ++it) {
						GameObject* obj2 = *it;

						if (obj2->active && obj2->b_EnablePhysics == true && obj->collider.CheckCollision(obj2->collider)) {

							// Call collision hit on both objects
							obj->OnCollisionHit(obj2);
							obj2->OnCollisionHit(obj);

							if (obj->b_EnableTrigger == false && obj2->b_EnableTrigger == false)
								obj->collider.ResolveCollision(obj2->collider, dt);
						}

					}

				}

				obj->WrapObject();
				obj->ReboundObject();
			}

		}

		if (!_iteratorUpdated) {
			++m_goListIterator;
		}
		else {
			// skip increment since vector.erase() already returned the value of the next valid iterator
			_iteratorUpdated = false;
		}
	}

}

void GameObjectManager::RenderObjects() {
	Scene* _scene = SceneManager::getInstance()->getCurrentScene();

	// render all objects in map
	for (auto &i : m_goList) {
		if (i->active) {
	 		i->Render();

			// Render Box Collider of all Objects onto screen, Mesh: Cube Size: 0.5f, 0.5f, 0.5f
			if (Scene::ShowDebugInfo == true) {

				if (i->collider.m_ColliderType == Collider::OBB) {

					Vector3 pos = i->pos;
					pos.z = 50;

					// Green Color
					_scene->meshList[Scene::GEO_CUBE]->color.Set(0, 1, 0);

					// Render the Wire framed cube
					_scene->textManager.queueRenderMesh(UIManager::MeshQueue{

						_scene->meshList[Scene::GEO_CUBE],
						pos,
						Vector3(0, 0, Math::RadianToDegree(atan2(i->dir.y, i->dir.x))),
						Vector3(i->collider.bboxSize.x, i->collider.bboxSize.y, i->collider.bboxSize.z), false , true

					});
				}
			}
		}
	}
}

void GameObjectManager::RenderObjectsReversed() {
	Scene* _scene = SceneManager::getInstance()->getCurrentScene();

	// render all objects in map
	for (auto &it = m_goList.rbegin(); it != m_goList.rend(); ++it) {
		if ((*it)->active) {
			(*it)->Render();

			// Render Box Collider of all Objects onto screen, Mesh: Cube Size: 0.5f, 0.5f, 0.5f
			if (Scene::ShowDebugInfo == true) {

				if ((*it)->collider.m_ColliderType == Collider::OBB) {

					Vector3 pos = (*it)->pos;
					pos.z = 50;

					// Green Color
					_scene->meshList[Scene::GEO_CUBE]->color.Set(0, 1, 0);

					// Render the Wire framed cube
					_scene->textManager.queueRenderMesh(UIManager::MeshQueue{

						_scene->meshList[Scene::GEO_CUBE],
						pos,
						Vector3(0, 0, Math::RadianToDegree(atan2((*it)->dir.y, (*it)->dir.x))),
						Vector3((*it)->collider.bboxSize.x, (*it)->collider.bboxSize.y, (*it)->collider.bboxSize.z), false , true

					});
				}
			}
		}
	}
}

void GameObjectManager::ValidateIterator(std::vector<GameObject*>::iterator it) {
	m_goListIterator = it;
	_iteratorUpdated = true;
}

void GameObjectManager::DequeueObjects() {
	while (!m_destroyQueue.empty()) {
		DestroyObject(m_destroyQueue.front());
		m_destroyQueue.pop();
	}
	while (!m_goQueue.empty()) {
		m_goList.push_back(m_goQueue.front());
		m_goQueue.pop();
	}
}

void GameObjectManager::Exit()
{
	DequeueObjects();

	if (m_goList.empty() == false) {
		for (auto &i : m_goList)
			delete i;
	}
	m_goList.clear();

	GameObject::index = 0;
}
