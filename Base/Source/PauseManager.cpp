#include "PauseManager.h"
#include "Application.h"
#include "SceneBase.h"
#include "Scene.h"
#include "SceneManager.h"
#include "Application.h"

#include "MeshBuilder.h"
#include <GLFW/glfw3.h>
#include <sstream>

#include "Main_Menu.h"
#include "SceneLevel01.h"
#include "SceneLevel02.h"
#include "SceneLevel03.h"

PauseManager::PauseManager(Scene * scene)
{
	_scene = scene;
}

PauseManager::~PauseManager()
{
}

void PauseManager::UpdatePauseMenu(float dt)
{
	GLFWwindow* window = glfwGetCurrentContext();

	// Check for Key Up
	if (!glfwGetKey(window, PAUSE_KEY))
	{
		_canPauseButtonPress = true;
	}

	// Check if pause button is pressed
	if (glfwGetKey(window, PAUSE_KEY) && _canPauseButtonPress == true) {
		_paused = !_paused; // Toggle between true and false
		_currentOption = 0;

		_canPauseButtonPress = false;
	}


	if (Application::IsKeyPressedAndReleased(VK_RETURN))
	{
		_canExitControlButtonPress = true;
	}

	if (renderControls == 1&& _canExitControlButtonPress== true && Application::IsKeyPressedOnce(VK_RETURN))
	{
		
			_paused = false;
			_canExitControlButtonPress = false;


	}
	
	// Nothing to do here since game is not paused
	if (_paused == false)
	{
		renderControls = 0;
		return;
	}



	if (!glfwGetKey(window, GLFW_KEY_UP) && !glfwGetKey(window, GLFW_KEY_DOWN)) {
		_canMenuPress = true;
	}

	if (glfwGetKey(window, GLFW_KEY_UP) && _canMenuPress) {
		--_currentOption;
		_canMenuPress = false;
	}

	if (glfwGetKey(window, GLFW_KEY_DOWN) && _canMenuPress) {
		++_currentOption;
		_canMenuPress = false;
	}

	_currentOption = Math::Clamp<short>(_currentOption, 0, 4);
	if (renderControls == 0)
	{
		if (glfwGetKey(window, GLFW_KEY_ENTER)) {
			switch (_currentOption) {

			case 0: // Resume
				_paused = false;
				break;

			case 1: // Restart
				SceneManager::getInstance()->changeScene(
					SceneManager::getInstance()->CreateScene(((unsigned)_scene->sceneType))
				);// Restart the scene
				return;

			case 2: // Main Menu
				SceneManager::getInstance()->changeScene(new Main_Menu());
				return;

			case 3:
				renderControls = 1;
				return;
				break;

			case 4: // Quit
				glfwSetWindowShouldClose(glfwGetCurrentContext(), true);
				break;

			}
		}
		
		

	}




}

void PauseManager::RenderPauseMenu()
{
	// Nothing to do here since game is not paused
	if (_paused == false)
		return;

	else if (_paused == true)
	{
		const Color color(0, 0, 0);

		string option1t = option1;
		string option2t = option2;
		string option3t = option3;
		string option4t = option4;
		string option5t = option5;
		switch (_currentOption) {

		case 0: // Resume
			option1t = ">" + option1 + "<";
			break;

		case 1: // Restart
			option2t = ">" + option2 + "<";
			break;

		case 2: // Main Menu
			option3t = ">" + option3 + "<";
			break;

		case 3: // controls
			option4t = ">" + option4 + "<";
			break;

		case 4: // Quit
			option5t = ">" + option5 + "<";
			break;

		}



		if (renderControls == 1)
		{
			//to do:render the controls page
			_scene->textManager.RenderMeshOnScreen(_scene->meshList[Scene::GEO_CONTROLS], ((float)Application::windowWidth() / 20), ((float)Application::windowHeight() / 20), Vector3(0, 0, 0), Vector3(80, 60, 1));
			_scene->textManager.reset();
		}
		else
		{
			//render the pause menu
			glDisable(GL_DEPTH_TEST);
			_scene->textManager.RenderMeshOnScreen(_scene->meshList[Scene::GEO_P_BACKGROUND], ((float)Application::windowWidth() / 20) - 10, ((float)Application::windowHeight() / 20) - 9, Vector3(0, 0, 0), Vector3(10, 16, 1));
			glEnable(GL_DEPTH_TEST);


			_scene->textManager.renderTextOnScreen(UIManager::Text{ title, color, UIManager::ANCHOR_CENTER_CENTER });
			_scene->textManager.renderTextOnScreen(UIManager::Text{ "", color, UIManager::ANCHOR_CENTER_CENTER });
			_scene->textManager.renderTextOnScreen(UIManager::Text{ option1t, color, UIManager::ANCHOR_CENTER_CENTER });
			_scene->textManager.renderTextOnScreen(UIManager::Text{ "", color, UIManager::ANCHOR_CENTER_CENTER });
			_scene->textManager.renderTextOnScreen(UIManager::Text{ option2t, color, UIManager::ANCHOR_CENTER_CENTER });
			_scene->textManager.renderTextOnScreen(UIManager::Text{ "", color, UIManager::ANCHOR_CENTER_CENTER });
			_scene->textManager.renderTextOnScreen(UIManager::Text{ option3t, color, UIManager::ANCHOR_CENTER_CENTER });
			_scene->textManager.renderTextOnScreen(UIManager::Text{ "", color, UIManager::ANCHOR_CENTER_CENTER });
			_scene->textManager.renderTextOnScreen(UIManager::Text{ option4t, color, UIManager::ANCHOR_CENTER_CENTER });
			_scene->textManager.renderTextOnScreen(UIManager::Text{ "", color, UIManager::ANCHOR_CENTER_CENTER });
			_scene->textManager.renderTextOnScreen(UIManager::Text{ option5t, color, UIManager::ANCHOR_CENTER_CENTER });
			_scene->textManager.reset();
		}


	}



}



bool PauseManager::isPaused()
{
	return _paused;
}
