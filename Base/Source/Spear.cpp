#include "Spear.h"
#include "Scene.h"
#include "Player.h"
#include "Mesh.h"
#include "ColliderFactory.h"
#include "GL\glew.h"
#include "UltimateProjectile.h"
#include "SoundManager.h"

Spear::Spear(Scene * scene, Player* player) : GameObject(scene, Scene::GEO_SPEAR)
{
	b_EnableLighting = false;
	b_EnablePhysics = false;

	scale.Set(15, 15, 1);

	collider = ColliderFactory::CreateOBBCollider(this, Vector3(scale.x * 1.75f, scale.y * 0.3f, scale.z));
	m_player = player;

	active = true;
	m_attackTimer = 0.f;

	m_damage = 40;

	b_usingUlt = false;

	b_attackAnimation = false;
}

bool Spear::Update()
{
	pos = m_player->pos;

	if (m_player->b_isFacingRight)
		m_offsetPos = pos.x + 200 * m_scene->m_dt + 3.f;
	else
		m_offsetPos = pos.x - 200 * m_scene->m_dt - 3.f;

	if (m_player->b_isFacingRight)
		rotation.y = 0;
	else
		rotation.y = 180;

	if (!b_canAttack && !b_attackAnimation || b_usingUlt)
	{
		if (m_player->b_isFacingRight)
		{
			m_offset.x -= 35 * m_scene->m_dt;
			m_offset.y = -1.5f;

			if (m_offset.x < 2.f)
				m_offset.x = 2.f;

		}
		else
		{
			m_offset.x += 35 * m_scene->m_dt;
			m_offset.y = -1.5f;

			if (m_offset.x > -2.f)
				m_offset.x = -2.f;
		}
	}
	else
	{
		if (m_scene->m_elapsedTime > m_attackTimer)
			b_attackAnimation = true;

		if (b_attackAnimation)
			b_canAttack = true;

		if (b_canAttack && !b_attackAnimation)
			b_completeLoop = false;

		if (m_player->b_isFacingRight)
		{
			if (!b_completeLoop && b_attackAnimation)
				m_offset.x += 35 * m_scene->m_dt;
			else if (b_attackAnimation)
				m_offset.x -= 35 * m_scene->m_dt;
			else if (!b_attackAnimation)
				m_offset.x = 2.f;

			if (pos.x + m_offset.x > m_offsetPos)
			{
				SoundManager::getInstance().Play2DSound("Stab", false, true, false);
				b_completeLoop = true;
				m_offset.x = m_offsetPos - pos.x;
				b_atTip = true;
			}
			else if (b_completeLoop && m_offset.x <= 2.f)
			{
				m_offset.x = 2.f;
				m_attackTimer = m_scene->m_elapsedTime + 0.15f;
				b_attackAnimation = false;
				b_canAttack = false;
			}
			else
				b_atTip = false;
		}
		else
		{
			if (!b_completeLoop && b_attackAnimation)
				m_offset.x -= 35 * m_scene->m_dt;
			else if (b_attackAnimation)
				m_offset.x += 35 * m_scene->m_dt;
			else
				m_offset.x = -2.f;

			if (pos.x + m_offset.x < m_offsetPos)
			{
				SoundManager::getInstance().Play2DSound("Stab", false, true, false);
				b_completeLoop = true;
				m_offset.x = m_offsetPos - pos.x;
				b_atTip = true;
			}
			else if (b_completeLoop && m_offset.x >= -2.f)
			{
				m_offset.x = -2.f;
				m_attackTimer = m_scene->m_elapsedTime + 0.15f;
				b_attackAnimation = false;
				b_canAttack = false;
			}
			else
				b_atTip = false;
		}
	}

	b_usingUlt = false;

	return true;
}

void Spear::Render()
{
	glDisable(GL_CULL_FACE);
	m_scene->modelStack.PushMatrix();
	m_scene->modelStack.Translate(pos.x + m_offset.x, pos.y + m_offset.y, currentIndex * 0.01f);
	m_scene->modelStack.Rotate(rotation.y, 0, 1, 0);
	m_scene->modelStack.Rotate(rotation.z, 0, 0, 1);
	m_scene->modelStack.Rotate(rotation.x, 1, 0, 0);
	m_scene->modelStack.Scale(scale.x, scale.y, scale.z);
	m_scene->meshList[type]->color = color;
	m_scene->meshList[type]->alpha = alpha;
	m_scene->RenderMesh(m_scene->meshList[type], b_EnableLighting);
	m_scene->modelStack.PopMatrix();
	glEnable(GL_CULL_FACE);
}

bool Spear::checkRange(GameObject * obj)
{
	Vector3 direction;
	if (m_player->b_isFacingRight)
	{
		if (pos.x > obj->pos.x)
			return false;
	}
	else
	{
		if (pos.x < obj->pos.x)
			return false;
	}
	if (collider.CheckCollision(obj->collider) && b_completeLoop && b_atTip)
	{
		if (m_player->b_isFacingRight)
			obj->m_force.x = Math::RandFloatMinMax(1000, 1500);
		else
			obj->m_force.x = Math::RandFloatMinMax(-1500, -1000);


		obj->m_force.y = 500.f;

		return true;
	}
	else
		return false;
}

void Spear::checkTimer()
{
	b_canAttack = true;
	
	if (!b_attackAnimation)
		b_completeLoop = false;

}

bool Spear::useUltimate()
{
	b_usingUlt = true;
	UltimateProjectile *bolt = new UltimateProjectile(m_scene);
	bolt->pos.Set(pos.x + m_offset.x, pos.y + 1.f, 0);
	if (m_player->b_isFacingRight)
		bolt->vel.Set(50, 0.f, 0.f);
	else
		bolt->vel.Set(-50, 0.f, 0.f);
	bolt->b_EnableGravity = false;
	bolt->vel.x += m_player->vel.x;
	Vector3 offset;
	offset = bolt->vel.Normalized() * (m_player->collider.bboxSize);
	bolt->pos += offset;
	m_scene->goManager.CreateObject(bolt);

	SoundManager::getInstance().Play2DSound("Ultimate1", false, true, false);

	return true;
}

int Spear::getAttack()
{
	return m_damage;
}

float Spear::getUltimateDuration()
{
	return 0.0f;
}
