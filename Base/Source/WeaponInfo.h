#ifndef WEAPONINFO_H
#define WEAPONINFO_H

class GameObject;

class WeaponInfo
{
public:
	WeaponInfo();
	virtual ~WeaponInfo() {};

	virtual bool Update() = 0;

	virtual void Render() = 0;

	virtual bool checkRange(GameObject* obj) = 0;

	virtual void checkTimer() = 0;

	virtual bool useUltimate() = 0;

	virtual int getAttack() = 0;

	virtual float getUltimateDuration() = 0;

	float m_angle;

	bool b_canAttack;

protected:
	float m_attackTimer;
	int m_damage;
};

#endif