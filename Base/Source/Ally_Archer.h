#ifndef ALLY_ARCHER_H
#define ALLY_ARCHER_H
#include"AI_Base.h"

class SpriteAnimation;
struct Animation;


class Ally_Archer :public AI_Base
{
public:
	Ally_Archer(Scene* scene,Player* player) ;
	~Ally_Archer();

	bool Update();


	//need ai pointer to attack enemy ai
	//void AI_Attack(AI_Base*AI, double dt);
	//void Attack(AI_Base* AI,double dt);
	void AI_Jump(double dt);
	void Engage(AI_Base* AI, double dt);
	void Intercept(AI_Base* AI, double dt);
	void AI_Chase(Player*player, double dt);
    void Render();
	void OnCollisionHit(GameObject * other);


private:
	SpriteAnimation * m_AnimAllyWarrior;
	Animation* m_Anim;

	Vector3 m_offsetPosition;
	float m_offset;
	float threshold = 50.f;
	float Distlimit = 1.f;
	AI_Base* m_CurrentTarget = nullptr;
	Vector3 ppos;
	Vector3 pscale;
	AI_STATES m_CurrentState = CHASE;
	AI_STATES m_PreviousState;
	float m_NextDamageTime = 0;
	float m_firingAngle = 0.0f;
	float m_attackCoolDown = 1.5f;

	// To use at spawning of darts
	bool b_HitSomething;
	float m_spawnTime;
	float m_lifeTime;

	// To lock direction of tranq shooter at contruction
	bool b_playerDir;
};

#endif
