#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include "SceneBase.h"
#include <vector>
#include "GameObject.h"

class SceneLevel01;

class Main_Menu : public SceneBase
{

public:
	Main_Menu();
	~Main_Menu();

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();


private:
	int mainChoice;
	int shopChoice;
	int levelChoice;

	bool Dkeypressed;
	bool Ukeypressed;
	bool Lkeypressed;
	bool Rkeypressed;
	bool EnterKeypressed;

	bool mainMenu;
	bool levelMenu;
	bool shopMenu;
	bool instructions;
	bool Credits;
	bool translateShopMenu;
	bool translateLevelMenu;
	bool translateCreditsMenu;
	bool translateInstruction;
};




#endif // !SCENE_PAUSE_MENU_H

