#pragma once


#include "AI_Base.h"

class Player;


class DemonSpear : public GameObject {


public:
	DemonSpear(Scene* scene, Vector3 startPos);
	~DemonSpear() {};

	virtual bool Update();
	virtual void Render();
	virtual void OnCollisionHit(GameObject* other);

private:
	Vector3 m_InitialPos;

	float m_IndicatorAlpha = 0;

	bool b_IsLaunched = false;

};