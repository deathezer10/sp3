#include "SoundManager.h"

using namespace irrklang;


SoundManager::SoundManager() {
	m_SoundEngine = createIrrKlangDevice();

	if (!m_SoundEngine)
		throw 0;
}

SoundManager::~SoundManager() {
	m_SoundMap.clear();

	if (m_SoundEngine) {
		m_SoundEngine->drop();
	}
}

void SoundManager::AddSound(std::string soundName, std::string filePath) {
	if (m_SoundMap.count(soundName) == 0)
		m_SoundMap.insert(std::make_pair(soundName, filePath));
}

void SoundManager::StopAllSound(){
	m_SoundEngine->stopAllSounds();
}

ISound* SoundManager::Play2DSound(string soundName, bool loop, bool ignorePlayback, bool track) {
	ISound* file = nullptr;
	string soundPath = m_SoundMap.at(soundName);

	if (m_SoundEngine->isCurrentlyPlaying(soundPath.c_str()) == false || ignorePlayback == true) {
		 file = m_SoundEngine->play2D(soundPath.c_str(), loop, false, track);
	}

	return file;
}
