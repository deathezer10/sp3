#include "Enemy_Demon.h"
#include "Player.h"
#include "Scene.h"
#include "UI_Bar.h"
#include "GameObject.h"
#include "PlayerManager.h"
#include "SpriteAnimation.h"
#include "ColliderFactory.h"
#include "Portal.h"

#include "DemonCrystal.h"
#include "DemonFireball.h"
#include "DemonSpear.h"
#include "DemonLightning.h"


Enemy_Demon::Enemy_Demon(Scene* scene, Player* player, Portal* deathThrone) :AI_Base(scene, Scene::GEO_DEMONSHEEPBOSS, player, 2000, 0)
{
	b_EnableGravity = false;
	b_EnableTrigger = true;
	b_EnablePhysicsResponse = false;
	b_EnablePhysics = true;
	b_EnableLighting = false;

	mass = 0;

	scale.Set(25, 15, 1.f);
	collider = ColliderFactory::CreateOBBCollider(this, scale);

	// Forcefield alpha 0
	m_scene->meshList[Scene::GEO_ENEMYDEMON_FORCEFIELD]->alpha = 0;

	m_AnimEnemyDemon = static_cast<SpriteAnimation*>(scene->meshList[Scene::GEO_DEMONSHEEPBOSS]);
	m_Anim = new Animation();
	m_Anim->Set(0, 0, 1, 1.f, true);

	bounciness = 0;
	friction = 1;

	alpha = 0;

	m_DeathThrone = deathThrone;

	b_IsInvulnerable = true;

	m_DropChance = 0;
}

Enemy_Demon::~Enemy_Demon() {
/*
	if (m_Laser1)
		delete m_Laser1;

	if (m_Laser2)
		delete m_Laser2;
*/
	if (m_Anim)
		delete m_Anim;
}

bool Enemy_Demon::Update() {

	if (b_IsDemonAppearing) {

		// Initial spawning of demon
		if (alpha < 1 || m_ForcefieldAlpha < 1) {
			alpha += m_scene->m_dt / 3;
			m_ForcefieldAlpha += m_scene->m_dt / 3;
		}
		else {
			// BOSS's Init
			m_CurrentCrystal = 1;
			b_EnableTrigger = false;
			b_IsDemonActive = true;
			b_IsDemonAppearing = false;
			m_CurrentSkill = FIREBALL;
			m_CurrentDemonState = FIREBALL;
			m_DemonNextFireballTime = m_scene->m_elapsedTime + 3;

			m_DemonNextStunThreshold = m_DefaultHP * 0.75f;
		}
	}
	else if (b_IsDemonActive) {

		if (m_CurrentDemonState != STUN) {
			if (m_CurrentCrystal == 1) {

				m_Crystal1->b_IsCrystalActive = true;

				if (m_Crystal1->GetCurrentHealth() > 0)
					b_IsForcefieldActive = true;
				else {
					StunDemon();
				}

			}
			else if (m_CurrentCrystal == 2) {

				m_Crystal2->b_IsCrystalActive = true;

				if (m_Crystal2->GetCurrentHealth() > 0)
					b_IsForcefieldActive = true;
				else {
					StunDemon();
				}

			}
			else if (m_CurrentCrystal == 3) {
				m_Crystal3->b_IsCrystalActive = true;

				if (m_Crystal3->GetCurrentHealth() > 0)
					b_IsForcefieldActive = true;
				else {
					StunDemon();
				}
			}
			else if (m_CurrentCrystal == 4) {
				m_Crystal4->b_IsCrystalActive = true;

				if (m_Crystal4->GetCurrentHealth() > 0)
					b_IsForcefieldActive = true;
				else {
					StunDemon();
				}
			}
		}

		switch (m_CurrentDemonState) {

		case Enemy_Demon::IDLE:
			if (m_scene->m_elapsedTime >= m_DemonNextSkillTime) {
				m_CurrentSkill = Math::Wrap<int>(++m_CurrentSkill, FIREBALL, LASER);
				m_CurrentDemonState = (DEMON_STATE)m_CurrentSkill;
			}
			break;

		case Enemy_Demon::STUN:
			Stunned();
			break;

		case Enemy_Demon::FIREBALL:
			SkillFireball();
			break;

		case Enemy_Demon::SPEAR:
			SkillSpear();
			break;

		case Enemy_Demon::LASER:
			SkillLaser();
			break;

		}

	}


	return true;
}

void Enemy_Demon::ActivateDemon(DemonCrystal * crystal1, DemonCrystal * crystal2, DemonCrystal * crystal3, DemonCrystal * crystal4) {

	if (!crystal1 || !crystal2 || !crystal3 || !crystal4)
		throw 0;

	m_Crystal1 = crystal1;
	m_Crystal2 = crystal2;
	m_Crystal3 = crystal3;
	m_Crystal4 = crystal4;

	b_IsDemonAppearing = true;
}

void Enemy_Demon::StunDemon() {
	// Crystal died
	b_IsForcefieldActive = false;
	b_IsInvulnerable = false;
	m_CurrentDemonState = STUN;
}

void Enemy_Demon::Stunned() {

	// End the laser first
	if (m_Laser1 != nullptr && m_Laser2 != nullptr) {
		m_CurrentLaserScale = 0;
		m_CurrentLaserTime = 0;

		m_scene->goManager.DestroyObjectQueue(m_Laser1);
		m_scene->goManager.DestroyObjectQueue(m_Laser2);

		m_Laser1 = nullptr;
		m_Laser2 = nullptr;
	}

	if (m_CurrentHP < m_DemonNextStunThreshold) {
		b_IsForcefieldActive = true;
		b_IsInvulnerable = true;
		m_CurrentCrystal++; // Increment the current crystal once recovered from stun
		m_CurrentDemonState = IDLE;
		m_DemonNextStunThreshold = m_DefaultHP - (m_CurrentCrystal * (m_DefaultHP * 0.25f));
	}
}

void Enemy_Demon::SkillFireball() {

	if (m_DemonFireballCount < m_DemonFireballMax && m_scene->m_elapsedTime > m_DemonNextFireballTime) {

		DemonFireball* ball = new DemonFireball(m_scene, thePlayer, thePlayer->pos - pos);
		ball->pos = pos;
		m_scene->goManager.CreateObjectQueue(ball);

		m_DemonFireballCount++;
		m_DemonNextFireballTime = m_scene->m_elapsedTime + m_DemonFireballInterval; // Cooldown		
	}

	if (m_DemonFireballCount >= m_DemonFireballMax) {
		m_DemonFireballCount = 0;
		m_CurrentDemonState = IDLE;
		m_DemonNextSkillTime = m_scene->m_elapsedTime + 3;
	}

}

void Enemy_Demon::SkillSpear() {

	// -3 to 3
	for (int i = -m_DemonSpearCount / 2; i < m_DemonSpearCount / 2; ++i) {

		Vector3 spearPos = pos;
		spearPos.x += i * Math::RandFloatMinMax(10, 30);

		DemonSpear* spear = new DemonSpear(m_scene, spearPos);
		m_scene->goManager.CreateObjectQueue(spear);

	}

	m_CurrentDemonState = IDLE;
	m_DemonNextSkillTime = m_scene->m_elapsedTime + 6;
}

void Enemy_Demon::SkillLaser() {

	if (m_Laser1 == nullptr && m_Laser2 == nullptr) {

		m_Laser1 = new DemonLightning(m_scene);
		m_Laser1->pos = pos;
		m_scene->goManager.CreateObjectQueue(m_Laser1);

		m_Laser2 = new DemonLightning(m_scene);
		m_Laser2->pos = pos;
		m_Laser2->dir.Set(0, 1, 0);
		m_scene->goManager.CreateObjectQueue(m_Laser2);

	}
	else {

		if (m_CurrentLaserScale < 150) {
			m_CurrentLaserScale += m_scene->m_dt * 75;

			m_Laser1->scale.y = m_CurrentLaserScale;
			m_Laser2->scale.y = m_CurrentLaserScale;
		}
		else {

			Mtx44 angle;
			angle.SetToRotation(10 * m_scene->m_dt, 0, 0, 1);
			m_Laser1->dir = angle * m_Laser1->dir;
			m_Laser2->dir = angle * m_Laser2->dir;

			if (m_CurrentLaserTime < 10) // How long it lasts
				m_CurrentLaserTime += m_scene->m_dt;
			else {
				// Finished rotating
				m_CurrentLaserScale = 0;
				m_CurrentLaserTime = 0;

				m_scene->goManager.DestroyObjectQueue(m_Laser1);
				m_scene->goManager.DestroyObjectQueue(m_Laser2);

				m_Laser1 = nullptr;
				m_Laser2 = nullptr;

				m_CurrentDemonState = IDLE;
				m_DemonNextSkillTime = m_scene->m_elapsedTime + 5;

			}

		}

	}

}

void Enemy_Demon::Render() {

	// Animation
	if (vel.x < 1 && vel.x > -1) { // Standing
		m_Anim->Set(0, 0, 1, 0.5f, true);
	}
	else if (!b_IsFacingRight) { // left
		m_Anim->Set(54, 65, 1, 0.5f, true);
	}
	else { // Right
		m_Anim->Set(54, 56, 1, 0.5f, true);
	}

	m_AnimEnemyDemon->Update(m_Anim, m_scene->m_dt);

	if (b_IsForcefieldActive) {

		m_OffsetPos.y = cos(m_scene->m_elapsedTime * 2);

		color.Set(1, 1, 1);

	}
	else {

		color.Set(1, 0.8f, 0);

	}

	glDisable(GL_CULL_FACE);
	m_Anim_CurrentFrame = m_Anim->m_currentFrame;
	GameObject::Render();

	if (b_IsForcefieldActive) {

		glDisable(GL_DEPTH_TEST);
		m_scene->modelStack.PushMatrix();
		m_scene->modelStack.Translate(pos.x, pos.y + m_OffsetPos.y, pos.z);
		m_scene->modelStack.Rotate(m_scene->m_elapsedTime * 180, 0, 0, 1);
		m_scene->modelStack.Scale(27, 27, 1);
		m_scene->meshList[Scene::GEO_ENEMYDEMON_FORCEFIELD]->alpha = m_ForcefieldAlpha;
		m_scene->RenderMesh(m_scene->meshList[Scene::GEO_ENEMYDEMON_FORCEFIELD], false);
		m_scene->modelStack.PopMatrix();
		glEnable(GL_DEPTH_TEST);

	}

	glEnable(GL_CULL_FACE);

	if (alpha >= 1) {
		// Render Health bar
		Vector3 barPos = pos;
		barPos.y += collider.bboxSize.y / 2;
		Mesh** meshList = m_scene->meshList;
		UI_BAR::RenderBar(m_scene, meshList[Scene::GEO_HP_BACKGROUND], meshList[Scene::GEO_HP_FOREGROUND], meshList[Scene::GEO_HP_FRAME], barPos, Vector3(7.5f, 0.75f), (float)GetCurrentHealth() / m_DefaultHP);
	}

}

void Enemy_Demon::OnDestroy() {
	m_DeathThrone->active = true;
}

void Enemy_Demon::OnReduceHealth(float damage) {
}

void Enemy_Demon::OnCollisionHit(GameObject * other) {
	AI_Base::OnCollisionHit(other);

}

