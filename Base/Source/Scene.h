#ifndef SCENE_H
#define SCENE_H

#include "DepthFBO.h"
#include "Light.h"

#include "UIManager.h"
#include "GameObjectManager.h"
#include "ParticleManager.h"
#include "PauseManager.h"
#include "PlayerManager.h"


class Scene
{
public:

	enum UNIFORM_TYPE {
		U_MVP = 0,
		U_MODELVIEW,
		U_MODELVIEW_INVERSE_TRANSPOSE,

		U_MESH_COLOR,
		U_MESH_ALPHA,

		U_MATERIAL_AMBIENT,
		U_MATERIAL_DIFFUSE,
		U_MATERIAL_SPECULAR,
		U_MATERIAL_SHININESS,

		U_LIGHTENABLED,
		U_NUMLIGHTS,

		U_LIGHT0_TYPE,
		U_LIGHT0_POSITION,
		U_LIGHT0_COLOR,
		U_LIGHT0_POWER,
		U_LIGHT0_KC,
		U_LIGHT0_KL,
		U_LIGHT0_KQ,
		U_LIGHT0_SPOTDIRECTION,
		U_LIGHT0_COSCUTOFF,
		U_LIGHT0_COSINNER,
		U_LIGHT0_EXPONENT,

		U_LIGHT1_TYPE,
		U_LIGHT1_POSITION,
		U_LIGHT1_COLOR,
		U_LIGHT1_POWER,
		U_LIGHT1_KC,
		U_LIGHT1_KL,
		U_LIGHT1_KQ,
		U_LIGHT1_SPOTDIRECTION,
		U_LIGHT1_COSCUTOFF,
		U_LIGHT1_COSINNER,
		U_LIGHT1_EXPONENT,

		U_COLOR_TEXTURE,
		U_COLOR_TEXTURE1,
		U_COLOR_TEXTURE2,
		U_COLOR_TEXTURE3,
		U_COLOR_TEXTURE4,
		U_COLOR_TEXTURE5,
		U_COLOR_TEXTURE6,
		U_COLOR_TEXTURE7,

		U_COLOR_TEXTURE_ENABLED,
		U_COLOR_TEXTURE_ENABLED1,
		U_COLOR_TEXTURE_ENABLED2,
		U_COLOR_TEXTURE_ENABLED3,
		U_COLOR_TEXTURE_ENABLED4,
		U_COLOR_TEXTURE_ENABLED5,
		U_COLOR_TEXTURE_ENABLED6,
		U_COLOR_TEXTURE_ENABLED7,

		U_SHADOW_COLOR_TEXTURE,
		U_SHADOW_COLOR_TEXTURE1,
		U_SHADOW_COLOR_TEXTURE2,
		U_SHADOW_COLOR_TEXTURE_ENABLED,
		U_SHADOW_COLOR_TEXTURE_ENABLED1,
		U_SHADOW_COLOR_TEXTURE_ENABLED2,

		U_TEXT_ENABLED,
		U_TEXT_COLOR,

		U_FOG_ENABLED,
		U_FOG_START,
		U_FOG_END,
		U_FOG_DENSITY,
		U_FOG_TYPE,
		U_FOG_COLOR,

		U_LIGHT_DEPTH_MVP_GPASS,
		U_LIGHT_DEPTH_MVP,
		U_SHADOW_MAP,

		U_TOTAL,
	};

	enum GEOMETRY_TYPE {
		GEO_TEXT = 0,

		// Pause menu and defeat menu background
		GEO_P_BACKGROUND,
		GEO_UI_DEFEAT,

		//health bar and cap bar and power bar
		GEO_HP_FOREGROUND,
		GEO_HP_BACKGROUND,
		GEO_CAP_FOREGROUND,
		GEO_POW_FOREGROUND,
		GEO_HP_FRAME,


		//hud items
		GEO_PORTRAIT,
		GEO_BAR_BG,
		GEO_HEART,
		GEO_POWER,


		// For debugging use
		GEO_CUBE,
		GEO_SPLASH,

		//Main menu selections
		GEO_MAINMENU,
		GEO_LEVEL,
		GEO_CREDITSSELECT,
		GEO_POINTER,
		//Other Main menu GEOS moved below to prevent error with terrain

		// Object Geometries

		// Start of Terrain, adding new enums before this will affect the terrain loading
		GEO_TERRAIN_GRASS,
		GEO_TERRAIN_MUDDY,
		GEO_TERRAIN_LAVA,
		GEO_TERRAIN_ICE,
		GEO_TERRAIN_BRICK,
		GEO_TERRAIN_CRATE,
		GEO_TERRAIN_INVISIBLE_WALL,
		// End of Terrain

		//Other MENUS
		GEO_SHOP,
		GEO_CONTROLS,
		GEO_TICK,
		GEO_LOCK,

		GEO_PIPEBOMB,
		GEO_FUR,
		GEO_ARMOUR,
		GEO_WOOL,

		GEO_SWORD,
		GEO_SHIELD,
		GEO_SHEARS,
		GEO_CROSSBOW,
		GEO_TRANQGUN,
		GEO_ARROW,
		GEO_DART,
		GEO_SPEAR,
		GEO_ULTIMATE_WAVE,
		GEO_ULTIMATE_ARROW,
		GEO_ULTIMATE_BALL,
		GEO_BLACKHOLE,

		GEO_CAPTUREPOINT,

		// Level 1 Parallax
		GEO_Parallax_L1_1,
		GEO_Parallax_L1_2,
		GEO_Parallax_L1_3,

		// Level 2 Parallax
		GEO_Parallax_L2_1,
		GEO_Parallax_L2_2,

		// Level 3 Parallax
		GEO_Parallax_L3_1,
		GEO_Parallax_L3_2,

		// Enemy Units
		GEO_ENEMYWARRIOR,
		GEO_ENEMYARCHER,
		GEO_ENEMYNINJA,
		GEO_ENEMYGOLEM,
		GEO_ENEMYGIANT,

		// Misc Objects
		GEO_PLAYER,
		GEO_CORE,
		GEO_PORTAL,
		GEO_ENEMYBOW,
		GEO_ENEMYGIANT_CLUB,
		GEO_ENEMYGOLEM_BOULDER,

		GEO_ENEMYDEMON,
		GEO_DEMONSHEEPBOSS,
		GEO_ENEMYDEMON_CRYSTALS,
		GEO_ENEMYDEMON_LASER,
		GEO_ENEMYDEMON_FIREBALL,
		GEO_ENEMYDEMON_FIERYSPEAR,
		GEO_ENEMYDEMON_FIERYSPEAR_INDICATOR,
		GEO_ENEMYDEMON_FORCEFIELD,

		GEO_ALLYWARRIOR,
		GEO_ALLYARCHER,

		//hud items part2
		GEO_W_FRAME,
		GEO_W_FRAME_ERROR,
		GEO_ARMOR_FOREGROUND,
		GEO_WLOCK,
		GEO_WARRIOR_ICON,
		GEO_ARCHER_ICON,

		//victory menu background
		GEO_UI_VICTORY,
		GEO_VIC_TEXT,
		GEO_DE_TEXT,

		GEO_HEALTHWELL,

		GEO_STAMPEDE,
		GEO_DISTRACT,
		GEO_ATTRACT,
		GEO_LOCK2,


		NUM_GEOMETRY,
	};

	enum RENDER_PASS {
		RENDER_PASS_PRE,
		RENDER_PASS_MAIN,
	};

	enum TYPE_SCENE {

		SCENE_MAINMENU = 0,
		SCENE_EDITOR,
		SCENE_GAMEOVER_VIC,
		SCENE_LEVEL01,
		SCENE_LEVEL02,
		SCENE_LEVEL03,
	};


	Scene(TYPE_SCENE type);
	~Scene() {}

	virtual void Init() = 0;
	virtual void Update(double dt) = 0;
	virtual void Render() = 0;
	virtual void Exit() = 0;

	MS modelStack, viewStack, projectionStack;

	Mesh* meshList[NUM_GEOMETRY];

	const TYPE_SCENE sceneType;

	float m_dt = 0;
	float m_elapsedTime = 0;

	unsigned m_parameters[U_TOTAL];
	unsigned m_vertexArrayID;
	unsigned m_programID;

	// Shadow
	void RenderPassGPass();
	void RenderPassMain();

	void RenderMesh(Mesh * mesh, bool enableLight, int indicesOffset = 0);
	void RenderMeshIn2D(Mesh *mesh, float scale = 1.0f);

	static bool ShowDebugInfo;

	// Managers
	UIManager textManager;
	GameObjectManager goManager;
	ParticleManager partManager;
	PauseManager pauseManager;
	PlayerManager playerManager;

protected:
	Light lights[2];

	unsigned m_gPassShaderID;
	DepthFBO m_lightDepthFBO;
	Mtx44 m_lightDepthProj;
	Mtx44 m_lightDepthView;
	RENDER_PASS m_renderPass;



};

#endif