#include "SceneBase.h"
#include "GL\glew.h"

#include "Application.h"

#include "shader.hpp"

#include "SpriteAnimation.h"
#include "MeshBuilder.h"
#include "Utility.h"
#include "LoadTGA.h"

#include "SoundManager.h"
#include "PlayerData.h"

#include <sstream>


SceneBase::SceneBase(TYPE_SCENE type) :Scene(type), m_BGScroller(this, type)
{
}

SceneBase::~SceneBase()
{
}

void SceneBase::Init()
{
	// Black background
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	// Enable depth test
	glEnable(GL_DEPTH_TEST);

	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	glEnable(GL_CULL_FACE);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glGenVertexArrays(1, &m_vertexArrayID);
	glBindVertexArray(m_vertexArrayID);

	m_programID = LoadShaders("Shader//shadow.vertexshader", "Shader//shadow.fragmentshader");

	// Get a handle for our uniform
	m_parameters[U_MVP] = glGetUniformLocation(m_programID, "MVP");

	m_parameters[U_MESH_ALPHA] = glGetUniformLocation(m_programID, "currentAlpha");
	m_parameters[U_MESH_COLOR] = glGetUniformLocation(m_programID, "currentColor");

	m_parameters[U_MODELVIEW] = glGetUniformLocation(m_programID, "MV");
	m_parameters[U_MODELVIEW_INVERSE_TRANSPOSE] = glGetUniformLocation(m_programID, "MV_inverse_transpose");

	m_parameters[U_MATERIAL_AMBIENT] = glGetUniformLocation(m_programID, "material.kAmbient");
	m_parameters[U_MATERIAL_DIFFUSE] = glGetUniformLocation(m_programID, "material.kDiffuse");
	m_parameters[U_MATERIAL_SPECULAR] = glGetUniformLocation(m_programID, "material.kSpecular");
	m_parameters[U_MATERIAL_SHININESS] = glGetUniformLocation(m_programID, "material.kShininess");

	m_parameters[U_LIGHTENABLED] = glGetUniformLocation(m_programID, "lightEnabled");
	m_parameters[U_NUMLIGHTS] = glGetUniformLocation(m_programID, "numLights");

	m_parameters[U_LIGHT0_TYPE] = glGetUniformLocation(m_programID, "lights[0].type");
	m_parameters[U_LIGHT0_POSITION] = glGetUniformLocation(m_programID, "lights[0].position_cameraspace");
	m_parameters[U_LIGHT0_COLOR] = glGetUniformLocation(m_programID, "lights[0].color");
	m_parameters[U_LIGHT0_POWER] = glGetUniformLocation(m_programID, "lights[0].power");
	m_parameters[U_LIGHT0_KC] = glGetUniformLocation(m_programID, "lights[0].kC");
	m_parameters[U_LIGHT0_KL] = glGetUniformLocation(m_programID, "lights[0].kL");
	m_parameters[U_LIGHT0_KQ] = glGetUniformLocation(m_programID, "lights[0].kQ");
	m_parameters[U_LIGHT0_SPOTDIRECTION] = glGetUniformLocation(m_programID, "lights[0].spotDirection");
	m_parameters[U_LIGHT0_COSCUTOFF] = glGetUniformLocation(m_programID, "lights[0].cosCutoff");
	m_parameters[U_LIGHT0_COSINNER] = glGetUniformLocation(m_programID, "lights[0].cosInner");
	m_parameters[U_LIGHT0_EXPONENT] = glGetUniformLocation(m_programID, "lights[0].exponent");

	m_parameters[U_LIGHT1_TYPE] = glGetUniformLocation(m_programID, "lights[1].type");
	m_parameters[U_LIGHT1_POSITION] = glGetUniformLocation(m_programID, "lights[1].position_cameraspace");
	m_parameters[U_LIGHT1_COLOR] = glGetUniformLocation(m_programID, "lights[1].color");
	m_parameters[U_LIGHT1_POWER] = glGetUniformLocation(m_programID, "lights[1].power");
	m_parameters[U_LIGHT1_KC] = glGetUniformLocation(m_programID, "lights[1].kC");
	m_parameters[U_LIGHT1_KL] = glGetUniformLocation(m_programID, "lights[1].kL");
	m_parameters[U_LIGHT1_KQ] = glGetUniformLocation(m_programID, "lights[1].kQ");
	m_parameters[U_LIGHT1_SPOTDIRECTION] = glGetUniformLocation(m_programID, "lights[1].spotDirection");
	m_parameters[U_LIGHT1_COSCUTOFF] = glGetUniformLocation(m_programID, "lights[1].cosCutoff");
	m_parameters[U_LIGHT1_COSINNER] = glGetUniformLocation(m_programID, "lights[1].cosInner");
	m_parameters[U_LIGHT1_EXPONENT] = glGetUniformLocation(m_programID, "lights[1].exponent");

	m_parameters[U_COLOR_TEXTURE] = glGetUniformLocation(m_programID, "colorTexture[0]");
	m_parameters[U_COLOR_TEXTURE1] = glGetUniformLocation(m_programID, "colorTexture[1]");
	m_parameters[U_COLOR_TEXTURE2] = glGetUniformLocation(m_programID, "colorTexture[2]");
	m_parameters[U_COLOR_TEXTURE3] = glGetUniformLocation(m_programID, "colorTexture[3]");
	m_parameters[U_COLOR_TEXTURE4] = glGetUniformLocation(m_programID, "colorTexture[4]");
	m_parameters[U_COLOR_TEXTURE5] = glGetUniformLocation(m_programID, "colorTexture[5]");
	m_parameters[U_COLOR_TEXTURE6] = glGetUniformLocation(m_programID, "colorTexture[6]");
	m_parameters[U_COLOR_TEXTURE7] = glGetUniformLocation(m_programID, "colorTexture[7]");

	m_parameters[U_COLOR_TEXTURE_ENABLED] = glGetUniformLocation(m_programID, "colorTextureEnabled[0]");
	m_parameters[U_COLOR_TEXTURE_ENABLED1] = glGetUniformLocation(m_programID, "colorTextureEnabled[1]");
	m_parameters[U_COLOR_TEXTURE_ENABLED2] = glGetUniformLocation(m_programID, "colorTextureEnabled[2]");
	m_parameters[U_COLOR_TEXTURE_ENABLED3] = glGetUniformLocation(m_programID, "colorTextureEnabled[3]");
	m_parameters[U_COLOR_TEXTURE_ENABLED4] = glGetUniformLocation(m_programID, "colorTextureEnabled[4]");
	m_parameters[U_COLOR_TEXTURE_ENABLED5] = glGetUniformLocation(m_programID, "colorTextureEnabled[5]");
	m_parameters[U_COLOR_TEXTURE_ENABLED6] = glGetUniformLocation(m_programID, "colorTextureEnabled[6]");
	m_parameters[U_COLOR_TEXTURE_ENABLED7] = glGetUniformLocation(m_programID, "colorTextureEnabled[7]");

	m_parameters[U_TEXT_ENABLED] = glGetUniformLocation(m_programID, "textEnabled");
	m_parameters[U_TEXT_COLOR] = glGetUniformLocation(m_programID, "textColor");

	// Use our shader
	glUseProgram(m_programID);

	lights[0].type = Light::LIGHT_DIRECTIONAL;
	lights[0].position.Set(10, 10, 15);
	lights[0].color.Set(1, 1, 1);
	lights[0].power = 2;

	lights[1].type = Light::LIGHT_SPOT;
	lights[1].position.Set(0, 0, 0);
	lights[1].color.Set(1, 1, 1);
	lights[1].power = 1.5F;
	lights[1].kC = 1.f;
	lights[1].kL = 0.01f;
	lights[1].kQ = 0.001f;
	lights[1].cosCutoff = cos(Math::DegreeToRadian(20));
	lights[1].cosInner = cos(Math::DegreeToRadian(10));
	lights[1].exponent = 3.f;
	lights[1].spotDirection.SetZero();

	glUniform1i(m_parameters[U_NUMLIGHTS], 2);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 0);

	glUniform1i(m_parameters[U_LIGHT0_TYPE], lights[0].type);
	glUniform3fv(m_parameters[U_LIGHT0_COLOR], 1, &lights[0].color.r);
	glUniform1f(m_parameters[U_LIGHT0_POWER], lights[0].power);
	glUniform1f(m_parameters[U_LIGHT0_KC], lights[0].kC);
	glUniform1f(m_parameters[U_LIGHT0_KL], lights[0].kL);
	glUniform1f(m_parameters[U_LIGHT0_KQ], lights[0].kQ);
	glUniform1f(m_parameters[U_LIGHT0_COSCUTOFF], lights[0].cosCutoff);
	glUniform1f(m_parameters[U_LIGHT0_COSINNER], lights[0].cosInner);
	glUniform1f(m_parameters[U_LIGHT0_EXPONENT], lights[0].exponent);

	glUniform1i(m_parameters[U_LIGHT1_TYPE], lights[1].type);
	glUniform3fv(m_parameters[U_LIGHT1_COLOR], 1, &lights[1].color.r);
	glUniform1f(m_parameters[U_LIGHT1_POWER], lights[1].power);
	glUniform1f(m_parameters[U_LIGHT1_KC], lights[1].kC);
	glUniform1f(m_parameters[U_LIGHT1_KL], lights[1].kL);
	glUniform1f(m_parameters[U_LIGHT1_KQ], lights[1].kQ);
	glUniform1f(m_parameters[U_LIGHT1_COSCUTOFF], lights[1].cosCutoff);
	glUniform1f(m_parameters[U_LIGHT1_COSINNER], lights[1].cosInner);
	glUniform1f(m_parameters[U_LIGHT1_EXPONENT], lights[1].exponent);

	camera.Init(Vector3(0, 0, 0), Vector3(0, 0, -1), Vector3(0, 1, 0));
	camera.SetZoomMutiplier(0.9f);

	for (int i = 0; i < NUM_GEOMETRY; ++i)
	{
		meshList[i] = NULL;
	}

	// meshList[GEO_AXES] = MeshBuilder::GenerateAxes("reference", 1000, 1000, 1000);
	meshList[GEO_CONTROLS] = MeshBuilder::GenerateQuad("Controls", Color(1.f, 1.f, 1.f));
	meshList[GEO_CONTROLS]->textureArray[0] = LoadTGA("Image//MainMenu//instructions.tga");


	meshList[GEO_TEXT] = MeshBuilder::GenerateText("text", 16, 16);
	meshList[GEO_TEXT]->textureArray[0] = LoadTGA("Image/arial.tga");
	textManager.LoadFontWidth("Image/arial.csv");

	//pause menu
	meshList[GEO_P_BACKGROUND] = MeshBuilder::GenerateUIQuad("PauseBG", Color(1.f, 1.0f, 1.0f));
	meshList[GEO_P_BACKGROUND]->textureArray[0] = LoadTGA("Image/PMenu.tga");//transparency bg
	meshList[GEO_P_BACKGROUND]->alpha = 0.8f;

	//bar texture
	meshList[GEO_HP_BACKGROUND] = MeshBuilder::GenerateUIQuad("HP_Background", Color(1.f, 1.f, 1.f));
	meshList[GEO_HP_BACKGROUND]->textureArray[0] = LoadTGA("Image/white_bg.tga");//transparency bg

	meshList[GEO_HP_FOREGROUND] = MeshBuilder::GenerateUIQuad("HP_Foreground", Color(1.f, 1.f, 1.f));
	meshList[GEO_HP_FOREGROUND]->textureArray[0] = LoadTGA("Image/health_bar.tga");//health texture 

	meshList[GEO_CAP_FOREGROUND] = MeshBuilder::GenerateUIQuad("CAP_Foreground", Color(1.f, 1.f, 1.f));
	meshList[GEO_CAP_FOREGROUND]->textureArray[0] = LoadTGA("Image/capture_bar.tga");//cap texture 

	meshList[GEO_POW_FOREGROUND] = MeshBuilder::GenerateUIQuad("Pow_Foreground", Color(1.f, 1.f, 1.f));
	meshList[GEO_POW_FOREGROUND]->textureArray[0] = LoadTGA("Image/pow_bar.tga");//power texture 

	meshList[GEO_ARMOR_FOREGROUND] = MeshBuilder::GenerateUIQuad("ARMOR_Foreground", Color(1.f, 1.f, 1.f));
	meshList[GEO_ARMOR_FOREGROUND]->textureArray[0] = LoadTGA("Image/armor_bar.tga");//armor texture 

	meshList[GEO_HP_FRAME] = MeshBuilder::GenerateUIQuad("HP_Frame", Color(1.f, 1.f, 1.f));
	meshList[GEO_HP_FRAME]->textureArray[0] = LoadTGA("Image/frame_bar01.tga");//health texture 

	//hud bar and character pic 
	meshList[GEO_PORTRAIT] = MeshBuilder::GenerateUIQuad("Portrait", Color(1.f, 1.f, 1.f));
	meshList[GEO_PORTRAIT]->textureArray[0] = LoadTGA("Image/portrait01.tga");//char portrait pic

	meshList[GEO_WARRIOR_ICON] = MeshBuilder::GenerateUIQuad("Warrior icon", Color(1.f, 1.f, 1.f));
	meshList[GEO_WARRIOR_ICON]->textureArray[0] = LoadTGA("Image/AWarrior_icon.tga");//hud warrior icon

	meshList[GEO_ARCHER_ICON] = MeshBuilder::GenerateUIQuad("Archer icon", Color(1.f, 1.f, 1.f));
	meshList[GEO_ARCHER_ICON]->textureArray[0] = LoadTGA("Image/AArcher_icon.tga");//hud archer icon

	meshList[GEO_DISTRACT] = MeshBuilder::GenerateUIQuad("Distract icon", Color(1.f, 1.f, 1.f));
	meshList[GEO_DISTRACT]->textureArray[0] = LoadTGA("Image/Distract_icon.tga");//hud distract icon

	meshList[GEO_ATTRACT] = MeshBuilder::GenerateUIQuad("Attract icon", Color(1.f, 1.f, 1.f));
	meshList[GEO_ATTRACT]->textureArray[0] = LoadTGA("Image/Attract_icon.tga");//hud attract icon


	meshList[GEO_STAMPEDE] = MeshBuilder::GenerateUIQuad("Stampede icon", Color(1.f, 1.f, 1.f));
	meshList[GEO_STAMPEDE]->textureArray[0] = LoadTGA("Image/stampede_icon.tga");//hud distract icon

	meshList[GEO_BAR_BG] = MeshBuilder::GenerateUIQuad("Bar_BG", Color(1.f, 1.f, 1.f));
	meshList[GEO_BAR_BG]->textureArray[0] = LoadTGA("Image/Border2.tga");//

	meshList[GEO_HEART] = MeshBuilder::GenerateUIQuad("heart_hp", Color(1.f, 1.f, 1.f));
	meshList[GEO_HEART]->textureArray[0] = LoadTGA("Image/heart_4.tga");//

	meshList[GEO_POWER] = MeshBuilder::GenerateUIQuad("power_sign", Color(1.f, 1.f, 1.f));
	meshList[GEO_POWER]->textureArray[0] = LoadTGA("Image/Star.tga");//

	meshList[GEO_W_FRAME] = MeshBuilder::GenerateUIQuad("weapon_hud_frame", Color(1.f, 1.f, 1.f));
	meshList[GEO_W_FRAME]->textureArray[0] = LoadTGA("Image/Hud_frame.tga");//

	meshList[GEO_W_FRAME_ERROR] = MeshBuilder::GenerateUIQuad("GEO_W_FRAME_ERROR", Color(1.f, 1.f, 1.f));
	meshList[GEO_W_FRAME_ERROR]->textureArray[0] = LoadTGA("Image/Hud_frame_Error.tga");//	

	meshList[GEO_LOCK2] = MeshBuilder::GenerateUIQuad("skill lock", Color(1.f, 1.f, 1.f));
	meshList[GEO_LOCK2]->textureArray[0] = LoadTGA("Image/Lock_icon.tga");//

	meshList[GEO_WLOCK] = MeshBuilder::GenerateUIQuad("weapon_hud_lock", Color(1.f, 1.f, 1.f));
	meshList[GEO_WLOCK]->textureArray[0] = LoadTGA("Image/wlock.tga");//
	meshList[GEO_WLOCK]->alpha = 0.8f;
	////////////////////////////////

	meshList[GEO_Parallax_L1_1] = MeshBuilder::GenerateQuad("GEO_Parallax_L1_1", Color(1, 1, 1));
	meshList[GEO_Parallax_L1_1]->textureArray[0] = LoadTGA("Image/ParallaxBackground/Level01/1.tga");

	meshList[GEO_Parallax_L1_2] = MeshBuilder::GenerateQuad("GEO_Parallax_L1_2", Color(1, 1, 1));
	meshList[GEO_Parallax_L1_2]->textureArray[0] = LoadTGA("Image/ParallaxBackground/Level01/2.tga");

	meshList[GEO_Parallax_L1_3] = MeshBuilder::GenerateQuad("background", Color(1, 1, 1), 1);
	meshList[GEO_Parallax_L1_3]->textureArray[0] = LoadTGA("Image/L1BG.tga");

	meshList[GEO_CUBE] = MeshBuilder::GenerateCube("Cube", Color(1, 1, 1));

	//player
	meshList[GEO_PLAYER] = MeshBuilder::GenerateSpriteAnimation("GEO_PLAYER", 8, 12, 1);
	meshList[GEO_PLAYER]->textureArray[0] = LoadTGA("Image/player.tga");

	//terrain
	meshList[GEO_TERRAIN_GRASS] = MeshBuilder::GenerateQuad("GEO_TERRAIN_DIRTGRASS", Color(1, 1, 1), 1, 10, 1);
	meshList[GEO_TERRAIN_GRASS]->textureArray[0] = LoadTGA("Image/grass.tga", true, true);

	meshList[GEO_TERRAIN_MUDDY] = MeshBuilder::GenerateQuad("GEO_TERRAIN_MUDDY", Color(1, 1, 1), 1, 10, 1);
	meshList[GEO_TERRAIN_MUDDY]->textureArray[0] = LoadTGA("Image/mud.tga", true, true);

	meshList[GEO_TERRAIN_ICE] = MeshBuilder::GenerateQuad("GEO_TERRAIN_ICE", Color(1, 1, 1), 1, 10, 1);
	meshList[GEO_TERRAIN_ICE]->textureArray[0] = LoadTGA("Image/snow.tga", true, true);

	meshList[GEO_TERRAIN_LAVA] = MeshBuilder::GenerateQuad("GEO_TERRAIN_LAVA", Color(1, 1, 1), 1, 10, 1);
	meshList[GEO_TERRAIN_LAVA]->textureArray[0] = LoadTGA("Image/lava.tga", true, true);

	//other world objects
	meshList[GEO_TERRAIN_BRICK] = MeshBuilder::GenerateQuad("GEO_TERRAIN_BRICK", Color(1, 1, 1), 1, 10, 1);
	meshList[GEO_TERRAIN_BRICK]->textureArray[0] = LoadTGA("Image/brick.tga", true, true);

	meshList[GEO_TERRAIN_CRATE] = MeshBuilder::GenerateQuad("GEO_TERRAIN_CRATE", Color(1, 1, 1));
	meshList[GEO_TERRAIN_CRATE]->textureArray[0] = LoadTGA("Image/crate.tga", true, true);

	meshList[GEO_TERRAIN_INVISIBLE_WALL] = MeshBuilder::GenerateQuad("GEO_TERRAIN_INVISIBLE_WALL", Color(1, 1, 1));

	meshList[GEO_PIPEBOMB] = MeshBuilder::GenerateQuad("GEO_PIPEBOMB", Color(1, 1, 1));
	meshList[GEO_PIPEBOMB]->textureArray[0] = LoadTGA("Image/cookie.tga");

	meshList[GEO_FUR] = MeshBuilder::GenerateQuad("GEO_FUR", Color(1, 1, 1));
	meshList[GEO_FUR]->textureArray[0] = LoadTGA("Image/fur.tga");

	//weapons
	meshList[GEO_SWORD] = MeshBuilder::GenerateQuad("GEO_SWORD", Color(1, 1, 1));
	meshList[GEO_SWORD]->textureArray[0] = LoadTGA("Image/sword.tga");

	meshList[GEO_SHIELD] = MeshBuilder::GenerateQuad("GEO_SHIELD", Color(1, 1, 1));
	meshList[GEO_SHIELD]->textureArray[0] = LoadTGA("Image/shield.tga");

	meshList[GEO_SHEARS] = MeshBuilder::GenerateQuad("GEO_SHEARS", Color(1, 1, 1));
	meshList[GEO_SHEARS]->textureArray[0] = LoadTGA("Image/Shears.tga");

	meshList[GEO_CROSSBOW] = MeshBuilder::GenerateQuad("GEO_CROSSBOW", Color(1, 1, 1));
	meshList[GEO_CROSSBOW]->textureArray[0] = LoadTGA("Image/bow.tga");

	meshList[GEO_TRANQGUN] = MeshBuilder::GenerateQuad("GEO_TRANQGUN", Color(1, 1, 1));
	meshList[GEO_TRANQGUN]->textureArray[0] = LoadTGA("Image/TranqGun.tga");

	meshList[GEO_ARROW] = MeshBuilder::GenerateQuad("GEO_ARROW", Color(1, 1, 1));
	meshList[GEO_ARROW]->textureArray[0] = LoadTGA("Image/Arrow.tga");

	meshList[GEO_DART] = MeshBuilder::GenerateQuad("GEO_DART", Color(1, 1, 1));
	meshList[GEO_DART]->textureArray[0] = LoadTGA("Image/Dart.tga");

	meshList[GEO_SPEAR] = MeshBuilder::GenerateQuad("GEO_SPEAR", Color(1, 1, 1));
	meshList[GEO_SPEAR]->textureArray[0] = LoadTGA("Image/spear.tga");

	//Armour kit
	meshList[GEO_ARMOUR] = MeshBuilder::GenerateQuad("GEO_ARMOUR", Color(1, 1, 1));
	meshList[GEO_ARMOUR]->textureArray[0] = LoadTGA("Image/armour.tga");

	meshList[GEO_WOOL] = MeshBuilder::GenerateQuad("GEO_WOOL", Color(1, 1, 1));
	meshList[GEO_WOOL]->textureArray[0] = LoadTGA("Image/armour.tga");

	//power attacks
	meshList[GEO_ULTIMATE_WAVE] = MeshBuilder::GenerateQuad("GEO_ULTIMATE_WAVE", Color(1, 1, 1));
	meshList[GEO_ULTIMATE_WAVE]->textureArray[0] = LoadTGA("Image//UltimateWave.tga");

	meshList[GEO_ULTIMATE_ARROW] = MeshBuilder::GenerateQuad("GEO_ULTIMATE_ARROW", Color(1, 1, 1));
	meshList[GEO_ULTIMATE_ARROW]->textureArray[0] = LoadTGA("Image/UltimateArrow.tga");

	meshList[GEO_ULTIMATE_BALL] = MeshBuilder::GenerateQuad("GEO_ULTIMATE_BALL", Color(1, 1, 1));
	meshList[GEO_ULTIMATE_BALL]->textureArray[0] = LoadTGA("Image//UltimateBall.tga");

	meshList[GEO_BLACKHOLE] = MeshBuilder::GenerateQuad("GEO_BLACKHOLE", Color(1, 1, 1));
	meshList[GEO_BLACKHOLE]->textureArray[0] = LoadTGA("Image//Blackhole.tga");

	//portal that spawns enemy
	meshList[GEO_PORTAL] = MeshBuilder::GenerateQuad("GEO_PORTAL", Color(1, 1, 1));
	meshList[GEO_PORTAL]->textureArray[0] = LoadTGA("Image//portal.tga");

	// Capture flag
	meshList[GEO_CAPTUREPOINT] = MeshBuilder::GenerateQuad("GEO_CAPTUREPOINT", Color(1, 1, 1));
	meshList[GEO_CAPTUREPOINT]->textureArray[0] = LoadTGA("Image//captureflag.tga");

	// Core
	meshList[GEO_CORE] = MeshBuilder::GenerateQuad("GEO_CORE", Color(1, 1, 1));
	meshList[GEO_CORE]->textureArray[0] = LoadTGA("Image//core.tga");

	//enemy AI
	meshList[GEO_ENEMYWARRIOR] = MeshBuilder::GenerateSpriteAnimation("GEO_ENEMYWARRIOR", 8, 12, 1);
	meshList[GEO_ENEMYWARRIOR]->textureArray[0] = LoadTGA("Image/sheep3.tga");

	meshList[GEO_ENEMYGOLEM] = MeshBuilder::GenerateSpriteAnimation("GEO_GOLEM", 8, 12, 1);
	meshList[GEO_ENEMYGOLEM]->textureArray[0] = LoadTGA("Image//sheep3.tga");

	meshList[GEO_ENEMYGOLEM_BOULDER] = MeshBuilder::GenerateQuad("GEO_GOLEM_BOULDER", Color(1, 1, 1));
	meshList[GEO_ENEMYGOLEM_BOULDER]->textureArray[0] = LoadTGA("Image//Boulder.tga");

	meshList[GEO_ENEMYARCHER] = MeshBuilder::GenerateSpriteAnimation("GEO_ENEMYARCHER", 8, 12, 1);
	meshList[GEO_ENEMYARCHER]->textureArray[0] = LoadTGA("Image//sheep3.tga");

	meshList[GEO_ENEMYBOW] = MeshBuilder::GenerateQuad("GEO_ENEMYBOW", Color(1.f, 1.f, 1.f));
	meshList[GEO_ENEMYBOW]->textureArray[0] = LoadTGA("Image//enemy_bow.tga");

	meshList[GEO_ENEMYNINJA] = MeshBuilder::GenerateSpriteAnimation("GEO_ENEMYNINJA", 8, 12, 1);
	meshList[GEO_ENEMYNINJA]->textureArray[0] = LoadTGA("Image/sheep3.tga");

	meshList[GEO_ENEMYGIANT] = MeshBuilder::GenerateSpriteAnimation("GEO_ENEMYGIANT", 4, 4, 1);
	meshList[GEO_ENEMYGIANT]->textureArray[0] = LoadTGA("Image/giant.tga");

	meshList[GEO_ENEMYGIANT_CLUB] = MeshBuilder::GenerateQuad("GEO_ENEMYGIANT_CLUB", Color(1, 1, 1));
	meshList[GEO_ENEMYGIANT_CLUB]->textureArray[0] = LoadTGA("Image/club.tga");

	meshList[GEO_DEMONSHEEPBOSS] = MeshBuilder::GenerateSpriteAnimation("GEO_BOSS", 4, 4, 1);
	meshList[GEO_DEMONSHEEPBOSS]->textureArray[0] = LoadTGA("Image/demonsheep.tga");

	meshList[GEO_ENEMYDEMON] = MeshBuilder::GenerateSpriteAnimation("GEO_ENEMYDEMON", 8, 12, 1);
	meshList[GEO_ENEMYDEMON]->textureArray[0] = LoadTGA("Image/sheep3.tga");

	meshList[GEO_ENEMYDEMON_CRYSTALS] = MeshBuilder::GenerateQuad("GEO_ENEMYDEMON_CRYSTALS", Color(1, 1, 1));
	meshList[GEO_ENEMYDEMON_CRYSTALS]->textureArray[0] = LoadTGA("Image/demoncrystal.tga");

	meshList[GEO_ENEMYDEMON_LASER] = MeshBuilder::GenerateSpriteAnimation("GEO_ENEMYDEMON_LASER", 1, 10, 1);
	meshList[GEO_ENEMYDEMON_LASER]->textureArray[0] = LoadTGA("Image/demonlaser.tga");

	meshList[GEO_ENEMYDEMON_FIREBALL] = MeshBuilder::GenerateQuad("GEO_ENEMYDEMON_FIREBALL", Color(1, 1, 1));
	meshList[GEO_ENEMYDEMON_FIREBALL]->textureArray[0] = LoadTGA("Image/demonfireball.tga");

	meshList[GEO_ENEMYDEMON_FIERYSPEAR] = MeshBuilder::GenerateQuad("GEO_ENEMYDEMON_FIERYSPEAR", Color(1, 1, 1));
	meshList[GEO_ENEMYDEMON_FIERYSPEAR]->textureArray[0] = LoadTGA("Image/demonspear.tga");

	meshList[GEO_ENEMYDEMON_FIERYSPEAR_INDICATOR] = MeshBuilder::GenerateQuad("GEO_ENEMYDEMON_FIERYSPEAR_INDICATOR", Color(0.8f, 0, 0));
	meshList[GEO_ENEMYDEMON_FIERYSPEAR_INDICATOR]->alpha = 0.75f;

	meshList[GEO_ENEMYDEMON_FORCEFIELD] = MeshBuilder::GenerateQuad("GEO_ENEMYDEMON_FORCEFIELD", Color(1, 1, 1));
	meshList[GEO_ENEMYDEMON_FORCEFIELD]->textureArray[0] = LoadTGA("Image/demonforcefield.tga");

	meshList[GEO_ALLYWARRIOR] = MeshBuilder::GenerateSpriteAnimation("GEO_ENEMYWARRIOR", 8, 12, 1);
	meshList[GEO_ALLYWARRIOR]->textureArray[0] = LoadTGA("Image/sheep3.tga");

	meshList[GEO_ALLYARCHER] = MeshBuilder::GenerateSpriteAnimation("GEO_ALLYARCHER", 8, 12, 1);
	meshList[GEO_ALLYARCHER]->textureArray[0] = LoadTGA("Image/Allies.tga");

	meshList[GEO_HEALTHWELL] = MeshBuilder::GenerateQuad("HEALTHWELL", Color(1, 1, 1));
	meshList[GEO_HEALTHWELL]->textureArray[0] = LoadTGA("Image/healthwell.tga");

	//Audio
	SoundManager::getInstance().AddSound("Block", "Audio//Block.ogg");
	SoundManager::getInstance().AddSound("Armor", "Audio//Armor.ogg");
	SoundManager::getInstance().AddSound("Swing", "Audio//Swing.ogg");
	SoundManager::getInstance().AddSound("ArrowFire", "Audio//ArrowFire.ogg");
	SoundManager::getInstance().AddSound("Stab", "Audio//Stab.ogg");
	SoundManager::getInstance().AddSound("Walking", "Audio//Walking.ogg");
	SoundManager::getInstance().AddSound("Ultimate1", "Audio//Ultimate1.ogg");
	SoundManager::getInstance().AddSound("Ultimate2", "Audio//Ultimate2.ogg");
	SoundManager::getInstance().AddSound("Ultimate3", "Audio//Ultimate3.ogg");
	SoundManager::getInstance().AddSound("Hit", "Audio//Hit.ogg");
	SoundManager::getInstance().AddSound("Dash", "Audio//Dash.ogg");

	//Calculating aspect ratio
	m_worldHeight = m_worldHeight_default = 100.f;
	m_worldWidth = m_worldWidth_default = m_worldHeight * (float)Application::windowDefaultWidth() / Application::windowDefaultHeight();

	m_speed = 1.f;

	Math::InitRNG();

	// Init all Managers
	playerManager.Init();
}

void SceneBase::Update(double dt)
{
	dt *= m_speed;
	m_dt = (float)dt;
	m_elapsedTime += m_dt;

	//Keyboard Section
	if (Application::IsKeyPressed(VK_F1))
		glEnable(GL_CULL_FACE);
	if (Application::IsKeyPressed(VK_F2))
		glDisable(GL_CULL_FACE);
	if (Application::IsKeyPressed(VK_F3))
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	if (Application::IsKeyPressed(VK_F4))
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	fps = (float)(1.f / dt);

	if (sceneType != Scene::SCENE_MAINMENU)
		camera.Update(dt);
	//Calculating aspect ratio
	// m_worldWidth = m_worldHeight * (float)Application::windowWidth() / Application::windowHeight();
}

void SceneBase::Render()
{

}

void SceneBase::UpdateLightUniforms() {

	if (lights[0].type == Light::LIGHT_DIRECTIONAL)
	{
		Vector3 lightDir(lights[0].position.x, lights[0].position.y, lights[0].position.z);
		Vector3 lightDirection_cameraspace = viewStack.Top() * lightDir;
		glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightDirection_cameraspace.x);
	}
	else if (lights[0].type == Light::LIGHT_SPOT)
	{
		Position lightPosition_cameraspace = viewStack.Top() * lights[0].position;
		glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightPosition_cameraspace.x);
		Vector3 spotDirection_cameraspace = viewStack.Top() * lights[0].spotDirection;
		glUniform3fv(m_parameters[U_LIGHT0_SPOTDIRECTION], 1, &spotDirection_cameraspace.x);
	}
	else
	{
		Position lightPosition_cameraspace = viewStack.Top() * lights[0].position;
		glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightPosition_cameraspace.x);
	}

	if (lights[1].type == Light::LIGHT_DIRECTIONAL)
	{
		Vector3 lightDir(lights[1].position.x, lights[1].position.y, lights[1].position.z);
		Vector3 lightDirection_cameraspace = viewStack.Top() * lightDir;
		glUniform3fv(m_parameters[U_LIGHT1_POSITION], 1, &lightDirection_cameraspace.x);
	}
	else if (lights[1].type == Light::LIGHT_SPOT)
	{
		Position lightPosition_cameraspace = viewStack.Top() * lights[1].position;
		glUniform3fv(m_parameters[U_LIGHT1_POSITION], 1, &lightPosition_cameraspace.x);
		Vector3 spotDirection_cameraspace = viewStack.Top() * lights[1].spotDirection;
		glUniform3fv(m_parameters[U_LIGHT1_SPOTDIRECTION], 1, &spotDirection_cameraspace.x);
	}
	else
	{
		Position lightPosition_cameraspace = viewStack.Top() * lights[1].position;
		glUniform3fv(m_parameters[U_LIGHT1_POSITION], 1, &lightPosition_cameraspace.x);
	}

}

void SceneBase::Exit()
{
	// Cleanup VBO
	for (int i = 0; i < NUM_GEOMETRY; ++i)
	{
		if (meshList[i])
			delete meshList[i];
	}
	glDeleteProgram(m_programID);
	glDeleteVertexArrays(1, &m_vertexArrayID);

	//Cleanup GameObjects
	goManager.Exit();
	PlayerDataManager::getInstance()->SaveData();
}
