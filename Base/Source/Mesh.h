#ifndef MESH_H
#define MESH_H

#define MAX_TEXTURES 8 // Define total number of textures to use

#include <string>
#include "Material.h"
#include "Vertex.h"

class Mesh
{
public:
	enum DRAW_MODE
	{
		DRAW_TRIANGLES, //default mode
		DRAW_TRIANGLE_STRIP,
		DRAW_LINES,
		DRAW_MODE_LAST,
	};

	Mesh(const std::string &meshName);
	virtual ~Mesh();
	virtual void Render();
	void Render(unsigned offset, unsigned count);

	const std::string name;
	DRAW_MODE mode;
	unsigned vertexBuffer;
	unsigned indexBuffer;
	unsigned indexSize;

	Material material;

	Color color;
	float alpha = 1.f; // Alpha of this mesh

	unsigned textureArray[MAX_TEXTURES]; // Define texture array
	
};

#endif