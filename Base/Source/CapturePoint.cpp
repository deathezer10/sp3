#include "CapturePoint.h"
#include "SceneBase.h"

#include "UI_Bar.h"
#include "ColliderFactory.h"


CapturePoint::CapturePoint(Scene * scene) : GameObject(scene, Scene::GEO_CAPTUREPOINT) {
	b_EnableLighting = false;
	b_EnablePhysics = true;
	b_EnablePhysicsResponse = false;
	b_EnableTrigger = true;

	color.Set(1, 1, 1);

	scale.Set(10, 20, 1);

	Vector3 boxSize(20, 20, 1);

	collider = ColliderFactory::CreateOBBCollider(this, boxSize);
	
}

bool CapturePoint::Update() {

	if (color.r > 0 && m_CurrentCaptureProgress >= 100) {
		color.r -= m_scene->m_dt;
		color.b = color.r;
	}		

	return true;
}

void CapturePoint::Render() {

	SceneBase* _scene = static_cast<SceneBase*>(m_scene);

	Vector3 barPos = pos;
	barPos.y += collider.bboxSize.y * 0.75f;

	Mesh** meshList = _scene->meshList;

	UI_BAR::RenderBar(_scene, meshList[Scene::GEO_HP_BACKGROUND], meshList[Scene::GEO_CAP_FOREGROUND], meshList[Scene::GEO_HP_FRAME], barPos, Vector3(10.0f, 1.0f, 1.0f), m_CurrentCaptureProgress / 100);

	GameObject::Render();
}

void CapturePoint::OnCollisionHit(GameObject * other) {
	if (other->type == Scene::GEO_PLAYER) {
		m_CurrentCaptureProgress += 10 * m_scene->m_dt;
		m_CurrentCaptureProgress = Math::Clamp<float>(m_CurrentCaptureProgress, 0, 100);
	}
}
