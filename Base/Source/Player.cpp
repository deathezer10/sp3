#include "GL\glew.h"
#include "Player.h"

#include "Camera.h"
#include "SceneBase.h"
#include "SceneManager.h"
#include "Application.h"
#include "SpriteAnimation.h"
#include "SoundManager.h"

#include "ColliderFactory.h"
#include "Shears.h"
#include "TranqGun.h"
//#include "Spear.h"

Player::Player(Scene * scene) : GameObject(scene, Scene::GEO_PLAYER)
{
	b_EnableGravity = true;
	b_EnableLighting = false;
	b_EnablePhysics = true;

	MAX_VELOCITY.Set(20, 40, 0);

	scale.Set(5, 5, 1.f);

	collider = ColliderFactory::CreateOBBCollider(this, Vector3(5, 5, 1));
	b_isJumping = true;

	m_weaponIndex = 0;
	m_weaponManager[0] = new Shears(scene, this);
	m_weaponManager[1] = new TranqGun(scene, this);
	//m_weaponManager[2] = new Spear(scene, this);

	b_isFacingRight = true;

	// Animation 
	m_AnimPlayer = static_cast<SpriteAnimation*>(scene->meshList[Scene::GEO_PLAYER]);
	m_Anim = new Animation();
	m_Anim->Set(0, 0, 1, 1.f, true);

	// Walk sound
	m_WalkSound = SoundManager::getInstance().Play2DSound("Walking", true, false, true);

}

Player::~Player()
{
	if (m_WalkSound) {
		m_WalkSound->stop();
		m_WalkSound->drop();
	}

	if (m_weaponManager[0])
		delete m_weaponManager[0];

	if (m_weaponManager[1])
		delete m_weaponManager[1];

	//if (m_weaponManager[2])
		//delete[] m_weaponManager[2];

	if (m_Anim)
		delete m_Anim;
}

bool Player::Update()
{
	//Calculating aspect ratio
	SceneBase* _scene = (SceneBase*)m_scene;

	m_weaponManager[m_weaponIndex]->Update();

	if (m_scene->playerManager.GetShielding())
		MAX_VELOCITY.x = 10.f;
	else if (m_scene->m_elapsedTime < m_DashDuration)
		MAX_VELOCITY.x = 50.f;
	else
		MAX_VELOCITY.x = 20.f;

	if (!b_isJumping && abs(vel.x) > 1)
		m_WalkSound->setIsPaused(false);
	else
		m_WalkSound->setIsPaused(true);

	if (m_InfiniteSpeed == true)
		MAX_VELOCITY.x = 0;

	return true;
}

void Player::Render() {
	// Play animation
	if (vel.x < 1 && vel.x > -1) {
		m_Anim->Set(28, 28, 1, 0.5f, true);
	}
	else if (b_isFacingRight) {
		m_Anim->Set(33, 35, 1, 0.5f, true);
		rotation.y = 0;
	}
	else {
		m_Anim->Set(33, 35, 1, 0.5f, true);
		rotation.y = 180;
	}

	m_AnimPlayer->Update(m_Anim, m_scene->m_dt);

	glDisable(GL_CULL_FACE);
	m_Anim_CurrentFrame = m_Anim->m_currentFrame;
	GameObject::Render();
	glEnable(GL_CULL_FACE);

	m_weaponManager[m_weaponIndex]->Render();
}

void Player::OnCollisionHit(GameObject * other) {
	// Allow player to jump again if landed on top of a terrain
	if (collider.manifold.normal.y < 0 && vel.y <= 0 && Application::IsKeyPressed(VK_SPACE) == false) {
		b_isJumping = false;
	}
}

void Player::useDash()
{
	if (m_scene->m_elapsedTime > m_DashCooldown)
	{
		m_DashDuration = m_scene->m_elapsedTime + 0.2f;
		m_DashCooldown = m_scene->m_elapsedTime + 0.5f;
		SoundManager::getInstance().Play2DSound("Dash", false, true, false);
	}
}
