#pragma once



#include "SceneBase.h"
#include <vector>
#include "GameObject.h"
#include "ParallaxScroller.h"
#include "UI_Bar.h"


class FurnitureMan;


class SceneEditor : public SceneBase
{

public:
	SceneEditor();
	~SceneEditor();

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();

	FurnitureMan* m_FurnitureMan = nullptr;

private:
	ParallaxScroller m_BGScroller;
	GameObject* m_SelectedObject = nullptr;

	float m_MoveSpeedMultiplier = 1;
	float m_CurrentZoom = 1;

	std::string GetTerrainString(unsigned type);

	GameObject* CreateNewObject(Scene::GEOMETRY_TYPE type);

	// Restore the default terrain sizes and variables
	void ResetSelectedObject();


};