#pragma once



class SceneBase;
class Camera;

class ParallaxScroller {


public:
	ParallaxScroller(SceneBase* scene, unsigned type);
	~ParallaxScroller() {};

	void Update();
	void RenderBackground();

private:
	SceneBase* m_scene;
	unsigned m_Type;

	Camera* m_Camera;

};