#ifndef SCENE_MANAGER_H
#define SCENE_MANAGER_H

#include "timer.h"


class Scene;
class Application;

class SceneManager {

	friend class Application;  // Allow Application to access Update()
	static SceneManager* _instance;

public:
	static SceneManager* getInstance();

	bool hasPendingScene() {
		return _hasPendingScene;
	}

	void changeScene(Scene* scene);

	Scene* getCurrentScene() { return currentScene; };

	// Used for creating a 'new' Scene using TYPE_SCENE as the determinant. No initialization is done for the newly created Scene
	Scene* CreateScene(unsigned type);

protected:
	SceneManager() {};
	~SceneManager() {}; // Call Exit() to destroy the Singleton

private:
	Scene* currentScene = nullptr;
	Scene* pendingScene = nullptr;

	bool _hasPendingScene = false;

	StopWatch m_timer;

	void Update();
	void Exit();

};
#endif