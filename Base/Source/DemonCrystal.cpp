#include "DemonCrystal.h"

#include "SceneBase.h"
#include "Player.h"

#include "UI_Bar.h"
#include "ColliderFactory.h"


DemonCrystal::DemonCrystal(Scene * scene, Player* player) : AI_Base(scene, Scene::GEO_ENEMYDEMON_CRYSTALS, player, 300, 0) {
	b_EnableLighting = false;
	b_EnablePhysics = true;
	b_EnableGravity = true;

	color.Set(1, 1, 1);

	scale.Set(2.5f, 5, 1);

	bounciness = 0;

	mass = 0;

	collider = ColliderFactory::CreateOBBCollider(this, scale);

	m_DropChance = 100;
}

bool DemonCrystal::Update() {

	if (b_IsCrystalActive == false)
		m_CurrentHP = m_DefaultHP;

	return true;
}

void DemonCrystal::Render() {

	SceneBase* _scene = static_cast<SceneBase*>(m_scene);

	if (b_IsCrystalActive && m_CurrentHP > 0) {
		Vector3 barPos = pos;
		barPos.y += collider.bboxSize.y * 0.75f;

		Mesh** meshList = _scene->meshList;

		UI_BAR::RenderBar(_scene, meshList[Scene::GEO_HP_BACKGROUND], meshList[Scene::GEO_HP_FOREGROUND], meshList[Scene::GEO_HP_FRAME], barPos, Vector3(4.0f, 0.35f, 1.0f), m_CurrentHP / m_DefaultHP);

		color = Color(1, 1, 1);
	}
	else {
		color = Color(.6f, .6f, .6f);
	}

	GameObject::Render();
}


void DemonCrystal::OnCollisionHit(GameObject * other) {
}


