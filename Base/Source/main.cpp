#include "Application.h"
#include "DetectMemoryLeak.h"
// #include "vld.h"

//
//// this ensures that _CrtDumpMemoryLeaks() is called after main() and right before program terminates
//struct AtExit {
//	~AtExit() { _CrtDumpMemoryLeaks(); }
//} doAtExit;

int main(void)
{
	Application app;
	app.Init();
	app.Run();
	app.Exit();
}