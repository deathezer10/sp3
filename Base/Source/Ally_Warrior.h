#ifndef ALLY_WARRIOR_H
#define ALLY_WARRIOR_H
#include"AI_Base.h"


class SwordShield;
class SpriteAnimation;
struct Animation;


class Ally_Warrior :public AI_Base
{
public:
	Ally_Warrior(Scene* scene,Player* player) ;
	~Ally_Warrior();

	bool Update();


	//need ai pointer to attack enemy ai
	//void AI_Attack(AI_Base*AI, double dt);
	//void Attack(AI_Base* AI,double dt);
	void AI_Jump(double dt);
	void Engage(AI_Base* AI, double dt);
	void Intercept(AI_Base* AI, double dt);
	void AI_Chase(Player*player, double dt);
    void Render();
	void OnCollisionHit(GameObject * other);


private:
	SpriteAnimation * m_AnimAllyWarrior;
	Animation* m_Anim;

	Vector3 m_offsetPosition;
	float m_offset;
	float threshold = 50.f;
	float Distlimit = 1.f;
	AI_Base* m_CurrentTarget = nullptr;
	Vector3 ppos;
	Vector3 pscale;

	float m_NextAttackDamageTime = 0;
	float m_NextDamageTime = 0;

	AI_STATES m_CurrentState = CHASE;
	AI_STATES m_PreviousState;

	//To make sure collision between this and enemy occurs once only
	bool m_Collided = false;

	//Countdown to despawn AI
	double m_countDown = 0;
};

#endif
