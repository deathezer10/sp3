#ifndef SCENE_GAMEOVER_H
#define SCENE_GAMEOVER_H

#include "Scene.h"
#include "SceneBase.h"


using std::string;

// Gameover / victory sceen scene
class SceneGameover : public SceneBase {

public:
	enum TYPE_MENU {

		MENU_GAMEOVER = 0,
		MENU_VICTORY

	};

	SceneGameover(const string &title, TYPE_MENU type, TYPE_SCENE previousScene, int currencyEarned = 0);
	~SceneGameover();

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();




private:
	string _title;

	unsigned _menuSelected;
	TYPE_MENU _menuType;
	TYPE_SCENE _previousScene;

	int _currencyEarned=0;
	bool canPressEnter = false;
	bool canPressUpDown = false;



};


#endif