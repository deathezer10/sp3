/******************************************************************************/
/*!
\file	Vector3.h
\author Wen Sheng Tang
\par	email: tang_wen_sheng\@nyp.edu.sg
\brief
Struct to define a 3D vector
*/
/******************************************************************************/

#ifndef VECTOR3_H
#define VECTOR3_H

#include "MyMath.h"
#include <iostream>

#pragma warning( disable: 4290 ) //for throw(DivideByZero)

/******************************************************************************/
/*!
		Class Vector3:
\brief	Defines a 3D vector and its methods
*/
/******************************************************************************/
struct Vector3
{
	float x, y, z;
	bool IsEqual(float a, float b) const;

	Vector3(float a = 0.0, float b = 0.0, float c = 0.0);
	Vector3(const Vector3 &rhs);
	~Vector3();

	// (0, 1, 0)
	static const Vector3 Up;

	// (0, -1, 0)
	static const Vector3 Down;

	// (1, 0, 0)
	static const Vector3 Left;

	// (-1, 0, 0)
	static const Vector3 Right;

	void Set(float a = 0, float b = 0, float c = 0); //Set all data
	void SetZero(void); //Set all data to zero
	bool IsZero(void) const; //Check if data is zero
	bool IsFacingVector(Vector3& point, Vector3& forward, float fovDegrees);

	Vector3 operator+(const Vector3& rhs) const; //Vector addition
	Vector3& operator+=(const Vector3& rhs);

	Vector3 operator-(const Vector3& rhs) const; //Vector subtraction
	Vector3& operator-=(const Vector3& rhs);

	Vector3 operator-(void) const; //Unary negation

	Vector3 operator/ (Vector3& rhs) const;
	Vector3 operator* (Vector3& rhs) const;
	Vector3 operator*(float scalar) const; //Scalar multiplication
	Vector3& operator*=(float scalar);

	Vector3 operator/(float scalar) const; //Vector division
	Vector3& operator/=(float scalar); //Vector division

	bool operator==(const Vector3& rhs) const; //Equality check
	bool operator!= (const Vector3& rhs) const; //Inequality check

	Vector3& operator=(const Vector3& rhs); //Assignment operator

	//Get magnitude
	float Length(void) const;

	//Get square of magnitude
	float LengthSquared(void) const;

	//Get magnitude
	float HorizontalLength(void) const;

	//Get square of magnitude
	float HorizontalLengthSquared(void) const;

	float Dot(const Vector3& rhs) const; //Dot product
	Vector3 Cross(const Vector3& rhs) const; //Cross product

	//Return a copy of this vector, normalized
	//Throw a divide by zero exception if normalizing a zero vector
	Vector3 Normalized(void) const throw(DivideByZero);

	//Normalize this vector and return a reference to it
	//Throw a divide by zero exception if normalizing a zero vector
	Vector3& Normalize(void) throw(DivideByZero);

	// Square roots the components of this Vector
	Vector3& Sqrt();

	// Rotate this vector around the given point in radian (Rotates in Z Axis)
	void RotateAround(const Vector3& point, float radian);

	// Moves this Vector towards the target with maxDistanceDelta being the rate of movement
	// If next delta distance moved will be greater than target then target will be assigned instead (will not overshoot target)
	// Returns: TRUE if reached the target
	bool MoveTowards(Vector3 target, float maxDistanceDelta);

	friend std::ostream& operator<<(std::ostream& os, Vector3& rhs); //print to ostream

	friend Vector3 operator*(float scalar, const Vector3& rhs);
};

#endif //VECTOR3_H
