[Requirements]
1. Make sure you loaded the SceneEditor scene

===Controls===

[Movement]
Up/Down/Left/Right - Basic no-clip movement
Tab - Toggle Gravity (Picking/Placing of Objects is disabled while toggled, mainly used to "Play Test" the scene) (Only toggles if you do not have a selected object)

Up (While Gravity Enabled) - Simulates a normal character jump


[Camera]
Shift - Zoom In
Ctrl - Zoom Out
Alt - Reset Zoom


[Object Properties]
1/2/3/4/5/6/7 - Change object type (See Terrain Geometry enums for the terrain types)

Z/C - Increase/Decrease scale X
S/X - Increase/Decrease scale Y

A/D - Rotate left/right
Q/E - Rotate left/right by 15 degrees


R - Reset selected object's scale and rotation
F - Reset selected object's rotation

M/N - Increase/Decrease movement speed
B - Set movement speed to 1.0


[Object Placement]
Spacebar - Place or Pick(if colliding) Objects
Backspace - Delete selected Object


[Misc]
F1 - Save scene (outputs level00.txt)
F2 - Load scene (reads from level00.txt)